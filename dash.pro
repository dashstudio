SUBDIRS += 3rdparty src tests
TEMPLATE = subdirs 
CONFIG += warn_on \
          qt \
          thread 

INSTALLS += pc menu icon mimexml mimedesktop
pc.path = /lib/pkgconfig
pc.files = dash.pc dashserver.pc

menu.path = /share/applications
menu.files = dash.desktop

icon.path = /share/icons
icon.files = dash.png

mimexml.path = /share/mime/application/
mimexml.files = x-dash.xml

mimedesktop.path = /share/mimelnk/application/
mimedesktop.files = x-dash.desktop

