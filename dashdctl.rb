
print "Loading... "
$stdout.flush

has_qt4 = false
begin
has_qt4 = require 'Qt4'
rescue LoadError
    puts "[FAILED]"
    puts "ruby qt4 is required!"
    exit -1
end

class Manager
    def self.restart_server(host, port)
        begin
            settings = Qt::Settings.new("dashd", "dashd")
            settings.beginGroup("Connection")
            
            settings.setValue("host", Qt::Variant.new(host) )
            settings.setValue("port", Qt::Variant.new(port) )
            
            settings.endGroup()
            
            settings.sync
            settings.dispose
        end
        
        `pkill dashd` # Restart the server
    end
    
    def self.server_port
        server = nil
        port = nil
        begin
            settings = Qt::Settings.new("dashd", "dashd");
            settings.beginGroup("Connection")
            
            server = settings.value("host").toString
            port = settings.value("port").toInt
            
            settings.endGroup()
        end
        
        [server, port]
    end
end

if not has_qt4
    puts "[OK]"
    
    
end

class Control < Qt::Frame
    slots 'restart()'
    def initialize
        super
        @layout = Qt::VBoxLayout.new(self)
        
        server, port = Manager.server_port
        
        @grid = Qt::GridLayout.new
        
        @host = Qt::LineEdit.new
        @host.setText server.to_s
        
        @port = Qt::SpinBox.new
        @port.setMaximum(65000)
        @port.setValue port.to_i
        
        @grid.addWidget(Qt::Label.new(tr("Host")), 0, 0)
        @grid.addWidget(@host, 0, 1)
        
        @grid.addWidget(Qt::Label.new(tr("Port")), 1, 0)
        @grid.addWidget(@port, 1, 1)
        
        
        @layout.addLayout @grid
        
        @buttons = Qt::HBoxLayout.new
        restart = Qt::PushButton.new(tr("Restart"))
        connect(restart, SIGNAL('clicked()'), self, SLOT('restart()'))
        @buttons.addWidget(restart)
        
        @layout.addLayout @buttons
    end
    
    def restart
        Manager.restart_server(@host.text, @port.value)
    end
end

class MainWindow < Qt::MainWindow
    def initialize
        super
        
        @control = Control.new
        setCentralWidget(@control)
    end
end


app = Qt::Application.new(ARGV)

mw = MainWindow.new
mw.show

puts "[OK]"
app.exec



