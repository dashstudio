# License, not of this script, but of the application it contains:
#
# 
# Copyright (C) 2007 David Cuadrado                                     
# krawek@gmail.com                                                      
#                                                                       
# This library is free software; you can redistribute it and/or         
# modify it under the terms of the GNU Lesser General Public            
# License as published by the Free Software Foundation; either          
# version 2.1 of the License, or (at your option) any later version.    
#                                                                       
# This library is distributed in the hope that it will be useful,       
# but WITHOUT ANY WARRANTY; without even the implied warranty of        
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     
# Lesser General Public License for more details.                       
#                                                                       
# You should have received a copy of the GNU Lesser General Public      
# License along with this library; if not, write to the Free Software   
# Foundation, Inc.,                                                     
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA              
# 

# License of this script, not of the application it contains:
#
# Copyright Erik Veenstra <tar2rubyscript@erikveen.dds.nl>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330,
# Boston, MA 02111-1307 USA.

# Parts of this code are based on code from Thomas Hurst
# <tom@hur.st>.

# Tar2RubyScript constants

unless defined?(BLOCKSIZE)
  ShowContent	= ARGV.include?("--tar2rubyscript-list")
  JustExtract	= ARGV.include?("--tar2rubyscript-justextract")
  ToTar		= ARGV.include?("--tar2rubyscript-totar")
  Preserve	= ARGV.include?("--tar2rubyscript-preserve")
end

ARGV.concat	[]

ARGV.delete_if{|arg| arg =~ /^--tar2rubyscript-/}

ARGV << "--tar2rubyscript-preserve"	if Preserve

# Tar constants

unless defined?(BLOCKSIZE)
  BLOCKSIZE		= 512

  NAMELEN		= 100
  MODELEN		= 8
  UIDLEN		= 8
  GIDLEN		= 8
  CHKSUMLEN		= 8
  SIZELEN		= 12
  MAGICLEN		= 8
  MODTIMELEN		= 12
  UNAMELEN		= 32
  GNAMELEN		= 32
  DEVLEN		= 8

  TMAGIC		= "ustar"
  GNU_TMAGIC		= "ustar  "
  SOLARIS_TMAGIC	= "ustar\00000"

  MAGICS		= [TMAGIC, GNU_TMAGIC, SOLARIS_TMAGIC]

  LF_OLDFILE		= '\0'
  LF_FILE		= '0'
  LF_LINK		= '1'
  LF_SYMLINK		= '2'
  LF_CHAR		= '3'
  LF_BLOCK		= '4'
  LF_DIR		= '5'
  LF_FIFO		= '6'
  LF_CONTIG		= '7'

  GNUTYPE_DUMPDIR	= 'D'
  GNUTYPE_LONGLINK	= 'K'	# Identifies the *next* file on the tape as having a long linkname.
  GNUTYPE_LONGNAME	= 'L'	# Identifies the *next* file on the tape as having a long name.
  GNUTYPE_MULTIVOL	= 'M'	# This is the continuation of a file that began on another volume.
  GNUTYPE_NAMES		= 'N'	# For storing filenames that do not fit into the main header.
  GNUTYPE_SPARSE	= 'S'	# This is for sparse files.
  GNUTYPE_VOLHDR	= 'V'	# This file is a tape/volume header.  Ignore it on extraction.
end

class Dir
  def self.rm_rf(entry)
    begin
      File.chmod(0755, entry)
    rescue
    end

    if File.ftype(entry) == "directory"
      pdir	= Dir.pwd

      Dir.chdir(entry)
        Dir.open(".") do |d|
          d.each do |e|
            Dir.rm_rf(e)	if not [".", ".."].include?(e)
          end
        end
      Dir.chdir(pdir)

      begin
        Dir.delete(entry)
      rescue => e
        $stderr.puts e.message
      end
    else
      begin
        File.delete(entry)
      rescue => e
        $stderr.puts e.message
      end
    end
  end
end

class Reader
  def initialize(filehandle)
    @fp	= filehandle
  end

  def extract
    each do |entry|
      entry.extract
    end
  end

  def list
    each do |entry|
      entry.list
    end
  end

  def each
    @fp.rewind

    while entry	= next_entry
      yield(entry)
    end
  end

  def next_entry
    buf	= @fp.read(BLOCKSIZE)

    if buf.length < BLOCKSIZE or buf == "\000" * BLOCKSIZE
      entry	= nil
    else
      entry	= Entry.new(buf, @fp)
    end

    entry
  end
end

class Entry
  attr_reader(:header, :data)

  def initialize(header, fp)
    @header	= Header.new(header)

    readdata =
    lambda do |header|
      padding	= (BLOCKSIZE - (header.size % BLOCKSIZE)) % BLOCKSIZE
      @data	= fp.read(header.size)	if header.size > 0
      dummy	= fp.read(padding)	if padding > 0
    end

    readdata.call(@header)

    if @header.longname?
      gnuname		= @data[0..-2]

      header		= fp.read(BLOCKSIZE)
      @header		= Header.new(header)
      @header.name	= gnuname

      readdata.call(@header)
    end
  end

  def extract
    if not @header.name.empty?
      if @header.symlink?
        begin
          File.symlink(@header.linkname, @header.name)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create symlink #{@header.name}: " + e.message
        end
      elsif @header.link?
        begin
          File.link(@header.linkname, @header.name)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create link #{@header.name}: " + e.message
        end
      elsif @header.dir?
        begin
          Dir.mkdir(@header.name, @header.mode)
        rescue SystemCallError => e
          $stderr.puts "Couldn't create dir #{@header.name}: " + e.message
        end
      elsif @header.file?
        begin
          File.open(@header.name, "wb") do |fp|
            fp.write(@data)
            fp.chmod(@header.mode)
          end
        rescue => e
          $stderr.puts "Couldn't create file #{@header.name}: " + e.message
        end
      else
        $stderr.puts "Couldn't handle entry #{@header.name} (flag=#{@header.linkflag.inspect})."
      end

      #File.chown(@header.uid, @header.gid, @header.name)
      #File.utime(Time.now, @header.mtime, @header.name)
    end
  end

  def list
    if not @header.name.empty?
      if @header.symlink?
        $stderr.puts "s %s -> %s" % [@header.name, @header.linkname]
      elsif @header.link?
        $stderr.puts "l %s -> %s" % [@header.name, @header.linkname]
      elsif @header.dir?
        $stderr.puts "d %s" % [@header.name]
      elsif @header.file?
        $stderr.puts "f %s (%s)" % [@header.name, @header.size]
      else
        $stderr.puts "Couldn't handle entry #{@header.name} (flag=#{@header.linkflag.inspect})."
      end
    end
  end
end

class Header
  attr_reader(:name, :uid, :gid, :size, :mtime, :uname, :gname, :mode, :linkflag, :linkname)
  attr_writer(:name)

  def initialize(header)
    fields	= header.unpack('A100 A8 A8 A8 A12 A12 A8 A1 A100 A8 A32 A32 A8 A8')
    types	= ['str', 'oct', 'oct', 'oct', 'oct', 'time', 'oct', 'str', 'str', 'str', 'str', 'str', 'oct', 'oct']

    begin
      converted	= []
      while field = fields.shift
        type	= types.shift

        case type
        when 'str'	then converted.push(field)
        when 'oct'	then converted.push(field.oct)
        when 'time'	then converted.push(Time::at(field.oct))
        end
      end

      @name, @mode, @uid, @gid, @size, @mtime, @chksum, @linkflag, @linkname, @magic, @uname, @gname, @devmajor, @devminor	= converted

      @name.gsub!(/^\.\//, "")
      @linkname.gsub!(/^\.\//, "")

      @raw	= header
    rescue ArgumentError => e
      raise "Couldn't determine a real value for a field (#{field})"
    end

    raise "Magic header value #{@magic.inspect} is invalid."	if not MAGICS.include?(@magic)

    @linkflag	= LF_FILE			if @linkflag == LF_OLDFILE or @linkflag == LF_CONTIG
    @linkflag	= LF_DIR			if @linkflag == LF_FILE and @name[-1] == '/'
    @size	= 0				if @size < 0
  end

  def file?
    @linkflag == LF_FILE
  end

  def dir?
    @linkflag == LF_DIR
  end

  def symlink?
    @linkflag == LF_SYMLINK
  end

  def link?
    @linkflag == LF_LINK
  end

  def longname?
    @linkflag == GNUTYPE_LONGNAME
  end
end

class Content
  @@count	= 0	unless defined?(@@count)

  def initialize
    @@count += 1

    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    temp	= File.expand_path(temp)
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count}"
  end

  def list
    begin
      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).list}
    ensure
      File.delete(@tempfile)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

class TempSpace
  @@count	= 0	unless defined?(@@count)

  def initialize
    @@count += 1

    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    @olddir	= Dir.pwd
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    temp	= File.expand_path(temp)
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count}"
    @tempdir	= "#{temp}/tar2rubyscript.d.#{Process.pid}.#{@@count}"

    @@tempspace	= self

    @newdir	= @tempdir

    @touchthread =
    Thread.new do
      loop do
        sleep 60*60

        touch(@tempdir)
        touch(@tempfile)
      end
    end
  end

  def extract
    Dir.rm_rf(@tempdir)	if File.exists?(@tempdir)
    Dir.mkdir(@tempdir)

    newlocation do

		# Create the temp environment.

      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).extract}

		# Eventually look for a subdirectory.

      entries	= Dir.entries(".")
      entries.delete(".")
      entries.delete("..")

      if entries.length == 1
        entry	= entries.shift.dup
        if File.directory?(entry)
          @newdir	= "#{@tempdir}/#{entry}"
        end
      end
    end

		# Remember all File objects.

    @ioobjects	= []
    ObjectSpace::each_object(File) do |obj|
      @ioobjects << obj
    end

    at_exit do
      @touchthread.kill

		# Close all File objects, opened in init.rb .

      ObjectSpace::each_object(File) do |obj|
        obj.close	if (not obj.closed? and not @ioobjects.include?(obj))
      end

		# Remove the temp environment.

      Dir.chdir(@olddir)

      Dir.rm_rf(@tempfile)
      Dir.rm_rf(@tempdir)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end

  def touch(entry)
    entry	= entry.gsub!(/[\/\\]*$/, "")	unless entry.nil?

    return	unless File.exists?(entry)

    if File.directory?(entry)
      pdir	= Dir.pwd

      begin
        Dir.chdir(entry)

        begin
          Dir.open(".") do |d|
            d.each do |e|
              touch(e)	unless [".", ".."].include?(e)
            end
          end
        ensure
          Dir.chdir(pdir)
        end
      rescue Errno::EACCES => error
        $stderr.puts error
      end
    else
      File.utime(Time.now, File.mtime(entry), entry)
    end
  end

  def oldlocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@olddir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @olddir)	if not file.nil?
    end

    res
  end

  def newlocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@newdir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @newdir)	if not file.nil?
    end

    res
  end

  def templocation(file="")
    if block_given?
      pdir	= Dir.pwd

      Dir.chdir(@tempdir)
        res	= yield
      Dir.chdir(pdir)
    else
      res	= File.expand_path(file, @tempdir)	if not file.nil?
    end

    res
  end

  def self.oldlocation(file="")
    if block_given?
      @@tempspace.oldlocation { yield }
    else
      @@tempspace.oldlocation(file)
    end
  end

  def self.newlocation(file="")
    if block_given?
      @@tempspace.newlocation { yield }
    else
      @@tempspace.newlocation(file)
    end
  end

  def self.templocation(file="")
    if block_given?
      @@tempspace.templocation { yield }
    else
      @@tempspace.templocation(file)
    end
  end
end

class Extract
  @@count	= 0	unless defined?(@@count)

  def initialize
    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    temp	= ENV["TEMP"]
    temp	= "/tmp"	if temp.nil?
    @tempfile	= "#{temp}/tar2rubyscript.f.#{Process.pid}.#{@@count += 1}"
  end

  def extract
    begin
      File.open(@tempfile, "wb")	{|f| f.write @archive}
      File.open(@tempfile, "rb")	{|f| Reader.new(f).extract}
    ensure
      File.delete(@tempfile)
    end

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

class MakeTar
  def initialize
    @archive	= File.open(File.expand_path(__FILE__), "rb"){|f| f.read}.gsub(/\r/, "").split(/\n\n/)[-1].split("\n").collect{|s| s[2..-1]}.join("\n").unpack("m").shift
    @tarfile	= File.expand_path(__FILE__).gsub(/\.rbw?$/, "") + ".tar"
  end

  def extract
    File.open(@tarfile, "wb")	{|f| f.write @archive}

    self
  end

  def cleanup
    @archive	= nil

    self
  end
end

def oldlocation(file="")
  if block_given?
    TempSpace.oldlocation { yield }
  else
    TempSpace.oldlocation(file)
  end
end

def newlocation(file="")
  if block_given?
    TempSpace.newlocation { yield }
  else
    TempSpace.newlocation(file)
  end
end

def templocation(file="")
  if block_given?
    TempSpace.templocation { yield }
  else
    TempSpace.templocation(file)
  end
end

if ShowContent
  Content.new.list.cleanup
elsif JustExtract
  Extract.new.extract.cleanup
elsif ToTar
  MakeTar.new.extract.cleanup
else
  TempSpace.new.extract.cleanup

  $:.unshift(templocation)
  $:.unshift(newlocation)
  $:.push(oldlocation)

  verbose	= $VERBOSE
  $VERBOSE	= nil
  s	= ENV["PATH"].dup
  if Dir.pwd[1..2] == ":/"	# Hack ???
    s << ";#{templocation.gsub(/\//, "\\")}"
    s << ";#{newlocation.gsub(/\//, "\\")}"
    s << ";#{oldlocation.gsub(/\//, "\\")}"
  else
    s << ":#{templocation}"
    s << ":#{newlocation}"
    s << ":#{oldlocation}"
  end
  ENV["PATH"]	= s
  $VERBOSE	= verbose

  TAR2RUBYSCRIPT	= true	unless defined?(TAR2RUBYSCRIPT)

  newlocation do
    if __FILE__ == $0
      $0.replace(File.expand_path("./init.rb"))

      if File.file?("./init.rb")
        load File.expand_path("./init.rb")
      else
        $stderr.puts "%s doesn't contain an init.rb ." % __FILE__
      end
    else
      if File.file?("./init.rb")
        load File.expand_path("./init.rb")
      end
    end
  end
end


# cW9uZmlndXJlLwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAADAwMDA3NTUAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDAwMDAw
# ADEwNzMyMzY2MTcyADAxMTIxMwAgNQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdl
# awAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUvcW9uZi5yYgAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAw
# MDE3NTEAMDAwMDAwMzM1MTMAMTA3MzIzNjYxNzIAMDEyNTEwACAwAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApyZXF1aXJlICd0
# ZXN0JwpyZXF1aXJlICdwa2djb25maWcnCnJlcXVpcmUgJ2Zyb250ZW5kZmFj
# dG9yeScKcmVxdWlyZSAnY29uZmlnJwpyZXF1aXJlICdxbWFrZScKcmVxdWly
# ZSAnbWFrZWZpbGUnCgptb2R1bGUgUW9uZgogIFZFUlNJT04gPSAiMC42Ii5m
# cmVlemUKICBWRVJTSU9OX0RBVEUgPSAiMjAwNy0xMi0wMiIKICAKICBjbGFz
# cyBRdFZlcnNpb24KICAgIGF0dHJfcmVhZGVyIDp2ZXJzaW9uLCA6b3AgIyAw
# OiBlcXVhbCAxOiBncmVhdGVyIDI6IGdyZWF0ZXIgZXF1YWwKICAgIAogICAg
# ZGVmIGluaXRpYWxpemUodmVyc2lvbikKICAgICAgQG9wID0gMgogICAgICBw
# YXJzZSh2ZXJzaW9uKQogICAgZW5kCiAgICAKICAgIGRlZiA9PSh2ZXIpCiAg
# ICAgIEBvcCA9IDAKICAgICAgcGFyc2UodmVyKQogICAgZW5kCiAgICAKICAg
# IGRlZiA+KHZlcikKICAgICAgQG9wID0gMQogICAgICBwYXJzZSh2ZXIpCiAg
# ICBlbmQKICAgIAogICAgZGVmID49KHZlcikKICAgICAgQG9wID0gMgogICAg
# ICBwYXJzZSh2ZXIpCiAgICBlbmQKICAgIAogICAgZGVmIHRvX3MKICAgICAg
# cyA9ICJRdCAiCiAgICAgIGNhc2UgQG9wCiAgICAgICAgd2hlbiAwCiAgICAg
# ICAgICBzKz0iPT0iCiAgICAgICAgd2hlbiAxCiAgICAgICAgICBzKz0iPiIK
# ICAgICAgICBlbHNlCiAgICAgICAgICBzKz0iPj0iCiAgICAgIGVuZAogICAg
# ICBzKz0gIiAiK0B2ZXJzaW9uCiAgICAgIAogICAgICBzCiAgICBlbmQKICAg
# IAogICAgcHJpdmF0ZQogICAgZGVmIHBhcnNlKHZlcnNpb24pCiAgICAgIEB2
# ZXJzaW9uID0gdmVyc2lvbi50b19zCiAgICAgIHZlciA9IEB2ZXJzaW9uLnRv
# X3Muc3BsaXQoJy4nKQogICAgICBzaXplID0gdmVyLnNpemUKICAgICAgICAK
# ICAgICAgZmluYWx2ZXIgPSBbXQogICAgICAzLnRpbWVzIHsgfGl8CiAgICAg
# ICAgdHAgPSB2ZXJbaV0KICAgICAgICAKICAgICAgICBpZiB0cC5uaWw/CiAg
# ICAgICAgICB0cCA9ICIwIgogICAgICAgIGVuZAogICAgICAgIAogICAgICAg
# IGZpbmFsdmVyIDw8IHRwCiAgICAgIH0KICAgICAgCiAgICAgIEB2ZXJzaW9u
# ID0gZmluYWx2ZXIuam9pbigiLiIpCiAgICBlbmQKICBlbmQKICAKICBjbGFz
# cyBTZXR1cAogICAgYXR0cl9yZWFkZXIgOm9wdGlvbnMsIDpwYWNrYWdlcywg
# OnRlc3RzLCA6cGtnX2NvbmZpZ3MKICAgIGRlZiBpbml0aWFsaXplCiAgICAg
# IEBvcHRpb25zID0gW10KICAgICAgQHBhY2thZ2VzID0gW10KICAgICAgQHRl
# c3RzID0gW10KICAgICAgQHBrZ19jb25maWdzID0gW10KICAgICAgQHF0X3Zl
# cnNpb24gPSBRdFZlcnNpb24ubmV3KCI0LjAuMCIpCiAgICAgIAogICAgICBh
# ZGRfb3B0aW9uKDpuYW1lID0+ICJwcmVmaXgiLCA6dHlwZSA9PiAicGF0aCIs
# IDpkZWZhdWx0ID0+ICIvdXNyL2xvY2FsLyIsIDpvcHRpb25hbCA9PiB0cnVl
# LCA6ZGVzY3JpcHRpb24gPT4gIlNldHMgdGhlIGluc3RhbGwgcHJlZml4IikK
# ICAgIGVuZAogICAgCiAgICBkZWYgYWRkX29wdGlvbihvcHRpb24pCiAgICAg
# IGlmIG5vdCBvcHRpb25bOnR5cGVdLm5pbD8KICAgICAgICBvcHRpb25bOnR5
# cGVdLmRvd25jYXNlIQogICAgICBlbmQKICAgICAgCiAgICAgIEBvcHRpb25z
# IDw8IG9wdGlvbgogICAgZW5kCiAgICAKICAgIGRlZiBmaW5kX3BhY2thZ2Uo
# b3B0aW9ucykKICAgICAgQHBhY2thZ2VzIDw8IG9wdGlvbnMKICAgIGVuZAog
# ICAgCiAgICBkZWYgYWRkX3Rlc3Qob3B0aW9uKQogICAgICByYWlzZSAiUGxl
# YXNlIHNldCBhIHZhbGlkIG5hbWUgdG8gdGhlIHRlc3QiIGlmIG9wdGlvbls6
# bmFtZV0udG9fcy5lbXB0eT8KICAgICAgcmFpc2UgIlBsZWFzZSBzZXQgYSB2
# YWxpZCBpZCB0byB0aGUgdGVzdCIgaWYgb3B0aW9uWzppZF0udG9fcy5lbXB0
# eT8KICAgICAgCiAgICAgIEB0ZXN0cyA8PCBvcHRpb24KICAgIGVuZAogICAg
# CiAgICBkZWYgZ2VuZXJhdGVfcGtnY29uZmlnKG9wdGlvbnMpCiAgICAgIHJh
# aXNlICJQbGVhc2Ugc2V0IGEgdmFsaWQgbmFtZSIgaWYgb3B0aW9uc1s6cGFj
# a2FnZV9uYW1lXS50b19zLmVtcHR5PwogICAgICBAcGtnX2NvbmZpZ3MgPDwg
# b3B0aW9ucwogICAgZW5kCiAgICAKICAgIGRlZiBxdAogICAgICBAcXRfdmVy
# c2lvbgogICAgZW5kCiAgICAKICBlbmQKICAKICBjbGFzcyBDb25maWd1cmUK
# ICAgIGRlZiBpbml0aWFsaXplCiAgICAgIEBhcmd1bWVudHMgPSB7fQogICAg
# ICBAY29uZmlnID0gQ29uZmlnLm5ldwogICAgICAKICAgICAgQGxvZ19maWxl
# ID0gb2xkbG9jYXRpb24oImNvbmZpZy5sb2ciKQogICAgICAKICAgICAgRmls
# ZS5vcGVuKEBsb2dfZmlsZSwgInciKSBkbyB8ZnwKICAgICAgICBmIDw8ICI9
# PT09PT09PiBHZW5lcmF0ZWQgYXQgI3tUaW1lLm5vd30gPD09PT09PT1cbiIK
# ICAgICAgZW5kCiAgICAgIAogICAgICBxbWFrZV9wYXRocyA9IFtdCiAgICAg
# IGlmKCBEZXRlY3RPUy5vcz8gIT0gMSApICMgVW5peAogICAgICAgIElPLnBv
# cGVuKCJsb2NhdGUgLXIgL3FtYWtlJCAyPiAje25ld2xvY2F0aW9uKCdsb2Nh
# dGVfbG9nJyl9IikgeyB8cHJjfAogICAgICAgICAgcHJlX3BhdGhzID0gcHJj
# LnJlYWRsaW5lcwogICAgICAgICAgcHJlX3BhdGhzLmVhY2ggeyB8cW18CiAg
# ICAgICAgICAgIHFtLnN0cmlwIQogICAgICAgICAgICBpZiBGaWxlLmV4aXN0
# cz8ocW0pCiAgICAgICAgICAgICAgc3RhdCA9IEZpbGUuc3RhdChxbSkKICAg
# ICAgICAgICAgICAKICAgICAgICAgICAgICBpZiAobm90IChzdGF0LnN5bWxp
# bms/IG9yIHN0YXQuZGlyZWN0b3J5PykpIGFuZCBzdGF0LmV4ZWN1dGFibGU/
# CiAgICAgICAgICAgICAgICBxbWFrZV9wYXRocyA8PCBxbQogICAgICAgICAg
# ICAgIGVuZAogICAgICAgICAgICBlbmQKICAgICAgICAgIH0KICAgICAgICB9
# CiAgICAgIGVuZAogICAgICAKICAgICAgQHFtYWtlID0gUU1ha2UubmV3KHFt
# YWtlX3BhdGhzLCBAbG9nX2ZpbGUpCiAgICAgIAogICAgICBjZmcgPSBTZXR1
# cC5uZXcKICAgICAgY29uZmlndXJlKGNmZykKICAgICAgCiAgICAgIHByaW50
# ICI9PiBTZWFyY2hpbmcgZm9yICN7Y2ZnLnF0fS4uLiAiCiAgICAgIGlmIEBx
# bWFrZS5maW5kX3FtYWtlKGNmZy5xdC52ZXJzaW9uLCBjZmcucXQub3ApCiAg
# ICAgICAgcHV0cyAiKCN7QHFtYWtlLnBhdGh9KSBbWUVTXSIKICAgICAgZWxz
# ZQogICAgICAgIHB1dHMgIltOT10iCiAgICAgICAgZXhpdCgtMSkKICAgICAg
# ZW5kCiAgICAgIAogICAgICBAb3B0aW9ucyA9IGNmZy5vcHRpb25zCiAgICAg
# IEBwYWNrYWdlcyA9IGNmZy5wYWNrYWdlcwogICAgICBAdGVzdHMgPSBjZmcu
# dGVzdHMKICAgICAgQHBrZ19jb25maWdzID0gY2ZnLnBrZ19jb25maWdzCiAg
# ICAgIEBzdGF0dXNGaWxlID0gRGlyLmdldHdkKyIvdXBkYXRlLW1ha2VmaWxl
# IgogICAgZW5kCiAgICAKICAgIGRlZiBmaW5kX3BhY2thZ2VzICMgVE9ETzog
# c29wb3J0YXIgaW5mb3JtYWNpb24gcGFyYSBlbCBzaXN0ZW1hIG9wZXJhdGl2
# byBzb2JyZSBjb21vIGluc3RhbGFyIGVsIHBhcXVldGUKICAgICAgQHBhY2th
# Z2VzLmVhY2ggeyB8cGFja2FnZXwKICAgICAgICBwcmludCAiPT4gU2VhcmNo
# aW5nIGZvciBwYWNrYWdlOiAje3BhY2thZ2VbOm5hbWVdfS4uLiAgIgogICAg
# ICAgIHBrZ2NvbmZpZyA9IFBrZ0NvbmZpZy5uZXcocGFja2FnZVs6bmFtZV0p
# CiAgICAgICAgcGtnY29uZmlnLmFkZF9zZWFyY2hfcGF0aCgiI3tAYXJndW1l
# bnRzWyJwcmVmaXgiXX0vbGliL3BrZ2NvbmZpZyIpCiAgICAgICAgYmVnaW4K
# ICAgICAgICAgIHNldHVwX3BrZ2NvbmZpZyhwa2djb25maWcsIEBhcmd1bWVu
# dHMpCiAgICAgICAgcmVzY3VlIE5vTWV0aG9kRXJyb3IgPT4gZQogICAgICAg
# ICAgaWYgbm90IGUubWVzc2FnZSA9fiAvc2V0dXBfcGtnY29uZmlnLwogICAg
# ICAgICAgICBwdXRzICJFcnJvcjogI3tlLm1lc3NhZ2V9IgogICAgICAgICAg
# ICBwdXRzIGUuYmFja3RyYWNlCiAgICAgICAgICAgIGV4aXQoLTEpCiAgICAg
# ICAgICBlbmQKICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBpZiBub3Qg
# cGtnY29uZmlnLmZpbmQocGFja2FnZVs6bmFtZV0pCiAgICAgICAgICBwdXRz
# ICJbTk9dIgogICAgICAgICAgCiAgICAgICAgICBvcHRpb25hbCA9IHBhY2th
# Z2VbOm9wdGlvbmFsXS5uaWw/ID8gZmFsc2UgOiBwYWNrYWdlWzpvcHRpb25h
# bF0KICAgICAgICAgIAogICAgICAgICAgaWYgbm90IG9wdGlvbmFsCiAgICAg
# ICAgICAgIHB1dHMgIj0+IFRoZSBwYWNrYWdlICcje3BhY2thZ2VbOm5hbWVd
# fScgaXMgcmVxdWlyZWQuIgogICAgICAgICAgICBleGl0KC0xKQogICAgICAg
# ICAgZW5kCiAgICAgICAgZWxzZQogICAgICAgICAgcHV0cyAiW1lFU10iCiAg
# ICAgICAgICBnbG9iYWwgPSBwYWNrYWdlWzpnbG9iYWxdLm5pbD8gPyBmYWxz
# ZSA6IHBhY2thZ2VbOmdsb2JhbF0KICAgICAgICAgIAogICAgICAgICAgcW9u
# ZnBrZyA9IEBjb25maWcKICAgICAgICAgIAogICAgICAgICAgaWYgbm90IGds
# b2JhbAogICAgICAgICAgICBxb25mcGtnID0gUGFja2FnZS5uZXcocGFja2Fn
# ZVs6bmFtZV0pCiAgICAgICAgICBlbmQKICAgICAgICAgIAogICAgICAgICAg
# aWYgJERFQlVHCiAgICAgICAgICAgIHB1dHMgIj0+IFBhY2thZ2UgaXMgZ2xv
# YmFsPyAje2dsb2JhbH0iCiAgICAgICAgICBlbmQKICAgICAgICAgIAojICAg
# ICAgICAgICBwa2djb25maWcucmVxdWlyZXMuZWFjaCB7IHxyZXF8CiMgICAg
# ICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9wYWNrYWdlKHJlcSkKIyAgICAg
# ICAgICAgfQogICAgICAgICAgCiAgICAgICAgICBxb25mcGtnLmFkZF9saWIo
# cGtnY29uZmlnLmxpYnMpCiAgICAgICAgICAKICAgICAgICAgIHBrZ2NvbmZp
# Zy5pbmNsdWRlX3BhdGguZWFjaCB7IHxpbmNwYXRofAogICAgICAgICAgICBp
# ZiAkREVCVUcKICAgICAgICAgICAgICBwdXRzICI9PiBBZGRpbmcgaW5jbHVk
# ZSBwYXRoOiAje2luY3BhdGh9IgogICAgICAgICAgICBlbmQKICAgICAgICAg
# ICAgcW9uZnBrZy5hZGRfaW5jbHVkZV9wYXRoKGluY3BhdGgpCiAgICAgICAg
# ICB9CiAgICAgICAgICAKICAgICAgICAgIHBrZ2NvbmZpZy5kZWZpbmVzLmVh
# Y2ggeyB8ZGVmaW5lfAogICAgICAgICAgICBxb25mcGtnLmFkZF9kZWZpbmUo
# ZGVmaW5lKQogICAgICAgICAgfQogICAgICAgICAgCiAgICAgICAgICBpZiBu
# b3QgZ2xvYmFsCiAgICAgICAgICAgIEBjb25maWcuYWRkX3FvbmZfcGFja2Fn
# ZShxb25mcGtnKQogICAgICAgICAgZWxzZQogICAgICAgICAgICBAY29uZmln
# LmFkZF9xb25mX2ZlYXR1cmUocGFja2FnZVs6bmFtZV0pCiAgICAgICAgICBl
# bmQKICAgICAgICBlbmQKICAgICAgfQogICAgICBpZiAkREVCVUcKICAgICAg
# ICBwdXRzICJJTkNMVURFUEFUSD0je0Bjb25maWcuaW5jbHVkZV9wYXRoLmpv
# aW4oIiAiKX0iCiAgICAgIGVuZAogICAgZW5kCiAgICAKICAgIGRlZiBydW5f
# dGVzdHMKICAgICAgQHRlc3RzLmVhY2ggeyB8dGVzdG9wdHwKICAgICAgICBw
# cmludCAiPT4gVGVzdGluZyAje3Rlc3RvcHRbOm5hbWVdfS4uLiAgIgogICAg
# ICAgIAogICAgICAgIHRzdCA9IFRlc3QubmV3KHRlc3RvcHQsIEBjb25maWcs
# IEBsb2dfZmlsZSkKICAgICAgICAKICAgICAgICBiZWdpbgogICAgICAgICAg
# c2V0dXBfdGVzdCh0ZXN0b3B0WzppZF0sIHRzdCwgQGFyZ3VtZW50cykKICAg
# ICAgICByZXNjdWUgTm9NZXRob2RFcnJvciA9PiBlCiAgICAgICAgICBpZiBu
# b3QgZS5tZXNzYWdlID1+IC9zZXR1cF90ZXN0LwogICAgICAgICAgICBwdXRz
# ICJFcnJvcjogI3tlfSIKICAgICAgICAgICAgcHV0cyBlLmJhY2t0cmFjZQog
# ICAgICAgICAgICBleGl0KC0xKQogICAgICAgICAgZW5kCiAgICAgICAgZW5k
# CiAgICAgICAgCiAgICAgICAgb3B0aW9uYWwgPSB0ZXN0b3B0WzpvcHRpb25h
# bF0ubmlsPyA/IGZhbHNlIDogdGVzdG9wdFs6b3B0aW9uYWxdCiAgICAgICAg
# aWYgdHN0LnJ1bihAcW1ha2UpCiAgICAgICAgICBnbG9iYWwgPSB0ZXN0b3B0
# WzpnbG9iYWxdLm5pbD8gPyBmYWxzZSA6IHRlc3RvcHRbOmdsb2JhbF0KICAg
# ICAgICAgIAogICAgICAgICAgaWYgJERFQlVHCiAgICAgICAgICAgIHB1dHMg
# Ij0+IFRlc3QgaXMgZ2xvYmFsOiAje2dsb2JhbH0iCiAgICAgICAgICBlbmQK
# ICAgICAgICAgIAogICAgICAgICAgcW9uZnBrZyA9IEBjb25maWcKICAgICAg
# ICAgIAogICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAgICAgICBxb25m
# cGtnID0gUGFja2FnZS5uZXcodHN0LmlkKQogICAgICAgICAgZW5kCiAgICAg
# ICAgICAKICAgICAgICAgIHRzdC5pbmNsdWRlX3BhdGguZWFjaCB7IHxpbmNw
# YXRofAogICAgICAgICAgICBpZiAkREVCVUcKICAgICAgICAgICAgICBwdXRz
# ICI9PiBBZGRpbmcgaW5jbHVkZSBwYXRoOiAje2luY3BhdGh9IgogICAgICAg
# ICAgICBlbmQKICAgICAgICAgICAgcW9uZnBrZy5hZGRfaW5jbHVkZV9wYXRo
# KGluY3BhdGgpCiAgICAgICAgICB9CiAgICAgICAgICAKICAgICAgICAgIHRz
# dC5kZWZpbmVzLmVhY2ggeyB8ZGVmaW5lfAogICAgICAgICAgICBxb25mcGtn
# LmFkZF9kZWZpbmUoZGVmaW5lKQogICAgICAgICAgfQogICAgICAgICAgCiAg
# ICAgICAgICBxb25mcGtnLmFkZF9saWIodHN0LmxpYnMudG9fcykKICAgICAg
# ICAgIAogICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAgICAgICBAY29u
# ZmlnLmFkZF9xb25mX3BhY2thZ2UocW9uZnBrZykKICAgICAgICAgIGVsc2UK
# ICAgICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9mZWF0dXJlKHRzdC5pZCkK
# ICAgICAgICAgIGVuZAogICAgICAgICAgcHV0cyAiW09LXSIKICAgICAgICBl
# bHNlCiAgICAgICAgICBwdXRzICJbRkFJTEVEXSIKICAgICAgICAgIAogICAg
# ICAgICAgaWYgbm90IG9wdGlvbmFsCiAgICAgICAgICAgIHB1dHMgIj0+ICN7
# dGVzdG9wdFs6bmFtZV19IGlzIHJlcXVpcmVkIgogICAgICAgICAgICBleGl0
# KC0xKQogICAgICAgICAgZW5kCiAgICAgICAgZW5kCiAgICAgIH0KICAgIGVu
# ZAogICAgCiAgICBkZWYgZ2VuZXJhdGVfbWFrZWZpbGVzCiAgICAgIHB1dHMg
# Ij0+IENyZWF0aW5nIG1ha2VmaWxlcy4uLiIKICAgICAgQHFtYWtlLnJ1bigi
# IiwgdHJ1ZSkKICAgICAgCiAgICAgIHB1dHMgIj0+IFVwZGF0aW5nIG1ha2Vm
# aWxlcy4uLiIKICAgICAgCiAgICAgIEBtYWtlZmlsZXMgPSBNYWtlZmlsZTo6
# ZmluZF9tYWtlZmlsZXMoRGlyLmdldHdkKQogICAgICBAbWFrZWZpbGVzLmVh
# Y2ggeyB8bWFrZWZpbGV8CiAgICAgICAgICBNYWtlZmlsZTo6b3ZlcnJpZGUo
# bWFrZWZpbGUsIEZpbGUuZXhwYW5kX3BhdGgoQGFyZ3VtZW50c1sicHJlZml4
# Il0udG9fcyksIEBzdGF0dXNGaWxlKQogICAgICB9CiAgICAgIAogICAgICBG
# aWxlLm9wZW4oQHN0YXR1c0ZpbGUsICJ3IiApIHsgfGZpbGV8CiAgICAgICAg
# ZmlsZSA8PCAlQAoje0ZpbGUucmVhZChuZXdsb2NhdGlvbigibWFrZWZpbGUu
# cmIiKSl9ClFvbmY6Ok1ha2VmaWxlOjpvdmVycmlkZSggQVJHVlswXS50b19z
# LCAiI3tAYXJndW1lbnRzWyJwcmVmaXgiXX0iLCAiI3tAc3RhdHVzRmlsZX0i
# ICkKQAoKaWYgRGV0ZWN0T1Mub3M/ID09IDEgI3dpbjMyCiAgICAgICAgZmls
# ZSA8PCAlQApRb25mOjpNYWtlZmlsZTo6b3ZlcnJpZGUoIEFSR1ZbMF0udG9f
# cysiLnJlbGVhc2UiLCAiI3tAYXJndW1lbnRzWyJwcmVmaXgiXX0iLCAiI3tA
# c3RhdHVzRmlsZX0iICkKUW9uZjo6TWFrZWZpbGU6Om92ZXJyaWRlKCBBUkdW
# WzBdLnRvX3MrIi5kZWJ1ZyIsICIje0Bhcmd1bWVudHNbInByZWZpeCJdfSIs
# ICIje0BzdGF0dXNGaWxlfSIgKQpACmVuZAogICAgICB9CiAgICBlbmQKICAg
# IAogICAgZGVmIGdldF9hcmdzKGFyZ3YpCiAgICAgIG9wdGMgPSAwCiAgICAg
# IGxhc3Rfb3B0ID0gIiIKICAgICAgCiAgICAgIEBvcHRpb25zLmVhY2ggeyB8
# b3B0aW9ufAogICAgICAgIGdpdmVuX29wdGlvbiA9IG5pbAogICAgICAgIAog
# ICAgICAgIGFyZ3YuZWFjaCB7IHxhcmd8CiAgICAgICAgICBhcmcuc3RyaXAh
# CiAgICAgICAgICBnaXZlbiA9IGZhbHNlCiAgICAgICAgICAKICAgICAgICAg
# IGNhc2Ugb3B0aW9uWzp0eXBlXS50b19zCiAgICAgICAgICAgIHdoZW4gInN0
# cmluZyIKICAgICAgICAgICAgICBpZiBhcmcgPX4gLy0tI3tvcHRpb25bOm5h
# bWVdfT0oW1xXXHddKikvCiAgICAgICAgICAgICAgICBnaXZlbiA9IHRydWUK
# ICAgICAgICAgICAgICAgIGdpdmVuX29wdGlvbiA9ICQxCiAgICAgICAgICAg
# ICAgZW5kCiAgICAgICAgICAgIHdoZW4gInBhdGgiCiAgICAgICAgICAgICAg
# aWYgYXJnID1+IC8tLSN7b3B0aW9uWzpuYW1lXX09KFtcV1x3XSopLwogICAg
# ICAgICAgICAgICAgZ2l2ZW4gPSB0cnVlCiAgICAgICAgICAgICAgICBnaXZl
# bl9vcHRpb24gPSBGaWxlLmV4cGFuZF9wYXRoKCQxKQogICAgICAgICAgICAg
# IGVuZAogICAgICAgICAgICB3aGVuICJib29sIgogICAgICAgICAgICAgIGlm
# IGFyZyA9fiAvLS11c2UtI3tvcHRpb25bOm5hbWVdfT0oXHcrKS8KICAgICAg
# ICAgICAgICAgIGdpdmVuID0gdHJ1ZQogICAgICAgICAgICAgICAgZ2l2ZW5f
# b3B0aW9uID0gJDEKICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgZWxz
# ZQogICAgICAgICAgICAgIGlmIGFyZyA9fiAvLS0je29wdGlvbls6bmFtZV19
# LwogICAgICAgICAgICAgICAgZ2l2ZW5fb3B0aW9uID0gInllcyIKICAgICAg
# ICAgICAgICAgIGdpdmVuID0gdHJ1ZQogICAgICAgICAgICAgIGVuZAogICAg
# ICAgICAgZW5kCiAgICAgICAgICAKICAgICAgICAgIGlmIGdpdmVuCiAgICAg
# ICAgICAgIGFyZ3YuZGVsZXRlKGFyZykKICAgICAgICAgIGVuZAogICAgICAg
# IH0KICAgICAgICAKICAgICAgICBvcHRpb25bOnZhbHVlXSA9IGdpdmVuX29w
# dGlvbgogICAgICB9CiAgICAgIAogICAgICBmcm9udGVuZF9rZXkgPSAidGsi
# CiAgICAgIGFyZ3YuZWFjaCB7IHx1bmtub3dufAogICAgICAgIHVua25vd24u
# c3RyaXAhCiAgICAgICAgaWYgdW5rbm93biA9fiAvLS13aXRoLWNvbmZpZy1m
# cm9udGVuZD0oXHcrKS8KICAgICAgICAgIGZyb250ZW5kX2tleSA9ICQxCiAg
# ICAgICAgZWxzaWYgdW5rbm93biA9fiAvLS1oZWxwLwogICAgICAgICAgdXNh
# Z2UKICAgICAgICAgIGV4aXQoMCkKICAgICAgICBlbHNpZiB1bmtub3duID1+
# IC8tLXZlcnNpb24vCiAgICAgICAgICBwdXRzICJRT25mICN7VkVSU0lPTn0g
# WyN7VkVSU0lPTl9EQVRFfV0iCiAgICAgICAgICBleGl0KDApCiAgICAgICAg
# ZWxzaWYgdW5rbm93biA9fiAvLS1kZWJ1Zy8KICAgICAgICAgIEBjb25maWcu
# d2l0aF9kZWJ1ZwogICAgICAgIGVuZAogICAgICB9CiAgICAgIAogICAgICBm
# cm9udGVuZCA9IEZyb250ZW5kRmFjdG9yeS5jcmVhdGUoZnJvbnRlbmRfa2V5
# KQogICAgICBpZiBmcm9udGVuZC5uaWw/CiAgICAgICAgcHV0cyAiPT4gSW52
# YWxpZCBmcm9udGVuZCIKICAgICAgICB1c2FnZSgpCiAgICAgICAgZXhpdCgt
# MSkKICAgICAgZWxzaWYgbm90IGZyb250ZW5kLnBhcnNlKEBvcHRpb25zKQog
# ICAgICAgIHVzYWdlKCkKICAgICAgICBleGl0KC0xKQogICAgICBlbmQKICAg
# ICAgCiAgICAgIEBvcHRpb25zLmVhY2ggeyB8b3B0aW9ufAogICAgICAgIHZh
# bHVlID0gb3B0aW9uWzp2YWx1ZV0ubmlsPyA/IG9wdGlvbls6ZGVmYXVsdF0u
# dG9fcyA6IG9wdGlvbls6dmFsdWVdCiAgICAgICAgQGFyZ3VtZW50c1tvcHRp
# b25bOm5hbWVdXSA9IHZhbHVlCiAgICAgIH0KICAgICAgCiAgICBlbmQKICAg
# IAogICAgZGVmIHNhdmUocGF0aCkKICAgICAgYmVnaW4KICAgICAgICBzZXR1
# cF9jb25maWcoQGNvbmZpZywgQGFyZ3VtZW50cykKICAgICAgcmVzY3VlIE5v
# TWV0aG9kRXJyb3IgPT4gZQogICAgICAgIGlmIG5vdCBlLm1lc3NhZ2UgPX4g
# L3NldHVwX2NvbmZpZy8KICAgICAgICAgIHB1dHMgIkVycm9yOiAje2V9Igog
# ICAgICAgICAgcHV0cyBlLmJhY2t0cmFjZQogICAgICAgICAgZXhpdCgtMSkK
# ICAgICAgICBlbmQKICAgICAgZW5kCiAgICAgIAogICAgICBAY29uZmlnLmFk
# ZF9kZWZpbmUoJUBfX1BSRUZJWF9fPSdcXFxcIicje0ZpbGUuZXhwYW5kX3Bh
# dGggQGFyZ3VtZW50c1sicHJlZml4Il19J1xcXFwiJ0ApCiAgICAgIAogICAg
# ICBzYXZlX3BrZ2NvbmZpZwogICAgICBAY29uZmlnLnNhdmUocGF0aCkKICAg
# ICAgCiAgICAgIGxhdW5jaGVyID0gbmlsCiAgICAgIGlmIERldGVjdE9TLm9z
# PyA9PSAxICMgV2luZG93cwogICAgICAgIGxhdW5jaGVyID0gRmlsZS5uZXco
# ImxhdW5jaGVyLmJhdCIsICJ3IikKICAgICAgICAKICAgICAgICBsYXVuY2hl
# ciA8PCAic2V0IFBBVEg9I3tAY29uZmlnLmJ1aWxkX2Rpci5nc3ViKCcvJywg
# J1xcJyl9XFxiaW47I3tAY29uZmlnLmJ1aWxkX2Rpci5nc3ViKCcvJywgJ1xc
# Jyl9XFxsaWI7I3tAY29uZmlnLmxpYnJhcnlfcGF0aC5qb2luKCI7IikuZ3N1
# YignLycsICdcXCcpfTslUEFUSCVcbiIKICAgICAgICBsYXVuY2hlciA8PCAi
# JTFcbiIKICAgICAgZWxzZQogICAgICAgIGxhdW5jaGVyID0gRmlsZS5uZXco
# ImxhdW5jaGVyLnNoIiwidyIpCiAgICAgICAgbGF1bmNoZXIgPDwgIiMhL2Jp
# bi9iYXNoXG4iCiAgICAgICAgbGF1bmNoZXIgPDwgImV4cG9ydCBQQVRIPSN7
# QGNvbmZpZy5idWlsZF9kaXJ9L2Jpbjoke1BBVEh9XG4iCiAgICAgICAgbGF1
# bmNoZXIgPDwgImV4cG9ydCBMRF9MSUJSQVJZX1BBVEg9I3tAY29uZmlnLmJ1
# aWxkX2Rpcn0vYmluOiN7QGNvbmZpZy5idWlsZF9kaXJ9L2xpYjoje0Bjb25m
# aWcubGlicmFyeV9wYXRoLmpvaW4oIjoiKX06JHtMRF9MSUJSQVJZX1BBVEh9
# OiR7RFlfTERfTElCUkFSWV9QQVRIfVxuIgogICAgICAgIGxhdW5jaGVyIDw8
# ICJleHBvcnQgRFlMRF9MSUJSQVJZX1BBVEg9JHtMRF9MSUJSQVJZX1BBVEh9
# XG4iCiAgICAgICAgbGF1bmNoZXIgPDwgIkFQUD0kMVxuIgogICAgICAgIGxh
# dW5jaGVyIDw8ICJzaGlmdFxuIgogICAgICAgIGxhdW5jaGVyIDw8ICJlY2hv
# IExhdW5jaGluZzogJEFQUFxuIgogICAgICAgIGxhdW5jaGVyIDw8ICIkQVBQ
# ICQqXG4iCiAgICAgIGVuZAogICAgICAKICAgICAgRmlsZS5jaG1vZCgwNzU1
# LCBsYXVuY2hlci5wYXRoKQogICAgICAKICAgIGVuZAogICAgCiAgICBkZWYg
# c2F2ZV9wa2djb25maWcKICAgICAgaWYgbm90IEBwa2dfY29uZmlncy5lbXB0
# eT8KICAgICAgICBAcGtnX2NvbmZpZ3MuZWFjaCB7IHxwa2dfY29uZmlnfAog
# ICAgICAgICAgRmlsZS5vcGVuKCBwa2dfY29uZmlnWzpwYWNrYWdlX25hbWVd
# KyIucGMiLCAidyIpIGRvIHxmaWxlfAogICAgICAgICAgICBmaWxlIDw8ICJw
# cmVmaXg9IiA8PCBAYXJndW1lbnRzWyJwcmVmaXgiXSA8PCAiXG4iCiAgICAg
# ICAgICAgIGZpbGUgPDwgImV4ZWNfcHJlZml4PSR7cHJlZml4fSIgPDwgIlxu
# IgogICAgICAgICAgICBmaWxlIDw8ICJsaWJkaXI9I3twa2dfY29uZmlnWzps
# aWJkaXJdLm5pbD8gPyAiJHtwcmVmaXh9L2xpYiIgOiBwa2dfY29uZmlnWzps
# aWJkaXJdIH0iIDw8ICJcbiIKICAgICAgICAgICAgZmlsZSA8PCAiaW5jbHVk
# ZWRpcj0je3BrZ19jb25maWdbOmluY2x1ZGVkaXJdLm5pbD8gPyAiJHtwcmVm
# aXh9L2luY2x1ZGUiIDogcGtnX2NvbmZpZ1s6aW5jbHVkZWRpcl0gfSIgPDwg
# IlxuXG5cbiIKICAgICAgICAgICAgCiAgICAgICAgICAgIGZpbGUgPDwgIk5h
# bWU6ICIgPDwgcGtnX2NvbmZpZ1s6cGFja2FnZV9uYW1lXSA8PCAiXG4iCiAg
# ICAgICAgICAgIGZpbGUgPDwgIkRlc2NyaXB0aW9uOiAiIDw8IHBrZ19jb25m
# aWdbOmRlc2NyaXB0aW9uXSA8PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwg
# IlJlcXVpcmVzOiAiIDw8IHBrZ19jb25maWdbOnJlcXVpcmVzXS5qb2luKCIg
# IikgPDwgIlxuIiBpZiBwa2dfY29uZmlnWzpyZXF1aXJlc10uY2xhc3MgPT0g
# QXJyYXkKICAgICAgICAgICAgZmlsZSA8PCAiVmVyc2lvbjogIiA8PCBwa2df
# Y29uZmlnWzp2ZXJzaW9uXSA8PCAiXG4iCiAgICAgICAgICAgIAogICAgICAg
# ICAgICBmaWxlIDw8ICJMaWJzOiAiIDw8IHBrZ19jb25maWdbOmxpYnNdIDw8
# ICJcbiIKICAgICAgICAgICAgZmlsZSA8PCAiQ2ZsYWdzOiAiIDw8IHBrZ19j
# b25maWdbOmNmbGFnc10gPDwgIlxuIgogICAgICAgICAgZW5kCiAgICAgICAg
# fQogICAgICBlbmQKICAgIGVuZAogICAgCiAgICAKICAgIGRlZiBsb2codHh0
# KQogICAgICBGaWxlLm9wZW4oQGxvZ19maWxlLCAiYSIpIGRvIHxmfAogICAg
# ICAgIGYgPDwgdHh0IDw8ICJcbiIKICAgICAgZW5kCiAgICBlbmQKICAgIAog
# ICAgZGVmIHVzYWdlCiAgICAgIHB1dHMgIk9wdGlvbnM6IgogICAgICBAb3B0
# aW9ucy5lYWNoIHsgfG9wdGlvbnwKICAgICAgICBjYXNlIG9wdGlvbls6dHlw
# ZV0KICAgICAgICAgIHdoZW4gInN0cmluZyIKICAgICAgICAgICAgcHJpbnQg
# IlxuICAtLSN7b3B0aW9uWzpuYW1lXX09W3ZhbHVlXSIKICAgICAgICAgICAg
# cHJpbnQgIlx0XHQuLi4uLi4je29wdGlvbls6ZGVzY3JpcHRpb25dfSAiCiAg
# ICAgICAgICAgIAogICAgICAgICAgICBpZiBub3Qgb3B0aW9uWzpkZWZhdWx0
# XS5uaWw/CiAgICAgICAgICAgICAgcHJpbnQgIltkZWZhdWx0PSN7b3B0aW9u
# WzpkZWZhdWx0XX1dIgogICAgICAgICAgICBlbmQKICAgICAgICAgIHdoZW4g
# InBhdGgiCiAgICAgICAgICAgIHByaW50ICJcbiAgLS0je29wdGlvbls6bmFt
# ZV19PVsvYS9wYXRoXSIKICAgICAgICAgICAgcHJpbnQgIlx0XHQuLi4uLi4j
# e29wdGlvbls6ZGVzY3JpcHRpb25dfSAiCiAgICAgICAgICAgIAogICAgICAg
# ICAgICBpZiBub3Qgb3B0aW9uWzpkZWZhdWx0XS5uaWw/CiAgICAgICAgICAg
# ICAgcHJpbnQgIltkZWZhdWx0PSN7b3B0aW9uWzpkZWZhdWx0XX1dIgogICAg
# ICAgICAgICBlbmQKICAgICAgICAgIHdoZW4gImJvb2wiCiAgICAgICAgICAg
# IHByaW50ICJcbiAgLS11c2UtI3tvcHRpb25bOm5hbWVdfT1beWVzfG5vXSIK
# ICAgICAgICAgICAgaWYgbm90IG9wdGlvbls6ZGVmYXVsdF0ubmlsPwogICAg
# ICAgICAgICAgIGRlZmF1bHQgPSBvcHRpb25bOmRlZmF1bHRdCiAgICAgICAg
# ICAgICAgaWYgZGVmYXVsdCA9PSB0cnVlIG9yIGRlZmF1bHQgPT0gInllcyIg
# b3IgZGVmYXVsdCA9PSAidHJ1ZSIKICAgICAgICAgICAgICAgIGRlZmF1bHQg
# PSAieWVzIgogICAgICAgICAgICAgIGVsc2lmIGRlZmF1bHQgPT0gZmFsc2Ug
# b3IgZGVmYXVsdCA9PSAibm8iIG9yIGRlZmF1bHQgPT0gImZhbHNlIgogICAg
# ICAgICAgICAgICAgZGVmYXVsdCA9ICJubyIKICAgICAgICAgICAgICBlbHNl
# CiAgICAgICAgICAgICAgICBkZWZhdWx0ID0gIm5vIgogICAgICAgICAgICAg
# IGVuZAogICAgICAgICAgICAgIAogICAgICAgICAgICAgIHByaW50ICJcdFx0
# Li4uLi4uI3tvcHRpb25bOmRlc2NyaXB0aW9uXX0gW2RlZmF1bHQ9I3tkZWZh
# dWx0fV0iCiAgICAgICAgICAgIGVuZAogICAgICAgICAgZWxzZQogICAgICAg
# ICAgICBwcmludCAiXG4gIC0tI3tvcHRpb25bOm5hbWVdfSIKICAgICAgICAg
# ICAgcHJpbnQgIlx0XHQuLi4uLi4je29wdGlvbls6ZGVzY3JpcHRpb25dfSIK
# ICAgICAgICBlbmQKICAgICAgfQogICAgICAKICAgICAgcHJpbnQgIlxuICAt
# LXdpdGgtY29uZmlnLWZyb250ZW5kPVtwbGFpbnx0a10iCiAgICAgIHByaW50
# ICJcdFx0Li4uLi4uU2V0cyB0aGUgZnJvbnRlbmQgdG8gY29uZmlndXJlIHRo
# ZSBhcHBsaWNhdGlvbiBbZGVmYXVsdD10a10iCiAgICAgIHByaW50ICJcbiAg
# LS1kZWJ1ZyIKICAgICAgcHJpbnQgIlx0XHQuLi4uLi5Db21waWxlIHdpdGgg
# ZGVidWcgc3VwcG9ydCBbZGVmYXVsdD1ub10iCiAgICAgIHByaW50ICJcbiAg
# LS1oZWxwIgogICAgICBwcmludCAiXG4gIC0tdmVyc2lvbiIKICAgICAgcHJp
# bnQgIlx0XHQuLi4uLi5TaG93cyB0aGlzIG1lc3NhZ2UiCiAgICAgIAogICAg
# ICBwdXRzICIiCiAgICBlbmQKICBlbmQKZW5kCgpiZWdpbgogIGxvYWQgInNl
# dHVwLnJiIgpyZXNjdWUgTG9hZEVycm9yCiAgRmlsZS5vcGVuKG9sZGxvY2F0
# aW9uKCJzZXR1cC5yYiIpLCAidyIpIGRvIHxmaWxlfAogICAgZmlsZSA8PCBG
# aWxlLnJlYWQoInNldHVwX3JiLnRwbCIpIDw8ICJcbiIKICBlbmQKICBwdXRz
# ICI9PiBBIHRlbXBsYXRlIGZpbGUgd2FzIGdlbmVyYXRlZCBpbiBzZXR1cC5y
# YiwgcGxlYXNlIGVkaXQgaXQgYW5kIHJlcnVuIHRoZSBjb25maWd1cmUgc2Ny
# aXB0LiIKICBleGl0KC0xKQplbmQKCmlmIF9fRklMRV9fID09ICQwCiAgcW9u
# ZiA9IFFvbmY6OkNvbmZpZ3VyZS5uZXcKICBxb25mLmdldF9hcmdzKEFSR1Yp
# CiAgcW9uZi5maW5kX3BhY2thZ2VzCiAgcW9uZi5ydW5fdGVzdHMKICAKICBx
# b25mLnNhdmUoImNvbmZpZy5wcmkiKQogIAogIHFvbmYuZ2VuZXJhdGVfbWFr
# ZWZpbGVzCmVuZAoKCgoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL2RldGVjdG9zLnJiAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAx
# NzUxADAwMDAwMDAxNzI0ADEwNjczNzQwMzAzADAxMzM1MgAgMAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKcmVxdWlyZSAncmJj
# b25maWcnCgptb2R1bGUgUW9uZgoKY2xhc3MgRGV0ZWN0T1MKCU9TID0gewoJ
# CTAgPT4gIlVua25vd24iLAoJCTEgPT4gIldpbmRvd3MiLAoJCTIgPT4gIlVu
# aXgiLAoJCTMgPT4gIkxpbnV4IiwKCQkzMSA9PiAiRGViaWFuIiwKCQkzMTEg
# PT4gIlVidW50dSIsCgkJMzIgPT4gIkdlbnRvbyIsCgkJNCA9PiAiTWFjIE9T
# IFgiCgl9CgkKCWRlZiBzZWxmLm9zPwoJCUBvcyA9IDAKCQkKCQlpZiA6OkNv
# bmZpZzo6Q09ORklHWyJidWlsZF9vcyJdID09ICJtc3dpbjMyIgoJCQlyZXR1
# cm4gMQoJCWVsc2UKCQkJQG9zID0gMgoJCQkKCQkJbGludXggPSBEZXRlY3RP
# Uy5maW5kX2xpbnV4CgkJCUBvcyA9IGxpbnV4IGlmIGxpbnV4ID4gMAoJCWVu
# ZAoJCQoJCQoJCXJldHVybiBAb3MKCWVuZAoJCglkZWYgc2VsZi5maW5kX2xp
# bnV4CgkJY3dkID0gRGlyLmdldHdkCgkJRGlyLmNoZGlyKCIvZXRjLyIpCgkJ
# CgkJRGlyWyIqey0sX317dmVyc2lvbixyZWxlYXNlfSJdLmVhY2ggeyB8cGF0
# aHwKCQkJaWYgcGF0aC5kb3duY2FzZSA9fiAvXihcdyopKC18XykodmVyc2lv
# bnxyZWxlYXNlKS8KCQkJCURldGVjdE9TOjpPUy5lYWNoIHsgfGlkLCBvc3wK
# CQkJCQlpZiBvcy5kb3duY2FzZSA9PSAkMQoJCQkJCQlEaXIuY2hkaXIoY3dk
# KQoJCQkJCQkKCQkJCQkJaWYgJDEgPT0gImRlYmlhbiIKCQkJCQkJCWlmIGBh
# cHQtY2FjaGUgLXZgLmluY2x1ZGU/KCJ1YnVudHUiKQoJCQkJCQkJCXJldHVy
# biAzMTEKCQkJCQkJCWVuZAoJCQkJCQllbmQKCQkJCQkJCgkJCQkJCXJldHVy
# biBpZAoJCQkJCWVuZAoJCQkJfQoJCQllbmQKCQl9CgkJCgkJRGlyLmNoZGly
# KGN3ZCkKCQkKCQlpZiBgdW5hbWVgLmRvd25jYXNlID09ICJsaW51eCIKCQkJ
# cmV0dXJuIDIKCQllbmQKCQkKCQlyZXR1cm4gMAoJZW5kCmVuZAoKZW5kCgpp
# ZiBfX0ZJTEVfXyA9PSAkMAoJcHV0cyBRb25mOjpEZXRlY3RPUzo6T1NbUW9u
# Zjo6RGV0ZWN0T1Mub3M/XQplbmQKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL21ha2VmaWxlLnJifgAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1
# MQAwMDAxNzUxADAwMDAwMDAyMjA0ADEwNzEyMTI2MDA3ADAxMzQ3NgAgMAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANCnJlcXVp
# cmUgJ3JiY29uZmlnJw0KDQptb2R1bGUgUW9uZg0KICBjbGFzcyBNYWtlZmls
# ZQ0KICAgIGRlZiBzZWxmLmZpbmRfbWFrZWZpbGVzKHBhdGgpDQogICAgICBt
# YWtlZmlsZXMgPSBbXQ0KICAgICAgRGlyLmZvcmVhY2gocGF0aCkgeyB8ZnwN
# CiAgICAgICAgZmlsZSA9ICIje3BhdGh9LyN7Zn0iDQogICAgICAgIGlmIEZp
# bGUuc3RhdChmaWxlKS5kaXJlY3Rvcnk/DQogICAgICAgICAgaWYgbm90IGYg
# PX4gL15cLi8NCiAgICAgICAgICAgIG1ha2VmaWxlcy5jb25jYXQgZmluZF9t
# YWtlZmlsZXMoZmlsZSkNCiAgICAgICAgICBlbmQNCiAgICAgICAgZWxzaWYg
# Zi5kb3duY2FzZSA9fiAvXm1ha2VmaWxlKC5kZWJ1Z3wucmVsZWFzZSl7MCwx
# fS8NCiAgICAgICAgICBtYWtlZmlsZXMgPDwgZmlsZQ0KICAgICAgICBlbmQN
# CiAgICAgIH0NCiAgICAgIG1ha2VmaWxlcw0KICAgIGVuZA0KICAgIA0KICAg
# IGRlZiBzZWxmLm92ZXJyaWRlKG1ha2VmaWxlLCBkZXN0ZGlyLCBzdGF0dXNG
# aWxlKQ0KICAgICAgbmV3bWFrZWZpbGUgPSAiIg0KICAgICAgDQogICAgICBy
# dWJ5X2JpbiA9ICIjezo6Q29uZmlnOjpDT05GSUdbImJpbmRpciJdfS8jezo6
# Q29uZmlnOjpDT05GSUdbInJ1YnlfaW5zdGFsbF9uYW1lIl19Ig0KICAgICAg
# DQogICAgICBpZiBGaWxlOjpBTFRfU0VQQVJBVE9SDQogICAgICAgIHJ1Ynlf
# YmluLmdzdWIhKCIvIiwgRmlsZTo6QUxUX1NFUEFSQVRPUikNCiAgICAgICAg
# ZGVzdGRpci5nc3ViISgiLyIsIEZpbGU6OkFMVF9TRVBBUkFUT1IpDQogICAg
# ICBlbmQNCiAgICAgIA0KICAgICAgRmlsZS5vcGVuKG1ha2VmaWxlLCAiciIp
# IHsgfGZ8DQogICAgICAgIGxpbmVzID0gZi5yZWFkbGluZXMNCiAgICAgIAkN
# CiAgICAgICAgbGluZXMuZWFjaCB7IHxsaW5lfA0KICAgICAgICAgIGlmIGxp
# bmUgPX4gL15JTlNUQUxMX0RJUi8NCiAgICAgICAgICAJbmV3bWFrZWZpbGUg
# Kz0gIklOU1RBTExfUk9PVCA9ICN7ZGVzdGRpcn0iDQogICAgICAgICAgZW5k
# DQogICAgICAgICAgbmV3bWFrZWZpbGUgKz0gbGluZQ0KICAgICAgICB9DQog
# ICAgICB9DQogICAgICANCiAgICAgIEZpbGUub3BlbihtYWtlZmlsZSwgInci
# KSB7IHxmfA0KICAgICAgICBmIDw8IG5ld21ha2VmaWxlDQogICAgICB9DQog
# ICAgZW5kDQogIGVuZA0KZW5kDQoNCg0KAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABx
# b25maWd1cmUvcGxhaW5fZnJvbnRlbmQucmIAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAwMDAwMDA0NjYA
# MTA2NzM3NDAzMDMAMDE0NTQ0ACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFyICAAa3Jhd2Vr
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAApyZXF1aXJlICdmcm9udGVuZCcKCm1vZHVsZSBR
# b25mCgpjbGFzcyBQbGFpbkZyb250ZW5kIDwgRnJvbnRlbmQKICBkZWYgcGFy
# c2Uob3B0aW9ucykKICAgIG9wdGlvbnMuZWFjaCB7IHxvcHRpb258CiAgICAg
# IGlmIG9wdGlvbls6dmFsdWVdLnRvX3MuZW1wdHk/IGFuZCBvcHRpb25bOm9w
# dGlvbmFsXSA9PSBmYWxzZQogICAgICAgIHB1dHMgIj0+IFRoZSBvcHRpb24g
# JyN7b3B0aW9uWzpuYW1lXX0nIGlzIHJlcXVpcmVkLiIKICAgICAgICByZXR1
# cm4gZmFsc2UKICAgICAgZW5kCiAgICB9CiAgICAKICAgIHRydWUKICBlbmQK
# ZW5kCgplbmQKCgoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL2Nv
# bmZpZy5yYgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAw
# MDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDA3NDE1ADEwNzMyMzY1Mjc2
# ADAxMzAyMAAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAKcmVxdWlyZSAnZGV0ZWN0b3MnCgptb2R1bGUgUW9uZgoKY2xhc3Mg
# UGFja2FnZQogIGF0dHJfcmVhZGVyIDppbmNsdWRlX3BhdGgsIDpsaWJzLCA6
# bW9kdWxlcywgOmRlZmluZXMsIDpuYW1lCiAgZGVmIGluaXRpYWxpemUobmFt
# ZSkKICAgIEBpbmNsdWRlX3BhdGggPSBbXQogICAgQGxpYnMgPSBbXQogICAg
# QG1vZHVsZXMgPSBbXQogICAgQGRlZmluZXMgPSBbXQogICAgQG5hbWUgPSBu
# YW1lCiAgZW5kCiAgCiAgZGVmIGFkZF9pbmNsdWRlX3BhdGgocGF0aCkKICAg
# IEBpbmNsdWRlX3BhdGggPDwgcGF0aAogIGVuZAogIAogIGRlZiBhZGRfbGli
# KGxpYikKICAgIEBsaWJzIDw8IGxpYi50b19zCiAgZW5kCiAgCiAgZGVmIGFk
# ZF9xdG1vZHVsZShtb2QpCiAgICBAbW9kdWxlcyA8PCBtb2QKICBlbmQKICAK
# ICBkZWYgYWRkX2RlZmluZShkZWZpbmUpCiAgICBAZGVmaW5lcyA8PCBkZWZp
# bmUKICBlbmQKICAKICBkZWYgd3JpdGUoZikKICAgIGlmIG5vdCBAaW5jbHVk
# ZV9wYXRoLmVtcHR5PwogICAgICAgIGYgPDwgIklOQ0xVREVQQVRIICs9ICIg
# PDwgQGluY2x1ZGVfcGF0aC51bmlxLmpvaW4oIiAiKSA8PCAiXG4iCiAgICBl
# bmQKICAgIAogICAgaWYgbm90IEBsaWJzLmVtcHR5PwogICAgICAgIGYgPDwg
# IkxJQlMgKz0gIiA8PCBAbGlicy51bmlxLmpvaW4oIiAiKSA8PCAiXG4iCiAg
# ICBlbmQKICAgIAogICAgaWYgbm90IEBtb2R1bGVzLmVtcHR5PwogICAgICAg
# IGYgPDwgIlFUICs9ICIgPDwgQG1vZHVsZXMudW5pcS5qb2luKCIgIikgPDwg
# IlxuIgogICAgZW5kCiAgICAKICAgIGlmIG5vdCBAZGVmaW5lcy5lbXB0eT8K
# ICAgICAgICBmIDw8ICJERUZJTkVTICs9ICIgPDwgQGRlZmluZXMudW5pcS5q
# b2luKCIgIikgPDwgIlxuIgogICAgZW5kCiAgZW5kCmVuZAoKY2xhc3MgQ29u
# ZmlnIDwgUGFja2FnZQogIGF0dHJfcmVhZGVyIDpidWlsZF9kaXIKICBkZWYg
# aW5pdGlhbGl6ZQogICAgc3VwZXIobmlsKQogICAgQHFvbmZfZmVhdHVyZXMg
# PSBbXQogICAgQHFvbmZfcGFja2FnZXMgPSBbXQogICAgQGJ1aWxkX2RpciA9
# IG9sZGxvY2F0aW9uKCIiKSsiL19idWlsZF8iCiAgICBAdXNlX2RlYnVnID0g
# ZmFsc2UKICBlbmQKICAKICBkZWYgd2l0aF9kZWJ1ZwogICAgQHVzZV9kZWJ1
# ZyA9IHRydWUKICBlbmQKCiAgZGVmIGFkZF9xb25mX3BhY2thZ2UocGtnKQog
# ICAgQHFvbmZfcGFja2FnZXMgPDwgcGtnCiAgZW5kCiAgCiAgZGVmIGFkZF9x
# b25mX2ZlYXR1cmUoZnRyKQogICAgQHFvbmZfZmVhdHVyZXMgPDwgZnRyCiAg
# ZW5kIAoKICBkZWYgaW5jbHVkZV9wYXRoCiAgICBpbmNwYXRoID0gQGluY2x1
# ZGVfcGF0aC5kdXAKICAgIAogICAgQHFvbmZfcGFja2FnZXMuZWFjaCB7IHxw
# a2d8CiAgICAgICAgaW5jcGF0aC5jb25jYXQgcGtnLmluY2x1ZGVfcGF0aAog
# ICAgfQogICAgCiAgICBpbmNwYXRoCiAgZW5kCiAgCiAgZGVmIGxpYnMKICAg
# IGxpYiA9IEBsaWJzLmR1cAogICAgQHFvbmZfcGFja2FnZXMuZWFjaCB7IHxw
# a2d8CiAgICAgICAgbGliLmNvbmNhdCBwa2cubGlicwogICAgfQogICAgCiAg
# ICBsaWIKICBlbmQKICAKICBkZWYgbGlicmFyeV9wYXRoCiAgICBsaWJzX2Eg
# PSBbXQoKICAgIHNwbGl0dGVkID0gQGxpYnMuam9pbigiICIpLnNwbGl0KCIg
# IikKICAgIHNwbGl0dGVkLmVhY2ggeyB8c3B8CiAgICAgICAgaWYoIHNwID1+
# IC8tTCguKylccyovICkKICAgICAgICAgICAgbGlic19hIDw8ICQxCiAgICAg
# ICAgZW5kCiAgICB9CiAgICAKICAgIGxpYnNfYQogIGVuZAogIAogIGRlZiBk
# ZWZpbmVzCiAgICBkZWZzID0gQGRlZmluZXMuZHVwCiAgICAKICAgIEBxb25m
# X3BhY2thZ2VzLmVhY2ggeyB8cGtnfAogICAgICAgIGRlZnMuY29uY2F0IHBr
# Zy5kZWZpbmVzCiAgICB9CiAgICAKICAgIGRlZnMKICBlbmQKICAKICBkZWYg
# c2F2ZShwYXRoKQogICAgaWYgcGF0aFswXS5jaHIgPT0gRmlsZTo6U0VQQVJB
# VE9SCiAgICAgICAgcGF0aCA9IHBhdGgKICAgIGVsc2UKICAgICAgICBwYXRo
# ID0gIiN7RGlyLmdldHdkfS8je3BhdGh9IgogICAgZW5kCiAgICAKICAgIEZp
# bGUub3BlbihwYXRoLCAidyIpIHsgfGZ8CiAgICAgICAgZiA8PCAiIyBHZW5l
# cmF0ZWQgYXV0b21hdGljYWxseSBhdCAje1RpbWUubm93fSEgUExFQVNFIERP
# IE5PVCBFRElUIE1BTlVBTExZISI8PCAiXG4iCiAgICAgICAgCiAgICAgICAg
# ZiA8PCAiUFJPSkVDVF9ESVIgPSAiIDw8IG9sZGxvY2F0aW9uKCIiKSA8PCAi
# XG4iCiAgICAgICAgZiA8PCAiQlVJTERfRElSID0gI3tAYnVpbGRfZGlyfVxu
# IgogICAgICAgIAogICAgICAgIGYgPDwgJ2NvbnRhaW5zKFRFTVBMQVRFLCAi
# YXBwIikgeycgPDwgIlxuIgogICAgICAgIGYgPDwgIkRFU1RESVIgPSAkJEJV
# SUxEX0RJUi9iaW5cbiIKICAgICAgICBmIDw8ICd9IGVsc2U6Y29udGFpbnMo
# VEVNUExBVEUsICJsaWIiKSB7JyA8PCAiXG4iCiAgICAgICAgZiA8PCAiREVT
# VERJUiA9ICQkQlVJTERfRElSL2xpYlxuIgogICAgICAgIGYgPDwgJ30gZWxz
# ZSB7JyA8PCAiXG4iCiAgICAgICAgZiA8PCAiREVTVERJUiA9ICQkQlVJTERf
# RElSXG4iCiAgICAgICAgZiA8PCAifVxuXG5cbiIKICAgICAgICAKICAgICAg
# ICBpZiBAdXNlX2RlYnVnCiAgICAgICAgICAgIGYgPDwgIkNPTkZJRyArPSBk
# ZWJ1Z1xuIgogICAgICAgIGVsc2UKICAgICAgICAgICAgZiA8PCAiQ09ORklH
# ICs9IHJlbGVhc2VcbiIKICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBm
# IDw8ICJMSUJTICs9IC1MJCRCVUlMRF9ESVIvbGliXG4iCiAgICAgICAgZiA8
# PCAiUkVMQVRJVkVfUEFUSCA9ICQkcmVwbGFjZShfUFJPX0ZJTEVfUFdEXywg
# JCRQUk9KRUNUX0RJUiwgIiIpXG4iCiAgICAgICAgZiA8PCAiQ1VSUkVOVF9C
# VUlMRF9ESVIgPSAkJEJVSUxEX0RJUiQkUkVMQVRJVkVfUEFUSFxuIgogICAg
# ICAgIAppZiBEZXRlY3RPUy5vcz8gIT0gMSAjIERpc2FibGVkIGJlY2F1c2Ug
# V2luZG93cyBzdWNrcy4KICAgICAgICBmIDw8ICJPQkpFQ1RTX0RJUiA9ICQk
# Q1VSUkVOVF9CVUlMRF9ESVJcbiIKICAgICAgICBmIDw8ICJNT0NfRElSID0g
# JCRDVVJSRU5UX0JVSUxEX0RJUlxuIgogICAgICAgIGYgPDwgIlVJX0RJUiA9
# ICQkQ1VSUkVOVF9CVUlMRF9ESVJcblxuIgplbmQKICAgICAgICAKICAgICAg
# ICB3cml0ZShmKQogICAgICAgIAogICAgICAgIAogICAgICAgIGYgPDwgIlFP
# TkZfRkVBVFVSRVMgKz0gIiA8PCBAcW9uZl9wYWNrYWdlcy5tYXAgeyB8ZXwg
# ZS5uYW1lIH0uam9pbigiICIpICA8PCAiXG4iCiAgICAgICAgZiA8PCAiUU9O
# Rl9GRUFUVVJFUyArPSAiIDw8IEBxb25mX2ZlYXR1cmVzLmpvaW4oIiAiKSAg
# PDwgIlxuIgogICAgICAgIAogICAgICAgIGYgPDwgIlxuZGVmaW5lVGVzdChs
# aW5rX3dpdGgpIHsiIDw8ICJcbiIKICAgICAgICBmIDw8ICIgIHJldCA9IGZh
# bHNlXG4iCiAgICAgICAgQHFvbmZfcGFja2FnZXMuZWFjaCB7IHxxb25mfAog
# ICAgICAgICAgZiA8PCAlQCAgY29udGFpbnMoJCRsaXN0KCQkc3BsaXQoMSkp
# ICwiI3txb25mLm5hbWV9Iikge1xuQAogICAgICAgICAgZiA8PCAiICAgIHJl
# dCA9IHRydWVcbiIKICAgICAgICAgIGYgPDwgIiAgICBtZXNzYWdlKD0+IExp
# bmtpbmcgcW9uZiBtb2R1bGU6ICN7cW9uZi5uYW1lfSlcbiIKICAgICAgICAg
# IHFvbmYud3JpdGUoZikKICAgICAgICAgIGYgPDwgIiAgfVxuIgogICAgICAg
# IH0KICAgICAgICBmIDw8ICIgIGV4cG9ydChJTkNMVURFUEFUSClcbiIKICAg
# ICAgICBmIDw8ICIgIGV4cG9ydChMSUJTKVxuIgogICAgICAgIGYgPDwgIiAg
# ZXhwb3J0KFFUKVxuIgogICAgICAgIGYgPDwgIiAgZXhwb3J0KERFRklORVMp
# XG4iCiAgICAgICAgZiA8PCAiICByZXR1cm4oJCRyZXQpXG4iCiAgICAgICAg
# ZiA8PCAifVxuIgogICAgfQogIGVuZAplbmQKCmVuZAoKAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAcW9uZmlndXJlL3BrZ2NvbmZpZy5yYgAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAw
# MDA2NzE0ADEwNzIzMzQzMjI0ADAxMzUxMAAgMAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAg
# AGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKCnJlcXVpcmUgJ2RldGVjdG9zJwoK
# bW9kdWxlIFFvbmYKCmNsYXNzIFBrZ0NvbmZpZ1BhcnNlcgogIGRlZiBpbml0
# aWFsaXplCiAgICBAdmFyaWFibGVzID0ge30KICAgIEBzcGVjcyA9IHt9CiAg
# ZW5kCiAgCiAgZGVmIHBhcnNlKHBhdGgpCiAgICBGaWxlLm9wZW4ocGF0aCkg
# eyB8ZmlsZXwKICAgICAgZmlsZS5lYWNoIHsgfGxpbmV8CiAgICAgICAgaWYg
# bGluZSA9fiAvKFx3Kyk9KC4qKSQvCiAgICAgICAgICBAdmFyaWFibGVzWyQx
# XSA9ICQyCiAgICAgICAgZWxzaWYgbGluZSA9fiAvKFx3Kyk6XHMoLiopJC8K
# ICAgICAgICAgIEBzcGVjc1skMV0gPSAkMgogICAgICAgIGVuZAogICAgICB9
# CiAgICB9CiAgZW5kCiAgCiAgZGVmIHZhbHVlKHZhcmlhYmxlKQogICAgdmFs
# ID0gQHZhcmlhYmxlc1t2YXJpYWJsZV0KICAgIHdoaWxlIHZhbCA9fiAvKFwk
# XHsoXHcrKVx9KS8KICAgICAgdmFsLmdzdWIhKCQxLCB2YWx1ZSgkMikpCiAg
# ICBlbmQKICAgIAogICAgdmFsCiAgZW5kCiAgCiAgZGVmIHNwZWModmFyaWFi
# bGUpCiAgICB2YWwgPSBAc3BlY3NbdmFyaWFibGVdCiAgICB3aGlsZSB2YWwg
# PX4gLyhcJFx7KFx3KylcfSkvCiAgICAgIHZhbC5nc3ViISgkMSwgdmFsdWUo
# JDIpKQogICAgZW5kCiAgICAKICAgIHZhbAogIGVuZAogIAogIGRlZiBsaWJz
# CiAgICBzcGVjKCJMaWJzIikKICBlbmQKICAKICBkZWYgY2ZsYWdzCiAgICBz
# cGVjKCJDZmxhZ3MiKQogIGVuZAogIAogIGRlZiByZXF1aXJlcwogICAgc3Bl
# YygiUmVxdWlyZXMiKS50b19zCiAgZW5kCmVuZAoKY2xhc3MgUGtnQ29uZmln
# CiAgYXR0cl9yZWFkZXIgOmlkLCA6bGlicywgOmNmbGFncywgOnJlcXVpcmVz
# CiAgCiAgZGVmIGluaXRpYWxpemUoaWQpCiAgICBAaWQgPSBpZAogICAgQHNl
# YXJjaF9wYXRoID0gW10KICAgIEByZXF1aXJlcyA9IFtdCiAgZW5kCiAgCiAg
# ZGVmIGZpbmQocGtnKQogICAgcGtncGF0aCA9IGZpbmRfcGF0aF9mb3JfcGFj
# a2FnZShwa2cpCiAgICAKICAgIGlmIG5vdCBwa2dwYXRoLm5pbD8KICAgICAg
# QGNmbGFncyA9ICIiCiAgICAgIEBsaWJzID0gIiIKICAgICAgcGFyc2UocGtn
# cGF0aCwgcGtnKQogICAgICAKICAgICAgcmV0dXJuIHRydWUKICAgIGVuZAog
# ICAgCiAgICBmYWxzZQogIGVuZAogIAogIGRlZiBhZGRfc2VhcmNoX3BhdGgo
# cGF0aCkKICAgIGlmIHBhdGgua2luZF9vZj8oQXJyYXkpCiAgICAgIEBzZWFy
# Y2hfcGF0aC5jb25jYXQocGF0aCkKICAgIGVsc2UKICAgICAgQHNlYXJjaF9w
# YXRoIDw8IHBhdGgudG9fcwogICAgZW5kCiAgZW5kCiAgCiAgZGVmIGluY2x1
# ZGVfcGF0aAogICAgaW5jcGF0aCA9IFtdCiAgICBjZmxhZ3MgPSBAY2ZsYWdz
# LmR1cAogICAgd2hpbGUgY2ZsYWdzID1+IC8oLUkoKFx3Oil7MCwxfShbXC9c
# XF1cUyspKykpLwogICAgICBpbmNwYXRoIDw8ICQyCiAgICAgIGNmbGFncy5z
# dWIhKCQxLCAiIikKICAgIGVuZAogICAgCiAgICBpbmNwYXRoLnVuaXEKICBl
# bmQKICAKICBkZWYgZGVmaW5lcwogICAgZGVmaW5lcyA9IFtdCiAgICBjZmxh
# Z3MgPSBAY2ZsYWdzLmR1cAogICAgd2hpbGUgY2ZsYWdzID1+IC8oLURccyoo
# KFxXXHcrKSspKS8KICAgICAgZGVmaW5lcyA8PCAkMgogICAgICBkZWZpbmVz
# LnN1YiEoJDEsICIiKQogICAgZW5kCiAgICAKICAgIGRlZmluZXMudW5pcQog
# IGVuZAogIAogIGRlZiBmaW5kX3dpbjMyX3BhdGhzCiAgICBwYXRocyA9IGZp
# bmRfZW52X3BhdGhzCiAgICAKICAgIAogICAgcGF0aHMKICBlbmQKICAKICBk
# ZWYgZmluZF91bml4X3BhdGhzCiAgICBwYXRocyA9IGZpbmRfZW52X3BhdGhz
# CiAgICBpZiBGaWxlLmV4aXN0cz8oIi9ldGMvbGQuc28uY29uZiIpCiAgICAg
# IEZpbGUub3BlbigiL2V0Yy9sZC5zby5jb25mIikgZG8gfGZpbGV8CiAgICAg
# ICAgZmlsZS5lYWNoIHsgfGxpbmV8CiAgICAgICAgICBkaXIgPSBsaW5lLnN0
# cmlwKyIvcGtnY29uZmlnIgogICAgICAgICAgaWYgRmlsZS5leGlzdHM/KGRp
# cikKICAgICAgICAgICAgcGF0aHMgPDwgZGlyCiAgICAgICAgICBlbmQKICAg
# ICAgICB9CiAgICAgIGVuZAogICAgZW5kCiAgICAKICAgIHBhdGhzIDw8ICIv
# dXNyL2xpYi9wa2djb25maWciIDw8ICIvdXNyL2xvY2FsL2xpYi9wa2djb25m
# aWciIDw8IEVOVlsiSE9NRSJdLnRvX3MrIi9sb2NhbC9saWIvcGtnY29uZmln
# IgogICAgCiAgICBwYXRocwogIGVuZAogIAogIGRlZiBmaW5kX2Vudl9wYXRo
# cwogICAgc2VwcGF0aCA9IChEZXRlY3RPUy5vcz8gPT0gMSkgPyAiOyIgOiAi
# OiIKICAgIAogICAgcGF0aHMgPSBbICIuIiBdCiAgICAoRU5WWyJQS0dfQ09O
# RklHX1BBVEgiXS50b19zK3NlcHBhdGgrRU5WWyJMRF9MSUJSQVJZX1BBVEgi
# XS50b19zKS5zcGxpdChzZXBwYXRoKS5lYWNoIHsgfHBhdGh8CiAgICAgIGlm
# IEZpbGUuZXhpc3RzPyhwYXRoKQogICAgICAgIHBhdGhzIDw8IHBhdGgKICAg
# ICAgZW5kCiAgICB9CiAgICAKICAgIHBhdGhzCiAgZW5kCiAgCiAgcHJpdmF0
# ZQogIGRlZiBwYXJzZShwa2dwYXRoLCBwa2cpCiAgICByZXR1cm4gaWYgcGtn
# cGF0aC5uaWw/CiAgICAKICAgIHBhcnNlciA9IFBrZ0NvbmZpZ1BhcnNlci5u
# ZXcKICAgIHBhcnNlci5wYXJzZShwa2dwYXRoKQogICAgCiAgICBwYXJzZXIu
# cmVxdWlyZXMuc3BsaXQoLyx8XHMvKS5lYWNoIHsgfHJlcXwKICAgICAgcmVx
# LnN0cmlwIQogICAgICAKICAgICAgcGFyc2UoZmluZF9wYXRoX2Zvcl9wYWNr
# YWdlKHJlcSksIHJlcSkKICAgIH0KICAgIAogICAgY2ZsYWdzID0gcGFyc2Vy
# LmNmbGFncwogICAgbGlicyA9IHBhcnNlci5saWJzCiAgICAKICAgIGlmIGlk
# ICE9IHBrZyBhbmQgbm90IChjZmxhZ3MuZW1wdHk/IG9yIGxpYnMuZW1wdHk/
# KQogICAgICBAcmVxdWlyZXMgPDwgcGtnCiAgICBlbmQKICAgIAogICAgQGNm
# bGFncyA9ICIje2NmbGFnc30gI3tAY2ZsYWdzfSIKICAgIEBsaWJzID0gIiN7
# bGlic30gI3tAbGlic30iCiAgICAKICAgIG5pbAogIGVuZAogIAogIGRlZiBm
# aW5kX3BhdGhfZm9yX3BhY2thZ2UocGtnKQogICAgaWYgbm90IHBrZyA9fiAv
# XC5wYyQvCiAgICAgIHBrZyArPSAiLnBjIgogICAgZW5kCiAgICAKICAgIHBh
# dGhzID0gQHNlYXJjaF9wYXRoCiAgICAKICAgIGlmIERldGVjdE9TLm9zPyA9
# PSAxICMgV2luZG93cwogICAgICBwYXRocy5jb25jYXQgZmluZF93aW4zMl9w
# YXRocwogICAgZWxzZQogICAgICBwYXRocy5jb25jYXQgZmluZF91bml4X3Bh
# dGhzCiAgICBlbmQKICAgIAogICAgcGtncGF0aCA9IG5pbAogICAgcGF0aHMu
# ZWFjaCB7IHxwYXRofAogICAgICB0bXBwYXRoID0gcGF0aCsiLyIrcGtnCiAg
# ICAgIGlmIEZpbGUuZXhpc3RzPyh0bXBwYXRoKQogICAgICAgIHBrZ3BhdGgg
# PSB0bXBwYXRoCiAgICAgIGVsc2lmICRERUJVRwogICAgICAgIHB1dHMgIkZh
# aWxlZDogI3t0bXBwYXRofSIKICAgICAgZW5kCiAgICB9CiAgICAKICAgIHBr
# Z3BhdGgKICBlbmQKZW5kCgplbmQKCgppZiAkMCA9PSBfX0ZJTEVfXwogIHBr
# Z2NvbmZpZyA9IFFvbmY6OlBrZ0NvbmZpZy5uZXcKICBwa2djb25maWcuZmlu
# ZCgidGVzdF9xb25mIikKICAKICBwdXRzIHBrZ2NvbmZpZy5saWJzCmVuZAoK
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAHFvbmZpZ3VyZS90a19mcm9udGVuZC5yYgAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAw
# MDAwNjI3MwAxMDY3Mzc0MDMwMwAwMTQwNjEAIDAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIg
# IABrcmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgoKCnJlcXVpcmUgJ2Zyb250ZW5k
# JwoKbW9kdWxlIFFvbmYKICBjbGFzcyBUa0Zyb250ZW5kIDwgRnJvbnRlbmQK
# ICAgIGRlZiBpbml0aWFsaXplCiAgICBlbmQKICAgIAogICAgZGVmIHBhcnNl
# KG9wdHMpCiAgICAgIHJvb3QgPSBUa1Jvb3QubmV3KCkgeyB0aXRsZSAiQ29u
# ZmlndXJlLi4uIiB9CiAgICAgIG9wdHMuZWFjaCB7IHxvcHRpb258CiAgICAg
# ICAgbGFiZWwgPSBUa0xhYmVsLm5ldyhyb290KSB7IHRleHQgb3B0aW9uWzpu
# YW1lXSB9CiAgICAgICAgCiAgICAgICAgY2FzZSBvcHRpb25bOnR5cGVdLnRv
# X3MuZG93bmNhc2UKICAgICAgICAgIHdoZW4gInN0cmluZyIKICAgICAgICAg
# ICAgZW50cnkgPSBUa0VudHJ5Lm5ldyhyb290KQogICAgICAgICAgICBlbnRy
# eS5pbnNlcnQoMCwgKG9wdGlvbls6dmFsdWVdLm5pbD8gPyBvcHRpb25bOmRl
# ZmF1bHRdLnRvX3MgOiBvcHRpb25bOnZhbHVlXS50b19zICApKQogICAgICAg
# ICAgICBUa0dyaWQuZ3JpZChsYWJlbCwgZW50cnksICJjb2x1bW5zcGFuIiA9
# PiAyKQogICAgICAgICAgICAKICAgICAgICAgICAgb3B0aW9uWzppbnB1dF0g
# PSBlbnRyeQogICAgICAgICAgd2hlbiAicGF0aCIKICAgICAgICAgICAgZW50
# cnkgPSBUa0VudHJ5Lm5ldyhyb290KQogICAgICAgICAgICBlbnRyeS5pbnNl
# cnQoMCwgKG9wdGlvbls6dmFsdWVdLm5pbD8gPyBvcHRpb25bOmRlZmF1bHRd
# LnRvX3MgOiBvcHRpb25bOnZhbHVlXS50b19zICApKQogICAgICAgICAgICAK
# ICAgICAgICAgICAgZ2V0RmlsZSA9IFRrQnV0dG9uLm5ldyhyb290KSB7CiAg
# ICAgICAgICAgICAgdGV4dCAiQnJvd3NlLi4uIgogICAgICAgICAgICAgIGNv
# bW1hbmQgewogICAgICAgICAgICAgICAgdmFsID0gVGsuY2hvb3NlRGlyZWN0
# b3J5CiAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgIGVudHJ5LnZh
# bHVlID0gdmFsIGlmIG5vdCB2YWwudG9fcy5lbXB0eT8KICAgICAgICAgICAg
# ICB9CiAgICAgICAgICAgIH0KICAgICAgICAgICAgVGtHcmlkLmdyaWQobGFi
# ZWwsIGVudHJ5LCBnZXRGaWxlLCAiY29sdW1uc3BhbiIgPT4gMikKICAgICAg
# ICAgICAgCiAgICAgICAgICAgIG9wdGlvbls6aW5wdXRdID0gZW50cnkKICAg
# ICAgICAgIGVsc2UKICAgICAgICAgICAgZ2l2ZW4gPSAob3B0aW9uWzp2YWx1
# ZV0ubmlsPyA/IG9wdGlvbls6ZGVmYXVsdF0udG9fcyA6IG9wdGlvbls6dmFs
# dWVdLnRvX3MgICkKICAgICAgICAgICAgCiAgICAgICAgICAgIGlmIGdpdmVu
# ID09ICJmYWxzZSIgb3IgZ2l2ZW4gPT0gIm5vIiBvciBnaXZlbi5lbXB0eT8K
# ICAgICAgICAgICAgICB2YWx1ZSA9IDAKICAgICAgICAgICAgZWxzZQogICAg
# ICAgICAgICAgIHZhbHVlID0gMQogICAgICAgICAgICBlbmQKICAgICAgICAg
# ICAgCiAgICAgICAgICAgIG15Y2hlY2sgPSBUa1ZhcmlhYmxlLm5ldyh2YWx1
# ZSkKICAgICAgICAgICAgY2hlY2sgPSBUa0NoZWNrQnV0dG9uLm5ldyhyb290
# KSB7CiAgICAgICAgICAgICAgdmFyaWFibGUgbXljaGVjawogICAgICAgICAg
# ICB9CiAgICAgICAgICAgIAogICAgICAgICAgICBUa0dyaWQuZ3JpZChsYWJl
# bCwgY2hlY2ssICJjb2x1bW5zcGFuIiA9PiAyKQogICAgICAgICAgICAKICAg
# ICAgICAgICAgb3B0aW9uWzppbnB1dF0gPSBteWNoZWNrCiAgICAgICAgZW5k
# CiAgICAgIH0KICAgICAgCiAgICAgIG9rID0gVGtCdXR0b24ubmV3KHJvb3Qp
# IHsKICAgICAgICB0ZXh0ICJPayIKICAgICAgICBjb21tYW5kICB7CiAgICAg
# ICAgICBvayA9IHRydWUKICAgICAgICAgIHJlcXVpcmVkX2ZpZWxkcyA9IFtd
# CiAgICAgICAgICAKICAgICAgICAgIG9wdHMuZWFjaCB7IHxvcHRpb258CiAg
# ICAgICAgICAgIHZhbHVlID0gb3B0aW9uWzpkZWZhdWx0XQogICAgICAgICAg
# ICBjYXNlIG9wdGlvbls6dHlwZV0uZG93bmNhc2UKICAgICAgICAgICAgICB3
# aGVuICJzdHJpbmciCiAgICAgICAgICAgICAgICB2YWx1ZSA9IG9wdGlvbls6
# aW5wdXRdLnZhbHVlCiAgICAgICAgICAgICAgd2hlbiAicGF0aCIKICAgICAg
# ICAgICAgICAgIHZhbHVlID0gRmlsZS5leHBhbmRfcGF0aChvcHRpb25bOmlu
# cHV0XS52YWx1ZSkKICAgICAgICAgICAgICBlbHNlCiAgICAgICAgICAgICAg
# ICB2YWx1ZSA9IG9wdGlvbls6aW5wdXRdLnZhbHVlCiAgICAgICAgICAgIGVu
# ZAogICAgICAgICAgICAKICAgICAgICAgICAgaWYgb3B0aW9uWzpvcHRpb25h
# bF0gPT0gZmFsc2UgYW5kIHZhbHVlLnRvX3MuZW1wdHk/CiAgICAgICAgICAg
# ICAgcmVxdWlyZWRfZmllbGRzIDw8IG9wdGlvbls6bmFtZV0KICAgICAgICAg
# ICAgICBvayA9IGZhbHNlCiAgICAgICAgICAgIGVsc2UKICAgICAgICAgICAg
# ICBvcHRpb25bOnZhbHVlXSA9IHZhbHVlCiAgICAgICAgICAgIGVuZAogICAg
# ICAgICAgICAKICAgICAgICAgICAgb2sgPSBvayAmJiB0cnVlCiAgICAgICAg
# ICB9CiAgICAgICAgICAKICAgICAgICAgIGlmIG9rCiAgICAgICAgICAgIFRr
# LmV4aXQKICAgICAgICAgIGVsc2UKICAgICAgICAgICAgVGtXYXJuaW5nLnNo
# b3coIlRoZSBmaWVsZHMgJyN7cmVxdWlyZWRfZmllbGRzLmpvaW4oIiwgIil9
# JyBhcmUgcmVxdWlyZWQuIikKICAgICAgICAgIGVuZAogICAgICAgIH0KICAg
# ICAgfQogICAgICAKICAgICAgY2FuY2VsID0gVGtCdXR0b24ubmV3KHJvb3Qp
# IHsKICAgICAgICB0ZXh0ICJDYW5jZWwiCiAgICAgICAgY29tbWFuZCAgewog
# ICAgICAgICAgVGsuZXhpdAogICAgICAgICAgZXhpdCAwCiAgICAgICAgfQog
# ICAgICB9CiAgICAgICAgCiAgICAgIFRrR3JpZC5ncmlkKG9rLCBjYW5jZWws
# ICJjb2x1bW5zcGFuIiA9PiAzKQogICAgICAKICAgICAgVGsubWFpbmxvb3AK
# ICAgICAgCiAgICAgIHRydWUKICAgIGVuZAogIGVuZAplbmQKCgppZiBfX0ZJ
# TEVfXyA9PSAkMCAjIFRlc3QKICBvcHRzID0gW10KICBvcHRzIDw8IHs6bmFt
# ZSA9PiAibmFtZTEiLCA6dHlwZSA9PiAiU3RyaW5nIiwgOmRlZmF1bHQgPT4g
# InZhbHVlIiwgOm9wdGlvbmFsID0+IGZhbHNlIH0KICBvcHRzIDw8IHs6bmFt
# ZSA9PiAibmFtZTQiLCA6dHlwZSA9PiAiUGF0aCIsIDpkZWZhdWx0ID0+ICJ2
# YWx1ZSIgfQogIG9wdHMgPDwgezpuYW1lID0+ICJuYW1lMiIsIDp0eXBlID0+
# ICJCb29sIiwgOmRlZmF1bHQgPT4gdHJ1ZSB9CiAgb3B0cyA8PCB7Om5hbWUg
# PT4gIm5hbWUzIiwgOnR5cGUgPT4gIlZvaWQiLCA6ZGVmYXVsdCA9PiBmYWxz
# ZSB9CiAgCiAgdGsgPSBRb25mOjpUa0Zyb250ZW5kLm5ldwogIHRrLnBhcnNl
# KG9wdHMpCiAgCiAgcHV0cyBvcHRzLmZpcnN0Wzp2YWx1ZV0KZW5kCgoKCgAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAABxb25maWd1cmUvdGVzdC5yYn4AAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAw
# MDAwMDUxNzcAMTA2NzMwMjI3MDQAMDEyNzIxACAwAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFy
# ICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHJlcXVpcmUgJ3RtcGRpcicKCm1v
# ZHVsZSBRb25mCiAgY2xhc3MgVGVzdAogICAgYXR0cl9yZWFkZXIgOmluY2x1
# ZGVfcGF0aCwgOmxpYnMsIDpkZWZpbmVzCiAgICAKICAgIGRlZiBpbml0aWFs
# aXplKG9wdGlvbnMpCiAgICAgIEBvcHRpb25zID0gb3B0aW9ucwogICAgICAK
# ICAgICAgQGluY2x1ZGVfcGF0aCA9IFtdCiAgICAgIEBsaWJzID0gW10KICAg
# ICAgQGRlZmluZXMgPSBbXQogICAgICAKICAgICAgQGluY2x1ZGVfcGF0aCA9
# IG9wdGlvbnNbOmluY2x1ZGVwYXRoXSBpZiBub3Qgb3B0aW9uc1s6aW5jbHVk
# ZXBhdGhdLm5pbD8KICAgICAgQGxpYnMgPDwgb3B0aW9uc1s6bGlic10gaWYg
# bm90IG9wdGlvbnNbOmxpYnNdLm5pbD8KICAgICAgQGRlZmluZXMgPSBvcHRp
# b25zWzpkZWZpbmVzXSBpZiBub3Qgb3B0aW9uc1s6ZGVmaW5lc10ubmlsPwog
# ICAgZW5kCiAgICAKICAgIGRlZiBydW4ocW1ha2UpCiAgICAgICMgR2VuZXJh
# dGUKICAgICAgcmV0ID0gZmFsc2UKICAgICAgY3dkID0gRGlyLmdldHdkCiAg
# ICAgIAogICAgICBxb25mdGVzdF9kaXIgPSAiI3tEaXI6OnRtcGRpcn0vcW9u
# Zl90ZXN0cy4je1Byb2Nlc3MucGlkfSIKICAgICAgYmFzZXBhdGggPSBxb25m
# dGVzdF9kaXIrIi90ZXN0XyN7UHJvY2Vzcy5waWR9IgogICAgICAKICAgICAg
# ZmlsZXBhdGggPSBiYXNlcGF0aCsiLmNwcCIKICAgICAgcHJvcGF0aCA9IGJh
# c2VwYXRoKyIucHJvIgogICAgICB0YXJnZXQgPSAidGVzdF8je1Byb2Nlc3Mu
# cGlkfV9hcHAiCiAgICAgIAogICAgICBiZWdpbgogICAgICAgIERpci5ta2Rp
# cihxb25mdGVzdF9kaXIpIGlmIG5vdCBGaWxlLmV4aXN0cz8ocW9uZnRlc3Rf
# ZGlyKQogICAgICAgIEZpbGUub3BlbihmaWxlcGF0aCwgInciKSB7IHxmfAog
# ICAgICAgICAgCiAgICAgICAgICB3cml0ZV90ZXN0KGYpCiAgICAgICAgICAK
# ICAgICAgICAgIEZpbGUub3Blbihwcm9wYXRoLCAidyIpIHsgfHBmfAogICAg
# ICAgICAgICBwZiA8PCAiU09VUkNFUyArPSAiIDw8IGZpbGVwYXRoIDw8ICJc
# biIKICAgICAgICAgICAgcGYgPDwgIlRFTVBMQVRFID0gYXBwIiA8PCAiXG4i
# CiAgICAgICAgICAgIHBmIDw8ICJUQVJHRVQgPSAiIDw8ICJcbiIKICAgICAg
# ICAgICAgcGYgPDwgIklOQ0xVREVQQVRIICs9ICIgPDwgQGluY2x1ZGVfcGF0
# aC5qb2luKCIgIikgPDwgIlxuIgogICAgICAgICAgICBwZiA8PCAiTElCUyAr
# PSAiIDw8IEBsaWJzLmpvaW4oIiAiKSA8PCAiXG4iCiAgICAgICAgICAgIHBm
# IDw8ICJERUZJTkVTICs9ICIgPDwgQGRlZmluZXMuam9pbigiICIpIDw8ICJc
# biIKICAgICAgICAgIH0KICAgICAgICB9CiAgICAgICAgCiAgICAgICAgCiAg
# ICAgICAgRGlyLmNoZGlyKHFvbmZ0ZXN0X2RpcikKICAgICAgICAKICAgICAg
# ICBxbWFrZS5ydW4ocHJvcGF0aCkKICAgICAgICByZXQgPSBxbWFrZS5jb21w
# aWxlCiAgICAgICAgcW1ha2UuY2xlYW5fdXAKICAgICAgICAKICAgICAgICBE
# aXIuY2hkaXIoY3dkKQogICAgICAgIAogICAgICAgIEZpbGUudW5saW5rKHRh
# cmdldCkgaWYgRmlsZS5leGlzdHM/KHRhcmdldCkgIyBGSVhNRTogd2luZG93
# cwogICAgICAgIEZpbGUudW5saW5rKHByb3BhdGgpIGlmIEZpbGUuZXhpc3Rz
# Pyhwcm9wYXRoKQogICAgICAgIEZpbGUudW5saW5rKGZpbGVwYXRoKSBpZiBG
# aWxlLmV4aXN0cz8oZmlsZXBhdGgpCiAgICAgICAgRmlsZVV0aWxzLnJtX3Jm
# KHFvbmZ0ZXN0X2RpcikKICAgICAgICAKICAgICAgcmVzY3VlID0+IGUKICAg
# ICAgICBwdXRzIGUKICAgICAgICBwdXRzIGUuYmFja3RyYWNlCiAgICAgICAg
# CiAgICAgICAgRGlyLmNoZGlyKGN3ZCkKICAgICAgICBGaWxlLnVubGluayh0
# YXJnZXQpIGlmIEZpbGUuZXhpc3RzPyh0YXJnZXQpICMgRklYTUU6IHdpbmRv
# d3MKICAgICAgICBGaWxlLnVubGluayhwcm9wYXRoKSBpZiBGaWxlLmV4aXN0
# cz8ocHJvcGF0aCkKICAgICAgICBGaWxlLnVubGluayhmaWxlcGF0aCkgaWYg
# RmlsZS5leGlzdHM/KGZpbGVwYXRoKQogICAgICAgIEZpbGVVdGlscy5ybV9y
# Zihxb25mdGVzdF9kaXIpCiAgICAgICAgCiAgICAgICAgcmV0dXJuIGZhbHNl
# CiAgICAgIGVuc3VyZQogICAgICBlbmQKICAgICAgCiAgICAgIHJldAogICAg
# ZW5kCiAgICAKICAgIGRlZiBhZGRfaW5jbHVkZV9wYXRoKGluY3BhdGgpCiAg
# ICAgIEBpbmNsdWRlX3BhdGggPDwgaW5jcGF0aAogICAgZW5kCiAgICAKICAg
# IGRlZiBhZGRfbGliKGxpYikKICAgICAgQGxpYnMgPDwgbGliCiAgICBlbmQK
# ICAgIAogICAgZGVmIGFkZF9kZWZpbmUoZGVmaW5lKQogICAgICBAZGVmaW5l
# cyA8PCBkZWZpbmUKICAgIGVuZAogICAgCiAgICBwcml2YXRlCiAgICBkZWYg
# d3JpdGVfdGVzdChmKQogICAgICBoZWFkZXJzID0gIiIKICAgICAgaWYgbm90
# IEBvcHRpb25zWzpoZWFkZXJzXS5uaWw/CiAgICAgICAgQG9wdGlvbnNbOmhl
# YWRlcnNdLmVhY2ggeyB8aGVhZGVyfAogICAgICAgICAgaGVhZGVycyArPSAi
# I2luY2x1ZGUgPCN7aGVhZGVyfT5cbiIKICAgICAgICB9CiAgICAgIGVuZAog
# ICAgICBjb2RlID0gJUAKaW50IG1haW4oKQp7CiAgcmV0dXJuIDA7Cn0KQAog
# ICAgICBpZiBub3QgQG9wdGlvbnNbOmN1c3RvbV0ubmlsPwogICAgICAgIGNv
# ZGUgPSBAb3B0aW9uc1s6Y3VzdG9tXQogICAgICBlbmQKICAgICAgCiAgICAg
# IGYgPDwgaGVhZGVycyA8PCAiXG4iIDw8IGNvZGUgPDwgIlxuIgogICAgZW5k
# CiAgZW5kCmVuZAoKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3Vy
# ZS90ZXN0LnJiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwNjMyMAAxMDcwMjMx
# MzIyMwAwMTI1MDIAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAcmVxdWlyZSAndG1wZGlyJwpyZXF1aXJlICdmaWxldXRpbHMn
# Cgptb2R1bGUgUW9uZgogIGNsYXNzIFRlc3QKICAgIGF0dHJfcmVhZGVyIDpp
# bmNsdWRlX3BhdGgsIDpsaWJzLCA6ZGVmaW5lcywgOmlkCiAgICAKICAgIGRl
# ZiBpbml0aWFsaXplKG9wdGlvbnMsIGNvbmZpZywgbG9nX2ZpbGUgPSBuaWwp
# CiAgICAgIEBvcHRpb25zID0gb3B0aW9ucwogICAgICAKICAgICAgQGluY2x1
# ZGVfcGF0aCA9IFtdCiAgICAgIEBsaWJzID0gW10KICAgICAgQGRlZmluZXMg
# PSBbXQogICAgICBAaWQgPSBvcHRpb25zWzppZF0KICAgICAgCiAgICAgIEBp
# bmNsdWRlX3BhdGggPSBvcHRpb25zWzppbmNsdWRlcGF0aF0gaWYgbm90IG9w
# dGlvbnNbOmluY2x1ZGVwYXRoXS5uaWw/CiAgICAgIEBsaWJzIDw8IG9wdGlv
# bnNbOmxpYnNdIGlmIG5vdCBvcHRpb25zWzpsaWJzXS5uaWw/CiAgICAgIEBk
# ZWZpbmVzID0gb3B0aW9uc1s6ZGVmaW5lc10gaWYgbm90IG9wdGlvbnNbOmRl
# ZmluZXNdLm5pbD8KICAgICAgCiAgICAgIEBjb25maWcgPSBjb25maWcKICAg
# ICAgCiAgICAgIEBsb2dfZmlsZSA9IGxvZ19maWxlCiAgICBlbmQKICAgIAog
# ICAgZGVmIHJ1bihxbWFrZSkKICAgICAgIyBHZW5lcmF0ZQogICAgICByZXQg
# PSBmYWxzZQogICAgICBjd2QgPSBEaXIuZ2V0d2QKICAgICAgCiAgICAgIHFv
# bmZ0ZXN0X2RpciA9ICIje0Rpcjo6dG1wZGlyfS9xb25mX3Rlc3RzLiN7UHJv
# Y2Vzcy5waWR9IgogICAgICBiYXNlcGF0aCA9IHFvbmZ0ZXN0X2RpcisiL3Rl
# c3RfI3tQcm9jZXNzLnBpZH0iCiAgICAgIAogICAgICBmaWxlcGF0aCA9IGJh
# c2VwYXRoKyIuY3BwIgogICAgICBwcm9wYXRoID0gYmFzZXBhdGgrIi5wcm8i
# CiAgICAgIHRhcmdldCA9ICJ0ZXN0XyN7UHJvY2Vzcy5waWR9X2FwcCIKICAg
# ICAgCiAgICAgIGJlZ2luCiAgICAgICAgRGlyLm1rZGlyKHFvbmZ0ZXN0X2Rp
# cikgaWYgbm90IEZpbGUuZXhpc3RzPyhxb25mdGVzdF9kaXIpCiAgICAgICAg
# RmlsZS5vcGVuKGZpbGVwYXRoLCAidyIpIHsgfGZ8CiAgICAgICAgICAKICAg
# ICAgICAgIHdyaXRlX3Rlc3QoZikKICAgICAgICAgIAogICAgICAgICAgRmls
# ZS5vcGVuKHByb3BhdGgsICJ3IikgeyB8cGZ8CiAgICAgICAgICAgIHBmIDw8
# ICJTT1VSQ0VTICs9ICIgPDwgZmlsZXBhdGggPDwgIlxuIgogICAgICAgICAg
# ICBwZiA8PCAiVEVNUExBVEUgPSBhcHAiIDw8ICJcbiIKICAgICAgICAgICAg
# cGYgPDwgIlRBUkdFVCA9ICIgPDwgIlxuIgogICAgICAgICAgICBwZiA8PCAi
# SU5DTFVERVBBVEggKz0gIiA8PCBAaW5jbHVkZV9wYXRoLmpvaW4oIiAiKSA8
# PCAiICIgPDwgQGNvbmZpZy5pbmNsdWRlX3BhdGguam9pbigiICIpIDw8ICJc
# biIKICAgICAgICAgICAgcGYgPDwgIkxJQlMgKz0gIiA8PCBAbGlicy5qb2lu
# KCIgIikgPDwgIiAiIDw8ICBAY29uZmlnLmxpYnMuam9pbigiICIpIDw8ICJc
# biIKICAgICAgICAgICAgcGYgPDwgIkRFRklORVMgKz0gIiA8PCBAZGVmaW5l
# cy5qb2luKCIgIikgPDwgIiAiIDw8IEBjb25maWcuZGVmaW5lcy5qb2luKCIg
# IikgPDwgIlxuIgogICAgICAgICAgICBwZiA8PCAiQ09ORklHIC09IGRlYnVn
# IiA8PCAiXG4iCiAgICAgICAgICAgIHBmIDw8ICJDT05GSUcgKj0gcmVsZWFz
# ZSIgPDwgIlxuIgogICAgICAgICAgICBwZiA8PCAiREVTVERJUiA9ICN7cW9u
# ZnRlc3RfZGlyfSIgPDwgIlxuIgogICAgICAgICAgfQogICAgICAgIH0KICAg
# ICAgICAKICAgICAgICAKICAgICAgICBEaXIuY2hkaXIocW9uZnRlc3RfZGly
# KQogICAgICAgIAogICAgICAgIHFtYWtlLnJ1bihwcm9wYXRoKQogICAgICAg
# IHJldCA9IHFtYWtlLmNvbXBpbGUKICAgICAgICBxbWFrZS5jbGVhbl91cAog
# ICAgICAgIAogICAgICAgIERpci5jaGRpcihjd2QpCiAgICAgICAgCiAgICAg
# ICAgRmlsZS51bmxpbmsodGFyZ2V0KSBpZiBGaWxlLmV4aXN0cz8odGFyZ2V0
# KSAjIEZJWE1FOiB3aW5kb3dzCiAgICAgICAgRmlsZS51bmxpbmsocHJvcGF0
# aCkgaWYgRmlsZS5leGlzdHM/KHByb3BhdGgpCiAgICAgICAgRmlsZS51bmxp
# bmsoZmlsZXBhdGgpIGlmIEZpbGUuZXhpc3RzPyhmaWxlcGF0aCkKICAgICAg
# ICBGaWxlVXRpbHMucm1fcmYocW9uZnRlc3RfZGlyKQogICAgICAgIAogICAg
# ICByZXNjdWUgPT4gZQogICAgICAgIHB1dHMgZQogICAgICAgIHB1dHMgZS5i
# YWNrdHJhY2UKICAgICAgICAKICAgICAgICBEaXIuY2hkaXIoY3dkKQogICAg
# ICAgIEZpbGUudW5saW5rKHRhcmdldCkgaWYgRmlsZS5leGlzdHM/KHRhcmdl
# dCkgIyBGSVhNRTogd2luZG93cwogICAgICAgIEZpbGUudW5saW5rKHByb3Bh
# dGgpIGlmIEZpbGUuZXhpc3RzPyhwcm9wYXRoKQogICAgICAgIEZpbGUudW5s
# aW5rKGZpbGVwYXRoKSBpZiBGaWxlLmV4aXN0cz8oZmlsZXBhdGgpCiAgICAg
# ICAgRmlsZVV0aWxzLnJtX3JmKHFvbmZ0ZXN0X2RpcikKICAgICAgICAKICAg
# ICAgICByZXR1cm4gZmFsc2UKICAgICAgZW5zdXJlCiAgICAgIGVuZAogICAg
# ICAKICAgICAgcmV0CiAgICBlbmQKICAgIAogICAgZGVmIGFkZF9pbmNsdWRl
# X3BhdGgoaW5jcGF0aCkKICAgICAgQGluY2x1ZGVfcGF0aCA8PCBpbmNwYXRo
# CiAgICBlbmQKICAgIAogICAgZGVmIGFkZF9saWIobGliKQogICAgICBAbGli
# cyA8PCBsaWIKICAgIGVuZAogICAgCiAgICBkZWYgYWRkX2RlZmluZShkZWZp
# bmUpCiAgICAgIEBkZWZpbmVzIDw8IGRlZmluZQogICAgZW5kCiAgICAKICAg
# IHByaXZhdGUKICAgIGRlZiB3cml0ZV90ZXN0KGYpCiAgICAgIGhlYWRlcnMg
# PSAiIgogICAgICBpZiBub3QgQG9wdGlvbnNbOmhlYWRlcnNdLm5pbD8KICAg
# ICAgICBAb3B0aW9uc1s6aGVhZGVyc10uZWFjaCB7IHxoZWFkZXJ8CiAgICAg
# ICAgICBoZWFkZXJzICs9ICIjaW5jbHVkZSA8I3toZWFkZXJ9PlxuIgogICAg
# ICAgIH0KICAgICAgZW5kCiAgICAgIGNvZGUgPSAlQAppbnQgbWFpbigpCnsK
# ICByZXR1cm4gMDsKfQpACiAgICAgIGlmIG5vdCBAb3B0aW9uc1s6Y3VzdG9t
# XS5uaWw/CiAgICAgICAgY29kZSA9IEBvcHRpb25zWzpjdXN0b21dCiAgICAg
# IGVuZAogICAgICAKICAgICAgZiA8PCBoZWFkZXJzIDw8ICJcbiIgPDwgY29k
# ZSA8PCAiXG4iCiAgICAgIAogICAgICBpZiggbm90IEBsb2dfZmlsZS5uaWw/
# ICkKICAgICAgICBGaWxlLm9wZW4oQGxvZ19maWxlLCAiYSIpIGRvIHxsZmls
# ZXwKICAgICAgICAgIGxmaWxlIDw8ICJcblxuXG4tLS0tLS0tLS0tLS1cbiIg
# PDwgaGVhZGVycyA8PCAiXG4iIDw8IGNvZGUgPDwgIlxuLS0tLS0tLS0tLS0t
# XG5cblxuIgogICAgICAgIGVuZAogICAgICBlbmQKICAgIGVuZAogIGVuZApl
# bmQKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABxb25maWd1
# cmUvY29uZmlnLnJifgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEAMDAwMDAwMDc0MDYAMTA3MzIz
# NjUyNzMAMDEzMjEzACAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAApyZXF1aXJlICdkZXRlY3RvcycKCm1vZHVsZSBRb25mCgpj
# bGFzcyBQYWNrYWdlCiAgYXR0cl9yZWFkZXIgOmluY2x1ZGVfcGF0aCwgOmxp
# YnMsIDptb2R1bGVzLCA6ZGVmaW5lcywgOm5hbWUKICBkZWYgaW5pdGlhbGl6
# ZShuYW1lKQogICAgQGluY2x1ZGVfcGF0aCA9IFtdCiAgICBAbGlicyA9IFtd
# CiAgICBAbW9kdWxlcyA9IFtdCiAgICBAZGVmaW5lcyA9IFtdCiAgICBAbmFt
# ZSA9IG5hbWUKICBlbmQKICAKICBkZWYgYWRkX2luY2x1ZGVfcGF0aChwYXRo
# KQogICAgQGluY2x1ZGVfcGF0aCA8PCBwYXRoCiAgZW5kCiAgCiAgZGVmIGFk
# ZF9saWIobGliKQogICAgQGxpYnMgPDwgbGliLnRvX3MKICBlbmQKICAKICBk
# ZWYgYWRkX3F0bW9kdWxlKG1vZCkKICAgIEBtb2R1bGVzIDw8IG1vZAogIGVu
# ZAogIAogIGRlZiBhZGRfZGVmaW5lKGRlZmluZSkKICAgIEBkZWZpbmVzIDw8
# IGRlZmluZQogIGVuZAogIAogIGRlZiB3cml0ZShmKQogICAgaWYgbm90IEBp
# bmNsdWRlX3BhdGguZW1wdHk/CiAgICAgICAgZiA8PCAiSU5DTFVERVBBVEgg
# Kz0gIiA8PCBAaW5jbHVkZV9wYXRoLnVuaXEuam9pbigiICIpIDw8ICJcbiIK
# ICAgIGVuZAogICAgCiAgICBpZiBub3QgQGxpYnMuZW1wdHk/CiAgICAgICAg
# ZiA8PCAiTElCUyArPSAiIDw8IEBsaWJzLnVuaXEuam9pbigiICIpIDw8ICJc
# biIKICAgIGVuZAogICAgCiAgICBpZiBub3QgQG1vZHVsZXMuZW1wdHk/CiAg
# ICAgICAgZiA8PCAiUVQgKz0gIiA8PCBAbW9kdWxlcy51bmlxLmpvaW4oIiAi
# KSA8PCAiXG4iCiAgICBlbmQKICAgIAogICAgaWYgbm90IEBkZWZpbmVzLmVt
# cHR5PwogICAgICAgIGYgPDwgIkRFRklORVMgKz0gIiA8PCBAZGVmaW5lcy51
# bmlxLmpvaW4oIiAiKSA8PCAiXG4iCiAgICBlbmQKICBlbmQKZW5kCgpjbGFz
# cyBDb25maWcgPCBQYWNrYWdlCiAgYXR0cl9yZWFkZXIgOmJ1aWxkX2Rpcgog
# IGRlZiBpbml0aWFsaXplCiAgICBzdXBlcihuaWwpCiAgICBAcW9uZl9mZWF0
# dXJlcyA9IFtdCiAgICBAcW9uZl9wYWNrYWdlcyA9IFtdCiAgICBAYnVpbGRf
# ZGlyID0gb2xkbG9jYXRpb24oIiIpKyIvX2J1aWxkXyIKICAgIEB1c2VfZGVi
# dWcgPSBmYWxzZQogIGVuZAogIAogIGRlZiB3aXRoX2RlYnVnCiAgICBAdXNl
# X2RlYnVnID0gdHJ1ZQogIGVuZAoKICBkZWYgYWRkX3FvbmZfcGFja2FnZShw
# a2cpCiAgICBAcW9uZl9wYWNrYWdlcyA8PCBwa2cKICBlbmQKICAKICBkZWYg
# YWRkX3FvbmZfZmVhdHVyZShmdHIpCiAgICBAcW9uZl9mZWF0dXJlcyA8PCBm
# dHIKICBlbmQgCgogIGRlZiBpbmNsdWRlX3BhdGgKICAgIGluY3BhdGggPSBA
# aW5jbHVkZV9wYXRoLmR1cAogICAgCiAgICBAcW9uZl9wYWNrYWdlcy5lYWNo
# IHsgfHBrZ3wKICAgICAgICBpbmNwYXRoLmNvbmNhdCBwa2cuaW5jbHVkZV9w
# YXRoCiAgICB9CiAgICAKICAgIGluY3BhdGgKICBlbmQKICAKICBkZWYgbGli
# cwogICAgbGliID0gQGxpYnMuZHVwCiAgICBAcW9uZl9wYWNrYWdlcy5lYWNo
# IHsgfHBrZ3wKICAgICAgICBsaWIuY29uY2F0IHBrZy5saWJzCiAgICB9CiAg
# ICAKICAgIGxpYgogIGVuZAogIAogIGRlZiBsaWJyYXJ5X3BhdGgKICAgIGxp
# YnNfYSA9IFtdCgogICAgc3BsaXR0ZWQgPSBAbGlicy5qb2luKCIgIikuc3Bs
# aXQoIiAiKQogICAgc3BsaXR0ZWQuZWFjaCB7IHxzcHwKICAgICAgICBpZigg
# c3AgPX4gLy1MKC4rKVxzKi8gKQogICAgICAgICAgICBsaWJzX2EgPDwgJDEK
# ICAgICAgICBlbmQKICAgIH0KICAgIAogICAgbGlic19hCiAgZW5kCiAgCiAg
# ZGVmIGRlZmluZXMKICAgIGRlZnMgPSBAZGVmaW5lcy5kdXAKICAgIAogICAg
# QHFvbmZfcGFja2FnZXMuZWFjaCB7IHxwa2d8CiAgICAgICAgZGVmcy5jb25j
# YXQgcGtnLmRlZmluZXMKICAgIH0KICAgIAogICAgZGVmcwogIGVuZAogIAog
# IGRlZiBzYXZlKHBhdGgpCiAgICBpZiBwYXRoWzBdLmNociA9PSBGaWxlOjpT
# RVBBUkFUT1IKICAgICAgICBwYXRoID0gcGF0aAogICAgZWxzZQogICAgICAg
# IHBhdGggPSAiI3tEaXIuZ2V0d2R9LyN7cGF0aH0iCiAgICBlbmQKICAgIAog
# ICAgRmlsZS5vcGVuKHBhdGgsICJ3IikgeyB8ZnwKICAgICAgICBmIDw8ICIj
# IEdlbmVyYXRlZCBhdXRvbWF0aWNhbGx5IGF0ICN7VGltZS5ub3d9ISBQTEVB
# U0UgRE8gTk9UIEVESVQgTUFOVUFMTFkhIjw8ICJcbiIKICAgICAgICAKICAg
# ICAgICBmIDw8ICJQUk9KRUNUX0RJUiA9ICIgPDwgb2xkbG9jYXRpb24oIiIp
# IDw8ICJcbiIKICAgICAgICBmIDw8ICJCVUlMRF9ESVIgPSAje0BidWlsZF9k
# aXJ9XG4iCiAgICAgICAgCiAgICAgICAgZiA8PCAnY29udGFpbnMoVEVNUExB
# VEUsICJhcHAiKSB7JyA8PCAiXG4iCiAgICAgICAgZiA8PCAiREVTVERJUiA9
# ICQkQlVJTERfRElSL2JpblxuIgogICAgICAgIGYgPDwgJ30gZWxzZTpjb250
# YWlucyhURU1QTEFURSwgImxpYiIpIHsnIDw8ICJcbiIKICAgICAgICBmIDw8
# ICJERVNURElSID0gJCRCVUlMRF9ESVIvbGliXG4iCiAgICAgICAgZiA8PCAn
# fSBlbHNlIHsnIDw8ICJcbiIKICAgICAgICBmIDw8ICJERVNURElSID0gJCRC
# VUlMRF9ESVJcbiIKICAgICAgICBmIDw8ICJ9XG5cblxuIgogICAgICAgIAog
# ICAgICAgIGlmIEB1c2VfZGVidWcKICAgICAgICAgICAgZiA8PCAiQ09ORklH
# ICs9IGRlYnVnXG4iCiAgICAgICAgZWxzZQogICAgICAgICAgICBmIDw8ICJD
# T05GSUcgKz0gXG4iCiAgICAgICAgZW5kCiAgICAgICAgCiAgICAgICAgZiA8
# PCAiTElCUyArPSAtTCQkQlVJTERfRElSL2xpYlxuIgogICAgICAgIGYgPDwg
# IlJFTEFUSVZFX1BBVEggPSAkJHJlcGxhY2UoX1BST19GSUxFX1BXRF8sICQk
# UFJPSkVDVF9ESVIsICIiKVxuIgogICAgICAgIGYgPDwgIkNVUlJFTlRfQlVJ
# TERfRElSID0gJCRCVUlMRF9ESVIkJFJFTEFUSVZFX1BBVEhcbiIKICAgICAg
# ICAKaWYgRGV0ZWN0T1Mub3M/ICE9IDEgIyBEaXNhYmxlZCBiZWNhdXNlIFdp
# bmRvd3Mgc3Vja3MuCiAgICAgICAgZiA8PCAiT0JKRUNUU19ESVIgPSAkJENV
# UlJFTlRfQlVJTERfRElSXG4iCiAgICAgICAgZiA8PCAiTU9DX0RJUiA9ICQk
# Q1VSUkVOVF9CVUlMRF9ESVJcbiIKICAgICAgICBmIDw8ICJVSV9ESVIgPSAk
# JENVUlJFTlRfQlVJTERfRElSXG5cbiIKZW5kCiAgICAgICAgCiAgICAgICAg
# d3JpdGUoZikKICAgICAgICAKICAgICAgICAKICAgICAgICBmIDw8ICJRT05G
# X0ZFQVRVUkVTICs9ICIgPDwgQHFvbmZfcGFja2FnZXMubWFwIHsgfGV8IGUu
# bmFtZSB9LmpvaW4oIiAiKSAgPDwgIlxuIgogICAgICAgIGYgPDwgIlFPTkZf
# RkVBVFVSRVMgKz0gIiA8PCBAcW9uZl9mZWF0dXJlcy5qb2luKCIgIikgIDw8
# ICJcbiIKICAgICAgICAKICAgICAgICBmIDw8ICJcbmRlZmluZVRlc3QobGlu
# a193aXRoKSB7IiA8PCAiXG4iCiAgICAgICAgZiA8PCAiICByZXQgPSBmYWxz
# ZVxuIgogICAgICAgIEBxb25mX3BhY2thZ2VzLmVhY2ggeyB8cW9uZnwKICAg
# ICAgICAgIGYgPDwgJUAgIGNvbnRhaW5zKCQkbGlzdCgkJHNwbGl0KDEpKSAs
# IiN7cW9uZi5uYW1lfSIpIHtcbkAKICAgICAgICAgIGYgPDwgIiAgICByZXQg
# PSB0cnVlXG4iCiAgICAgICAgICBmIDw8ICIgICAgbWVzc2FnZSg9PiBMaW5r
# aW5nIHFvbmYgbW9kdWxlOiAje3FvbmYubmFtZX0pXG4iCiAgICAgICAgICBx
# b25mLndyaXRlKGYpCiAgICAgICAgICBmIDw8ICIgIH1cbiIKICAgICAgICB9
# CiAgICAgICAgZiA8PCAiICBleHBvcnQoSU5DTFVERVBBVEgpXG4iCiAgICAg
# ICAgZiA8PCAiICBleHBvcnQoTElCUylcbiIKICAgICAgICBmIDw8ICIgIGV4
# cG9ydChRVClcbiIKICAgICAgICBmIDw8ICIgIGV4cG9ydChERUZJTkVTKVxu
# IgogICAgICAgIGYgPDwgIiAgcmV0dXJuKCQkcmV0KVxuIgogICAgICAgIGYg
# PDwgIn1cbiIKICAgIH0KICBlbmQKZW5kCgplbmQKCgAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAABxb25maWd1cmUvc2V0dXBfcmIudHBsAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEA
# MDAwMDAwMDYyMzcAMTA2NzM1MDIwNDMAMDEzNTYwACAwAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVz
# dGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAoKCiMgc2V0dXAgaXMgYSBv
# YmplY3QgdGhhdCBoZWxwcyB0byBjb25maWd1cmUgdGhlIHByb2plY3QKIyBJ
# dCBjb250YWlucyBzb21lIG1ldGhvZHMgbGlrZToKIyAgIGFkZF9vcHRpb24K
# IyAgIGZpbmRfcGFja2FnZQojICAgYWRkX3Rlc3QKIyAgIGdlbmVyYXRlX3Br
# Z2NvbmZpZwpkZWYgY29uZmlndXJlKHNldHVwKQojIyMjIAojIyMjIFZlcmlm
# eSBmb3IgUXQgdmVyc2lvbi4KIyMjIyAKIyBzZXR1cC5xdCA+PSAiNC4yLjAi
# CgojIyMjIAojIyMjIGFkZF9vcHRpb24gYWRkcyBhbiBvcHRpb24gdG8gdGhl
# IHNjcmlwdCwgaXQgaGFzIHRoZSBmb2xsb3dpbmcgb3B0aW9uczoKIyMjIyA6
# bmFtZSAgICAgICAgIFRoZSBuYW1lIG9mIHRoZSBvcHRpb24uCiMjIyMgOnR5
# cGUgICAgICAgICBUaGUgdHlwZSBvZiB0aGUgb3B0aW9uICgicGF0aCIsICJz
# dHJpbmciLCAiYm9vbCIgb3IgInZvaWQiKS4KIyMjIyA6b3B0aW9uYWwgICAg
# IFNheXMgaWYgdGhlIG9wdGlvbiBpcyByZXF1aXJlZCBvciBub3QuCiMjIyMg
# OmRlc2NyaXB0aW9uICBEZXNjcmlwdGlvbiBvZiB0aGUgb3B0aW9uCiMjIyMg
# OmRlZmF1bHQgICAgICBUaGUgZGVmYXVsdCB2YWx1ZQojIyMjIAojICAgc2V0
# dXAuYWRkX29wdGlvbig6bmFtZSA9PiAib3B0aW9uLW5hbWUiLCA6dHlwZSA9
# PiAicGF0aCIsIDpvcHRpb25hbCA9PiBmYWxzZSwgOmRlc2NyaXB0aW9uID0+
# ICJTZXRzIGFuIG9wdGlvbiIpCiAgCiMjIyMgCiMjIyMgZmluZF9wYWNrYWdl
# IHNlYXJjaCBmb3IgYSBwa2djb25maWcgc3BlYyBpbiB5b3VyIGNvbXB1dGVy
# LCBpdCBzZWFyY2ggaW4gdGhlIFBLR19DT05GSUdfUEFUSCBlbnZpcm9ubWVu
# dCB2YXJpYWJsZSBvciBMRF9MSUJSQVJZX1BBVEgKIyMjIyBUaGUgb3B0aW9u
# cyBhcmU6CiMjIyMgOm5hbWUgICAgICAgUGFja2FnZSBuYW1lCiMjIyMgOm9w
# dGlvbmFsICAgVHJ1ZSBpZiB0aGUgcGFja2FnZSBpcyBvcHRpb25hbAojIyMj
# IDpnbG9iYWwgICAgIExpbmsgZ2xvYmFsbHkKIyMjIyAKIyAgIHNldHVwLmZp
# bmRfcGFja2FnZSg6bmFtZSA9PiAiZGJ1cy0xIiwgOm9wdGlvbmFsID0+IGZh
# bHNlLCA6Z2xvYmFsID0+IHRydWUpCgojIyMjIAojIyMjIGFkZF90ZXN0IGFk
# ZHMgYW5kIHJ1biBhIHRlc3QgZm9yIHNvbWUgZmVhdHVyZSBvciBwYWNrYWdl
# LCB0aGUgb3B0aW9ucyBhcmU6CiMjIyMgICA6aWQgICAgICAgICAgIFRoZSBp
# ZCBvZiB0aGUgdGVzdAojIyMjICAgOm5hbWUgICAgICAgICBUaGUgdGVzdCBu
# YW1lCiMjIyMgICA6ZGVmaW5lcyAgICAgIERlZmluZXMgdG8gdGVzdAojIyMj
# ICAgOmluY2x1ZGVwYXRoICBBIGxpc3Qgd2l0aCB0aGUgaW5jbHVkZSBwYXRo
# CiMjIyMgICA6bGlicyAgICAgICAgIFRoZSBsaWJzIHJlcXVpcmVkIGZvciB0
# aGUgdGVzdAojIyMjICAgOm9wdGlvbmFsICAgICBUcnVlIGlmIHRoZSB0ZXN0
# IGlzIG9wdGlvbmFsCiMjIyMgICA6Y3VzdG9tICAgICAgIEEgY29kZSB0byB0
# ZXN0CiMjIyMgICA6Z2xvYmFsICAgICAgIExpbmsgZ2xvYmFsbHkKIyMjIyAK
# IyBzZXR1cC5hZGRfdGVzdCg6aWQgPT4gImZlYXR1cmUiLCA6bmFtZSA9PiAi
# RmVhdHVyZSBzdXBwb3J0IiwgOmRlZmluZXMgPT4gWyJIQVZFX0ZFQVRVUkUi
# XSwgOm9wdGlvbmFsID0+IHRydWUsIDpoZWFkZXJzID0+IFsiZmVhdHVyZS5o
# Il0sIDpsaWJzID0+ICItbGZlYXR1cmUiLCA6Z2xvYmFsID0+IHRydWUgICkK
# CiMjIyMgCiMjIyMgZ2VuZXJhdGVfcGtnY29uZmlnIEdlbmVyYXRlcyBhIHNp
# bXBsZSBwa2djb25maWcgZmlsZQojIyMjIFRoZSBvcHRpb25zIGFyZToKIyMj
# IyAgIDpwYWNrYWdlX25hbWUgIFRoZSBwYWNrYWdlIG5hbWUgCiMjIyMgICA6
# bmFtZSAgICAgICAgICBUaGUgYXBwbGljYXRpb24gbmFtZQojIyMjICAgOmRl
# c2NyaXB0aW9uICAgVGhlIGFwcGxpY2F0aW9uIGRlc2NyaXB0aW9uCiMjIyMg
# ICA6dmVyc2lvbiAgICAgICBUaGUgdmVyc2lvbiBvZiB0aGUgYXBwbGljYXRp
# b24KIyMjIyAgIDpsaWJkaXIgICAgICAgIFRoZSBkaXJlY3Rvcnkgd2hlcmUg
# bGlicmFyaWVzIGFyZSwgYnkgZGVmYXVsdCBpcyAkKHByZWZpeCkvbGliCiMj
# IyMgICA6aW5jbHVkZWRpciAgICBUaGUgZGlyZWN0b3J5IHdoZXJlIGhlYWRl
# cnMgYXJlLCBieSBkZWZhdWx0IGlzICQocHJlZml4KS9pbmNsdWRlCiMjIyMg
# ICA6bGlicyAgICAgICAgICBMaWJyYXJpZXMgdGhhdCBsaW5rcyB0aGUgcGFj
# a2FnZQojIyMjICAgOmNmbGFncyAgICAgICAgQ2ZsYWdzIGZvciB0aGUgcGFj
# a2FnZQojIyMjICAgOnJlcXVpcmVzICAgICAgQSBsaXN0IG9mIHBhY2thZ2Ug
# bmFtZXMgcmVxdWlyZWQgZm9yIHRoaXMgcGFja2FnZS4KIyMjIyAKIyBzZXR1
# cC5nZW5lcmF0ZV9wa2djb25maWcoOnBhY2thZ2VfbmFtZSA9PiAiZGxpYi1l
# ZGl0b3IiLCA6bmFtZSA9PiAiRExpYiBRdCBsaWJyYXJ5IiwgOmRlc2NyaXB0
# aW9uID0+ICJBIGV4dGVuc2lvbiBmb3IgUXQ0IiwgOnZlcnNpb24gPT4gIjAu
# MWJldGEiLCA6bGliZGlyID0+IG5pbCwgOmluY2x1ZGVkaXIgPT4gbmlsLCA6
# bGlicyA9PiAiLUwke2xpYmRpcn0gLWxkY29yZSAtbGRndWkgLWxkZWRpdG9y
# IiwgOmNmbGFncyA9PiAiLUkke2luY2x1ZGVkaXJ9IiwgOnJlcXVpcmVzID0+
# IFsic29tZXBhY2thZ2UiLCAib3RoZXJwYWNrYWdlIiBdICkKZW5kCgojIFRo
# aXMgbWV0aG9kIGlzIGNhbGxlZCBiZWZvcmUgdG8gc2VhcmNoIGEgcGFja2Fn
# ZQpkZWYgc2V0dXBfcGtnY29uZmlnKHBrZ2NvbmZpZywgYXJncykKIyAgIGNh
# c2UgcGtnY29uZmlnLmlkCiMgICAgIHdoZW4gImRidXMtMSIKIyAgICAgICBw
# a2djb25maWcuYWRkX3NlYXJjaF9wYXRoKCIvYS9wYXRoL3RvL3RoZS9wYyIp
# CiMgICBlbmQKZW5kCgpkZWYgc2V0dXBfdGVzdChpZCwgdHN0LCBhcmdzKQoj
# ICAgY2FzZSBpZAojICAgICB3aGVuICJmZWF0dXJlIgojICAgICAgIHRzdC5h
# ZGRfZGVmaW5lKCJNWURFRklORSIpCiMgICAgICAgdHN0LmFkZF9saWIoIi1s
# bXlsaWIiKQojICAgICAgIHRzdC5hZGRfaW5jbHVkZV9wYXRoKCIvc29tZS9p
# bmNsdWRlL3BhdGgvcmVxdWlyZWQvdG8vY29tcGlsZS90aGUvdGVzdCIpCiMg
# ICBlbmQKZW5kCgpkZWYgc2V0dXBfY29uZmlnKGNmZywgYXJncykKIyAgIGNm
# Zy5hZGRfaW5jbHVkZV9wYXRoIEZpbGUuZXhwYW5kX3BhdGgoInNyYyIpCiMg
# ICBjZmcuYWRkX2RlZmluZSgiX19TVERDX0NPTlNUQU5UX01BQ1JPUyIpCiMg
# ICBjZmcuYWRkX3F0bW9kdWxlKCJ4bWwiKQojICAgY2ZnLmFkZF9xdG1vZHVs
# ZSgibmV0d29yayIpCmVuZAoKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAcW9uZmlndXJlL3FtYWtlLnJiAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUx
# ADAwMDAwMDA3MzMzADEwNzA1MDIwNTI3ADAxMjYzMwAgMAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1
# c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKcmVxdWlyZSAnb3BlbjMn
# CnJlcXVpcmUgJ2RldGVjdG9zJwoKbW9kdWxlIFFvbmYKCmNsYXNzIFFNYWtl
# CiAgYXR0cl9yZWFkZXIgOnBhdGgKICAKICBkZWYgaW5pdGlhbGl6ZShwYXRo
# cyA9IFtdLCBsb2dfZmlsZSA9IG5pbCkKICAgIEBwYXRocyA9IFsgInFtYWtl
# LXF0NCIsICJxbWFrZTQiLCAicW1ha2UiIF0KICAgIEBwYXRocy5jb25jYXQo
# cGF0aHMpCiAgICAKICAgIGlmKCAkREVCVUcgKQogICAgICAkc3RkZXJyIDw8
# ICJRTWFrZSBwYXRoczogIiA8PCBAcGF0aHMuam9pbigiICIpIDw8ICJcbiIK
# ICAgIGVuZAogICAgCiAgICBpZiBub3QgZmluZF9xbWFrZSgiNC4wLjAiKQog
# ICAgICAgICAgICByYWlzZSAiQ2FuJ3QgZmluZCB2YWxpZCBxbWFrZSAtIHF0
# NCIKICAgIGVuZAogICAgCiAgICBAbG9nX2ZpbGUgPSBsb2dfZmlsZQogICAg
# aWYoIEBsb2dfZmlsZS5uaWw/ICkKICAgICAgQGxvZ19maWxlID0gb2xkbG9j
# YXRpb24oImNvbmZpZy5sb2ciKQogICAgZW5kCiAgICAKICAgIEBtYWtlID0g
# Im1ha2UiCiAgICAKICAgIGlmIERldGVjdE9TLm9zPyA9PSAxCiAgICAgIEBt
# YWtlX29wdGlvbnMgPSAiMj4+ICN7QGxvZ19maWxlfSIKICAgIGVsc2UKICAg
# ICAgQG1ha2Vfb3B0aW9ucyA9ICI+PiAje0Bsb2dfZmlsZX0gMj4+ICN7QGxv
# Z19maWxlfSIKICAgIGVuZAogIGVuZAogICAgICAgIAogIGRlZiBmaW5kX3Ft
# YWtlKG1pbnF0dmVyc2lvbiwgb3AgPSAyKQogICAgCiAgICAKICAgIG1pbnZl
# ciA9IG1pbnF0dmVyc2lvbi5zcGxpdCgiLiIpCiAgICBAcGF0aHMuZWFjaCB7
# IHxwYXRofAogICAgICBiZWdpbgogICAgICAgIHZlcnNpb24gPSBbXQogICAg
# ICAgIG9rID0gdHJ1ZQogICAgICAgIAogICAgICAgIElPLnBvcGVuKCIje3Bh
# dGh9IC1xdWVyeSBRVF9WRVJTSU9OIDI+ICN7bmV3bG9jYXRpb24oInFtYWtl
# X2xvZyIpfSIpIHsgfHByY3wKICAgICAgICAgIHZlcnNpb24gPSBwcmMucmVh
# ZGxpbmVzLmpvaW4oIiIpLnNwbGl0KCIuIikKICAgICAgICB9CiAgICAgICAg
# CiAgICAgICAgbmV4dCBpZiAkPyAhPSAwCiAgICAgICAgCiAgICAgICAgdmVy
# c2lvbi5zaXplLnRpbWVzIHsgfGl8CiAgICAgICAgICBpZiB2ZXJzaW9uLnNp
# emUgPCBpIGFuZCBtaW52ZXIuc2l6ZSA+PSAzCiAgICAgICAgICAgICAgICAg
# IGJyZWFrCiAgICAgICAgICBlbmQKICAgICAgICAgIAogICAgICAgICAgY2Fz
# ZSBvcAogICAgICAgICAgICB3aGVuIDAKICAgICAgICAgICAgICBpZiB2ZXJz
# aW9uW2ldICE9IG1pbnZlcltpXQogICAgICAgICAgICAgICAgb2sgPSBmYWxz
# ZQogICAgICAgICAgICAgICAgYnJlYWsKICAgICAgICAgICAgICBlbmQKICAg
# ICAgICAgICAgd2hlbiAxCiAgICAgICAgICAgICAgaWYgdmVyc2lvbltpXSA8
# IG1pbnZlcltpXQogICAgICAgICAgICAgICAgb2sgPSBmYWxzZQogICAgICAg
# ICAgICAgICAgYnJlYWsKICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAg
# d2hlbiAyCiAgICAgICAgICAgICAgaWYgdmVyc2lvbltpXSA8IG1pbnZlcltp
# XQogICAgICAgICAgICAgICAgb2sgPSBmYWxzZQogICAgICAgICAgICAgICAg
# YnJlYWsKICAgICAgICAgICAgICBlbmQKICAgICAgICAgIGVuZAogICAgICAg
# IH0KICAgICAgICAKICAgICAgICBpZiBvawogICAgICAgICAgQHBhdGggPSBw
# YXRoCiAgICAgICAgICAKICAgICAgICAgIGlmICRERUJVRwogICAgICAgICAg
# ICBwdXRzICJVc2luZyBxbWFrZTogI3tAcGF0aH0gI3t2ZXJzaW9uLmpvaW4o
# Ii4iKX0iCiAgICAgICAgICBlbmQKICAgICAgICAgIAogICAgICAgICAgcmV0
# dXJuIG9rCiAgICAgICAgZW5kCiAgICAgIHJlc2N1ZQogICAgICBlbnN1cmUK
# ICAgICAgZW5kCiAgICB9CiAgCiAgICByZXR1cm4gZmFsc2UKICBlbmQKICAg
# ICAgICAKICAgICAgICBkZWYgcXVlcnkodmFyKQogICAgICAgICAgICAgICAg
# b3V0cHV0ID0gYCN7QHBhdGh9IC1xdWVyeSAje3Zhcn1gCiAgICAgICAgICAg
# ICAgICBvdXRwdXQKICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBkZWYg
# cnVuKGFyZ3MgPSAiIiwgcmVjdXIgPSBmYWxzZSkKICAgICAgICAgICAgICAg
# IGlmICRERUJVRwogICAgICAgICAgICAgICAgICBwdXRzICJBUkdTID0gI3th
# cmdzfSIKICAgICAgICAgICAgICAgIGVuZAogICAgICAgICAgICAgICAgb3B0
# aW9ucyA9ICIiCiAgICAgICAgICAgICAgICBpZiByZWN1cgogICAgICAgICAg
# ICAgICAgICAgICAgICBvcHRpb25zICs9ICItciIKICAgICAgICAgICAgICAg
# IGVuZAogICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICBvdXRwdXQg
# PSBgI3tAcGF0aH0gI3thcmdzfSAje29wdGlvbnN9IGAKICAgICAgICAgICAg
# ICAgIGlmIG91dHB1dC5zdHJpcC5lbXB0eT8KICAgICAgICAgICAgICAgICAg
# ICAgICAgcmV0dXJuIHRydWUKICAgICAgICAgICAgICAgIGVuZAoKICAgICAg
# ICAgICAgICAgIHJldHVybiBmYWxzZQogICAgICAgIGVuZAogICAgICAgIAog
# ICAgICAgIGRlZiBjb21waWxlCiAgICAgICAgICAgICAgICBiZWdpbgogICAg
# ICAgICAgICAgICAgICAgICAgICBJTy5wb3BlbigiI3tAbWFrZX0gY2xlYW4g
# I3tAbWFrZV9vcHRpb25zfSIpIHsgfGN8CiAgICAgICAgICAgICAgICAgICAg
# ICAgICAgICAgICAgb3V0cHV0ID0gYy5yZWFkbGluZXMKICAgICAgICAgICAg
# ICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgICAgICAgICAKICAgICAg
# ICAgICAgICAgICAgICAgICAgdGltZXMgPSAwCiAgICAgICAgICAgICAgICAg
# ICAgICAgIGVuZGNvZGUgPSAyCiAgICAgICAgICAgICAgICAgICAgICAgIAog
# ICAgICAgICAgICAgICAgICAgICAgICBGaWxlLm9wZW4oQGxvZ19maWxlLCAi
# YSIgKSBkbyB8ZnwKICAgICAgICAgICAgICAgICAgICAgICAgICBmIDw8ICI9
# PT09PT4gQ09NUElMSU5HIFxuIgogICAgICAgICAgICAgICAgICAgICAgICBl
# bmQKICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAg
# ICAgICAgIHdoaWxlIGVuZGNvZGUgPT0gMiBhbmQgdGltZXMgPD0gMwogICAg
# ICAgICAgICAgICAgICAgICAgICAgICAgICAgIElPLnBvcGVuKCIje0BtYWtl
# fSAje0BtYWtlX29wdGlvbnN9IiwgInIiKSB7IHxjfAogICAgICAgICAgICAg
# ICAgICAgICAgICAgICAgICAgICAgICAgICAgb3V0cHV0ID0gYy5yZWFkbGlu
# ZXMKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9CiAgICAgICAg
# ICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAg
# ICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg
# ZW5kY29kZSA9ICQ/ID4+IDgKICAgICAgICAgICAgICAgICAgICAgICAgICAg
# ICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aW1lcyAr
# PSAxCiAgICAgICAgICAgICAgICAgICAgICAgIGVuZAogICAgICAgICAgICAg
# ICAgICAgICAgICAKICAgICAgICAgICAgICAgIHJlc2N1ZQogICAgICAgICAg
# ICAgICAgICAgICAgICByZXR1cm4gZmFsc2UKICAgICAgICAgICAgICAgIGVu
# ZAogICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICBpZiBlbmRjb2Rl
# ICE9IDAKICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlCiAg
# ICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAgICAgIAogICAgICAgICAg
# ICAgICAgcmV0dXJuIHRydWUKICAgICAgICBlbmQKICAgICAgICAKICAgICAg
# ICBkZWYgY2xlYW5fdXAKICAgICAgSU8ucG9wZW4oIiN7QG1ha2V9IGRpc3Rj
# bGVhbiAyPj4gZGlzdGNsZWFuIikgeyB8Y3wKICAgICAgICAgIG91dHB1dCA9
# IGMucmVhZGxpbmVzCiAgICAgIH0KICAgICAgICBlbmQKZW5kCgplbmQgI21v
# ZHVsZQoKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL3BrZ2Nv
# bmZpZy5yYn4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2
# NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDA2NzE1ADEwNzIzMzQzMTIyADAx
# MzcwNAAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAKCnJlcXVpcmUgJ2RldGVjdG9zJwoKbW9kdWxlIFFvbmYKCmNsYXNzIFBr
# Z0NvbmZpZ1BhcnNlcgogIGRlZiBpbml0aWFsaXplCiAgICBAdmFyaWFibGVz
# ID0ge30KICAgIEBzcGVjcyA9IHt9CiAgZW5kCiAgCiAgZGVmIHBhcnNlKHBh
# dGgpCiAgICBGaWxlLm9wZW4ocGF0aCkgeyB8ZmlsZXwKICAgICAgZmlsZS5l
# YWNoIHsgfGxpbmV8CiAgICAgICAgaWYgbGluZSA9fiAvKFx3Kyk9KC4qKSQv
# CiAgICAgICAgICBAdmFyaWFibGVzWyQxXSA9ICQyCiAgICAgICAgZWxzaWYg
# bGluZSA9fiAvKFx3Kyk6XHMoLiopJC8KICAgICAgICAgIEBzcGVjc1skMV0g
# PSAkMgogICAgICAgIGVuZAogICAgICB9CiAgICB9CiAgZW5kCiAgCiAgZGVm
# IHZhbHVlKHZhcmlhYmxlKQogICAgdmFsID0gQHZhcmlhYmxlc1t2YXJpYWJs
# ZV0KICAgIHdoaWxlIHZhbCA9fiAvKFwkXHsoXHcrKVx9KS8KICAgICAgdmFs
# LmdzdWIhKCQxLCB2YWx1ZSgkMikpCiAgICBlbmQKICAgIAogICAgdmFsCiAg
# ZW5kCiAgCiAgZGVmIHNwZWModmFyaWFibGUpCiAgICB2YWwgPSBAc3BlY3Nb
# dmFyaWFibGVdCiAgICB3aGlsZSB2YWwgPX4gLyhcJFx7KFx3KylcfSkvCiAg
# ICAgIHZhbC5nc3ViISgkMSwgdmFsdWUoJDIpKQogICAgZW5kCiAgICAKICAg
# IHZhbAogIGVuZAogIAogIGRlZiBsaWJzCiAgICBzcGVjKCJMaWJzIikKICBl
# bmQKICAKICBkZWYgY2ZsYWdzCiAgICBzcGVjKCJDZmxhZ3MiKQogIGVuZAog
# IAogIGRlZiByZXF1aXJlcwogICAgc3BlYygiUmVxdWlyZXMiKS50b19zCiAg
# ZW5kCmVuZAoKY2xhc3MgUGtnQ29uZmlnCiAgYXR0cl9yZWFkZXIgOmlkLCA6
# bGlicywgOmNmbGFncywgOnJlcXVpcmVzCiAgCiAgZGVmIGluaXRpYWxpemUo
# aWQpCiAgICBAaWQgPSBpZAogICAgQHNlYXJjaF9wYXRoID0gW10KICAgIEBy
# ZXF1aXJlcyA9IFtdCiAgZW5kCiAgCiAgZGVmIGZpbmQocGtnKQogICAgcGtn
# cGF0aCA9IGZpbmRfcGF0aF9mb3JfcGFja2FnZShwa2cpCiAgICAKICAgIGlm
# IG5vdCBwa2dwYXRoLm5pbD8KICAgICAgQGNmbGFncyA9ICIiCiAgICAgIEBs
# aWJzID0gIiIKICAgICAgcGFyc2UocGtncGF0aCwgcGtnKQogICAgICAKICAg
# ICAgcmV0dXJuIHRydWUKICAgIGVuZAogICAgCiAgICBmYWxzZQogIGVuZAog
# IAogIGRlZiBhZGRfc2VhcmNoX3BhdGgocGF0aCkKICAgIGlmIHBhdGgua2lu
# ZF9vZj8oQXJyYXkpCiAgICAgIEBzZWFyY2hfcGF0aC5jb25jYXQocGF0aCkK
# ICAgIGVsc2UKICAgICAgQHNlYXJjaF9wYXRoIDw8IHBhdGgudG9fcwogICAg
# ZW5kCiAgZW5kCiAgCiAgZGVmIGluY2x1ZGVfcGF0aAogICAgaW5jcGF0aCA9
# IFtdCiAgICBjZmxhZ3MgPSBAY2ZsYWdzLmR1cAogICAgd2hpbGUgY2ZsYWdz
# ID1+IC8oLUkoKFx3Oil7MCwxfShbXC9cXF1cUyspKykpLwogICAgICBpbmNw
# YXRoIDw8ICQyCiAgICAgIGNmbGFncy5zdWIhKCQxLCAiIikKICAgIGVuZAog
# ICAgCiAgICBpbmNwYXRoLnVuaXEKICBlbmQKICAKICBkZWYgZGVmaW5lcwog
# ICAgZGVmaW5lcyA9IFtdCiAgICBjZmxhZ3MgPSBAY2ZsYWdzLmR1cAogICAg
# d2hpbGUgY2ZsYWdzID1+IC8oLURccyooKFxXXHcrKSspKS8KICAgICAgZGVm
# aW5lcyA8PCAkMgogICAgICBkZWZpbmVzLnN1YiEoJDEsICIiKQogICAgZW5k
# CiAgICAKICAgIGRlZmluZXMudW5pcQogIGVuZAogIAogIGRlZiBmaW5kX3dp
# bjMyX3BhdGhzCiAgICBwYXRocyA9IGZpbmRfZW52X3BhdGhzCiAgICAKICAg
# IAogICAgcGF0aHMKICBlbmQKICAKICBkZWYgZmluZF91bml4X3BhdGhzCiAg
# ICBwYXRocyA9IGZpbmRfZW52X3BhdGhzCiAgICBpZiBGaWxlLmV4aXN0cz8o
# Ii9ldGMvbGQuc28uY29uZiIpCiAgICAgIEZpbGUub3BlbigiL2V0Yy9sZC5z
# by5jb25mIikgZG8gfGZpbGV8CiAgICAgICAgZmlsZS5lYWNoIHsgfGxpbmV8
# CiAgICAgICAgICBkaXIgPSBsaW5lLnN0cmlwKyIvcGtnY29uZmlnIgogICAg
# ICAgICAgaWYgRmlsZS5leGlzdHM/KGRpcikKICAgICAgICAgICAgcGF0aHMg
# PDwgZGlyCiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAgIGVuZAogICAg
# ZW5kCiAgICAKICAgIHBhdGhzIDw8ICIvdXNyL2xpYi9wa2djb25maWciIDw8
# ICIvdXNyL2xvY2FsL2xpYi9wa2djb25maWciIDw8IEVOVlsiSE9NRSJdLnRv
# X3MrIi9sb2NhbC9saWIvcGtnY29uZmlnIgogICAgCiAgICBwYXRocwogIGVu
# ZAogIAogIGRlZiBmaW5kX2Vudl9wYXRocwogICAgc2VwcGF0aCA9IChEZXRl
# Y3RPUy5vcz8gPT0gMSkgPyAiOyIgOiAiOiIKICAgIAogICAgcGF0aHMgPSBb
# ICIuIiBdCiAgICAoRU5WWyJQS0dfQ09ORklHX1BBVEgiXS50b19zK3NlcHBh
# dGgrRU5WWyJMRF9MSUJSQVJZX1BBVEgiXS50b19zKS5zcGxpdChzZXBwYXRo
# KS5lYWNoIHsgfHBhdGh8CiAgICAgIGlmIEZpbGUuZXhpc3RzPyhwYXRoKQog
# ICAgICAgIHBhdGhzIDw8IHBhdGgKICAgICAgZW5kCiAgICB9CiAgICAKICAg
# IHBhdGhzCiAgZW5kCiAgCiAgcHJpdmF0ZQogIGRlZiBwYXJzZShwa2dwYXRo
# LCBwa2cpCiAgICByZXR1cm4gaWYgcGtncGF0aC5uaWw/CiAgICAKICAgIHBh
# cnNlciA9IFBrZ0NvbmZpZ1BhcnNlci5uZXcKICAgIHBhcnNlci5wYXJzZShw
# a2dwYXRoKQogICAgCiAgICBwYXJzZXIucmVxdWlyZXMuc3BsaXQoLyx8XHMv
# KS5lYWNoIHsgfHJlcXwKICAgICAgcmVxLnN0cmlwIQogICAgICAKICAgICAg
# cGFyc2UoZmluZF9wYXRoX2Zvcl9wYWNrYWdlKHJlcSksIHJlcSkKICAgIH0K
# ICAgIAogICAgY2ZsYWdzID0gcGFyc2VyLmNmbGFncwogICAgbGlicyA9IHBh
# cnNlci5saWJzCiAgICAKICAgIGlmIGlkICE9IHBrZyBhbmQgbm90IChjZmxh
# Z3MuZW1wdHk/IG9yIGxpYnMuZW1wdHk/KQogICAgICBAcmVxdWlyZXMgPDwg
# cGtnCiAgICBlbmQKICAgIAogICAgQGNmbGFncyA9ICIje2NmbGFnc30gI3tA
# Y2ZsYWdzfSIKICAgIEBsaWJzID0gIiN7bGlic30gI3tAbGlic30iCiAgICAK
# ICAgIG5pbAogIGVuZAogIAogIGRlZiBmaW5kX3BhdGhfZm9yX3BhY2thZ2Uo
# cGtnKQogICAgaWYgbm90IHBrZyA9fiAvXC5wYyQvCiAgICAgIHBrZyArPSAi
# LnBjIgogICAgZW5kCiAgICAKICAgIHBhdGhzID0gQHNlYXJjaF9wYXRoCiAg
# ICAKICAgIGlmIERldGVjdE9TLm9zPyA9PSAxICMgV2luZG93cwogICAgICBw
# YXRocy5jb25jYXQgZmluZF93aW4zMl9wYXRocwogICAgZWxzZQogICAgICBw
# YXRocy5jb25jYXQgZmluZF91bml4X3BhdGhzCiAgICBlbmQKICAgIAogICAg
# cGtncGF0aCA9IG5pbAogICAgcGF0aHMuZWFjaCB7IHxwYXRofAogICAgICB0
# bXBwYXRoID0gcGF0aCsiLyIrcGtnCiAgICAgIGlmIEZpbGUuZXhpc3RzPyh0
# bXBwYXRoKQogICAgICAgIHBrZ3BhdGggPSB0bXBwYXRoCiAgICAgIGVsc2lm
# ICRERUJVRwogICAgICAgIHB1dHMgIkZhaWxlZDogI3t0ZW1wcGF0aH0iCiAg
# ICAgIGVuZAogICAgfQogICAgCiAgICBwa2dwYXRoCiAgZW5kCmVuZAoKZW5k
# CgoKaWYgJDAgPT0gX19GSUxFX18KICBwa2djb25maWcgPSBRb25mOjpQa2dD
# b25maWcubmV3CiAgcGtnY29uZmlnLmZpbmQoInRlc3RfcW9uZiIpCiAgCiAg
# cHV0cyBwa2djb25maWcubGlicwplbmQKCgAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9mcm9u
# dGVuZC5yYgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAw
# NjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwMDEyNAAxMDY3Mzc0MDMwMwAw
# MTMzNTAAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAACm1vZHVsZSBRb25mCgpjbGFzcyBGcm9udGVuZAogIGRlZiBpbml0aWFs
# aXplCiAgZW5kCiAgCiAgZGVmIHBhcnNlCiAgZW5kCmVuZAoKZW5kCgoKAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAABxb25maWd1cmUvZnJvbnRlbmRmYWN0b3J5
# LnJiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUx
# ADAwMDE3NTEAMDAwMDAwMDA2NDMAMTA2NzM3NDAzMDMAMDE0NzQ2ACAwAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAHVzdGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApyZXF1aXJl
# ICdwbGFpbl9mcm9udGVuZCcKCm1vZHVsZSBRb25mCgpjbGFzcyBGcm9udGVu
# ZEZhY3RvcnkKICBkZWYgc2VsZi5jcmVhdGUobmFtZSA9ICJ0ayIpCiAgICBj
# YXNlIG5hbWUKICAgICAgd2hlbiAicGxhaW4iCiAgICAgICAgUGxhaW5Gcm9u
# dGVuZC5uZXcKICAgICAgd2hlbiAidGsiCiAgICAgICAgaGF2ZV90ayA9IGZh
# bHNlCgogICAgICAgIGJlZ2luCiAgICAgICAgICBoYXZlX3RrID0gcmVxdWly
# ZSAndGsnCiAgICAgICAgcmVzY3VlIExvYWRFcnJvcgogICAgICAgICAgcmV0
# dXJuIGNyZWF0ZSgicGxhaW4iKQogICAgICAgIGVuZAogICAgICAgIAogICAg
# ICAgIHJlcXVpcmUgJ3RrX2Zyb250ZW5kJwogICAgICAgIFRrRnJvbnRlbmQu
# bmV3CiAgICAgIGVsc2UKICAgICAgICBuaWwKICAgIGVuZAogIGVuZAplbmQK
# CmVuZAoKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAcW9uZmlndXJlL2Zyb250ZW5kLnJifgAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUxADAw
# MDAwMDAwMTIzADEwNjcyNTcxMjE2ADAxMzU1MQAgMAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3Rh
# ciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKbW9kdWxlIFFvbmYKCmNsYXNz
# IEJhY2tlbmQKICBkZWYgaW5pdGlhbGl6ZQogIGVuZAogIAogIGRlZiBwYXJz
# ZQogIGVuZAplbmQKCmVuZAoKCgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFv
# bmZpZ3VyZS9zZXR1cF9yYi50cGx+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAwMDAwNjQ0ADAwMDE3NTEAMDAwMTc1MQAwMDAwMDAwNTcxMgAx
# MDY3MzAyNzc0NgAwMTM3NjcAIDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAACgoKIyBzZXR1cCBpcyBhIG9iamVjdCB0aGF0IGhl
# bHBzIHRvIGNvbmZpZ3VyZSB0aGUgcHJvamVjdAojIEl0IGNvbnRhaW5zIHNv
# bWUgbWV0aG9kcyBsaWtlOgojICAgYWRkX29wdGlvbgojICAgZmluZF9wYWNr
# YWdlCiMgICBhZGRfdGVzdAojICAgZ2VuZXJhdGVfcGtnY29uZmlnCmRlZiBj
# b25maWd1cmUoc2V0dXApCiMjIyMgCiMjIyMgYWRkX29wdGlvbiBhZGRzIGFu
# IG9wdGlvbiB0byB0aGUgc2NyaXB0LCBpdCBoYXMgdGhlIGZvbGxvd2luZyBv
# cHRpb25zOgojIyMjIDpuYW1lICAgICAgICAgVGhlIG5hbWUgb2YgdGhlIG9w
# dGlvbi4KIyMjIyA6dHlwZSAgICAgICAgIFRoZSB0eXBlIG9mIHRoZSBvcHRp
# b24gKCJwYXRoIiwgInN0cmluZyIsICJib29sIiBvciAidm9pZCIpLgojIyMj
# IDpvcHRpb25hbCAgICAgU2F5cyBpZiB0aGUgb3B0aW9uIGlzIHJlcXVpcmVk
# IG9yIG5vdC4KIyMjIyA6ZGVzY3JpcHRpb24gIERlc2NyaXB0aW9uIG9mIHRo
# ZSBvcHRpb24KIyMjIyA6ZGVmYXVsdCAgICAgIFRoZSBkZWZhdWx0IHZhbHVl
# CiMjIyMgCiMgICBzZXR1cC5hZGRfb3B0aW9uKDpuYW1lID0+ICJvcHRpb24t
# bmFtZSIsIDp0eXBlID0+ICJwYXRoIiwgOm9wdGlvbmFsID0+IGZhbHNlLCA6
# ZGVzY3JpcHRpb24gPT4gIlNldHMgYW4gb3B0aW9uIikKICAKIyMjIyAKIyMj
# IyBmaW5kX3BhY2thZ2Ugc2VhcmNoIGZvciBhIHBrZ2NvbmZpZyBzcGVjIGlu
# IHlvdXIgY29tcHV0ZXIsIGl0IHNlYXJjaCBpbiB0aGUgUEtHX0NPTkZJR19Q
# QVRIIGVudmlyb25tZW50IHZhcmlhYmxlIG9yIExEX0xJQlJBUllfUEFUSAoj
# IyMjIAojIyMjIEZpcnN0IG9wdGlvbiBpcyB0aGUgcGFja2FnZSBuYW1lIGFu
# ZCBpZiBpdCBpcyBvcHRpb25hbCBvciBub3QKIyMjIyAKIyAgIHNldHVwLmZp
# bmRfcGFja2FnZSgiZGJ1cy0xIiwgZmFsc2UpCgojIyMjIAojIyMjIGFkZF90
# ZXN0IGFkZHMgYW5kIHJ1biBhIHRlc3QgZm9yIHNvbWUgZmVhdHVyZSBvciBw
# YWNrYWdlLCB0aGUgb3B0aW9ucyBhcmU6CiMjIyMgICA6aWQgICAgICAgICAg
# IFRoZSBpZCBvZiB0aGUgdGVzdAojIyMjICAgOm5hbWUgICAgICAgICBUaGUg
# dGVzdCBuYW1lCiMjIyMgICA6ZGVmaW5lcyAgICAgIERlZmluZXMgdG8gdGVz
# dAojIyMjICAgOmluY2x1ZGVwYXRoICBBIGxpc3Qgd2l0aCB0aGUgaW5jbHVk
# ZSBwYXRoCiMjIyMgICA6bGlicyAgICAgICAgIFRoZSBsaWJzIHJlcXVpcmVk
# IGZvciB0aGUgdGVzdAojIyMjICAgOm9wdGlvbmFsICAgICBUcnVlIGlmIHRo
# ZSB0ZXN0IGlzIG9wdGlvbmFsCiMjIyMgICA6Y3VzdG9tICAgICAgIEEgY29k
# ZSB0byB0ZXN0CiMjIyMgCiMgc2V0dXAuYWRkX3Rlc3QoOmlkID0+ICJmZWF0
# dXJlIiwgOm5hbWUgPT4gIkZlYXR1cmUgc3VwcG9ydCIsIDpkZWZpbmVzID0+
# IFsiSEFWRV9GRUFUVVJFIl0sIDpvcHRpb25hbCA9PiB0cnVlLCA6aGVhZGVy
# cyA9PiBbImZlYXR1cmUuaCJdLCA6bGlicyA9PiAiLWxmZWF0dXJlIiAgKQoK
# IyMjIyAKIyMjIyBnZW5lcmF0ZV9wa2djb25maWcgR2VuZXJhdGVzIGEgc2lt
# cGxlIHBrZ2NvbmZpZyBmaWxlCiMjIyMgVGhlIG9wdGlvbnMgYXJlOgojIyMj
# ICAgOnBhY2thZ2VfbmFtZSAgVGhlIHBhY2thZ2UgbmFtZSAKIyMjIyAgIDpu
# YW1lICAgICAgICAgIFRoZSBhcHBsaWNhdGlvbiBuYW1lCiMjIyMgICA6ZGVz
# Y3JpcHRpb24gICBUaGUgYXBwbGljYXRpb24gZGVzY3JpcHRpb24KIyMjIyAg
# IDp2ZXJzaW9uICAgICAgIFRoZSB2ZXJzaW9uIG9mIHRoZSBhcHBsaWNhdGlv
# bgojIyMjICAgOmxpYmRpciAgICAgICAgVGhlIGRpcmVjdG9yeSB3aGVyZSBs
# aWJyYXJpZXMgYXJlLCBieSBkZWZhdWx0IGlzICQocHJlZml4KS9saWIKIyMj
# IyAgIDppbmNsdWRlZGlyICAgIFRoZSBkaXJlY3Rvcnkgd2hlcmUgaGVhZGVy
# cyBhcmUsIGJ5IGRlZmF1bHQgaXMgJChwcmVmaXgpL2luY2x1ZGUKIyMjIyAg
# IDpsaWJzICAgICAgICAgIExpYnJhcmllcyB0aGF0IGxpbmtzIHRoZSBwYWNr
# YWdlCiMjIyMgICA6Y2ZsYWdzICAgICAgICBDZmxhZ3MgZm9yIHRoZSBwYWNr
# YWdlCiMjIyMgICA6cmVxdWlyZXMgICAgICBBIGxpc3Qgb2YgcGFja2FnZSBu
# YW1lcyByZXF1aXJlZCBmb3IgdGhpcyBwYWNrYWdlLgojIyMjIAojIHNldHVw
# LmdlbmVyYXRlX3BrZ2NvbmZpZyg6cGFja2FnZV9uYW1lID0+ICJkbGliLWVk
# aXRvciIsIDpuYW1lID0+ICJETGliIFF0IGxpYnJhcnkiLCA6ZGVzY3JpcHRp
# b24gPT4gIkEgZXh0ZW5zaW9uIGZvciBRdDQiLCA6dmVyc2lvbiA9PiAiMC4x
# YmV0YSIsIDpsaWJkaXIgPT4gbmlsLCA6aW5jbHVkZWRpciA9PiBuaWwsIDps
# aWJzID0+ICItTCR7bGliZGlyfSAtbGRjb3JlIC1sZGd1aSAtbGRlZGl0b3Ii
# LCA6Y2ZsYWdzID0+ICItSSR7aW5jbHVkZWRpcn0iLCA6cmVxdWlyZXMgPT4g
# WyJzb21lcGFja2FnZSIsICJvdGhlcnBhY2thZ2UiIF0gKQplbmQKCiMgVGhp
# cyBtZXRob2QgaXMgY2FsbGVkIGJlZm9yZSB0byBzZWFyY2ggYSBwYWNrYWdl
# CmRlZiBzZXR1cF9wa2djb25maWcocGtnY29uZmlnLCBhcmdzKQojICAgY2Fz
# ZSBwa2djb25maWcuaWQKIyAgICAgd2hlbiAiZGJ1cy0xIgojICAgICAgIHBr
# Z2NvbmZpZy5hZGRfc2VhcmNoX3BhdGgoIi9hL3BhdGgvdG8vdGhlL3BjIikK
# IyAgIGVuZAplbmQKCmRlZiBzZXR1cF90ZXN0KGlkLCB0c3QsIGFyZ3MpCiMg
# ICBjYXNlIGlkCiMgICAgIHdoZW4gImZlYXR1cmUiCiMgICAgICAgdHN0LmFk
# ZF9kZWZpbmUoIk1ZREVGSU5FIikKIyAgICAgICB0c3QuYWRkX2xpYigiLWxt
# eWxpYiIpCiMgICAgICAgdHN0LmFkZF9pbmNsdWRlX3BhdGgoIi9zb21lL2lu
# Y2x1ZGUvcGF0aC9yZXF1aXJlZC90by9jb21waWxlL3RoZS90ZXN0IikKIyAg
# IGVuZAplbmQKCmRlZiBzZXR1cF9jb25maWcoY2ZnLCBhcmdzKQojICAgY2Zn
# LmFkZF9pbmNsdWRlX3BhdGggRmlsZS5leHBhbmRfcGF0aCgic3JjIikKIyAg
# IGNmZy5hZGRfZGVmaW5lKCJfX1NURENfQ09OU1RBTlRfTUFDUk9TIikKIyAg
# IGNmZy5hZGRfcXRtb2R1bGUoInhtbCIpCiMgICBjZmcuYWRkX3F0bW9kdWxl
# KCJuZXR3b3JrIikKZW5kCgoKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW9uZmlndXJlL21ha2VmaWxl
# LnJiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAwMDA2NDQA
# MDAwMTc1MQAwMDAxNzUxADAwMDAwMDAzMjc1ADEwNzEzNzQ1MDI1ADAxMzMy
# MQAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdlawAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAN
# CnJlcXVpcmUgJ3JiY29uZmlnJw0KDQptb2R1bGUgUW9uZg0KICBjbGFzcyBN
# YWtlZmlsZQ0KICAgIGRlZiBzZWxmLmZpbmRfbWFrZWZpbGVzKHBhdGgpDQog
# ICAgICBtYWtlZmlsZXMgPSBbXQ0KICAgICAgRGlyLmZvcmVhY2gocGF0aCkg
# eyB8ZnwNCiAgICAgICAgZmlsZSA9ICIje3BhdGh9LyN7Zn0iDQogICAgICAg
# IGlmIEZpbGUuc3RhdChmaWxlKS5kaXJlY3Rvcnk/DQogICAgICAgICAgaWYg
# bm90IGYgPX4gL15cLi8NCiAgICAgICAgICAgIG1ha2VmaWxlcy5jb25jYXQg
# ZmluZF9tYWtlZmlsZXMoZmlsZSkNCiAgICAgICAgICBlbmQNCiAgICAgICAg
# ZWxzaWYgZi5kb3duY2FzZSA9fiAvXm1ha2VmaWxlKC5kZWJ1Z3wucmVsZWFz
# ZSl7MCwxfS8NCiAgICAgICAgICBtYWtlZmlsZXMgPDwgZmlsZQ0KICAgICAg
# ICBlbmQNCiAgICAgIH0NCiAgICAgIG1ha2VmaWxlcw0KICAgIGVuZA0KICAg
# IA0KICAgIGRlZiBzZWxmLm92ZXJyaWRlKG1ha2VmaWxlLCBkZXN0ZGlyLCBz
# dGF0dXNGaWxlKQ0KICAgICAgbmV3bWFrZWZpbGUgPSAiIg0KICAgICAgDQog
# ICAgICBydWJ5X2JpbiA9ICIjezo6Q29uZmlnOjpDT05GSUdbImJpbmRpciJd
# fS8jezo6Q29uZmlnOjpDT05GSUdbInJ1YnlfaW5zdGFsbF9uYW1lIl19Ig0K
# ICAgICAgDQogICAgICBGaWxlLm9wZW4obWFrZWZpbGUsICJyIikgeyB8ZnwN
# CiAgICAgICAgbGluZXMgPSBmLnJlYWRsaW5lcw0KICAgICAgICANCiAgICAg
# ICAgaWYgRmlsZTo6QUxUX1NFUEFSQVRPUg0KICAgICAgICAgIGRlc3RkaXIu
# Z3N1YiEoIi8iLCBGaWxlOjpBTFRfU0VQQVJBVE9SKQ0KICAgICAgICBlbmQN
# CiAgICAgICAgDQogICAgICAgIGluZGV4ID0gMA0KICAgICAgICByZXBsYWNl
# ZCA9IGZhbHNlDQogICAgICAgIHdoaWxlIGluZGV4IDwgbGluZXMuc2l6ZQ0K
# ICAgICAgICAgIGxpbmUgPSBsaW5lc1tpbmRleF0NCiAgICAgICAgICBpZiBs
# aW5lID1+IC9eXHMrW0BdezAsMX1cJFwoUU1BS0VcKS8NCiAgICAgICAgICAg
# IG5ld21ha2VmaWxlICs9IGxpbmUNCiAgICAgICAgICAgIG5ld21ha2VmaWxl
# ICs9ICJcdCN7cnVieV9iaW59ICN7c3RhdHVzRmlsZX0gI3ttYWtlZmlsZX1c
# biINCiAgICAgICAgICAgIGluZGV4ICs9IDENCiAgICAgICAgICBlbHNlDQog
# ICAgICAgICAgICBpZiBub3QgcmVwbGFjZWQNCiAgICAgICAgICAgICAgaWYg
# bGluZSA9fiAvXklOU1RBTExfRElSLw0KICAgICAgICAgICAgICAgIG5ld21h
# a2VmaWxlICs9ICJJTlNUQUxMX1JPT1QgPSAje2Rlc3RkaXJ9XG4iDQogICAg
# ICAgICAgICAgICAgcmVwbGFjZWQgPSB0cnVlDQogICAgICAgICAgICAgIGVu
# ZA0KICAgICAgICAgICAgZWxzZQ0KICAgICAgICAgICAgICBpZiBsaW5lID1+
# IC9cJFwoSU5TVEFMTF9ST09UXCkvDQogICAgICAgICAgICAgICAgaWYgRmls
# ZTo6QUxUX1NFUEFSQVRPUg0KICAgICAgICAgICAgICAgICAgbGluZS5nc3Vi
# ISgiLyIsIEZpbGU6OkFMVF9TRVBBUkFUT1IpDQogICAgICAgICAgICAgICAg
# ZW5kDQogICAgICAgICAgICAgIGVuZA0KICAgICAgICAgICAgZW5kDQogICAg
# ICAgICAgICBuZXdtYWtlZmlsZSArPSBsaW5lDQogICAgICAgICAgZW5kDQog
# ICAgICAgICAgaW5kZXggKz0gMQ0KICAgICAgICBlbmQNCiAgICAgIH0NCiAg
# ICAgIA0KICAgICAgRmlsZS5vcGVuKG1ha2VmaWxlLCAidyIpIHsgfGZ8DQog
# ICAgICAgIGYgPDwgbmV3bWFrZWZpbGUNCiAgICAgIH0NCiAgICBlbmQNCiAg
# ZW5kDQplbmQNCg0KDQoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFvbmZpZ3VyZS9pbml0LnJifgAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwMDAwNjQ0ADAwMDE3
# NTEAMDAwMTc1MQAwMDAwMDAwMDMzMAAxMDY3MzUwMjA0MwAwMTI2NjYAIDAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAdXN0YXIgIABrcmF3ZWsAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACnJlcXVp
# cmUgJ3FvbmYnCgpEaXIuY2hkaXIoRmlsZS5kaXJuYW1lKG9sZGxvY2F0aW9u
# KCJxb25mLnJiIikpKQoKcW9uZiA9IFFvbmY6OkNvbmZpZ3VyZS5uZXcKcW9u
# Zi5nZXRfYXJncyhBUkdWKQpxb25mLmZpbmRfcGFja2FnZXMKcW9uZi5ydW5f
# dGVzdHMKCnFvbmYuc2F2ZShGaWxlLmV4cGFuZF9wYXRoKCJjb25maWcucHJp
# IikpCgpxb25mLmdlbmVyYXRlX21ha2VmaWxlcwoKAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAABxb25maWd1cmUvaW5pdC5yYgAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAMDAwMDY0NAAwMDAxNzUxADAwMDE3NTEA
# MDAwMDAwMDAzMDYAMTA2NzM3NDAzMDMAMDEyNDc2ACAwAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHVz
# dGFyICAAa3Jhd2VrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApyZXF1aXJlICdxb25mJwoK
# RGlyLmNoZGlyKEZpbGUuZGlybmFtZShvbGRsb2NhdGlvbigicW9uZi5yYiIp
# KSkKCnFvbmYgPSBRb25mOjpDb25maWd1cmUubmV3CnFvbmYuZ2V0X2FyZ3Mo
# QVJHVikKcW9uZi5maW5kX3BhY2thZ2VzCnFvbmYucnVuX3Rlc3RzCgpxb25m
# LnNhdmUoImNvbmZpZy5wcmkiKQoKcW9uZi5nZW5lcmF0ZV9tYWtlZmlsZXMK
# CgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# cW9uZmlndXJlL3FvbmYucmJ+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAADAwMDA2NDQAMDAwMTc1MQAwMDAxNzUxADAwMDAwMDMzNTEy
# ADEwNzMyMzY1NTAzADAxMjcwMgAgMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB1c3RhciAgAGtyYXdl
# awAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAKcmVxdWlyZSAndGVzdCcKcmVxdWlyZSAncGtn
# Y29uZmlnJwpyZXF1aXJlICdmcm9udGVuZGZhY3RvcnknCnJlcXVpcmUgJ2Nv
# bmZpZycKcmVxdWlyZSAncW1ha2UnCnJlcXVpcmUgJ21ha2VmaWxlJwoKbW9k
# dWxlIFFvbmYKICBWRVJTSU9OID0gIjAuNiIuZnJlZXplCiAgVkVSU0lPTl9E
# QVRFID0gIjIwMDctMTItMDIiCiAgCiAgY2xhc3MgUXRWZXJzaW9uCiAgICBh
# dHRyX3JlYWRlciA6dmVyc2lvbiwgOm9wICMgMDogZXF1YWwgMTogZ3JlYXRl
# ciAyOiBncmVhdGVyIGVxdWFsCiAgICAKICAgIGRlZiBpbml0aWFsaXplKHZl
# cnNpb24pCiAgICAgIEBvcCA9IDIKICAgICAgcGFyc2UodmVyc2lvbikKICAg
# IGVuZAogICAgCiAgICBkZWYgPT0odmVyKQogICAgICBAb3AgPSAwCiAgICAg
# IHBhcnNlKHZlcikKICAgIGVuZAogICAgCiAgICBkZWYgPih2ZXIpCiAgICAg
# IEBvcCA9IDEKICAgICAgcGFyc2UodmVyKQogICAgZW5kCiAgICAKICAgIGRl
# ZiA+PSh2ZXIpCiAgICAgIEBvcCA9IDIKICAgICAgcGFyc2UodmVyKQogICAg
# ZW5kCiAgICAKICAgIGRlZiB0b19zCiAgICAgIHMgPSAiUXQgIgogICAgICBj
# YXNlIEBvcAogICAgICAgIHdoZW4gMAogICAgICAgICAgcys9Ij09IgogICAg
# ICAgIHdoZW4gMQogICAgICAgICAgcys9Ij4iCiAgICAgICAgZWxzZQogICAg
# ICAgICAgcys9Ij49IgogICAgICBlbmQKICAgICAgcys9ICIgIitAdmVyc2lv
# bgogICAgICAKICAgICAgcwogICAgZW5kCiAgICAKICAgIHByaXZhdGUKICAg
# IGRlZiBwYXJzZSh2ZXJzaW9uKQogICAgICBAdmVyc2lvbiA9IHZlcnNpb24u
# dG9fcwogICAgICB2ZXIgPSBAdmVyc2lvbi50b19zLnNwbGl0KCcuJykKICAg
# ICAgc2l6ZSA9IHZlci5zaXplCiAgICAgICAgCiAgICAgIGZpbmFsdmVyID0g
# W10KICAgICAgMy50aW1lcyB7IHxpfAogICAgICAgIHRwID0gdmVyW2ldCiAg
# ICAgICAgCiAgICAgICAgaWYgdHAubmlsPwogICAgICAgICAgdHAgPSAiMCIK
# ICAgICAgICBlbmQKICAgICAgICAKICAgICAgICBmaW5hbHZlciA8PCB0cAog
# ICAgICB9CiAgICAgIAogICAgICBAdmVyc2lvbiA9IGZpbmFsdmVyLmpvaW4o
# Ii4iKQogICAgZW5kCiAgZW5kCiAgCiAgY2xhc3MgU2V0dXAKICAgIGF0dHJf
# cmVhZGVyIDpvcHRpb25zLCA6cGFja2FnZXMsIDp0ZXN0cywgOnBrZ19jb25m
# aWdzCiAgICBkZWYgaW5pdGlhbGl6ZQogICAgICBAb3B0aW9ucyA9IFtdCiAg
# ICAgIEBwYWNrYWdlcyA9IFtdCiAgICAgIEB0ZXN0cyA9IFtdCiAgICAgIEBw
# a2dfY29uZmlncyA9IFtdCiAgICAgIEBxdF92ZXJzaW9uID0gUXRWZXJzaW9u
# Lm5ldygiNC4wLjAiKQogICAgICAKICAgICAgYWRkX29wdGlvbig6bmFtZSA9
# PiAicHJlZml4IiwgOnR5cGUgPT4gInBhdGgiLCA6ZGVmYXVsdCA9PiAiL3Vz
# ci9sb2NhbC8iLCA6b3B0aW9uYWwgPT4gdHJ1ZSwgOmRlc2NyaXB0aW9uID0+
# ICJTZXRzIHRoZSBpbnN0YWxsIHByZWZpeCIpCiAgICBlbmQKICAgIAogICAg
# ZGVmIGFkZF9vcHRpb24ob3B0aW9uKQogICAgICBpZiBub3Qgb3B0aW9uWzp0
# eXBlXS5uaWw/CiAgICAgICAgb3B0aW9uWzp0eXBlXS5kb3duY2FzZSEKICAg
# ICAgZW5kCiAgICAgIAogICAgICBAb3B0aW9ucyA8PCBvcHRpb24KICAgIGVu
# ZAogICAgCiAgICBkZWYgZmluZF9wYWNrYWdlKG9wdGlvbnMpCiAgICAgIEBw
# YWNrYWdlcyA8PCBvcHRpb25zCiAgICBlbmQKICAgIAogICAgZGVmIGFkZF90
# ZXN0KG9wdGlvbikKICAgICAgcmFpc2UgIlBsZWFzZSBzZXQgYSB2YWxpZCBu
# YW1lIHRvIHRoZSB0ZXN0IiBpZiBvcHRpb25bOm5hbWVdLnRvX3MuZW1wdHk/
# CiAgICAgIHJhaXNlICJQbGVhc2Ugc2V0IGEgdmFsaWQgaWQgdG8gdGhlIHRl
# c3QiIGlmIG9wdGlvbls6aWRdLnRvX3MuZW1wdHk/CiAgICAgIAogICAgICBA
# dGVzdHMgPDwgb3B0aW9uCiAgICBlbmQKICAgIAogICAgZGVmIGdlbmVyYXRl
# X3BrZ2NvbmZpZyhvcHRpb25zKQogICAgICByYWlzZSAiUGxlYXNlIHNldCBh
# IHZhbGlkIG5hbWUiIGlmIG9wdGlvbnNbOnBhY2thZ2VfbmFtZV0udG9fcy5l
# bXB0eT8KICAgICAgQHBrZ19jb25maWdzIDw8IG9wdGlvbnMKICAgIGVuZAog
# ICAgCiAgICBkZWYgcXQKICAgICAgQHF0X3ZlcnNpb24KICAgIGVuZAogICAg
# CiAgZW5kCiAgCiAgY2xhc3MgQ29uZmlndXJlCiAgICBkZWYgaW5pdGlhbGl6
# ZQogICAgICBAYXJndW1lbnRzID0ge30KICAgICAgQGNvbmZpZyA9IENvbmZp
# Zy5uZXcKICAgICAgCiAgICAgIEBsb2dfZmlsZSA9IG9sZGxvY2F0aW9uKCJj
# b25maWcubG9nIikKICAgICAgCiAgICAgIEZpbGUub3BlbihAbG9nX2ZpbGUs
# ICJ3IikgZG8gfGZ8CiAgICAgICAgZiA8PCAiPT09PT09PT4gR2VuZXJhdGVk
# IGF0ICN7VGltZS5ub3d9IDw9PT09PT09XG4iCiAgICAgIGVuZAogICAgICAK
# ICAgICAgcW1ha2VfcGF0aHMgPSBbXQogICAgICBpZiggRGV0ZWN0T1Mub3M/
# ICE9IDEgKSAjIFVuaXgKICAgICAgICBJTy5wb3BlbigibG9jYXRlIC1yIC9x
# bWFrZSQgMj4gI3tuZXdsb2NhdGlvbignbG9jYXRlX2xvZycpfSIpIHsgfHBy
# Y3wKICAgICAgICAgIHByZV9wYXRocyA9IHByYy5yZWFkbGluZXMKICAgICAg
# ICAgIHByZV9wYXRocy5lYWNoIHsgfHFtfAogICAgICAgICAgICBxbS5zdHJp
# cCEKICAgICAgICAgICAgaWYgRmlsZS5leGlzdHM/KHFtKQogICAgICAgICAg
# ICAgIHN0YXQgPSBGaWxlLnN0YXQocW0pCiAgICAgICAgICAgICAgCiAgICAg
# ICAgICAgICAgaWYgKG5vdCAoc3RhdC5zeW1saW5rPyBvciBzdGF0LmRpcmVj
# dG9yeT8pKSBhbmQgc3RhdC5leGVjdXRhYmxlPwogICAgICAgICAgICAgICAg
# cW1ha2VfcGF0aHMgPDwgcW0KICAgICAgICAgICAgICBlbmQKICAgICAgICAg
# ICAgZW5kCiAgICAgICAgICB9CiAgICAgICAgfQogICAgICBlbmQKICAgICAg
# CiAgICAgIEBxbWFrZSA9IFFNYWtlLm5ldyhxbWFrZV9wYXRocywgQGxvZ19m
# aWxlKQogICAgICAKICAgICAgY2ZnID0gU2V0dXAubmV3CiAgICAgIGNvbmZp
# Z3VyZShjZmcpCiAgICAgIAogICAgICBwcmludCAiPT4gU2VhcmNoaW5nIGZv
# ciAje2NmZy5xdH0uLi4gIgogICAgICBpZiBAcW1ha2UuZmluZF9xbWFrZShj
# ZmcucXQudmVyc2lvbiwgY2ZnLnF0Lm9wKQogICAgICAgIHB1dHMgIigje0Bx
# bWFrZS5wYXRofSkgW1lFU10iCiAgICAgIGVsc2UKICAgICAgICBwdXRzICJb
# Tk9dIgogICAgICAgIGV4aXQoLTEpCiAgICAgIGVuZAogICAgICAKICAgICAg
# QG9wdGlvbnMgPSBjZmcub3B0aW9ucwogICAgICBAcGFja2FnZXMgPSBjZmcu
# cGFja2FnZXMKICAgICAgQHRlc3RzID0gY2ZnLnRlc3RzCiAgICAgIEBwa2df
# Y29uZmlncyA9IGNmZy5wa2dfY29uZmlncwogICAgICBAc3RhdHVzRmlsZSA9
# IERpci5nZXR3ZCsiL3VwZGF0ZS1tYWtlZmlsZSIKICAgIGVuZAogICAgCiAg
# ICBkZWYgZmluZF9wYWNrYWdlcyAjIFRPRE86IHNvcG9ydGFyIGluZm9ybWFj
# aW9uIHBhcmEgZWwgc2lzdGVtYSBvcGVyYXRpdm8gc29icmUgY29tbyBpbnN0
# YWxhciBlbCBwYXF1ZXRlCiAgICAgIEBwYWNrYWdlcy5lYWNoIHsgfHBhY2th
# Z2V8CiAgICAgICAgcHJpbnQgIj0+IFNlYXJjaGluZyBmb3IgcGFja2FnZTog
# I3twYWNrYWdlWzpuYW1lXX0uLi4gICIKICAgICAgICBwa2djb25maWcgPSBQ
# a2dDb25maWcubmV3KHBhY2thZ2VbOm5hbWVdKQogICAgICAgIHBrZ2NvbmZp
# Zy5hZGRfc2VhcmNoX3BhdGgoIiN7QGFyZ3VtZW50c1sicHJlZml4Il19L2xp
# Yi9wa2djb25maWciKQogICAgICAgIGJlZ2luCiAgICAgICAgICBzZXR1cF9w
# a2djb25maWcocGtnY29uZmlnLCBAYXJndW1lbnRzKQogICAgICAgIHJlc2N1
# ZSBOb01ldGhvZEVycm9yID0+IGUKICAgICAgICAgIGlmIG5vdCBlLm1lc3Nh
# Z2UgPX4gL3NldHVwX3BrZ2NvbmZpZy8KICAgICAgICAgICAgcHV0cyAiRXJy
# b3I6ICN7ZS5tZXNzYWdlfSIKICAgICAgICAgICAgcHV0cyBlLmJhY2t0cmFj
# ZQogICAgICAgICAgICBleGl0KC0xKQogICAgICAgICAgZW5kCiAgICAgICAg
# ZW5kCiAgICAgICAgCiAgICAgICAgaWYgbm90IHBrZ2NvbmZpZy5maW5kKHBh
# Y2thZ2VbOm5hbWVdKQogICAgICAgICAgcHV0cyAiW05PXSIKICAgICAgICAg
# IAogICAgICAgICAgb3B0aW9uYWwgPSBwYWNrYWdlWzpvcHRpb25hbF0ubmls
# PyA/IGZhbHNlIDogcGFja2FnZVs6b3B0aW9uYWxdCiAgICAgICAgICAKICAg
# ICAgICAgIGlmIG5vdCBvcHRpb25hbAogICAgICAgICAgICBwdXRzICI9PiBU
# aGUgcGFja2FnZSAnI3twYWNrYWdlWzpuYW1lXX0nIGlzIHJlcXVpcmVkLiIK
# ICAgICAgICAgICAgZXhpdCgtMSkKICAgICAgICAgIGVuZAogICAgICAgIGVs
# c2UKICAgICAgICAgIHB1dHMgIltZRVNdIgogICAgICAgICAgZ2xvYmFsID0g
# cGFja2FnZVs6Z2xvYmFsXS5uaWw/ID8gZmFsc2UgOiBwYWNrYWdlWzpnbG9i
# YWxdCiAgICAgICAgICAKICAgICAgICAgIHFvbmZwa2cgPSBAY29uZmlnCiAg
# ICAgICAgICAKICAgICAgICAgIGlmIG5vdCBnbG9iYWwKICAgICAgICAgICAg
# cW9uZnBrZyA9IFBhY2thZ2UubmV3KHBhY2thZ2VbOm5hbWVdKQogICAgICAg
# ICAgZW5kCiAgICAgICAgICAKICAgICAgICAgIGlmICRERUJVRwogICAgICAg
# ICAgICBwdXRzICI9PiBQYWNrYWdlIGlzIGdsb2JhbD8gI3tnbG9iYWx9Igog
# ICAgICAgICAgZW5kCiAgICAgICAgICAKIyAgICAgICAgICAgcGtnY29uZmln
# LnJlcXVpcmVzLmVhY2ggeyB8cmVxfAojICAgICAgICAgICAgIEBjb25maWcu
# YWRkX3FvbmZfcGFja2FnZShyZXEpCiMgICAgICAgICAgIH0KICAgICAgICAg
# IAogICAgICAgICAgcW9uZnBrZy5hZGRfbGliKHBrZ2NvbmZpZy5saWJzKQog
# ICAgICAgICAgCiAgICAgICAgICBwa2djb25maWcuaW5jbHVkZV9wYXRoLmVh
# Y2ggeyB8aW5jcGF0aHwKICAgICAgICAgICAgaWYgJERFQlVHCiAgICAgICAg
# ICAgICAgcHV0cyAiPT4gQWRkaW5nIGluY2x1ZGUgcGF0aDogI3tpbmNwYXRo
# fSIKICAgICAgICAgICAgZW5kCiAgICAgICAgICAgIHFvbmZwa2cuYWRkX2lu
# Y2x1ZGVfcGF0aChpbmNwYXRoKQogICAgICAgICAgfQogICAgICAgICAgCiAg
# ICAgICAgICBwa2djb25maWcuZGVmaW5lcy5lYWNoIHsgfGRlZmluZXwKICAg
# ICAgICAgICAgcW9uZnBrZy5hZGRfZGVmaW5lKGRlZmluZSkKICAgICAgICAg
# IH0KICAgICAgICAgIAogICAgICAgICAgaWYgbm90IGdsb2JhbAogICAgICAg
# ICAgICBAY29uZmlnLmFkZF9xb25mX3BhY2thZ2UocW9uZnBrZykKICAgICAg
# ICAgIGVsc2UKICAgICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9mZWF0dXJl
# KHBhY2thZ2VbOm5hbWVdKQogICAgICAgICAgZW5kCiAgICAgICAgZW5kCiAg
# ICAgIH0KICAgICAgaWYgJERFQlVHCiAgICAgICAgcHV0cyAiSU5DTFVERVBB
# VEg9I3tAY29uZmlnLmluY2x1ZGVfcGF0aC5qb2luKCIgIil9IgogICAgICBl
# bmQKICAgIGVuZAogICAgCiAgICBkZWYgcnVuX3Rlc3RzCiAgICAgIEB0ZXN0
# cy5lYWNoIHsgfHRlc3RvcHR8CiAgICAgICAgcHJpbnQgIj0+IFRlc3Rpbmcg
# I3t0ZXN0b3B0WzpuYW1lXX0uLi4gICIKICAgICAgICAKICAgICAgICB0c3Qg
# PSBUZXN0Lm5ldyh0ZXN0b3B0LCBAY29uZmlnLCBAbG9nX2ZpbGUpCiAgICAg
# ICAgCiAgICAgICAgYmVnaW4KICAgICAgICAgIHNldHVwX3Rlc3QodGVzdG9w
# dFs6aWRdLCB0c3QsIEBhcmd1bWVudHMpCiAgICAgICAgcmVzY3VlIE5vTWV0
# aG9kRXJyb3IgPT4gZQogICAgICAgICAgaWYgbm90IGUubWVzc2FnZSA9fiAv
# c2V0dXBfdGVzdC8KICAgICAgICAgICAgcHV0cyAiRXJyb3I6ICN7ZX0iCiAg
# ICAgICAgICAgIHB1dHMgZS5iYWNrdHJhY2UKICAgICAgICAgICAgZXhpdCgt
# MSkKICAgICAgICAgIGVuZAogICAgICAgIGVuZAogICAgICAgIAogICAgICAg
# IG9wdGlvbmFsID0gdGVzdG9wdFs6b3B0aW9uYWxdLm5pbD8gPyBmYWxzZSA6
# IHRlc3RvcHRbOm9wdGlvbmFsXQogICAgICAgIGlmIHRzdC5ydW4oQHFtYWtl
# KQogICAgICAgICAgZ2xvYmFsID0gdGVzdG9wdFs6Z2xvYmFsXS5uaWw/ID8g
# ZmFsc2UgOiB0ZXN0b3B0WzpnbG9iYWxdCiAgICAgICAgICAKICAgICAgICAg
# IGlmICRERUJVRwogICAgICAgICAgICBwdXRzICI9PiBUZXN0IGlzIGdsb2Jh
# bDogI3tnbG9iYWx9IgogICAgICAgICAgZW5kCiAgICAgICAgICAKICAgICAg
# ICAgIHFvbmZwa2cgPSBAY29uZmlnCiAgICAgICAgICAKICAgICAgICAgIGlm
# IG5vdCBnbG9iYWwKICAgICAgICAgICAgcW9uZnBrZyA9IFBhY2thZ2UubmV3
# KHRzdC5pZCkKICAgICAgICAgIGVuZAogICAgICAgICAgCiAgICAgICAgICB0
# c3QuaW5jbHVkZV9wYXRoLmVhY2ggeyB8aW5jcGF0aHwKICAgICAgICAgICAg
# aWYgJERFQlVHCiAgICAgICAgICAgICAgcHV0cyAiPT4gQWRkaW5nIGluY2x1
# ZGUgcGF0aDogI3tpbmNwYXRofSIKICAgICAgICAgICAgZW5kCiAgICAgICAg
# ICAgIHFvbmZwa2cuYWRkX2luY2x1ZGVfcGF0aChpbmNwYXRoKQogICAgICAg
# ICAgfQogICAgICAgICAgCiAgICAgICAgICB0c3QuZGVmaW5lcy5lYWNoIHsg
# fGRlZmluZXwKICAgICAgICAgICAgcW9uZnBrZy5hZGRfZGVmaW5lKGRlZmlu
# ZSkKICAgICAgICAgIH0KICAgICAgICAgIAogICAgICAgICAgcW9uZnBrZy5h
# ZGRfbGliKHRzdC5saWJzLnRvX3MpCiAgICAgICAgICAKICAgICAgICAgIGlm
# IG5vdCBnbG9iYWwKICAgICAgICAgICAgQGNvbmZpZy5hZGRfcW9uZl9wYWNr
# YWdlKHFvbmZwa2cpCiAgICAgICAgICBlbHNlCiAgICAgICAgICAgIEBjb25m
# aWcuYWRkX3FvbmZfZmVhdHVyZSh0c3QuaWQpCiAgICAgICAgICBlbmQKICAg
# ICAgICAgIHB1dHMgIltPS10iCiAgICAgICAgZWxzZQogICAgICAgICAgcHV0
# cyAiW0ZBSUxFRF0iCiAgICAgICAgICAKICAgICAgICAgIGlmIG5vdCBvcHRp
# b25hbAogICAgICAgICAgICBwdXRzICI9PiAje3Rlc3RvcHRbOm5hbWVdfSBp
# cyByZXF1aXJlZCIKICAgICAgICAgICAgZXhpdCgtMSkKICAgICAgICAgIGVu
# ZAogICAgICAgIGVuZAogICAgICB9CiAgICBlbmQKICAgIAogICAgZGVmIGdl
# bmVyYXRlX21ha2VmaWxlcwogICAgICBwdXRzICI9PiBDcmVhdGluZyBtYWtl
# ZmlsZXMuLi4iCiAgICAgIEBxbWFrZS5ydW4oIiIsIHRydWUpCiAgICAgIAog
# ICAgICBwdXRzICI9PiBVcGRhdGluZyBtYWtlZmlsZXMuLi4iCiAgICAgIAog
# ICAgICBAbWFrZWZpbGVzID0gTWFrZWZpbGU6OmZpbmRfbWFrZWZpbGVzKERp
# ci5nZXR3ZCkKICAgICAgQG1ha2VmaWxlcy5lYWNoIHsgfG1ha2VmaWxlfAog
# ICAgICAgICAgTWFrZWZpbGU6Om92ZXJyaWRlKG1ha2VmaWxlLCBGaWxlLmV4
# cGFuZF9wYXRoKEBhcmd1bWVudHNbInByZWZpeCJdLnRvX3MpLCBAc3RhdHVz
# RmlsZSkKICAgICAgfQogICAgICAKICAgICAgRmlsZS5vcGVuKEBzdGF0dXNG
# aWxlLCAidyIgKSB7IHxmaWxlfAogICAgICAgIGZpbGUgPDwgJUAKI3tGaWxl
# LnJlYWQobmV3bG9jYXRpb24oIm1ha2VmaWxlLnJiIikpfQpRb25mOjpNYWtl
# ZmlsZTo6b3ZlcnJpZGUoIEFSR1ZbMF0udG9fcywgIiN7QGFyZ3VtZW50c1si
# cHJlZml4Il19IiwgIiN7QHN0YXR1c0ZpbGV9IiApCkAKCmlmIERldGVjdE9T
# Lm9zPyA9PSAxICN3aW4zMgogICAgICAgIGZpbGUgPDwgJUAKUW9uZjo6TWFr
# ZWZpbGU6Om92ZXJyaWRlKCBBUkdWWzBdLnRvX3MrIi5yZWxlYXNlIiwgIiN7
# QGFyZ3VtZW50c1sicHJlZml4Il19IiwgIiN7QHN0YXR1c0ZpbGV9IiApClFv
# bmY6Ok1ha2VmaWxlOjpvdmVycmlkZSggQVJHVlswXS50b19zKyIuZGVidWci
# LCAiI3tAYXJndW1lbnRzWyJwcmVmaXgiXX0iLCAiI3tAc3RhdHVzRmlsZX0i
# ICkKQAplbmQKICAgICAgfQogICAgZW5kCiAgICAKICAgIGRlZiBnZXRfYXJn
# cyhhcmd2KQogICAgICBvcHRjID0gMAogICAgICBsYXN0X29wdCA9ICIiCiAg
# ICAgIAogICAgICBAb3B0aW9ucy5lYWNoIHsgfG9wdGlvbnwKICAgICAgICBn
# aXZlbl9vcHRpb24gPSBuaWwKICAgICAgICAKICAgICAgICBhcmd2LmVhY2gg
# eyB8YXJnfAogICAgICAgICAgYXJnLnN0cmlwIQogICAgICAgICAgZ2l2ZW4g
# PSBmYWxzZQogICAgICAgICAgCiAgICAgICAgICBjYXNlIG9wdGlvbls6dHlw
# ZV0udG9fcwogICAgICAgICAgICB3aGVuICJzdHJpbmciCiAgICAgICAgICAg
# ICAgaWYgYXJnID1+IC8tLSN7b3B0aW9uWzpuYW1lXX09KFtcV1x3XSopLwog
# ICAgICAgICAgICAgICAgZ2l2ZW4gPSB0cnVlCiAgICAgICAgICAgICAgICBn
# aXZlbl9vcHRpb24gPSAkMQogICAgICAgICAgICAgIGVuZAogICAgICAgICAg
# ICB3aGVuICJwYXRoIgogICAgICAgICAgICAgIGlmIGFyZyA9fiAvLS0je29w
# dGlvbls6bmFtZV19PShbXFdcd10qKS8KICAgICAgICAgICAgICAgIGdpdmVu
# ID0gdHJ1ZQogICAgICAgICAgICAgICAgZ2l2ZW5fb3B0aW9uID0gRmlsZS5l
# eHBhbmRfcGF0aCgkMSkKICAgICAgICAgICAgICBlbmQKICAgICAgICAgICAg
# d2hlbiAiYm9vbCIKICAgICAgICAgICAgICBpZiBhcmcgPX4gLy0tdXNlLSN7
# b3B0aW9uWzpuYW1lXX09KFx3KykvCiAgICAgICAgICAgICAgICBnaXZlbiA9
# IHRydWUKICAgICAgICAgICAgICAgIGdpdmVuX29wdGlvbiA9ICQxCiAgICAg
# ICAgICAgICAgZW5kCiAgICAgICAgICAgIGVsc2UKICAgICAgICAgICAgICBp
# ZiBhcmcgPX4gLy0tI3tvcHRpb25bOm5hbWVdfS8KICAgICAgICAgICAgICAg
# IGdpdmVuX29wdGlvbiA9ICJ5ZXMiCiAgICAgICAgICAgICAgICBnaXZlbiA9
# IHRydWUKICAgICAgICAgICAgICBlbmQKICAgICAgICAgIGVuZAogICAgICAg
# ICAgCiAgICAgICAgICBpZiBnaXZlbgogICAgICAgICAgICBhcmd2LmRlbGV0
# ZShhcmcpCiAgICAgICAgICBlbmQKICAgICAgICB9CiAgICAgICAgCiAgICAg
# ICAgb3B0aW9uWzp2YWx1ZV0gPSBnaXZlbl9vcHRpb24KICAgICAgfQogICAg
# ICAKICAgICAgZnJvbnRlbmRfa2V5ID0gInRrIgogICAgICBhcmd2LmVhY2gg
# eyB8dW5rbm93bnwKICAgICAgICB1bmtub3duLnN0cmlwIQogICAgICAgIGlm
# IHVua25vd24gPX4gLy0td2l0aC1jb25maWctZnJvbnRlbmQ9KFx3KykvCiAg
# ICAgICAgICBmcm9udGVuZF9rZXkgPSAkMQogICAgICAgIGVsc2lmIHVua25v
# d24gPX4gLy0taGVscC8KICAgICAgICAgIHVzYWdlCiAgICAgICAgICBleGl0
# KDApCiAgICAgICAgZWxzaWYgdW5rbm93biA9fiAvLS12ZXJzaW9uLwogICAg
# ICAgICAgcHV0cyAiUU9uZiAje1ZFUlNJT059IFsje1ZFUlNJT05fREFURX1d
# IgogICAgICAgICAgZXhpdCgwKQogICAgICAgIGVsc2lmIHVua25vd24gPX4g
# Ly0tZGVidWcvCiAgICAgICAgICBAY29uZmlnLnVzZV9kZWJ1ZwogICAgICAg
# IGVuZAogICAgICB9CiAgICAgIAogICAgICBmcm9udGVuZCA9IEZyb250ZW5k
# RmFjdG9yeS5jcmVhdGUoZnJvbnRlbmRfa2V5KQogICAgICBpZiBmcm9udGVu
# ZC5uaWw/CiAgICAgICAgcHV0cyAiPT4gSW52YWxpZCBmcm9udGVuZCIKICAg
# ICAgICB1c2FnZSgpCiAgICAgICAgZXhpdCgtMSkKICAgICAgZWxzaWYgbm90
# IGZyb250ZW5kLnBhcnNlKEBvcHRpb25zKQogICAgICAgIHVzYWdlKCkKICAg
# ICAgICBleGl0KC0xKQogICAgICBlbmQKICAgICAgCiAgICAgIEBvcHRpb25z
# LmVhY2ggeyB8b3B0aW9ufAogICAgICAgIHZhbHVlID0gb3B0aW9uWzp2YWx1
# ZV0ubmlsPyA/IG9wdGlvbls6ZGVmYXVsdF0udG9fcyA6IG9wdGlvbls6dmFs
# dWVdCiAgICAgICAgQGFyZ3VtZW50c1tvcHRpb25bOm5hbWVdXSA9IHZhbHVl
# CiAgICAgIH0KICAgICAgCiAgICBlbmQKICAgIAogICAgZGVmIHNhdmUocGF0
# aCkKICAgICAgYmVnaW4KICAgICAgICBzZXR1cF9jb25maWcoQGNvbmZpZywg
# QGFyZ3VtZW50cykKICAgICAgcmVzY3VlIE5vTWV0aG9kRXJyb3IgPT4gZQog
# ICAgICAgIGlmIG5vdCBlLm1lc3NhZ2UgPX4gL3NldHVwX2NvbmZpZy8KICAg
# ICAgICAgIHB1dHMgIkVycm9yOiAje2V9IgogICAgICAgICAgcHV0cyBlLmJh
# Y2t0cmFjZQogICAgICAgICAgZXhpdCgtMSkKICAgICAgICBlbmQKICAgICAg
# ZW5kCiAgICAgIAogICAgICBAY29uZmlnLmFkZF9kZWZpbmUoJUBfX1BSRUZJ
# WF9fPSdcXFxcIicje0ZpbGUuZXhwYW5kX3BhdGggQGFyZ3VtZW50c1sicHJl
# Zml4Il19J1xcXFwiJ0ApCiAgICAgIAogICAgICBzYXZlX3BrZ2NvbmZpZwog
# ICAgICBAY29uZmlnLnNhdmUocGF0aCkKICAgICAgCiAgICAgIGxhdW5jaGVy
# ID0gbmlsCiAgICAgIGlmIERldGVjdE9TLm9zPyA9PSAxICMgV2luZG93cwog
# ICAgICAgIGxhdW5jaGVyID0gRmlsZS5uZXcoImxhdW5jaGVyLmJhdCIsICJ3
# IikKICAgICAgICAKICAgICAgICBsYXVuY2hlciA8PCAic2V0IFBBVEg9I3tA
# Y29uZmlnLmJ1aWxkX2Rpci5nc3ViKCcvJywgJ1xcJyl9XFxiaW47I3tAY29u
# ZmlnLmJ1aWxkX2Rpci5nc3ViKCcvJywgJ1xcJyl9XFxsaWI7I3tAY29uZmln
# LmxpYnJhcnlfcGF0aC5qb2luKCI7IikuZ3N1YignLycsICdcXCcpfTslUEFU
# SCVcbiIKICAgICAgICBsYXVuY2hlciA8PCAiJTFcbiIKICAgICAgZWxzZQog
# ICAgICAgIGxhdW5jaGVyID0gRmlsZS5uZXcoImxhdW5jaGVyLnNoIiwidyIp
# CiAgICAgICAgbGF1bmNoZXIgPDwgIiMhL2Jpbi9iYXNoXG4iCiAgICAgICAg
# bGF1bmNoZXIgPDwgImV4cG9ydCBQQVRIPSN7QGNvbmZpZy5idWlsZF9kaXJ9
# L2Jpbjoke1BBVEh9XG4iCiAgICAgICAgbGF1bmNoZXIgPDwgImV4cG9ydCBM
# RF9MSUJSQVJZX1BBVEg9I3tAY29uZmlnLmJ1aWxkX2Rpcn0vYmluOiN7QGNv
# bmZpZy5idWlsZF9kaXJ9L2xpYjoje0Bjb25maWcubGlicmFyeV9wYXRoLmpv
# aW4oIjoiKX06JHtMRF9MSUJSQVJZX1BBVEh9OiR7RFlfTERfTElCUkFSWV9Q
# QVRIfVxuIgogICAgICAgIGxhdW5jaGVyIDw8ICJleHBvcnQgRFlMRF9MSUJS
# QVJZX1BBVEg9JHtMRF9MSUJSQVJZX1BBVEh9XG4iCiAgICAgICAgbGF1bmNo
# ZXIgPDwgIkFQUD0kMVxuIgogICAgICAgIGxhdW5jaGVyIDw8ICJzaGlmdFxu
# IgogICAgICAgIGxhdW5jaGVyIDw8ICJlY2hvIExhdW5jaGluZzogJEFQUFxu
# IgogICAgICAgIGxhdW5jaGVyIDw8ICIkQVBQICQqXG4iCiAgICAgIGVuZAog
# ICAgICAKICAgICAgRmlsZS5jaG1vZCgwNzU1LCBsYXVuY2hlci5wYXRoKQog
# ICAgICAKICAgIGVuZAogICAgCiAgICBkZWYgc2F2ZV9wa2djb25maWcKICAg
# ICAgaWYgbm90IEBwa2dfY29uZmlncy5lbXB0eT8KICAgICAgICBAcGtnX2Nv
# bmZpZ3MuZWFjaCB7IHxwa2dfY29uZmlnfAogICAgICAgICAgRmlsZS5vcGVu
# KCBwa2dfY29uZmlnWzpwYWNrYWdlX25hbWVdKyIucGMiLCAidyIpIGRvIHxm
# aWxlfAogICAgICAgICAgICBmaWxlIDw8ICJwcmVmaXg9IiA8PCBAYXJndW1l
# bnRzWyJwcmVmaXgiXSA8PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwgImV4
# ZWNfcHJlZml4PSR7cHJlZml4fSIgPDwgIlxuIgogICAgICAgICAgICBmaWxl
# IDw8ICJsaWJkaXI9I3twa2dfY29uZmlnWzpsaWJkaXJdLm5pbD8gPyAiJHtw
# cmVmaXh9L2xpYiIgOiBwa2dfY29uZmlnWzpsaWJkaXJdIH0iIDw8ICJcbiIK
# ICAgICAgICAgICAgZmlsZSA8PCAiaW5jbHVkZWRpcj0je3BrZ19jb25maWdb
# OmluY2x1ZGVkaXJdLm5pbD8gPyAiJHtwcmVmaXh9L2luY2x1ZGUiIDogcGtn
# X2NvbmZpZ1s6aW5jbHVkZWRpcl0gfSIgPDwgIlxuXG5cbiIKICAgICAgICAg
# ICAgCiAgICAgICAgICAgIGZpbGUgPDwgIk5hbWU6ICIgPDwgcGtnX2NvbmZp
# Z1s6cGFja2FnZV9uYW1lXSA8PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwg
# IkRlc2NyaXB0aW9uOiAiIDw8IHBrZ19jb25maWdbOmRlc2NyaXB0aW9uXSA8
# PCAiXG4iCiAgICAgICAgICAgIGZpbGUgPDwgIlJlcXVpcmVzOiAiIDw8IHBr
# Z19jb25maWdbOnJlcXVpcmVzXS5qb2luKCIgIikgPDwgIlxuIiBpZiBwa2df
# Y29uZmlnWzpyZXF1aXJlc10uY2xhc3MgPT0gQXJyYXkKICAgICAgICAgICAg
# ZmlsZSA8PCAiVmVyc2lvbjogIiA8PCBwa2dfY29uZmlnWzp2ZXJzaW9uXSA8
# PCAiXG4iCiAgICAgICAgICAgIAogICAgICAgICAgICBmaWxlIDw8ICJMaWJz
# OiAiIDw8IHBrZ19jb25maWdbOmxpYnNdIDw8ICJcbiIKICAgICAgICAgICAg
# ZmlsZSA8PCAiQ2ZsYWdzOiAiIDw8IHBrZ19jb25maWdbOmNmbGFnc10gPDwg
# IlxuIgogICAgICAgICAgZW5kCiAgICAgICAgfQogICAgICBlbmQKICAgIGVu
# ZAogICAgCiAgICAKICAgIGRlZiBsb2codHh0KQogICAgICBGaWxlLm9wZW4o
# QGxvZ19maWxlLCAiYSIpIGRvIHxmfAogICAgICAgIGYgPDwgdHh0IDw8ICJc
# biIKICAgICAgZW5kCiAgICBlbmQKICAgIAogICAgZGVmIHVzYWdlCiAgICAg
# IHB1dHMgIk9wdGlvbnM6IgogICAgICBAb3B0aW9ucy5lYWNoIHsgfG9wdGlv
# bnwKICAgICAgICBjYXNlIG9wdGlvbls6dHlwZV0KICAgICAgICAgIHdoZW4g
# InN0cmluZyIKICAgICAgICAgICAgcHJpbnQgIlxuICAtLSN7b3B0aW9uWzpu
# YW1lXX09W3ZhbHVlXSIKICAgICAgICAgICAgcHJpbnQgIlx0XHQuLi4uLi4j
# e29wdGlvbls6ZGVzY3JpcHRpb25dfSAiCiAgICAgICAgICAgIAogICAgICAg
# ICAgICBpZiBub3Qgb3B0aW9uWzpkZWZhdWx0XS5uaWw/CiAgICAgICAgICAg
# ICAgcHJpbnQgIltkZWZhdWx0PSN7b3B0aW9uWzpkZWZhdWx0XX1dIgogICAg
# ICAgICAgICBlbmQKICAgICAgICAgIHdoZW4gInBhdGgiCiAgICAgICAgICAg
# IHByaW50ICJcbiAgLS0je29wdGlvbls6bmFtZV19PVsvYS9wYXRoXSIKICAg
# ICAgICAgICAgcHJpbnQgIlx0XHQuLi4uLi4je29wdGlvbls6ZGVzY3JpcHRp
# b25dfSAiCiAgICAgICAgICAgIAogICAgICAgICAgICBpZiBub3Qgb3B0aW9u
# WzpkZWZhdWx0XS5uaWw/CiAgICAgICAgICAgICAgcHJpbnQgIltkZWZhdWx0
# PSN7b3B0aW9uWzpkZWZhdWx0XX1dIgogICAgICAgICAgICBlbmQKICAgICAg
# ICAgIHdoZW4gImJvb2wiCiAgICAgICAgICAgIHByaW50ICJcbiAgLS11c2Ut
# I3tvcHRpb25bOm5hbWVdfT1beWVzfG5vXSIKICAgICAgICAgICAgaWYgbm90
# IG9wdGlvbls6ZGVmYXVsdF0ubmlsPwogICAgICAgICAgICAgIGRlZmF1bHQg
# PSBvcHRpb25bOmRlZmF1bHRdCiAgICAgICAgICAgICAgaWYgZGVmYXVsdCA9
# PSB0cnVlIG9yIGRlZmF1bHQgPT0gInllcyIgb3IgZGVmYXVsdCA9PSAidHJ1
# ZSIKICAgICAgICAgICAgICAgIGRlZmF1bHQgPSAieWVzIgogICAgICAgICAg
# ICAgIGVsc2lmIGRlZmF1bHQgPT0gZmFsc2Ugb3IgZGVmYXVsdCA9PSAibm8i
# IG9yIGRlZmF1bHQgPT0gImZhbHNlIgogICAgICAgICAgICAgICAgZGVmYXVs
# dCA9ICJubyIKICAgICAgICAgICAgICBlbHNlCiAgICAgICAgICAgICAgICBk
# ZWZhdWx0ID0gIm5vIgogICAgICAgICAgICAgIGVuZAogICAgICAgICAgICAg
# IAogICAgICAgICAgICAgIHByaW50ICJcdFx0Li4uLi4uI3tvcHRpb25bOmRl
# c2NyaXB0aW9uXX0gW2RlZmF1bHQ9I3tkZWZhdWx0fV0iCiAgICAgICAgICAg
# IGVuZAogICAgICAgICAgZWxzZQogICAgICAgICAgICBwcmludCAiXG4gIC0t
# I3tvcHRpb25bOm5hbWVdfSIKICAgICAgICAgICAgcHJpbnQgIlx0XHQuLi4u
# Li4je29wdGlvbls6ZGVzY3JpcHRpb25dfSIKICAgICAgICBlbmQKICAgICAg
# fQogICAgICAKICAgICAgcHJpbnQgIlxuICAtLXdpdGgtY29uZmlnLWZyb250
# ZW5kPVtwbGFpbnx0a10iCiAgICAgIHByaW50ICJcdFx0Li4uLi4uU2V0cyB0
# aGUgZnJvbnRlbmQgdG8gY29uZmlndXJlIHRoZSBhcHBsaWNhdGlvbiBbZGVm
# YXVsdD10a10iCiAgICAgIHByaW50ICJcbiAgLS1kZWJ1ZyIKICAgICAgcHJp
# bnQgIlx0XHQuLi4uLi5Db21waWxlIHdpdGggZGVidWcgc3VwcG9ydCBbZGVm
# YXVsdD1ub10iCiAgICAgIHByaW50ICJcbiAgLS1oZWxwIgogICAgICBwcmlu
# dCAiXG4gIC0tdmVyc2lvbiIKICAgICAgcHJpbnQgIlx0XHQuLi4uLi5TaG93
# cyB0aGlzIG1lc3NhZ2UiCiAgICAgIAogICAgICBwdXRzICIiCiAgICBlbmQK
# ICBlbmQKZW5kCgpiZWdpbgogIGxvYWQgInNldHVwLnJiIgpyZXNjdWUgTG9h
# ZEVycm9yCiAgRmlsZS5vcGVuKG9sZGxvY2F0aW9uKCJzZXR1cC5yYiIpLCAi
# dyIpIGRvIHxmaWxlfAogICAgZmlsZSA8PCBGaWxlLnJlYWQoInNldHVwX3Ji
# LnRwbCIpIDw8ICJcbiIKICBlbmQKICBwdXRzICI9PiBBIHRlbXBsYXRlIGZp
# bGUgd2FzIGdlbmVyYXRlZCBpbiBzZXR1cC5yYiwgcGxlYXNlIGVkaXQgaXQg
# YW5kIHJlcnVuIHRoZSBjb25maWd1cmUgc2NyaXB0LiIKICBleGl0KC0xKQpl
# bmQKCmlmIF9fRklMRV9fID09ICQwCiAgcW9uZiA9IFFvbmY6OkNvbmZpZ3Vy
# ZS5uZXcKICBxb25mLmdldF9hcmdzKEFSR1YpCiAgcW9uZi5maW5kX3BhY2th
# Z2VzCiAgcW9uZi5ydW5fdGVzdHMKICAKICBxb25mLnNhdmUoImNvbmZpZy5w
# cmkiKQogIAogIHFvbmYuZ2VuZXJhdGVfbWFrZWZpbGVzCmVuZAoKCgoAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
# AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
