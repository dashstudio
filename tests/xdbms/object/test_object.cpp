
#include <QtTest/QtTest>
#include <QXmlStreamWriter>

#include <dashserver/xdbms/manager.h>
#include <dashserver/xdbms/table.h>
#include <dashserver/xdbms/object.h>


/**
 * @brief Prueba unitaria de la clase XDBMS::Manager.
*/
class TestObject : public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		
		void addObject();
};

/**
 * Se comprueba que el motor de base de datos se inicie de manera apropiada.
 */
void TestObject::initTestCase()
{
}

void TestObject::cleanupTestCase()
{
}

void TestObject::addObject()
{
	XDBMS::Object obj1("obj1");
	XDBMS::Object obj2("obj2");
	obj2.add("prop1", "val1");
	
	obj1.add(&obj2);
	
	QString xml;
	{
		QXmlStreamWriter writer(&xml);
		obj1.save(&writer);
	}
	
	XDBMS::Object obj3;
	obj3.load(xml);
	
	QString xml2;
	{
		QXmlStreamWriter writer(&xml2);
		obj3.save(&writer);
	}
	
	QCOMPARE(xml, xml2);
}


QTEST_MAIN(TestObject);
#include "test_object.moc"
