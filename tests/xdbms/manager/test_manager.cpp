
#include <QtTest/QtTest>

#include <dashserver/xdbms/manager.h>
#include <dashserver/xdbms/table.h>
#include <dashserver/xdbms/object.h>
/**
 * @brief Prueba unitaria de la clase XDBMS::Manager.
*/
class TestManager: public QObject
{
	Q_OBJECT
	private slots:
		void initTestCase();
		void cleanupTestCase();
		void addObject();
		void updateObject();
		void removeObject();
		
};

/**
 * Se comprueba que el motor de base de datos se inicie de manera apropiada.
 */
void TestManager::initTestCase()
{
	XDBMS::Manager::self()->initialize("database_test");
	
	QDir dir("database_test");
	
	QVERIFY(dir.exists());
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("table_test");
	
	if(!table)
	{
		QVERIFY2(false, "not create a table");
	}
}

void TestManager::cleanupTestCase()
{
	QDir dir;
	dir.remove("database_test/table_test.xdt");
	dir.rmdir("database_test");
	
}
/**
 * Se prueba la función addObject, creando un objeto de prueba añadiéndolo y cargandolo de nuevo para verificar que los datos que se han guardado son correctos.
 */
void TestManager::addObject()
{
	XDBMS::Object object;
	object.setId("object_id");
	object.add("attr1", "1");
	object.add("attr2", "2");
	object.add("attr3", "3");
	
	QStringList list = QStringList() << "a" << "b" << "c";
	
	object.addList("letters", list);
	
	XDBMS::Manager::self()->addObject("table_test", &object);
	
	XDBMS::Object *loadObject = XDBMS::Manager::self()->object("table_test", "object_id");
	
	QVERIFY(loadObject->value("attr1") == "1");
	QVERIFY(loadObject->value("attr2") == "2");
	QVERIFY(loadObject->value("attr3") == "3");
	
	QVERIFY(loadObject->list("letters") == list);
}

/**
 * Se prueba la función updateObject, cargando el objeto de prueba, haciendo un cambio en uno de sus atributos y actualizando la base de datos,
 * Despues se comprueba que los datos se han guardado correctamente con el mismo procedimiento que se hace en la prueba para addObject().
 */

void TestManager::updateObject()
{
	XDBMS::Object *object = XDBMS::Manager::self()->object("table_test", "object_id");
	object->add("attr1", "4");
	
	XDBMS::Manager::self()->update("table_test", object);
	
	XDBMS::Object *loadObject = XDBMS::Manager::self()->object("table_test", "object_id");
	
	QVERIFY(loadObject->value("attr1") == "4");
}

/**
 * Prueba la función removeObject, eliminando el objeto de prueba y comprabando que no exista en la tabla de prueba.
 */
void TestManager::removeObject()
{
	XDBMS::Object *object = XDBMS::Manager::self()->object("table_test", "object_id");
	
	XDBMS::Manager::self()->remove("table_test", object);
	
	QVERIFY(!XDBMS::Manager::self()->table("table_test")->contains("table_test"));
}



QTEST_MAIN(TestManager);
#include "test_manager.moc"
