/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "brushsynchronizer.h"

#include "mixer.h"
#include "palettes.h"

// namespace Dash {
namespace Component {

/**
 * @~spanish
 * Contructor
 */
BrushSynchronizer::BrushSynchronizer(QObject *parent)
 : QObject(parent)
{
	m_mixer = new Mixer();
	connect(m_mixer, SIGNAL(brushChanged(const QBrush &)), this, SLOT(onBrushChanged(const QBrush &)));
	
	m_palettes = new Component::Palettes();
	m_palettes->setWindowTitle(tr("Color palette") );
	connect(m_palettes, SIGNAL(brushSelected(const QBrush &)), this, SLOT(onBrushSelected(const QBrush &)));
}

/**
 * @~spanish
 * Destructor
 */
BrushSynchronizer::~BrushSynchronizer()
{
}

/**
 * @~spanish
 * Retorna el mezclador de brochas.
 */
Mixer *BrushSynchronizer::mixer() const
{
	return m_mixer;
}

/**
 * @~spanish
 * Retorna la paleta de brochas.
 */
Component::Palettes *BrushSynchronizer::palettes() const
{
	return m_palettes;
}

/**
 * @~spanish
 * Cambia la brocha de los dos componentes.
 */
void BrushSynchronizer::setBrush(const QBrush& brush)
{
	m_palettes->setBrush(brush);
	m_mixer->setBrush(brush);
}

/**
 * @internal
 * @~spanish
 * Notifica del cambio de brocha en el mezclador.
 */
void BrushSynchronizer::onBrushChanged( const QBrush & brush )
{
	m_palettes->setBrush(brush);
	emit brushChanged(brush);
}

/**
 * @internal
 * @~spanish
 * Notifica de la selección de brocha en la paleta.
 */
void BrushSynchronizer::onBrushSelected( const QBrush & brush )
{
	m_mixer->setBrush(brush);
	emit brushChanged(brush);
}

}
// }
