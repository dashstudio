/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "colorcreator.h"

/**
 * @~spanish
 * Constructor
 */
ColorCreator::ColorCreator(QWidget *parent)
 : QWidget(parent)
{
	builder.setupUi(this);
	
	connect(builder.color, SIGNAL(newCol(int, int)), this, SLOT( changeHS(int, int)));
	
	connect(builder.luminance, SIGNAL(newHsv(int, int, int )), this, SLOT(changeHSV(int, int, int) ));
	
	connect(builder.r, SIGNAL(valueChanged(int)), this, SLOT(syncColorRGB()));
	connect(builder.g, SIGNAL(valueChanged(int)), this, SLOT(syncColorRGB()));
	connect(builder.b, SIGNAL(valueChanged(int)), this, SLOT(syncColorRGB()));
	
	connect(builder.h, SIGNAL(valueChanged(int)), this, SLOT(syncColorHSV()));
	connect(builder.s, SIGNAL(valueChanged(int)), this, SLOT(syncColorHSV()));
	connect(builder.v, SIGNAL(valueChanged(int)), this, SLOT(syncColorHSV()));
	
	builder.percent->setChecked(builder.a->showAsPercent());
	
	for(int i = 0; i <= 100; i += 25)
	{
		builder.a->addPercent(i);
	}
	
	connect(builder.percent, SIGNAL(toggled( bool )), builder.a, SLOT(setShowAsPercent( bool)));
	
	connect(builder.percent, SIGNAL(stateChanged( int )), this, SLOT(onStateChanged( int)));
	
	connect(builder.a, SIGNAL(editingFinished()), this, SLOT(syncColorRGB()));
	connect(builder.a, SIGNAL(activated(int)), this, SLOT(syncColorRGB()));
	
	connect(builder.name, SIGNAL(editingFinished()), this, SLOT(onNameChanged()));
	
	setColor(Qt::black);
}

/**
 * @~spanish
 * Destructor
 */
ColorCreator::~ColorCreator()
{
}

/**
 * @~spanish
 * Cambia el color mostrado.
 */
void ColorCreator::setColor(const QColor& color )
{
	builder.color->blockSignals(true);
	builder.luminance->blockSignals(true);
	builder.r->blockSignals(true);
	builder.g->blockSignals(true);
	builder.b->blockSignals(true);
	builder.h->blockSignals(true);
	builder.s->blockSignals(true);
	builder.v->blockSignals(true);
	builder.a->blockSignals(true);
	builder.name->blockSignals(true);
	
	builder.color->setCol(color.hue(), color.saturation());
	builder.luminance->setCol(color.hue(), color.saturation(), color.value());
	builder.r->setValue(color.red());
	builder.g->setValue(color.green());
	builder.b->setValue(color.blue());
	builder.h->setValue(color.hue());
	builder.s->setValue(color.saturation());
	builder.v->setValue(color.value());
	builder.a->setValue( builder.a->currentIndex(), color.alpha() );
	builder.name->setText(color.name());
	
	builder.color->blockSignals(false);
	builder.luminance->blockSignals(false);
	builder.r->blockSignals(false);
	builder.g->blockSignals(false);
	builder.b->blockSignals(false);
	builder.h->blockSignals(false);
	builder.s->blockSignals(false);
	builder.v->blockSignals(false);
	builder.a->blockSignals(false);
	builder.name->blockSignals(false);
}

/**
 * @~spanish
 * Obtiene el color mostrado actualmente.
 */
QColor ColorCreator::color() const
{
	return QColor(builder.r->value(), builder.g->value(), builder.b->value(), static_cast<int>(builder.a->value()));
}

/**
 * @internal
 * @~spanish
 * Función que actualiza el color cuando el usuario modifica el valor de iluminación.
 */
void ColorCreator::changeHS(int h, int s)
{
	changeHSV(h, s, builder.luminance->value());
}

/**
 * @internal
 * @~spanish
 * Función que actualiza el color cuando el usuario modifica los valores de HSV.
 */
void ColorCreator::changeHSV(int h, int s, int v)
{
	QColor newColor = QColor::fromHsv(h,s,v);
	newColor.setAlpha(static_cast<int>(builder.a->value()));
	setColor(newColor);
	
	emit colorChanged(newColor);
}

/**
 * @internal
 * @~spanish
 * Función que actualiza el color cuando el usuario modifica los valores de RGB.
 */
void ColorCreator::syncColorRGB()
{
	QColor newColor(static_cast<int>(builder.r->value()), static_cast<int>(builder.g->value()), static_cast<int>(builder.b->value()), static_cast<int>(builder.a->value()));
	setColor(newColor);
	emit colorChanged(newColor);
}

/**
 * @internal
 * @~spanish
 * Función que actualiza el color cuando el usuario modifica los valores de HSV.
 */
void ColorCreator::syncColorHSV()
{
	changeHSV(builder.h->value(), builder.s->value(), builder.v->value());
}

/**
 * @internal
 * @~spanish
 */
void ColorCreator::onStateChanged(int state)
{
	if(state == Qt::Checked)
	{
		builder.a->setShowAsPercent(true);
	}
	else
	{
		builder.a->setShowAsPercent(false);
	}
}

/**
 * @internal
 * @~spanish
 * Función que actualiza el color cuando el usuario modifica el valor del nombre.
 */
void ColorCreator::onNameChanged()
{
	QColor color(builder.name->text());
	if(color.isValid())
	{
		setColor(color);
		emit colorChanged(color);
	}
}
