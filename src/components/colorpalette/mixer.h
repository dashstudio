/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef MIXER_H
#define MIXER_H

#include <components/base/widget.h>

/**
 * Clase que agrupa el mezclador de colores, con el creador de gradientes.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class Mixer : public QWidget
{
	Q_OBJECT;
	public:
		Mixer(QWidget *parent = 0);
		~Mixer();
		
		void setBrush( const QBrush & brush);
		QBrush brush() const;
		
	private slots:
		void onColorChanged( const QColor & color );
		void onGradientChanged(const QBrush &);
		
	signals:
		void brushChanged( const QBrush & brush );
		
	private:
		struct Private;
		Private *const d;
		
};

#endif
