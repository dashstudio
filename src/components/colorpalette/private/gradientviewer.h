/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef GRADIENTVIEWER_H
#define GRADIENTVIEWER_H

#include <QFrame>
#include <QGradient>
#include <QList>

/**
 * @if english
 * @short translate me
 * @elseif spanish
 * @short Esta clase provee de una visualizardor de gradientes, ademas de permite modificar:
 * @n-Si es Lineal: el punto inical y punto final 
 * @n-Si es Radial: el centro, punto focal 
 * @n-Si es Conico: el centro,
 * @endif
 * @author Jorge Cuadrado <kuadrosx@gmail.com>
*/
class GradientViewer : public QFrame
{
	Q_OBJECT
	public:
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Constructor por defecto.
		 * @endif
		 */
		GradientViewer(QWidget *parent = 0);
		/**
		 * Destructor
		 */
		~GradientViewer();
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Devuelve el gradiente actualmente visualizado, normalizado a un cuadro de 100x100, por ejemplo: si se quiere ubicar en un cuadro de 200x300 se debe de escalar el gradiente a sx=200/100 y sy=300/100.
		 * @endif
		 */
		QGradient gradient();
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Crea el gradiente a visualizar con la informacion que tenga de este.
		 * @endif
		 */
		void createGradient();
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * pone el "spread" del gradiente a vidualizar
		 * @endif
		 * @see QGradient
		 */
		void setSpread(int spread);

		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Cambia el gradiente a visualizar.
		 * @endif
		 * @see QGradient
		 */
		void setGradient(const QGradient* gradient);
		
	protected:
		virtual void paintEvent( QPaintEvent * );
		virtual void mousePressEvent(QMouseEvent *e);
		virtual void mouseMoveEvent( QMouseEvent * e );
		
		virtual QSize sizeHint() const;
		virtual void resizeEvent ( QResizeEvent * event );
		
	private:
		QPointF normalizePoint(const QPointF &);
		
		
	public slots:
		/**
		* @if english
		* Translate
		* @elseif spanish
		* Cambia los "GradientStops" del actual gradiente.
		* @endif
		* @see QGradient
		 */
		void changeGradientStops( const QGradientStops& );
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Cambia el tipo del acutal gradiente.
		 * @endif
		 * @see QGradient
		 */
		void changeType(int  type);
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Cambia el radio del acutal gradiente, si es radial.
		 * @endif
		 * @see QGradient
		 */
		void changeRadius(int radius);
		
	signals:
		void gradientChanged();
		
		
	private:
		struct Private;
		Private *const d;
};

#endif
