/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef GRADIENTSELECTOR_H
#define GRADIENTSELECTOR_H

#include <QAbstractSlider>
#include <QColor>
#include <QImage>
#include <QPixmap>
#include <QPainter>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QPaintEvent>
#include <QPoint>
#include <QLinearGradient>
#include <QPainterPath>

#include <QList>

/**
 * @if english
 * @short translate me
 * @elseif spanish
 * @short Esta clase provee de una simple interfaz grafica para generar "GradientStops".
 * @endif
 * @author Jorge Cuadrado <kuadrosx@gmail.com>
 */
class GradientSelector : public QAbstractSlider
{
	Q_OBJECT;
		
	public:

		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Constructor por defecto.
		 * @endif
		 */
		GradientSelector( QWidget *parent=0 );
		
		/**
		 * Destructor
		 */
		~GradientSelector();
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Pone un GradienStops, para ser editado.
		 * @endif
		 */
		void setStops(const QGradientStops &);
		QRect contentsRect() const;
		void setValue(int value);
		int value() const;

		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Pone el numero maximo de flechas, que definen el "GradienStops".
		 * @endif
		 */
		void setMaxArrows(int value);
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Devuelve el "GradienStops" actual.
		 * @endif
		 */
		QGradientStops  gradientStops() const;
		
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Crea el "GradienStops" actual apartir de las flechas acutales.
		 * @endif
		 */
		void  createGradient();
		/**
		 * @if english
		 * Translate
		 * @elseif spanish
		 * Cambia el color de la flecha actualmente seleccionada.
		 * @endif
		 */
		void setCurrentColor(const QColor& color);
		
	signals:
		void newValue( int value );
		void gradientChanged(  const QGradientStops& );
		void arrowAdded();
		
	public slots:
		void addArrow(QPoint position, QColor color);
		virtual void valueChange( int value);
		
	protected:
		virtual void drawContents( QPainter * );
		virtual void paintEvent( QPaintEvent * );
		virtual void mousePressEvent( QMouseEvent *e );
		virtual void mouseMoveEvent( QMouseEvent *e );
		virtual void wheelEvent( QWheelEvent * );

		virtual QSize minimumSize() const;
		
		virtual void resizeEvent ( QResizeEvent * event );
		virtual QSize sizeHint() const;
		
		
	private:
		QPoint calcArrowPos( int val );
		void moveArrow( const QPoint &pos );
		double valueToGradient(int _value) const;
		
	private:
		struct Private;
		Private *d;
};

#endif
