/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "gradientselector.h"

#include <QPolygon>
#include <qdrawutil.h>

#include <dcore/debug.h>

#include <QMatrix>


class GradientArrow
{
	public:
		GradientArrow(QPoint pos, const QColor& color)
		{
			m_color = color;
			QPolygon array(6);
			array.setPoint( 0, pos.x()+0, pos.y()+0 );
			array.setPoint( 1, pos.x()+5, pos.y()+5 );
			array.setPoint( 2, pos.x()+5, pos.y()+9 );
			array.setPoint( 3, pos.x()-5, pos.y()+9 );
			array.setPoint( 4, pos.x()-5, pos.y()+5 );
			array.setPoint( 5, pos.x()+0, pos.y()+0 );
			m_form.addPolygon(array);
		}
		~GradientArrow(){}
		
		double position()
		{
			return m_form.currentPosition().x();
		}
		
		bool contains ( const QPoint & pt )
		{
			return m_form.contains (pt);
		}
		
		void moveArrow( const QPoint &pos )
		{
			QMatrix matrix;
			matrix.translate(pos.x() - m_form.currentPosition().x(), 0);
			m_form = matrix.map(m_form);
		}
		
		QPainterPath form()
		{
			return m_form;
		}
		
		QColor color() const
		{
			return m_color;
		}
		
		void setColor(const QColor &color)
		{
			m_color = color;
		}
		
		void moveVertical(const QPoint &pos)
		{
			QMatrix matrix;
			matrix.translate(0, pos.y() - m_form.currentPosition().y());
			m_form = matrix.map(m_form);
		}
		
		QPainterPath m_form;
		QColor m_color;
};


struct GradientSelector::Private
{
	Qt::Orientation _orientation;
	
	int currentArrowIndex;
	QLinearGradient gradient;
	QList<GradientArrow*> arrows;
	bool update;
	int maxArrows;
	QColor currentColor;
};

GradientSelector::GradientSelector( QWidget *parent ) : QAbstractSlider( parent ), d(new Private)
{
	d->currentArrowIndex = 0;
	d->gradient = QLinearGradient(0,0,1,1);
	d->update = true;
	d->maxArrows = 10;
	d->currentColor = Qt::black;
	
	
	setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
	
/*	setMaximumWidth(100);
	setMinimumWidth(100);*/
	
	setStops(d->gradient.stops());
	
	createGradient();
	emit gradientChanged(  d->gradient.stops());
}

GradientSelector::~GradientSelector()
{
	delete d;
}

QRect GradientSelector::contentsRect() const
{
	if ( orientation() == Qt::Vertical )
		return QRect( 2, 2, width()-14, height());
	else
		return QRect( 2, 2, width(), height()-14 );
}

void GradientSelector::setValue(int value)
{
	QAbstractSlider::setValue(value);
}

int GradientSelector::value() const
{
	return QAbstractSlider::value();
}

void  GradientSelector::setMaxArrows(int value)
{
	d->maxArrows = value;
	while(d->maxArrows < d->arrows.count())
	{
		d->arrows.removeLast();
	}
	update();
}

QGradientStops GradientSelector::gradientStops() const
{
	return d->gradient.stops();
}

void GradientSelector::paintEvent( QPaintEvent * )
{
	QPainter painter;
	painter.begin( this );

	drawContents( &painter );

	QBrush brush;
	
	for(int i = 0; i < d->arrows.count(); i++)
	{
		painter.setBrush(d->arrows[i]->color());
		if(i == d->currentArrowIndex)
		{
			painter.setPen(QPen(palette().highlight(), 3));
		}
		else
		{
			painter.setPen(Qt::black);
		}
		painter.drawPath(d->arrows[i]->form());
	}
	
	
	
	painter.end();
}

void GradientSelector::mousePressEvent( QMouseEvent *e )
{
	
	bool select = false;
	for(int i = 0; i < d->arrows.count(); i++)
	{
		GradientArrow *aArrow = d->arrows[i];
		if ( aArrow->contains(e->pos() ) )
		{
			d->currentArrowIndex = i;
			select = true;
			break;
		}
	}
	if(d->arrows.count() > 2 && e->button() == Qt::RightButton )
	{
		d->arrows.removeAt(d->currentArrowIndex);
		d->currentArrowIndex = 0;
		repaint();
	}
	else if(!select)
	{
		int val;
		if ( orientation() == Qt::Vertical )
		{
			val = ( maximum() - minimum() ) * (height()-e->y())
				/ (height()) + minimum();
		}
		else
		{
			val = ( maximum() - minimum() ) * (width()-e->x())/width() + minimum();
		}
		if(!d->arrows.isEmpty())
		{
			addArrow( calcArrowPos(val), d->arrows[d->currentArrowIndex]->color());
		}
		else
		{
			addArrow( calcArrowPos(val), d->currentColor);
		}
	}
}

void GradientSelector::mouseMoveEvent( QMouseEvent *e )
{
	moveArrow( e->pos() );
	
}

void GradientSelector::wheelEvent( QWheelEvent *e )
{
	int val = value() + e->delta()/120;
	setValue( val );
}

QSize GradientSelector::minimumSize() const
{
	return sizeHint(); 
}

QSize GradientSelector::sizeHint() const
{
	return QSize(width(), 35);
}

void GradientSelector::resizeEvent ( QResizeEvent * event )
{
	QAbstractSlider::setRange(0,width() );
	QAbstractSlider::setMaximum(width());
	d->update = true;
	for(int i =0; i < d->arrows.count(); i++)
	{
		d->arrows[i]->moveVertical(  calcArrowPos( (int)d->arrows[i]->position() ));
	}

	QWidget::resizeEvent (event);
	
}

void GradientSelector::valueChange( int newV)
{
	update();
	
	emit newValue( newV );
}

void GradientSelector::moveArrow( const QPoint &pos )
{
	if ( orientation() == Qt::Vertical && (pos.y() <= minimum() || pos.y() >= maximum()) )
	{
		return;
	}
	if(  orientation() == Qt::Horizontal &&  (pos.x() <= minimum() || pos.x() >= maximum()) )
	{
		return;
	}
	int val;

	if ( orientation() == Qt::Vertical )
	{
		val = ( maximum() - minimum() ) * (height()-pos.y())
				/ (height()) + minimum();
	}
	else
	{
		val = ( maximum() - minimum() ) * (width()-pos.x())
				/ (width()) + minimum();
	}

	setValue( val );
	
	d->arrows[d->currentArrowIndex]->moveArrow(pos);
	d->update = true;
	
	emit gradientChanged(d->gradient.stops());
}

QPoint GradientSelector::calcArrowPos( int val )
{
	QPoint p;

	if ( orientation() == Qt::Vertical )
	{
		p.setY( height() - ( (height()-10) * val
				/ ( maximum() - minimum() ) + 5 ) );
		p.setX( width() - 10 );
	}
	else
	{
		p.setX( width() - ( (width()) * val
				/ ( maximum() - minimum() )  ) );
		p.setY( height() - 10 );
	}

	return p;
}

void GradientSelector::drawContents( QPainter *painter )
{
	createGradient();
	painter->setBrush(d->gradient);
	painter->drawRect(contentsRect());
	
	if(!isEnabled () )
	{
		QColor disabled = palette().color(QPalette::Disabled, QPalette::WindowText);
		disabled.setAlpha(120);
		painter->fillRect(contentsRect(), disabled);
	}
}

void  GradientSelector::createGradient()
{
	d->gradient = QLinearGradient(contentsRect().topLeft(), contentsRect().topRight () );
	for(int i = 0; i < d->arrows.count(); i++)
	{
		d->gradient.setColorAt( valueToGradient((int) d->arrows[i]->position()), d->arrows[i]->color());
	}
}

double GradientSelector::valueToGradient(int value) const
{
	float factor = static_cast<float>( ( value ))/ maximum();
	if(factor > 1.0)
	{
		factor = 1.0;
	}
	
	if(factor < 0)
	{
		factor = 0;
	}
	
	
	return factor;
}

void GradientSelector::addArrow(QPoint position, QColor color)
{
	if(d->arrows.count() < d->maxArrows)
	{
		GradientArrow *arrow = new GradientArrow(position, color);
		d->arrows << arrow;
		d->currentArrowIndex = d->arrows.count()-1;
		update();
		
		emit arrowAdded();
	}
	
}

void GradientSelector::setCurrentColor(const QColor& color)
{
	if ( d->arrows.count() > 0 )
	{
		GradientArrow *arrow  = d->arrows[d->currentArrowIndex];
		if ( arrow )
		{
			d->arrows[d->currentArrowIndex]->setColor(color);
		}
	}
	
	createGradient();
	emit gradientChanged(d->gradient.stops());
	repaint();
	d->currentColor = color;
}

void GradientSelector::setStops(const QGradientStops &stops)
{
	d->gradient.setStops(stops);
	d->arrows.clear();
	for(int i = 0; i < stops.count(); i++)
	{
		addArrow( calcArrowPos( (int) (100 - (stops[i].first * 100)) ), stops[i].second );
	}
	update();
}
