/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include <QPoint>
#include <QColor>
#include <QImage>
#include <QSizePolicy>
#include <QPixmap>
#include <QSize>
#include <QRect>
#include <QPainter>
#include <QMouseEvent>

#include "colorpicker.h"
#include <dcore/debug.h>


namespace Component {

// static int pWidth = 220;
// static int pHeight = 200;
static int pWidth = 100;
static int pHeight = 80;
// static int pWidth = 150;
// static int pHeight = 130;

struct ColorPicker::Private
{
	int hue;
	int sat;
	QPixmap *pix;
	~Private()
	{
		delete pix;
	};
};


QPoint ColorPicker::colPt()
{ 
	return QPoint((360-d->hue)*(pWidth-1)/360, (255-d->sat)*(pHeight-1)/255); 
}
int ColorPicker::huePt(const QPoint &pt)
{ 
	return 360 - pt.x()*360/(pWidth-1); 
}

int ColorPicker::satPt(const QPoint &pt)
{ 
	return 255 - pt.y()*255/(pHeight-1) ; 
}

void ColorPicker::setCol(const QPoint &pt)
{ 
	setCol(huePt(pt), satPt(pt)); 
}

ColorPicker::ColorPicker(QWidget* parent) : QFrame(parent), d( new Private)
{
	d->hue = 0;
	d->sat = 0;
	setCol(150, 255);

	QImage img(pWidth, pHeight, QImage::Format_RGB32);
	int x,y;
	for (y = 0; y < pHeight; y++)
	{
		for (x = 0; x < pWidth; x++) 
		{
			QPoint p(x, y);
			QColor c;
			c.setHsv(huePt(p), satPt(p), 200);
			img.setPixel(x, y, c.rgb());
		}
	}
    
	d->pix = new QPixmap(QPixmap::fromImage(img));
	setAttribute(Qt::WA_NoSystemBackground);
	setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) );

}

ColorPicker::~ColorPicker()
{
	delete d;
	D_END;
}

QSize ColorPicker::sizeHint() const
{
	return QSize(pWidth + 2*frameWidth(), pHeight + 2*frameWidth());
}

void ColorPicker::setCol(int h, int s)
{
	int nhue = qMin(qMax(0,h), 359);
	int nsat = qMin(qMax(0,s), 255);
	if (nhue == d->hue && nsat == d->sat)
		return;
	QRect r(colPt(), QSize(20,20));
	d->hue = nhue; d->sat = nsat;
	r = r.unite(QRect(colPt(), QSize(20,20)));
	r.translate(contentsRect().x()-9, contentsRect().y()-9);
    //    update(r);
	repaint(r);
}

void ColorPicker::setH(int h)
{
	int nhue = qMin(qMax(0,h), 359);
	if (nhue == d->hue )
		return;
	QRect r(colPt(), QSize(20,20));
	d->hue = nhue;
	r = r.unite(QRect(colPt(), QSize(20,20)));
	r.translate(contentsRect().x()-9, contentsRect().y()-9);
	repaint(r);
	
}

void ColorPicker::setS(int s)
{
	int nsat = qMin(qMax(0,s), 255);
	if ( nsat == d->sat)
		return;
	QRect r(colPt(), QSize(20,20));
	d->sat = nsat;
	r = r.unite(QRect(colPt(), QSize(20,20)));
	r.translate(contentsRect().x()-9, contentsRect().y()-9);
    //    update(r);
	repaint(r);
}


void ColorPicker::mouseMoveEvent(QMouseEvent *m)
{
	QPoint p = m->pos() - contentsRect().topLeft();
	setCol(p);
	emit newCol(d->hue, d->sat);
}

void ColorPicker::mousePressEvent(QMouseEvent *m)
{
	QPoint p = m->pos() - contentsRect().topLeft();
	setCol(p);
	emit newCol(d->hue, d->sat);
}

void ColorPicker::paintEvent(QPaintEvent* )
{
	QPainter p(this);
	drawFrame(&p);
	QRect r = contentsRect();

	p.drawPixmap(r.topLeft(), *d->pix);
	QPoint pt = colPt() + r.topLeft();
	p.setPen(Qt::black);

	p.fillRect(pt.x()-9, pt.y(), 20, 2, Qt::black);
	p.fillRect(pt.x(), pt.y()-9, 2, 20, Qt::black);
}

int ColorPicker::hue()
{
	return d->hue;
}

int ColorPicker::sat()
{
	return d->sat;
}

}
