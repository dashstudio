/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "cellscolor.h"
#include <dcore/debug.h>
#include "palettedocument.h"

#include <QFile>
#include <QDir>
#include <QDragEnterEvent>
#include <QMouseEvent>
#include <QDrag>
#include <QApplication>
#include <QPainter>

#include "paletteparser.h"

namespace Component {

struct CellsColor::Private
{
	CellsColor::Type type;
	QString name;
	bool readOnly;
	QPoint startDragPosition;
	
	QString fileName;
};

CellsColor::CellsColor(QWidget *parent)
	: DGui::CellView(12, parent), d(new Private)
{
	d->type = Color;
	d->readOnly = false;
	setAcceptDrops(true);
}


CellsColor::~CellsColor()
{
	save();
	delete d;
}


bool CellsColor::load(const QString &file)
{
	PaletteParser parser;
	QFile f(file);
	
	if(parser.parse(&f))
	{
		QList<QBrush> brushes = parser.brushes();
		
		foreach(QBrush brush, brushes)
		{
			addItem(brush);
		}
		
		d->name = parser.paletteName();
		d->readOnly = !parser.isEditable();
		
		d->fileName = file;
		
		return true;
	}
	
	dError() << "Error while parse palette file: " << file;
	return false;
}

void CellsColor::setReadOnly(bool enable)
{
	d->readOnly = enable;
}

bool CellsColor::isReadOnly() const
{
	return d->readOnly;
}

void CellsColor::setType(Type type)
{
	d->type = type;
}

int CellsColor::type() const
{
	return d->type;
}

QString CellsColor::name() const
{
	return d->name;
}

void CellsColor::setName(const QString& name)
{
	d->name = name;
}

void CellsColor::setFilePath(const QString &path)
{
	d->fileName = path;
}

QString CellsColor::filePath() const
{
	return d->fileName;
}

void CellsColor::clear()
{
	DGui::CellView::clear();
	
// 	while(rowCount() > 0)
// 	{
// 		removeRow(0);
// 	}
}

void CellsColor::save()
{
	if( !d->readOnly )
	{
		if( !d->fileName.isEmpty() )
		{
			save(d->fileName);
		}
		else
		{
			QDir brushesDir(CONFIG_DIR+"/palettes");
			
			bool ok = brushesDir.exists();
			if ( ! ok )
			{
				ok = brushesDir.mkdir(brushesDir.path() );
			}
			
			if( ok )
			{
				save(brushesDir.absoluteFilePath(d->name+".dpl"));
			}
		}
	}
}

void CellsColor::save( const QString &path)
{
	QFile save(path);
	Component::PaletteDocument document(d->name, true);
	
	for(int i = 0; i < columnCount() ; i++)
	{
		for (int  j = 0; j < rowCount() ; j++)
		{
			QTableWidgetItem *tmpItem = itemAt(i*25, j*25);
			if(tmpItem)
			{
				if(tmpItem->background().gradient())
				{
					document.addGradient(*tmpItem->background().gradient());
				}
				else if(tmpItem->background().color().isValid())
				{
					document.addColor(tmpItem->background().color());
				}
			}
		}
	}
	if ( save.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream out(&save);
		out << document.toString();
		save.close();
	}
}

void CellsColor::dragEnterEvent( QDragEnterEvent *event )
{
	setFocus();

	if (event->mimeData()->hasColor()) 
	{
		if (event->source() == this) 
		{
			event->setDropAction(Qt::MoveAction);
			event->accept();
		} 
		else
		{
			event->acceptProposedAction();
		}
	} 
	else 
	{
		event->ignore();
	}
}

void CellsColor::dropEvent( QDropEvent *event )
{
	if (event->mimeData()->hasColor())
	{
		QColor color = qvariant_cast<QColor>(event->mimeData()->colorData());
		
		// TODO: crear item in cellscolor.cpp
		
		if (event->source() == this) 
		{
			event->setDropAction(Qt::MoveAction);
			event->accept();
		} 
		else 
		{
			event->acceptProposedAction();
		}
	} 
	else 
	{
		event->ignore();
	}
}

void CellsColor::mousePressEvent(QMouseEvent* e)
{
	DGui::CellView::mousePressEvent(e);
	d->startDragPosition = e->pos();
	
}

void CellsColor::mouseMoveEvent(QMouseEvent* e)
{
	DGui::CellView::mouseMoveEvent(e);
	
	if ((e->pos() - d->startDragPosition).manhattanLength() <  QApplication::startDragDistance() || !currentItem() )
		return;

	QDrag *drag = new QDrag( this );
	QPixmap pix( 25, 25 );
	QColor color=  currentItem()->background().color();
	pix.fill( color);
	
	QPainter painter( &pix );
	painter.drawRect( 0, 0, pix.width(), pix.height() );
	painter.end();
		
	QMimeData *mimeData = new QMimeData;
	mimeData->setColorData(currentItem()->background().color());
		
	drag->setMimeData(mimeData);
	drag->setPixmap( pix );
		
	/*Qt::DropAction dropAction = */drag->start(Qt::MoveAction);

}


}
