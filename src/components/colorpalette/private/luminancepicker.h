/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef COMPONENTSLUMINACEPICKER_H
#define COMPONENTSLUMINACEPICKER_H


#include <QPaintEvent>
#include <QPixmap>
#include <QMouseEvent>
#include <QWidget>

namespace Component {

class LuminancePicker : public QWidget
{
	Q_OBJECT
	public:
		LuminancePicker(QWidget* parent=0);
		~LuminancePicker();
		int value();
		
	public slots:
		void setCol(int h, int s, int v);
		void setCol(int h, int s);
		void setVal(int v);

	signals:
		void newHsv(int h, int s, int v);

	protected:
		void paintEvent(QPaintEvent*);
		void mouseMoveEvent(QMouseEvent *);
		void mousePressEvent(QMouseEvent *);

	private:
		enum { foff = 3, coff = 4 }; //frame and contents offset
		
		struct Private;
		Private *const d;
		
		int y2val(int y);
		int val2y(int val);

};
}

#endif

