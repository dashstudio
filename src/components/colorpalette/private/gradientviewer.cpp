/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gradientviewer.h"
#include <QPainter>
#include <dcore/debug.h>
#include <dgraphics/algorithm.h>

#include <QMouseEvent>
#include <QRectF>

struct GradientViewer::Private
{
	class ControlPoint
	{
		public:
			ControlPoint()
			{
				QRadialGradient gradient(QPoint(5, 5), 10, QPoint(5,5) );
				
				gradient.setColorAt(0.5, Qt::gray);
				gradient.setColorAt(0, Qt::blue);
				
				normalPoint = QBrush(gradient);
				
				gradient.setColorAt(0, Qt::red);
				selectedPoint = QBrush(gradient);
				
				currentIndex = 0;
				points << QPointF(10,50) << QPointF(60,50);
			}
			~ControlPoint() {}
			QVector<QPointF> points;
			int currentIndex;
			
			void selectPoint(const QPointF &point)
			{
				int rate = 10;
				QRectF rect(point - QPointF(rate/2,rate/2) , QSizeF(rate, rate ));
				
				QVector<QPointF>::const_iterator it;
				for (it = points.begin(); it != points.end(); ++it)
				{
					if( rect.contains(*it) )
					{
						currentIndex = points.indexOf(*it);
						break;
					}
				}
			}
			void drawPoints(QPainter *p)
			{
				foreach(QPointF point, points)
				{
					QRectF br( point - QPoint(5,5), QSizeF(10,10)  );
					p->save();
					p->setRenderHint(QPainter::Antialiasing);
					if(point == points[currentIndex])
					{
						selectedPoint.setMatrix(QMatrix().translate(br.x(), br.y()));
						p->setBrush(selectedPoint);
					}
					else
					{
						normalPoint.setMatrix(QMatrix().translate(br.x(), br.y()));
						p->setBrush(normalPoint);
					}
					
					p->drawEllipse(br);
					p->restore();
					
	// 				p->drawPoint(point);
				}
			}
			
			QBrush normalPoint;
			QBrush selectedPoint;
	};
	ControlPoint *controlPoint;
	QGradientStops gradientStops;
	QGradient gradient;
	
	double radius;
	QGradient::Type type;
	QGradient::Spread spread;
};



GradientViewer::GradientViewer(QWidget *parent)
	: QFrame(parent), d(new Private)
{
	d->radius = 50;

	d->controlPoint = new Private::ControlPoint();
	d->type = QGradient::LinearGradient;
	d->spread =  QGradient::PadSpread;
	setMidLineWidth(2);
	setLineWidth(2);
	setFrameStyle( QFrame::StyledPanel | QFrame::Sunken );
	createGradient();
	repaint();
	
// 	setMinimumSize(sizeHint());
}


GradientViewer::~GradientViewer()
{
	delete d->controlPoint;
	delete d;
}

void GradientViewer::paintEvent( QPaintEvent* e)
{
	createGradient();
	QPainter p(this);
	
	p.setPen(Qt::NoPen);
	QBrush brush(d->gradient);
	
	QMatrix m;
	m.scale(100/rect().width(), 100/rect().height());
	brush.setMatrix(m);
	
	p.setBrush(brush);
	
	p.drawRect(rect());
	
	if(isEnabled () )
	{
		d->controlPoint->drawPoints(&p);
	}
	else
	{
		QColor disabled = palette().color(QPalette::Disabled, QPalette::WindowText);
		disabled.setAlpha(120);
		p.fillRect(rect(), disabled);
	}
	
	p.end();
	
	QFrame::paintEvent(e);
}

QSize GradientViewer::sizeHint() const
{
	return QSize(100,100);
}

void GradientViewer::resizeEvent ( QResizeEvent * event )
{
	int s = qMin(event->size().width(), event->size().height());
	resize(QSize(s,s));
}

void GradientViewer::createGradient()
{
	switch(d->type)
	{
		case  QGradient::LinearGradient:
		{
			d->gradient = QLinearGradient(d->controlPoint->points[0], d->controlPoint->points[1]);
			break;
		}
		case QGradient::RadialGradient:
		{
			d->gradient = QRadialGradient(d->controlPoint->points[0], d->radius, d->controlPoint->points[1] );
			break;
		}
		case QGradient::ConicalGradient:
		{
			d->gradient = QConicalGradient(d->controlPoint->points[0], DGraphics::Algorithm::angleForPos( d->controlPoint->points[1], d->controlPoint->points[0 ])*180/M_PI);
			break;
		}
		default:
		{
			dFatal() << "Fatal error, the gradient type doesn't exists!";
		}
	}
	d->gradient.setStops( d->gradientStops);
	d->gradient.setSpread(d->spread);
	
}
void GradientViewer::changeGradientStops( const QGradientStops& stops)
{
	d->gradientStops = stops;
	
	repaint();
}

void GradientViewer::changeType(int type)
{
	d->type = QGradient::Type(type);
	repaint();
}


void GradientViewer::setSpread(int spread)
{
	d->spread = QGradient::Spread(spread);
	repaint();
}



QGradient GradientViewer::gradient()
{
	QGradient gradientNormalized;
	switch(d->gradient.type())
	{
		case  QGradient::LinearGradient:
		{
			gradientNormalized = QLinearGradient(
					normalizePoint(d->controlPoint->points[0]), normalizePoint(d->controlPoint->points[1]));
			break;
		}
		case QGradient::RadialGradient:
		{
			gradientNormalized = QRadialGradient(normalizePoint(d->controlPoint->points[0]), d->radius, normalizePoint(d->controlPoint->points[1]) );
			break;
		}
		case QGradient::ConicalGradient:
		{
			gradientNormalized = QConicalGradient(normalizePoint(d->controlPoint->points[0]), DGraphics::Algorithm::angleForPos(normalizePoint(d->controlPoint->points[1]), normalizePoint(d->controlPoint->points[0]))*180/M_PI);
			break;
		}
		default:
		{
			dFatal() << "Fatal error, the gradient type doesn't exists!";
		}
	}
	gradientNormalized.setStops( d->gradientStops);
	gradientNormalized.setSpread(d->spread);
	return gradientNormalized;
}

void GradientViewer::mousePressEvent(QMouseEvent *e)
{
	d->controlPoint->selectPoint(e->pos());
	
	update();
}

void GradientViewer::mouseMoveEvent( QMouseEvent * e )
{
	d->controlPoint->points[d->controlPoint->currentIndex] = e->pos();
	update();
	emit gradientChanged();
}

void GradientViewer::changeRadius(int radius)
{
	d->radius = radius;
	update();
	emit gradientChanged();
}

void GradientViewer::setGradient(const QGradient* gradient)
{
	d->gradientStops = gradient->stops();
	d->spread = gradient->spread();
	d->type = gradient->type();
	switch(d->type)
	{
		case  QGradient::LinearGradient:
		{
			d->controlPoint->points[0] = static_cast<const QLinearGradient*>(gradient)->start();
			d->controlPoint->points[1] = static_cast<const QLinearGradient*>(gradient)->finalStop();
			break;
		}
		case QGradient::RadialGradient:
		{
			d->controlPoint->points[0] = static_cast<const QRadialGradient*>(gradient)->center();
			d->controlPoint->points[1] = static_cast<const QRadialGradient*>(gradient)->focalPoint();
			d->radius = static_cast<const QRadialGradient*>(gradient)->radius();
			break;
		}
		case QGradient::ConicalGradient:
		{
			d->controlPoint->points[0] = static_cast<const QConicalGradient*>(gradient)->center();
			
			double angle = static_cast<const QConicalGradient*>(gradient)->angle();
			
			QMatrix m;
			m.rotate(-angle);
			QPointF point = m.map(QPointF(10, 0));
			
			d->controlPoint->points[1] = point + d->controlPoint->points[0];
			
			
			break;
		}
		default:
		{
			dFatal() << "Fatal error, the gradient type doesn't exists!";
		}
	}
	repaint();
}

QPointF GradientViewer::normalizePoint(const QPointF & point)
{
	return QPointF(point.x()*(100/width()), point.y()*(100/height()));
}

