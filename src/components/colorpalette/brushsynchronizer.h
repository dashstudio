/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASHBRUSHSYNCHRONIZER_H
#define DASHBRUSHSYNCHRONIZER_H

#include <QObject>
#include <QBrush>

class Mixer;

// namespace Dash {
namespace Component {
class Palettes;

/**
 * @~spanish
 * @brief Esta clase que se encarga de la sincronización de la paleta de brochas con el mezclador.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class BrushSynchronizer : public QObject
{
	Q_OBJECT;
	public:
		BrushSynchronizer(QObject *parent = 0);
		~BrushSynchronizer();
		
		void setBrush(const QBrush& brush);
		
		Mixer *mixer() const;
		Component::Palettes *palettes() const;
		
	private slots:
		void onBrushChanged( const QBrush & brush );
		void onBrushSelected( const QBrush & brush );
		
	signals:
		void brushChanged(const QBrush & brusn);
		
	private:
		Mixer *m_mixer;
		Component::Palettes *m_palettes;
};

}
// }

#endif
