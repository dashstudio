/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef COMPONENTSPALETTES_H
#define COMPONENTSPALETTES_H

#include <QFrame>
#include <QComboBox>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QBrush>

#include "private/cellscolor.h"


namespace Component {

/**
 * @brief Esta clase provee de una interfaz gráfica para la selección de brochas desde un grupo de paletas y gestionar las paletas de la aplicación.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class Palettes : public QFrame
{
	Q_OBJECT
	public:
		Palettes(QWidget *parent = 0);
		~Palettes();
		
		void load(const QString &file);
		void setBrush(const QBrush & b);
		
		void setProjectPalette(const QString &filePath);
		
	public slots:
		void addCustomPalette();
		void removeCurrentPalette();
		
		void addCurrentColor();
		void removeCurrentColor();
		
		// 		void addPalette(const QString & name, const QList<QBrush> & brushes, bool editable );
		
	private:
		void setupForm();
		void readPalettes(const QString &paletteDir);
		void fillDefaultColors();
		void addDefaultColor(int i , int j, const QColor &);
		void fillNamedColor();
		void addPalette(CellsColor *palette);
		void setupButtons();
		
	private slots:
		void changeColor(QTableWidgetItem *item);
		
	signals:
		void brushSelected(const QBrush &);
		
	private:
		struct Private;
		Private *const d;
		
};

}

#endif
