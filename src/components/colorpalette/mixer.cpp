/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "mixer.h"

#include "colorcreator.h"
#include "gradientcreator.h"


//Qt
#include <QVBoxLayout>
#include <QGroupBox>

struct Mixer::Private {
	ColorCreator *mixer;
	Component::GradientCreator *gradientCreator;
	QGroupBox *gradientBox;
};

/**
 * @~spanish
 * Constructor
 */
Mixer::Mixer(QWidget *parent)
 : QWidget(parent), d (new Private)
{
	setWindowTitle("Color Mixer");
	
	QGroupBox *colorBox = new QGroupBox("Color");
	QVBoxLayout *colorBoxLayout = new QVBoxLayout(colorBox);
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	d->mixer = new ColorCreator(colorBox);
	connect(d->mixer, SIGNAL(colorChanged(const QColor &)), this, SLOT(onColorChanged(const QColor &)));
	colorBoxLayout->addWidget(d->mixer);
	
	
	layout->addWidget(colorBox);
	
	d->gradientBox = new QGroupBox("Gradient");
	
	QVBoxLayout *gradientBoxLayout = new QVBoxLayout(d->gradientBox);
	
	d->gradientBox->setCheckable(true);
	d->gradientBox->setChecked(false);
	
	d->gradientCreator = new Component::GradientCreator;
	gradientBoxLayout->addWidget(d->gradientCreator);
	connect(d->gradientCreator, SIGNAL(gradientChanged(const QBrush &)), this, SLOT(onGradientChanged(const QBrush &)));
	
	colorBoxLayout->addWidget(d->mixer);
	
	layout->addWidget(d->gradientBox);
}

/**
 * @~spanish
 * Destructor
 */
Mixer::~Mixer()
{
	delete d;
}

/**
 * @~spanish
 * Cambia la brocha mostrada, si la brocha es un gradiente activa el creador de gradientes y le asigna la brocha y de lo contrario lo desactiva y le asigna la brocha al selector de colores.
 */
void Mixer::setBrush( const QBrush & brush)
{
	const QGradient *gradient = brush.gradient();
	
	if(gradient)
	{
		d->gradientBox->setChecked(true);
		d->gradientCreator->setGradient(brush);
	}
	else if(brush.color().isValid())
	{
		d->gradientBox->setChecked(false);
		d->mixer->setColor(brush.color());
	}
}

/**
 * @~spanish
 * Retorna la brocha mostrada actualmente, si el creador de gradientes esta activo retornara el gradiente que actualemente esta mostrando, de lo contrario retornara el color que muestre el selector de colores.
 */
QBrush Mixer::brush() const
{
	if(d->gradientBox->isChecked())
	{
		return d->gradientCreator->currentGradient();
	}
	
	return QBrush(d->mixer->color());
}

/**
 * @internal
 * @~spanish
 * Sincroniza los widgets cuando el usuario selecciona un color, si el creador de gradientes esta activo cambiara el color del actual "stop", de lo contrario notificara que el cambio de brocha.
 */
void Mixer::onColorChanged( const QColor & color )
{
	if(d->gradientBox->isChecked())
	{
		d->gradientCreator->setCurrentColor(color);
	}
	else
	{
		emit brushChanged(QBrush(color));
	}
}

/**
 * @internal
 * @~spanish
 * Notifica el cambio de brocha cuando el usuario modifica el creador de gradientes.
 */
void Mixer::onGradientChanged(const QBrush &brush)
{
	if(d->gradientBox->isChecked())
	{
		emit brushChanged(brush);
	}
}
