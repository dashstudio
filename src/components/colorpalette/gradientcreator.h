/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GRADIENTMANAGER_H
#define GRADIENTMANAGER_H


#include <QScrollArea>
#include <QGroupBox>
#include <QSpinBox>
#include <QLabel>
#include <QBoxLayout>

namespace Component {

class SpinControl;

 /**
  * 
  * @~english
  * @brief translate me
  * @~spanish
  * @brief Esta clase provee de una simple interfaz grafica para crear gradientes.
  * Consta de un visualizador de gradientes, un creador de "stops", y un selector del tipo y difusión del gradiente.
  * 
  * @author Jorge Cuadrado <kuadrosxx@gmail.com>
  */
class GradientCreator : public QScrollArea
{
	Q_OBJECT
	public:
		GradientCreator(QWidget *parent = 0);
		~GradientCreator();
		void setCurrentColor(const QColor &);
		int gradientType();
		QBrush currentGradient();
		virtual QSize sizeHint () const;
		
	public slots:
		void changeType(int type);
		void changeSpread(int spread);
		void changeGradientStops( const QGradientStops& );
		void setGradient(const QBrush & gradient);
		
	private slots:
		void emitGradientChanged();
		
	signals:
		void gradientChanged(const QBrush &);
		void controlArrowAdded();
		
	private:
		struct Private;
		Private *const d;

};



}

#endif
