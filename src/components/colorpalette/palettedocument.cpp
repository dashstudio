/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "palettedocument.h"

#include <yamf/item/serializer.h>

namespace Component {


/**
* @~spanish
* Construye un documento de paleta con un nombre
*/
PaletteDocument::PaletteDocument(const QString &name, bool isEditable) : QDomDocument()
{
	QDomProcessingInstruction header = this->createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
	this->appendChild(header);
	
	QDomElement root = createElement( "palette" );
	root.setAttribute("name", name);
	
	if ( isEditable )
	{
		root.setAttribute("editable", "true" );
	}
	else
	{
		root.setAttribute("editable", "false" );
	}
	
	appendChild( root );	
}

/**
* @~spanish
* Destructor
*/
PaletteDocument::~PaletteDocument()
{
}

/**
* @~spanish
* Añade un color al documento.
*/
void PaletteDocument::addColor(const QColor &color)
{
	QDomElement element = createElement("color");
	
	element.setAttribute("name", color.name());
	element.setAttribute("alpha", QString::number(color.alpha()) );
	
	documentElement().appendChild(element);
	
}

/**
* @~spanish
* Añade un gradiente al documento.
*/
void PaletteDocument::addGradient(const QGradient &gradient)
{
	documentElement().appendChild(YAMF::Item::Serializer::gradient(&gradient, *this));
}

/**
* @~spanish
* Pone una lista de colores o gradientes en el documento
*/
void PaletteDocument::setElements(const QList<QBrush > &brushes)
{
	foreach( QBrush brush, brushes)
	{
		if(brush.gradient())
		{
			addGradient(*brush.gradient());
		}
		else
		{
			addColor(brush.color());
		}
	}
}

}

