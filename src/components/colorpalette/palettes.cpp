/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "palettes.h"
#include <dcore/debug.h>
#include <dcore/global.h>

#include <QScrollArea>
#include <QGroupBox>
#include <QDir>

#include <dgui/imagebutton.h>
#include <dgui/iconloader.h>

#include <dcore/config.h>

#include <QInputDialog>

namespace Component {

struct Palettes::Private
{
	QComboBox *chooserPalette;
	QStackedWidget *containerPalette;
	
	CellsColor *defaultPalette;
	CellsColor *qtColorPalette;
	CellsColor *projectPalette;
	
	int numColorRecent;
	QBrush currentBrush;
};

/**
 * @~spanish
 * Constructor
 */
Palettes::Palettes(QWidget *parent) : QFrame(parent), d(new Private)
{
	d->numColorRecent = 0;
	QVBoxLayout *layout = new QVBoxLayout;
	layout->setMargin(0);
	layout->setSpacing(0);
	
	setLayout(layout);
	setFrameStyle ( QFrame::Box | QFrame::Raised);
	setupForm();
	setupButtons();
	
	DCONFIG->beginGroup("ColorPalette");
	int lastIndex = DCONFIG->value("last_palette").toInt();
	DCONFIG->endGroup();
	
	if ( lastIndex > 0 )
	{
		d->chooserPalette->setCurrentIndex(lastIndex);
		d->containerPalette->setCurrentIndex(lastIndex);
	}
}

/**
 * @~spanish
 * Destructor
 */
Palettes::~Palettes()
{
	DCONFIG->beginGroup("ColorPalette");
	DCONFIG->setValue("last_palette", d->chooserPalette->currentIndex());
	DCONFIG->endGroup();
	
	d->projectPalette->setReadOnly(true);
	delete d;
}

/**
 * @~spanish
 * Carga una paleta desde el archivo @p file.
 */
void Palettes::load(const QString &file)
{
	CellsColor *palette = new  CellsColor;
	if( palette->load(file) )
	{
		addPalette(palette);
	}
	else
	{
		delete palette;
	}
}

/**
 * @~spanish
 * Cambia la brocha actual.
 */
void Palettes::setBrush(const QBrush& b)
{
	d->currentBrush = b;
}

/**
 * @~spanish
 * Crea la paleta del proyecto.
 */
void Palettes::setProjectPalette(const QString &filePath)
{
// 	int current = d->containerPalette->currentIndex();
	int index = d->containerPalette->indexOf( d->projectPalette );
	d->containerPalette->removeWidget(d->projectPalette);
	
// 	d->projectPalette->setReadOnly(true);
	delete d->projectPalette; // FIXME: lame
	d->projectPalette = new CellsColor;
	d->projectPalette->setName(tr("Project palette"));
	
	if( QFile::exists(filePath) )
	{
		d->projectPalette->load(filePath);
	}
	
	d->projectPalette->setReadOnly(false);
	d->projectPalette->setFilePath(filePath);
// 	d->projectPalette->save();
	
	d->containerPalette->insertWidget(index, d->projectPalette);
	d->containerPalette->setCurrentIndex(index);
}

/**
 * @~spanish
 * Agrega una paleta personalizada.
 */
void Palettes::addCustomPalette()
{
	QString name = QInputDialog::getText(this, tr("Palette name"), tr("name"));
	
	if( !name.isEmpty() )
	{
		CellsColor *palette = new CellsColor;
		palette->setName(name);
		palette->setReadOnly(false);
		
		addPalette(palette);
		
		int index = d->chooserPalette->count()-1;
		d->chooserPalette->setCurrentIndex( index );
		d->containerPalette->setCurrentIndex(index);
		
	}
}

/**
 * @~spanish
 * Remueve la paleta actual.
 */
void Palettes::removeCurrentPalette()
{
	CellsColor* palette = qobject_cast<CellsColor*>(d->containerPalette->currentWidget());
	
	if( palette )
	{
		palette->setReadOnly(true);
		d->containerPalette->removeWidget(palette);
		d->chooserPalette->removeItem(d->chooserPalette->currentIndex());
	}
}

/**
 * @~spanish
 * Añade la actual brocha a la paleta actual, si esta se puede personalizar.
 */
void Palettes::addCurrentColor()
{
	CellsColor *palette = qobject_cast<CellsColor*>(d->containerPalette->currentWidget());
	
	if(palette)
	{
		if( !palette->isReadOnly())
		{
			palette->addItem(d->currentBrush);
			palette->save();
		}
	}
}

/**
 * @~spanish
 * Elimina la brocha seleccionada, si la paleta actual se puede personalizar.
 */
void Palettes::removeCurrentColor()
{
	DGui::CellView *palette = qobject_cast<DGui::CellView *>(d->containerPalette->currentWidget());
	if(palette)
	{
		if( d->defaultPalette != palette)
		{
			//TODO: implementar en DGui::CellView removeItem
		}
	}
}

/**
 * @internal
 * @~spanish
 */
void Palettes::setupForm()
{
	d->chooserPalette = new QComboBox(this);
	
	d->containerPalette = new QStackedWidget(this);
	layout()->addWidget(d->chooserPalette);
	layout()->addWidget(d->containerPalette);
	
	// Project palette
	
	d->projectPalette = new CellsColor;
	d->projectPalette->setName(tr("Project palette") );
	d->projectPalette->setReadOnly(true);
	
	addPalette(d->projectPalette);
	
	//Default Palette
	d->defaultPalette = new  CellsColor(d->containerPalette);
	d->defaultPalette->setName( tr("Default Palette") );
	d->defaultPalette->setReadOnly( true);
	
	fillDefaultColors();
	addPalette(d->defaultPalette);
	
	
	//Named Colors
	d->qtColorPalette = new  CellsColor(d->containerPalette);
	d->qtColorPalette->setReadOnly( true );
	d->qtColorPalette->setName( tr("Named Colors") );
	addPalette(d->qtColorPalette);
	
	fillNamedColor();
	
	connect(d->chooserPalette, SIGNAL(activated ( int  )), d->containerPalette, SLOT(setCurrentIndex ( int )));
	
	readPalettes(__PREFIX__+QString("/share/dash/palettes")); // Pre-installed
	readPalettes(CONFIG_DIR+"/palettes"); // Locals
}

/**
 * @internal
 */
void Palettes::readPalettes(const QString &paletteDir)
{
	dDebug("palette") << "Reading palettes from: " << paletteDir;
	QDir dir(paletteDir);
	
	if(dir.exists ())
	{
		QStringList files = dir.entryList ( QStringList() << "*.dpl" );
		QStringList::ConstIterator it = files.begin();
		
		while(it != files.end())
		{
			load(dir.absoluteFilePath(*it) );
			++it;
		}
	}
}


/**
 * @internal
 * @~spanish
 */
void Palettes::addPalette(CellsColor *palette)
{
	connect(palette, SIGNAL(itemEntered(QTableWidgetItem *)), this, SLOT(changeColor(QTableWidgetItem *)));
	connect(palette, SIGNAL(itemPressed(QTableWidgetItem *)), this, SLOT(changeColor(QTableWidgetItem *)));
	d->chooserPalette->addItem(palette->name());
	d->containerPalette->addWidget(palette);
}

/**
 * @internal
 * @~spanish
 */
void Palettes::changeColor(QTableWidgetItem* item)
{
	D_FUNCINFO;
	if(item)
	{
		emit brushSelected(item->background());
	}
}
/**
 * @internal
 * @~spanish
 */
void Palettes::fillDefaultColors()
{
	int i, j;
	j =0;
    //First column, first 6 rows, a gray scale
	for ( i = 0; i <= 5; i++ )
	{
		d->defaultPalette->addItem(QColor( i * 51, i * 51, i * 51 ));
	}
    //First column, last 6 rows, basic colors
	d->defaultPalette->addItem(QColor( 255, 0, 0 ));
	d->defaultPalette->addItem(QColor( 0, 255, 0 ));
	d->defaultPalette->addItem(QColor( 0, 0, 255 ));
	d->defaultPalette->addItem(QColor( 255, 255, 0 ));
	d->defaultPalette->addItem(QColor( 0, 255, 255 ));
	d->defaultPalette->addItem(QColor( 255, 0, 255 ));

    //Segment from column 1 to 6 and row 0 to 5
	for ( i = 0; i <= 5; i++ )
	{
		for ( j = 1; j <= 6; j++ )
		{
			d->defaultPalette->addItem(QColor( 0, ( j - 1 ) * 51, i * 51 ));
		}
	}

    //Segment from column 1 to 6 and row 6 to 11
	for ( i = 6; i <= 11; i++ )
	{
		for ( j = 1; j <= 6; j++ )
		{
			d->defaultPalette->addItem(QColor( 153, ( j - 1 ) * 51, ( i - 6 ) * 51 ));
		}
	}

    //Segment from column 7 to 12 and row 0 to 5
	for ( i = 0; i <= 5; i++ )
	{
		for ( j = 7; j <= 12; j++ )
		{
			d->defaultPalette->addItem(QColor( 51, ( j - 7 ) * 51, i * 51 ));
		}
	}

    //Segment from column 7 to 12 and row 6 to 11
	for ( i = 6; i <= 11; i++ )
	{
		for ( j = 7; j <= 12; j++ )
		{
			d->defaultPalette->addItem(QColor( 204, ( j - 7 ) * 51, ( i - 6 ) * 51 ));
		}
	}

    //Segment from column 13 to 18 and row 0 to 5
	for ( i = 0; i <= 5; i++ )
	{
		for ( j = 13; j <= 18; j++ )
		{
			d->defaultPalette->addItem(QColor( 102, ( j - 13 ) * 51, i * 51 ));
		}
	}

    //Segment from column 13 to 18 and row 6 to 11
	for ( i = 6; i <= 11; i++ )
	{
		for ( j = 13; j <= 18; j++ )
		{
			d->defaultPalette->addItem(QColor( 255, ( j - 13 ) * 51, ( i - 6 ) * 51 ));
		}
	}
	
}

/**
 * @internal
 * @~spanish
 */
void Palettes::fillNamedColor()
{
	QStringList strColors = QColor::colorNames ();
	QStringList::ConstIterator it = strColors.begin();
	
	while(it != strColors.end() )
	{
		d->qtColorPalette->addItem( QColor(*it) );
		++it;
	}
	
	d->qtColorPalette->addItem(QColor(0,0,0,0));
	d->qtColorPalette->addItem(QColor(0,0,0,50));
}

/**
 * @internal
 * @~spanish
 */
void Palettes::setupButtons()
{
	QGroupBox *containerButtons = new QGroupBox(this);
	QBoxLayout *bLayout = new QBoxLayout(QBoxLayout::LeftToRight);
	bLayout->setMargin(0);
	bLayout->setSpacing(0);
	
	containerButtons->setLayout(bLayout);
	
	DGui::ImageButton *addPalette = new DGui::ImageButton( DGui::IconLoader::self()->load("folder-new.svg" ) , 22);
	connect( addPalette, SIGNAL( clicked() ), SLOT( addCustomPalette() ));
	addPalette->setToolTip(tr( "Create a new palette" ));
	bLayout->addWidget(addPalette);
	
	DGui::ImageButton *removePalette = new DGui::ImageButton( DGui::IconLoader::self()->load("edit-delete.svg" ) , 22);
	connect( removePalette, SIGNAL( clicked() ), SLOT( removeCurrentPalette() ));
	removePalette->setToolTip(tr( "Remove current palette" ));
	bLayout->addWidget(removePalette);
	
	
	DGui::ImageButton *addItem = new DGui::ImageButton( DGui::IconLoader::self()->load("list-add.svg" ) , 22);
	connect( addItem, SIGNAL( clicked() ), SLOT( addCurrentColor() ));
	addItem->setToolTip(tr( "Add color" ));
	bLayout->addWidget(addItem);
	
	DGui::ImageButton *removeColor = new DGui::ImageButton( DGui::IconLoader::self()->load("list-remove.svg" ), 22); //FIXME: cambiar los iconos
	
	connect( removeColor, SIGNAL( clicked() ), SLOT( removeCurrentColor() ) );
	removeColor->setToolTip(tr( "Remove color" ));
	bLayout->addWidget(removeColor);
	
	layout()->addWidget(containerButtons);
}






}
