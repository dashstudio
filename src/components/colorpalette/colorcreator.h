/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COLORCREATOR_H
#define COLORCREATOR_H

#include <QWidget>
#include "ui_colormixer.h"

/**
 * @~spanish
 * Clase que provee de una interfaz gráfica para la selección de un color a partir de sus valores RGBA o HSV.
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class ColorCreator : public QWidget
{
	Q_OBJECT
	public:
		ColorCreator(QWidget *parent = 0);
		~ColorCreator();
		
		void setColor(const QColor& color );
		QColor color() const;
		
	private slots:
		void changeHS(int h, int s);
		void changeHSV(int h, int s, int v);
		
		void syncColorRGB();
		void syncColorHSV();
		
		void onStateChanged(int state);
		void onNameChanged();
		
	signals:
		void colorChanged(const QColor& color);
		
	private:
		Ui::ColorMixer builder;
};

#endif
