/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado   *
 *   krawek@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "paletteparser.h"
#include <dcore/debug.h>

#include <yamf/item/serializer.h>

namespace Component {

struct PaletteParser::Private
{
		Private() : isEditable(false), gradient(0)
		{
		}
		~Private()
		{
			delete gradient;
		}
		
		
		
		QString root, qname;
		QString paletteName;
		bool isEditable;
		QList<QBrush> brushes;
		
		QGradientStops gradientStops;
		QGradient *gradient;
};

PaletteParser::PaletteParser(): DCore::XmlParserBase(), d(new Private)
{
}


PaletteParser::~PaletteParser()
{
	delete d;
}


bool PaletteParser::startTag(const QString &tag, const QXmlAttributes &atts)
{
	if(root() == "palette")
	{
		if(tag == root())
		{
			d->paletteName = atts.value("name");
			if ( atts.value("editable") == "true" )
			{
				d->isEditable = true;
			}
			else
			{
				d->isEditable = false;
			}
		}
		else if( tag == "color")
		{
			QColor c = QColor(atts.value("name"));
			c.setAlpha( atts.value("alpha").toInt() );
			
			if ( c.isValid() )
			{
				d->brushes << c;
			}
			else
			{
				dError() << "Invalid Color";
			}
		}
		else if(tag == "gradient" )
		{
			if ( d->gradient ) delete d->gradient;
			d->gradient = YAMF::Item::Serializer::createGradient(atts);
		}
		else if(tag == "stop" )
		{
			QColor c(atts.value("color") );
			c.setAlpha(atts.value("alpha").toInt() );
			
			d->gradientStops << qMakePair(atts.value("value").toDouble(), c);	
		}
	}
	return true;
}

bool PaletteParser::endTag(const QString& tag)
{
	if ( root() == "palette" )
	{
		if ( tag == "gradient" && d->gradient )
		{
			d->gradient->setStops(d->gradientStops);
			d->brushes << *d->gradient;
			d->gradientStops.clear(); 
		}
	}
	return true;
}

void PaletteParser::text(const QString& )
{
	
}

QList<QBrush> PaletteParser::brushes() const
{
	return d->brushes;
}

QString PaletteParser::paletteName() const
{
	return d->paletteName;
}

bool PaletteParser::isEditable() const
{
	return d->isEditable;
}

}
