/***************************************************************************
 *   Copyright (C) 2005 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "gradientcreator.h"

#include <dcore/debug.h>

#include <dgui/application.h>
#include <dgui/imagebutton.h>
#include <dgui/xyspinbox.h>

#include <QComboBox>

#include "private/gradientselector.h"
#include "private/gradientviewer.h"


namespace Component {

struct GradientCreator::Private
{
		GradientSelector *selector;
		GradientViewer *viewer;
		QComboBox *type, *spread ;
		QSpinBox *radius;
		SpinControl *spinControl;
};


/**
* @~english
* Translate
* @~spanish
* Constructor por defecto.
*/
GradientCreator::GradientCreator(QWidget *parent)
 : QScrollArea(parent), d(new Private)
{
	QFrame *viewport = new QFrame;
	
// 	setWidgetResizable ( true );
	QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
	setAlignment(Qt::AlignHCenter);
	layout->setSpacing(2);
	layout->setMargin(2);
	viewport->setLayout(layout);
	
	QBoxLayout *selectorAndViewer = new QBoxLayout(QBoxLayout::TopToBottom);
	
	d->selector = new GradientSelector(viewport);
	d->selector->setSizePolicy (QSizePolicy::Expanding, QSizePolicy::Fixed );
	d->viewer = new GradientViewer(viewport);
	d->viewer->setSizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding );
	
	connect(d->viewer, SIGNAL(gradientChanged()), this, SLOT(emitGradientChanged()));
	layout->addLayout(selectorAndViewer);
	
	selectorAndViewer->addWidget(d->viewer);
	selectorAndViewer->addWidget(d->selector);
// 	selectorAndViewer->addStretch(2);
	
	connect( d->selector, SIGNAL(gradientChanged(  const QGradientStops& )),this, SLOT(changeGradientStops( const QGradientStops& )));
	connect(d->selector, SIGNAL(arrowAdded()), this, SIGNAL(controlArrowAdded()));
	
	QBoxLayout *subLayout = new QBoxLayout(QBoxLayout::TopToBottom);
	layout->addLayout(subLayout);
	
	
	d->type = new QComboBox(viewport);
	QStringList list;
	list << tr( "Linear" ) << tr( "Radial" ) << tr("Conical");
	d->type->addItems ( list );
	connect(  d->type, SIGNAL(  activated ( int )),this, SLOT(changeType(int)));
	subLayout->addWidget( d->type);
	
	d->spread = new QComboBox(viewport);
	list.clear();
	list << tr( "Pad" ) << tr( "Reflect" ) << tr("Repeat");
	d->spread->addItems ( list );
	connect(  d->spread, SIGNAL(  activated ( int )),this, SLOT(changeSpread(int)));
	subLayout->addWidget( d->spread);
	
	d->radius = new QSpinBox;
	d->radius->setValue(50);
	d->radius->hide();
	connect(d->radius, SIGNAL(valueChanged(int)), d->viewer, SLOT(changeRadius(int)));
	
	subLayout->addWidget(d->radius);
	
	subLayout->setSpacing(2);
	subLayout->setMargin(2);
	
	subLayout->addStretch(2);
	setWidget(viewport);
}


/**
* Destructor
*/
GradientCreator::~GradientCreator()
{
	delete d;
}

/**
* @~english
* Translate
* @~spanish
* Cambia el color del "stop" actual.
*/
void GradientCreator::setCurrentColor(const QColor &color)
{
	d->selector->setCurrentColor(color);
	d->viewer->createGradient();
	emit gradientChanged(QBrush(d->viewer->gradient()));
}

/**
* @~english
* Translate
* @~spanish
* Retorna el tipo del gradiente actual.
* @see QGradient
*/
int GradientCreator::gradientType()
{
	return d->type->currentIndex();
}

/**
* @~english
* Translate
* @~spanish
* Retorna un QBrush con el gradiente actual.
* @see QBrush
*/
QBrush GradientCreator::currentGradient()
{
	return QBrush(d->viewer->gradient());
}

/**
* @~english
* Retorna el tamaño ideal.
*/
QSize GradientCreator::sizeHint () const
{
	QSize size = QFrame::sizeHint();
	
	return size.expandedTo(QApplication::globalStrut());
}

/**
* @~english
* Translate
* @~spanish
* Cambia el tipo del gradiente actual.
*/
void GradientCreator::changeType(int type)
{
	d->viewer->changeType( type);
	
	if( type == QGradient::RadialGradient)
	{
		d->radius->show();
	}
	else if(d->radius->isVisible())
	{
		d->radius->hide();
	}
	
	widget()->adjustSize();
	emitGradientChanged();
}

/**
* @~english
* Translate
* @~spanish
* Cambia el "spread" del gradiente actual.
* @see QGradient
*/
void GradientCreator::changeSpread(int spread)
{
	d->viewer->setSpread( spread);
	emitGradientChanged();
}


/**
* @~english
* Translate
* @~spanish
* Cambia los "GradientStops" del gradiente actual.
* @see QGradientStops
*/
void GradientCreator::changeGradientStops( const QGradientStops& stops )
{
	d->viewer->changeGradientStops( stops);
	emit gradientChanged(d->viewer->gradient());
}


/**
* @~english
* Translate
* @~spanish
* Cambia el gradiente actual.
*/
void GradientCreator::setGradient(const QBrush & gradient)
{
	const QGradient *gr = gradient.gradient();
	d->type->setCurrentIndex(gr->type());
	d->spread->setCurrentIndex(gr->spread());
	d->selector->setStops(gr->stops());
	d->viewer->setGradient( gr);
	
	if( gr->type() == QGradient::RadialGradient)
	{
		d->radius->show();
		d->radius->setValue( (int) static_cast<const QRadialGradient*>(gr)->radius());
	}
	else if(d->radius->isVisible())
	{
		d->radius->hide();
	}
}

/**
 * @internal
 */
void GradientCreator::emitGradientChanged()
{
	d->viewer->changeGradientStops(d->selector->gradientStops());
	emit gradientChanged(d->viewer->gradient());
}
}
