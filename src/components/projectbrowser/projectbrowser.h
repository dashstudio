/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_COMPONENTPROYECTBROWSER_H
#define DASH_COMPONENTPROYECTBROWSER_H

#include <components/base/widget.h>
class QGraphicsScene;

namespace Dash {

namespace Component {

/**
 * @author Jorge Humberto Cuadrado Cabrera <kuadrosxx@gmail.com>
*/
class ProjectBrowser : public Dash::Component::Widget
{
	Q_OBJECT;
	
	public:
		ProjectBrowser(Dash::Project *const project, QGraphicsScene *scene, QWidget *parent = 0);
		~ProjectBrowser();
		
		virtual Dash::Workspace workspace() const;
		virtual Qt::DockWidgetArea area() const;
		
		virtual Dash::DataSource::Types sources() const;
		virtual void update(const Dash::DataSource *datasource, bool undo = false);
		virtual void reset();
		
		void setProject(Dash::Project *const project);
		
	signals:
		void sceneSelected(int scene);
		void layerSelected(int scene, int layer);
		void frameSelected(int scene, int layer, int frame);
		void objectSelected(int scene, int layer, int frame, int object);
		
	private:
		struct Private;
		Private *const d;
		
		
};

}

}

#endif
