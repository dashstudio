/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "projectbrowser.h"

#include <QVBoxLayout>
#include <QGraphicsItem>
#include <QGraphicsScene>

#include <dgui/iconloader.h>

#include <yamf/model/project.h>
#include <yamf/model/scene.h>
#include <yamf/model/layer.h>
#include <yamf/model/frame.h>
#include <yamf/model/object.h>

#include <yamf/item/group.h>

#include "dash/project.h"

#include "ui_projectbrowser.h"


namespace Dash {

namespace Component {

struct ProjectBrowser::Private
{
	Ui::ProjectBrowser ui;
};

ProjectBrowser::ProjectBrowser(Dash::Project *const project, QGraphicsScene *scene, QWidget *parent) : Dash::Component::Widget(project,parent), d(new Private)
{
	setWindowTitle(tr("&Project browser"));
	setWindowIcon( DGui::IconLoader::self()->load(""));
	
	d->ui.setupUi(this);
	
	d->ui.projectView->setProject(project);
	
	d->ui.photogramView->setScene(scene);
	
	connect(d->ui.projectView, SIGNAL(sceneSelected(int)), this, SIGNAL(sceneSelected(int)));
	connect(d->ui.projectView, SIGNAL(layerSelected(int, int)), this, SIGNAL(layerSelected(int, int)));
	connect(d->ui.projectView, SIGNAL(frameSelected(int, int, int)), this, SIGNAL(frameSelected(int, int, int)));
	connect(d->ui.projectView, SIGNAL(objectSelected(int, int, int, int)), this, SIGNAL(objectSelected(int, int, int, int)));
}


ProjectBrowser::~ProjectBrowser()
{
	delete d;
}

Dash::Workspace ProjectBrowser::workspace() const
{
	return Dash::All;
}

Qt::DockWidgetArea ProjectBrowser::area() const
{
	return Qt::RightDockWidgetArea;
}

Dash::DataSource::Types ProjectBrowser::sources() const
{
	return Dash::DataSource::All;
}


void ProjectBrowser::update(const Dash::DataSource *datasource, bool undo)
{
// 	dfDebug << undo;
// 	dfDebug << datasource->data("root").toString();
// 	dfDebug << datasource->values("location")["index"];
// 	dfDebug << "xml" << datasource->data("xml").toString();
	
	switch (datasource->type())
	{
		case DataSource::Scene:
		{
		}
		break;
		case DataSource::Layer:
		{
		}
		break;
		case DataSource::Frame:
		{
		}
		break;
		case DataSource::Library:
		case DataSource::Project:
		case DataSource::Object:
		case DataSource::Unknown:
		{
		}
		break;
		case DataSource::All:
		{
		}
		break;
	}
}

void ProjectBrowser::reset()
{
}

void ProjectBrowser::setProject(Dash::Project *const project)
{
	d->ui.projectView->setProject(project);
}



}

}
