# Subdir relative project main directory: ./src/components/library
# Target is a library:  

HEADERS += librarywidget.h \
           gctable.h
SOURCES += librarywidget.cpp \
           gctable.cpp
CONFIG += static
TEMPLATE = lib

include(../components_config.pri)
