/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COMPONENTSGCTABLE_H
#define COMPONENTSGCTABLE_H

#include <QTreeWidget>

namespace YAMF {
namespace Model {
class Library;
class LibraryObject;
}
}

/**
 * @author David Cuadrado <krawek@gmail.com>
 * @todo Crear un item delegador para editar!
*/
class GCTable : public QTreeWidget
{
	Q_OBJECT
	public:
		GCTable(QWidget *parent = 0);
		~GCTable();
		QTreeWidgetItem *currentFolder();
		void setCurrentFolder(QTreeWidgetItem *cf);
		void removeCurrentFolder();
		
	signals:
		void requestRenameSymbol(const QString& symbol, const QString& newName);
		
	public slots:
		void createFolder(const QString &name = QString() );
		
	protected slots:
		void commitData( QWidget * editor );
		
	private:
		QTreeWidgetItem *m_currentFolder;
};

#endif
