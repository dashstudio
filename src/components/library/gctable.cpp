/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gctable.h"

#include <QHeaderView>

#include <dgui/iconloader.h>

#include <yamf/model/library.h>
#include <yamf/model/libraryobject.h>
#include <QLineEdit>

GCTable::GCTable(QWidget *parent)
	: QTreeWidget(parent), m_currentFolder(0)
{
	setHeaderLabels(QStringList() << "" << "" );
	header()->hide();
	header()->setResizeMode(QHeaderView::ResizeToContents);
}


GCTable::~GCTable()
{
}

void GCTable::createFolder(const QString &name)
{
	QTreeWidgetItem *newFolder = new QTreeWidgetItem(this);
	
	if ( name.isNull() )
	{
		newFolder->setText(0, tr("New folder %1").arg( topLevelItemCount ()) );
	}
	else
	{
		newFolder->setText(0, name );
	}
	
	newFolder->setIcon(0, DGui::IconLoader::self()->load( "folder.svg" ));
	m_currentFolder = newFolder;
	
	setCurrentItem(m_currentFolder);
}



QTreeWidgetItem *GCTable::currentFolder()
{
	return m_currentFolder;
}

void GCTable::setCurrentFolder(QTreeWidgetItem *cf)
{
	if ( cf )
	{
		m_currentFolder = cf;
	}
}

void GCTable::removeCurrentFolder()
{
	if ( m_currentFolder )
	{
		int index = indexOfTopLevelItem(m_currentFolder) - 1;
		
		delete m_currentFolder;
		
		m_currentFolder = topLevelItem (index);
		setCurrentItem(m_currentFolder);
	}
}

void GCTable::commitData( QWidget * editor )
{
	QLineEdit *lineEdit = qobject_cast<QLineEdit *>(editor);
	QString symbol;
	QTreeWidgetItem *item =  currentItem ();
	if(item)
	{
		symbol = item->text(0);
// 		QTreeWidget::commitData(0); // Don't rename
		
		if(lineEdit)
		{
			emit requestRenameSymbol(symbol, lineEdit->text());
		}
	}
	QTreeWidget::commitData(editor);
}
