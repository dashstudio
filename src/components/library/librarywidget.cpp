/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "librarywidget.h"

#include <cstdlib>
#include <ctime>

#include <QGroupBox>
#include <QFileDialog>

#include <QGraphicsSvgItem>
#include <QGraphicsPixmapItem>

#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMap>
#include <QDir>
#include <QTreeWidgetItemIterator>


#include <dcore/debug.h>
#include <dcore/config.h>

#include <dgui/imagebutton.h>
#include <dgui/osd.h>
#include <dgui/optionaldialog.h>
#include <dgui/iconloader.h>

#include <dmedia/player.h>

#include <yamf/model/project.h>
#include <yamf/model/library.h>
#include <yamf/model/libraryobject.h>
#include <yamf/item/preview.h>

#include "gctable.h"

#include "dash/project.h"

namespace Dash {
namespace Component {

struct LibraryWidget::Private
{
	Private()
	{
		currentFrame.frame = 0;
		currentFrame.layer = 0;
		currentFrame.scene = 0;
	}
	
	YAMF::Item::Preview *display;
	
	GCTable *libraryTree;
	int childCount;
	QDir libraryDir;
	
	struct Frame
	{
		int scene;
		int layer;
		int frame;
	} currentFrame;
	
	DMedia::Player *player;
	
	QHash<QTreeWidgetItem *, YAMF::Model::LibraryObject *> objects;
};

LibraryWidget::LibraryWidget(Dash::Project *const project, QWidget *parent) : Widget(project, parent), d(new Private)
{
	D_INIT;
	
	d->player = new DMedia::Player(this);
	d->player->loadEngine("gstreamer"); // FIXME: configure
	d->player->setVolume(70);
	
	QVBoxLayout *layout = new QVBoxLayout( this );
	
	d->childCount = 0;
	
	setWindowIcon(DGui::IconLoader::self()->load("library.png"));
	setWindowTitle(tr("&Library"));
	
	d->libraryDir = QDir(CONFIG_DIR+"/libraries");
	d->display = new YAMF::Item::Preview(this);
	
	d->libraryTree = new GCTable(this);

	connect(d->libraryTree, SIGNAL(itemClicked( QTreeWidgetItem *, int)), this, SLOT(previewItem(QTreeWidgetItem *, int)));
	connect(d->libraryTree, SIGNAL(requestRenameSymbol( const QString &, const QString &)), this, SLOT(onRequestRenameSymbol( const QString &, const QString & )));
	
	QGroupBox *buttons = new QGroupBox(this);
	QHBoxLayout *buttonLayout = new QHBoxLayout(buttons);
	buttonLayout->setMargin(0);
	buttonLayout->setSpacing(0);
	
	DGui::ImageButton *addGC = new DGui::ImageButton(DGui::IconLoader::self()->load("list-add.svg" ), 22, buttons);
	connect(addGC, SIGNAL(clicked()), this, SIGNAL(addCurrentItemsToLibrary()));
	
	buttonLayout->addWidget(addGC);
	addGC->setToolTip(tr( "Add the current graphic to Library" ));
	
	DGui::ImageButton *delGC = new DGui::ImageButton(DGui::IconLoader::self()->load("list-remove.svg" ), 22, buttons);
	connect(delGC, SIGNAL(clicked()), this, SLOT(removeCurrentGraphic()));
	
	delGC->setToolTip(tr( "Remove the selected Symbol from Library" ));
	buttonLayout->addWidget(delGC);
	
	DGui::ImageButton *gctoDrawingArea = new DGui::ImageButton(DGui::IconLoader::self()->load("insert_cg.png" ), 22, buttons);
	connect(gctoDrawingArea, SIGNAL(clicked()), this, SLOT(onAddSymbolToProject()));
	gctoDrawingArea->setToolTip(tr( "Inserts the selected symbol into the drawing area" ) );
	buttonLayout->addWidget(gctoDrawingArea);
	
	DGui::ImageButton *addFolderGC = new DGui::ImageButton(DGui::IconLoader::self()->load("folder-new.svg" ), 22, buttons);
	connect(addFolderGC, SIGNAL(clicked()), d->libraryTree, SLOT(createFolder()));
	addFolderGC->setToolTip(tr( "Adds a folder to the symbol list" ));
	buttonLayout->addWidget(addFolderGC);
	
	buttons->setLayout(buttonLayout);
	
	layout->addWidget( d->display );
	layout->addWidget( buttons );
	layout->addWidget( d->libraryTree );
}


LibraryWidget::~LibraryWidget()
{
	D_END;
	delete d;
}

Dash::Workspace LibraryWidget::workspace() const
{
	return Dash::Drawing;
}

Qt::DockWidgetArea LibraryWidget::area() const
{
	return Qt::LeftDockWidgetArea;
}

Dash::DataSource::Types LibraryWidget::sources() const
{
	return Dash::DataSource::Library;
}

void LibraryWidget::update(const Dash::DataSource *datasource, bool undo)
{
	QString root = datasource->values("command")["name"];
	YAMF::Model::Library *library = project()->library();
	
	if( root == "addlibraryobject" || root == "removelibraryobject" )
	{
		dfDebug << datasource->data("xml").toString();
		
		QString id = datasource->values("object")["id"];
		
		if( (!undo && root == "addlibraryobject") || (undo && root == "removelibraryobject") )
		{
			YAMF::Model::LibraryObject *object = library->findObject(id);
			if( object )
			{
				addObject(object);
			}
		}
		else
		{
			foreach(QTreeWidgetItem *item, d->objects.keys())
			{
				if( item->text(0) == id )
				{
					d->objects.remove(item);
					delete item;
					
					previewItem(d->libraryTree->currentItem());
					
					break;
				}
			}
		}
	}
}

void LibraryWidget::reset()
{
	d->childCount = 0;
	d->libraryTree->clear();
	
	d->currentFrame.frame = 0;
	d->currentFrame.layer = 0;
	d->currentFrame.scene = 0;
}

void LibraryWidget::addObject(YAMF::Model::LibraryObject *object)
{
	QTreeWidgetItem *item = new QTreeWidgetItem(d->libraryTree);
	item->setText(0, object->symbolName());
	item->setFlags(item->flags() | Qt::ItemIsEditable);
	
	d->objects.insert(item, object);
}


void LibraryWidget::previewItem(QTreeWidgetItem *item, int)
{
	D_FUNCINFO;
	
	if ( item )
	{
		YAMF::Model::LibraryObject *object = d->objects.value(item);
		if ( !object )
		{
			dDebug("library") << "Cannot find the object";
			return;
		}
		
		d->player->stop();
		
		switch (object->type() )
		{
			case YAMF::Model::LibraryObject::Item:
			{
				if ( YAMF::Model::LibraryItemObject* itemObject = dynamic_cast<YAMF::Model::LibraryItemObject *>(object) )
				{
					d->display->render( itemObject->item() );
					
#if 0 // test
					SymbolEditor *editor = new SymbolEditor;
					editor->setSymbol(object);
					emit postPage(editor);
#endif
				}
			}
			break;
			case YAMF::Model::LibraryObject::Svg:
			{
				if ( YAMF::Model::LibrarySvgObject *sObject = dynamic_cast<YAMF::Model::LibrarySvgObject *>(object) )
				{
					d->display->render( sObject->item() );
				}
			}
			break;
			case YAMF::Model::LibraryObject::Image:
			{
				if ( YAMF::Model::LibraryBitmapObject *sObject = dynamic_cast<YAMF::Model::LibraryBitmapObject *>(object) )
				{
					d->display->render( sObject->item() );
				}
			}
			break;
			case YAMF::Model::LibraryObject::Sound:
			{
				if ( YAMF::Model::LibrarySoundObject *sObject = dynamic_cast<YAMF::Model::LibrarySoundObject *>(object) )
				{
					d->player->load(sObject->url());
					d->player->play();
				}
			}
			break;
			default:
			{
				dDebug("library") << "Unknown symbol id: " << object->type();
			}
			break;
		}
	}
}

void LibraryWidget::onAddSymbolToProject()
{
	if ( !d->libraryTree->currentItem() ) return;
	
	QString symKey = d->libraryTree->currentItem()->text(0);
	
	emit addSymbolToProject(symKey);
}

void LibraryWidget::removeCurrentGraphic()
{
	if ( !d->libraryTree->currentItem() ) return;
	QString symKey = d->libraryTree->currentItem()->text(0);
	
	project()->library()->removeObject(symKey);
}

void LibraryWidget::renameObject( QTreeWidgetItem* item)
{
// 	if ( item )
// 	{
// 		KTGraphicComponent *graphic = d->graphics[item];
// 		
// 		if ( graphic )
// 		{
// 			graphic->setComponentName(item->text(0));
// 		}
// 		else // A Folder
// 		{
// 			foreach( QTreeWidgetItem *folder, d->libraryTree->topLevelItems() )
// 			{
// 				if ( folder != item && folder->text(0) == item->text(0) )
// 				{
// 					// Invalid name
// 					item->setFlags(item->flags() | Qt::ItemIsEditable );
// 					item->setText(0, item->text(0)+QString::number(() % 999) );
// 					d->libraryTree->editItem( item, 0);
// 					break;
// 				}
// 			}
// 		}
// 	}
}

void LibraryWidget::onRequestRenameSymbol(const QString &symbol, const QString newName)
{
	if(YAMF::Model::Project *project =  this->project())
	{
		project->library()->renameObject(symbol, newName);
	}
}

void LibraryWidget::importBitmap()
{
	QString image = QFileDialog::getOpenFileName ( this, tr("Import an image..."), QDir::homePath(),  tr("Images")+" (*.png *.xpm *.jpg)" );
	
	if( image.isEmpty() ) return;
	
	QFileInfo finfo(image);
	QString symName = finfo.baseName();
	
	if( QFile::exists(image) )
	{
		this->project()->library()->createBitmapSymbol(QUrl::fromLocalFile(image), symName);
	}
	else
	{
		DGui::Osd::self()->display(tr("Cannot open file: %1").arg(image));
	}
}

void LibraryWidget::importSvg()
{
	QString svg = QFileDialog::getOpenFileName ( this, tr("Import an SVG..."), QDir::homePath(),  tr("SVG")+" (*.svg)" );
	
	if( svg.isEmpty() ) return;
	
	QFileInfo finfo(svg);
	QString symName = finfo.baseName();
	
	if( QFile::exists(svg) )
	{
		this->project()->library()->createSvgSymbol(QUrl::fromLocalFile(svg), symName);
	}
	else
	{
		DGui::Osd::self()->display(tr("Cannot open file: %1").arg(svg));
	}
}

void LibraryWidget::importSound()
{
	QString sound = QFileDialog::getOpenFileName ( this, tr("Import an audio file..."), QDir::homePath(),  tr("Sound file")+" (*.ogg *.wav *.mp3)" );
	
	if( sound.isEmpty() ) return;
	
	QFileInfo finfo(sound);
	QString symName = finfo.baseName();
	
	if( QFile::exists(sound) )
	{
		this->project()->library()->createSoundSymbol(QUrl::fromLocalFile(sound), symName);
	}
	else
	{
		DGui::Osd::self()->display(tr("Cannot open file: %1").arg(sound));
	}
}


}
}
