/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COMPONENTSLIBRARYWIDGET_H
#define COMPONENTSLIBRARYWIDGET_H

#include <components/base/widget.h>

class QTreeWidgetItem;

namespace YAMF {
namespace Model {
 class Library;
 class LibraryObject;
}
}

namespace Dash {
namespace Component {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class LibraryWidget : public Widget
{
	Q_OBJECT
	public:
		LibraryWidget(Dash::Project *const project, QWidget *parent = 0);
		~LibraryWidget();
		
		virtual Dash::Workspace workspace() const;
		virtual Qt::DockWidgetArea area() const;
		
		virtual Dash::DataSource::Types sources() const;
		virtual void update(const Dash::DataSource *datasource, bool undo = false);
		
		virtual void reset();
		
	private slots:
		void addObject(YAMF::Model::LibraryObject *object);
		void previewItem(QTreeWidgetItem *, int=0);
		void onAddSymbolToProject();
		void removeCurrentGraphic();
		void renameObject( QTreeWidgetItem* item);
		void onRequestRenameSymbol(const QString &symbol, const QString newName);
		
	public slots:
		void importBitmap();
		void importSvg();
		void importSound();
		
	signals:
		void addSymbolToProject(const QString &name);
		void addCurrentItemsToLibrary();
	
	private:
		struct Private;
		Private *const d;
};
}
}

#endif

