
INCLUDEPATH += ../../

TEMPLATE = lib

CONFIG += staticlib \
warn_on

HEADERS += widget.h \
manager.h
SOURCES += widget.cpp \
manager.cpp

TARGET = componentsbase
DESTDIR = ../../../lib

include($$PWD/../../../config.pri)

LIBS += -ldash
