/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "widget.h"

namespace Dash {
namespace Component {

/**
 * @~spanish
 * Constructor
 */
Widget::Widget(Dash::Project *const project, QWidget *parent)
	: QWidget(parent), Dash::Module(project)
{
}

/**
 * @~spanish
 * Destructor
 */
Widget::~Widget()
{
	
}

/**
 * @~spanish
 * Retorna el área de trabajo del modulo.
 */
Dash::Workspace Widget::workspace() const
{
	return Dash::All;
}

/**
 * @~spanish
 * Retorna la posición en la ventana principal del modulo.
 */
Qt::DockWidgetArea Widget::area() const
{
	return Qt::LeftDockWidgetArea;
}

}
}

