/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "manager.h"
#include <ideality/mainwindow.h>

#include "widget.h"

namespace Dash {
namespace Component {

struct Manager::Private
{
	Private(Ideality::MainWindow *const mainWindow) : mainWindow(mainWindow) {}
	
	Ideality::MainWindow *const mainWindow;
	QList<Component::Widget *> components;
};

/**
 * @~spanish
 * Constructor
 */
Manager::Manager(Ideality::MainWindow *const mainWindow, QObject *parent)
 : QObject(parent), d(new Private(mainWindow))
{
}

/**
 * @~spanish
 * Destructor
 */
Manager::~Manager()
{
	delete d;
}

/**
 * @~spanish
 * Añade un componente al manejador, esto implica que el componente se añadira a la ventana pricipal.
 */
void Manager::add(Widget *w)
{
	d->mainWindow->addToolView(w, w->area(), w->workspace());
	d->components << w;
}

/**
 * @~spanish
 * Retorna los widgets que reprecentan componentes de la interfaz gráfica.
 */
QList<Widget *> Manager::widgets() const
{
	return d->components;
}

}
}

