/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef COMPONENTWIDGET_H
#define COMPONENTWIDGET_H

#include <QWidget>
#include <dash/dash.h>
#include <dash/module.h>

namespace Dash {
class Project;

namespace Component {

/**
 * @~spanish
 * @brief Esta clase provee de una interfaz gráfica para un modulo de la Dash, esta es la clase base para crear un componente de la interfaz gráfica.
 * @author David Cuadrado <krawek@gmail.com>
*/
class Widget : public QWidget, public Dash::Module
{
	Q_OBJECT;
	
	public:
		Widget(Dash::Project *const project, QWidget *parent = 0);
		~Widget();
		
		virtual Dash::Workspace workspace() const;
		virtual Qt::DockWidgetArea area() const;
};

}
}

#endif
