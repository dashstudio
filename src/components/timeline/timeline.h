/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_COMPONENTSTIMELINE_H
#define DASH_COMPONENTSTIMELINE_H

#include <components/base/widget.h>

#include <QSplitter>

#include <QStackedWidget>
#include <dgui/tabwidget.h>

#include <components/timeline/timelinewidget.h>

namespace YAMF {
namespace Model {
 class Library;
}
}

namespace Dash {
	class DataSource;
}

namespace Dash {

namespace Component {

class LayerManager;
class FramesTable;
class FramesTableItem;

/**
 * @~spanish
 * @brief Esta clase representa el componente TimeLine, este componente provee al usuario de una interfaz para la visualización y gestión del proyecto, en forma de linea de tiempo.
 * @author David Cuadrado \<krawek@gmail.com\>
*/
class TimeLine : public Widget
{
	Q_OBJECT;
	public:
		TimeLine(Dash::Project *const project, QWidget *parent = 0);
		~TimeLine();
		
		virtual Dash::Workspace workspace() const;
		virtual Qt::DockWidgetArea area() const;
		
		virtual Dash::DataSource::Types sources() const;
		virtual void update(const Dash::DataSource *datasource, bool undo = false);
		
		virtual void reset();
		
	private slots:
		void onFrameSelected(int scenePos, int layerPos, int framePos);
		void onEvent(const TimeLineWidget::Event &event );
		
	signals:
		void frameSelected(int scenePos, int layerPos, int framePos);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
