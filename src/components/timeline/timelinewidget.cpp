/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "timelinewidget.h"

#include "layermanager.h"
#include "framestable.h"

#include <QStackedWidget>
#include <QTreeWidget>
#include <QHeaderView>
#include <QLayout>
#include <QTimer>

#include <dgui/iconloader.h>
#include <dgui/imagebutton.h>

#include <dcore/debug.h>

#include "ui_frames.h"
#include "ui_layers.h"
#include "ui_scenes.h"

namespace Dash {
namespace Component {

struct TimeLineWidget::Private
{
	Ui::Frames frames;
	Ui::Layers layers;
	Ui::Scenes scenes;
	
	QButtonGroup actions;
	
	QTimer timer;
	
	
	TimeLineWidget::Event createEvent(TimeLineWidget *tl, TimeLineWidget::Action action)
	{
		Event event;
		event.action = Action(action);
		event.scene = tl->scene();
		event.layer = tl->layer();
		event.frame = tl->frame();
		return event;
	}
};

/**
 * @~spanish
 * Constructor
 */
TimeLineWidget::TimeLineWidget(QWidget * parent)
 : QSplitter(Qt::Horizontal, parent), d(new Private)
{
	QWidget *scenesWidget = new QWidget;
	d->scenes.setupUi(scenesWidget);
	addWidget(scenesWidget);
	
	QWidget *layersWidget = new QWidget;
	d->layers.setupUi(layersWidget);
	addWidget(layersWidget);
	
	QWidget *framesWidget = new QWidget;
	d->frames.setupUi(framesWidget);
	addWidget(framesWidget);
	
	d->scenes.container->setHeaderLabels(QStringList() << tr("Scene"));
	connect(d->scenes.container, SIGNAL(itemClicked( QTreeWidgetItem *, int)), this, SLOT(onItemClicked(QTreeWidgetItem *, int)));
	
	connect(d->scenes.container, SIGNAL(itemChanged( QTreeWidgetItem *, int)), this, SLOT(onItemChanged(QTreeWidgetItem *, int)));
	
	connect(&d->actions, SIGNAL(buttonClicked(int)), this, SLOT(onButtonClicked(int)));
	
	connect(&d->timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
	
	setupButtons();
	
	setSizes(QList<int>() << 100 << 200 << 800);
}


/**
 * @~spanish
 * Destructor
 */
TimeLineWidget::~TimeLineWidget()
{
	delete d;
}

/**
 * @~spanish
 * Obtiene la posición de la escena actualmente seleccionada.
 */
int TimeLineWidget::scene()
{
	if(d->scenes.container->topLevelItemCount() == 0)
		return -1;
	
	return d->scenes.container->indexOfTopLevelItem(d->scenes.container->currentItem());
}

/**
 * @~spanish
 * Obtiene la posición del layer actualmente seleccionad0.
 */
int TimeLineWidget::layer()
{
	const LayerManager *layers = layerManager(scene());
	
	if(layers )
	{
		if( layers->rowCount() == 0 )
		{
			return -1;
		}
		if(layers->currentRow() == -1)
		{
			return -2;
		}
		return layers->visualRow(layers->currentRow());
	}
	return -1;
}

/**
 * @~spanish
 * Obtiene la posición del frame actualmente seleccionado.
 */
int TimeLineWidget::frame()
{
	FramesTable *frames = framesTable(scene());
	if(frames)
	{
		if(frames->columnCount() == 0)
		{
			return -1;
		}
		
		if(frames->currentColumn() == -1)
		{
			return -2;
		}
		
		return frames->visualColumn(frames->currentColumn());
	}
	return -1;
}

/**
 * @~spanish
 * Inserta una escena el la posición @p position y con el nombre @p name.
 */
void TimeLineWidget::insertScene(int position, const QString &name)
{
	if ( position < 0 || position > d->scenes.container->topLevelItemCount())
	{
		return;
	}
	
	LayerManager *layerManager = new LayerManager( d->layers.container );
	
	connect(layerManager, SIGNAL(layerRenamed(int, const QString &)), this, SLOT(onLayerRenamed(int , const QString &)));
	connect(layerManager, SIGNAL(layerLocked(int, bool)), this, SLOT(onLayerLocked(int , bool)));
	connect(layerManager, SIGNAL(layerVisibilityChanged(int, bool)), this, SLOT(onLayerVisibilityChanged(int , bool)));
	
	layerManager->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	
	FramesTable *framesTable = new FramesTable();
	
	connect(framesTable, SIGNAL(frameSelected(int , int)), this, SLOT(onFrameSelected(int , int )));
	connect(framesTable, SIGNAL(layerSelected(int)), this, SLOT(onLayerSelected(int)));
	connect(framesTable, SIGNAL(requestInsertFrame(int, int)), this, SLOT(onRequestInsertFrame(int, int)));
	connect(framesTable, SIGNAL(requestRemoveFrame(int, int)), this, SLOT(onRequestRemoveFrame(int, int)));
	
	connect(framesTable, SIGNAL(requestCopyFrame(int, int)), this, SLOT(onRequestCopyFrame(int, int)));
	connect(framesTable, SIGNAL(requestPasteFrame(int, int)), this, SLOT(onRequestPasteFrame(int, int)));
	connect(framesTable, SIGNAL(requestCutFrame(int, int)), this, SLOT(onRequestCutFrame(int, int)));
	
	connect(framesTable, SIGNAL(requestChangeFrameVisibility(int, int, bool)), this, SLOT(onRequestChangeFrameVisibility(int, int, bool)));
	connect(framesTable, SIGNAL(requestLockFrame(int, int, bool)), this, SLOT(onRequestLockFrame(int, int, bool)));
	
	connect(framesTable, SIGNAL(requestExpandFrame(int, int, int)), this, SLOT(onRequestExpandFrame(int, int, int)));
	
	framesTable->setItemSize( 10, 20);
	layerManager->setRowHeight( 20 );
	
	connect(layerManager->verticalScrollBar(), SIGNAL(valueChanged (int)), framesTable->verticalScrollBar(), SLOT(setValue(int)));
	connect(framesTable->verticalScrollBar(), SIGNAL(valueChanged (int)), layerManager->verticalScrollBar(), SLOT(setValue(int)));
	
	
	d->frames.container->insertWidget(position, framesTable);
	d->layers.container->insertWidget(position, layerManager);
	
	QTreeWidgetItem *newScene = new QTreeWidgetItem;
	newScene->setFlags(Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	newScene->setText(0, name);
	d->scenes.container->insertTopLevelItem(position, newScene);
}


/**
 * @~spanish
 * Elimina la escena que esta en la posición @p position.
 */
void TimeLineWidget::removeScene(int position)
{
	if ( position >= 0 && position < d->frames.container->count() && position < d->layers.container->count() )
	{
		QWidget *w = d->frames.container->widget(position);
		d->frames.container->removeWidget(w);
		delete w;
		
		w = d->layers.container->widget(position);
		d->layers.container->removeWidget(w);
		delete w;
		
		delete d->scenes.container->topLevelItem(position);
	}
	
}

/**
 * @~spanish
 * Cambia el nombre de la escena que esta en la posición @p position a @p name.
 */
void TimeLineWidget::renameScene(int position, const QString &name)
{
	if(position < 0 || position >= d->scenes.container->topLevelItemCount() ) return;
	d->scenes.container->topLevelItem(position)->setText(0, name);
}

/**
 * @~spanish
 * Mueve la escena de la posición @p from a la posición @p to.
 */
void TimeLineWidget::moveScene(int from, int to)
{
	if(to < from)
	{
		int swap = to;
		to = from;
		from = swap;
	}
	
	for(int i = from; i < to; i++)
	{
		d->scenes.container->insertTopLevelItem(i, d->scenes.container->takeTopLevelItem(i+1) );
		
		d->frames.container->insertWidget(i, d->frames.container->widget(i+1));
		
		d->layers.container->insertWidget(i, d->layers.container->widget(i+1));
	}
}

/**
 * @~spanish
 * Selecciona la escena que esta en la posición @p position.
 */
void TimeLineWidget::setScene(int position)
{
	QTreeWidgetItem *item = d->scenes.container->topLevelItem ( position );
	if(item)
	{
		d->scenes.container->setCurrentItem(item);
	}
	
	d->frames.container->setCurrentIndex(position);
	d->layers.container->setCurrentIndex(position);
}


/**
 * @~spanish
 * Cierra todas las escenas.
 */
void TimeLineWidget::closeAllScenes()
{
	while(d->frames.container->currentWidget())
	{
		delete d->frames.container->currentWidget();
	}
	
	while(d->layers.container->currentWidget())
	{
		delete d->layers.container->currentWidget();
	}
	d->scenes.container->clear();
}

/**
 * @~spanish
 * Inserta un layer en la posición @p position de la escena @p scenePos, con el nombre @p name.
 */
void TimeLineWidget::insertLayer(int scenePos, int position, const QString &name)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	if(layerManager)
	{
		layerManager->insertLayer(position, name);
	}
	
	FramesTable *fst = framesTable( scenePos );
	if( fst )
	{
		fst->insertLayer(position, name);
	}
}

/**
 * @~spanish
 * Inserta un layer de audio en la posición @p position de la escena @p scenePos, con el nombre @p name y la posición del frame donde inicia el audio.
 */
void TimeLineWidget::insertAudioLayer(int scenePos, int position, const QString &name, int startFrame)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	if(layerManager)
	{
		layerManager->insertSoundLayer(position, name);
	}
	
	FramesTable *fst = framesTable( scenePos );
	if( fst )
	{
		fst->insertSoundLayer(position, name);
		for(int i = startFrame; i < fst->columnCount(); i++)
		{
			fst->insertFrame(position, i, tr(" "));
		}
	}
}

/**
 * @~spanish
 * Elimina el layer que esta en la posicion @p position de la escena @p scenePos.
 */
void TimeLineWidget::removeLayer(int scenePos, int position)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	if(layerManager)
	{
		layerManager->removeLayer(position);
	}
	
	FramesTable *fst = framesTable( scenePos );
	if( fst )
	{
		fst->removeLayer(position);
	}
}

/**
 * @~spanish
 * Cambia el nombre del layer que se encuentra en la posicion @p layerPos de la escena @p scenePos, por @p name.
 */
void TimeLineWidget::renameLayer(int scenePos, int layerPos, const QString &name)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	if(layerManager)
	{
		layerManager->renameLayer(layerPos, name);
	}
}

/**
 * @~spanish
 * Cambia el estado de visibilidad del layer que esta en la posición @p position de la escena @p scenePos, si @p isVisible es verdadero muestra el layer como visible y si es falso lo muestra como no visible.
 */
void TimeLineWidget::setLayerVisibility(int scenePos, int position, bool isVisible)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	layerManager->blockSignals(true);
	if(layerManager)
	{
		QTableWidgetItem *item = layerManager->item(layerManager->visualRow(position), 2);
		if(isVisible)
		{
			item->setCheckState(Qt::Checked);
		}
		else
		{
			item->setCheckState(Qt::Unchecked);
		}
			
	}
	layerManager->blockSignals(false);
}

/**
 * @~spanish
 * Cambia el estado de bloqueo del layer que esta en la posición @p position de la escena @p scenePos, si @p locked es verdadero muestra el layer como bloqueado y si es falso lo muestra como desbloqueado.
 */
void TimeLineWidget::setLayerLocked(int scenePos, int position, bool locked)
{
	LayerManager *layerManager = this->layerManager(scenePos);
	layerManager->blockSignals(true);
	if(layerManager)
	{
		QTableWidgetItem *item = layerManager->item(layerManager->visualRow( position ), 1);
		if(locked)
		{
			item->setCheckState(Qt::Checked);
		}
		else
		{
			item->setCheckState(Qt::Unchecked);
		}
	}
	layerManager->blockSignals(false);
}

/**
 * @~spanish
 * Mueve el layer de la posición @p from a la posición @p to.
 */
void TimeLineWidget::moveLayer(int scenePos, int from, int to)
{
	LayerManager *layerManager = this->layerManager( scenePos );
	if ( layerManager )
	{
		layerManager->moveLayer( from, to);
	}
	
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->moveLayer(from, to);
	}
}

/**
 * @~spanish
 * Selecciona el layer que esta en la posición @p position, de la escena @p scenePos.
 */
void TimeLineWidget::setLayer(int scenePos, int position)
{
	setScene(scenePos);
	
	LayerManager *layerManager = this->layerManager( scenePos );
	if(layerManager)
	{
		layerManager->setCurrentCell(layerManager->verticalHeader()->logicalIndex(position) , 0);
	}
}

/**
 * @~spanish
 * Inserta un frame en la posición @p position, del layer @p layerPos de la escena @p scenePos, con nombre @p name.
 */
void TimeLineWidget::insertFrame(int scenePos, int layerPos, int position, const QString &name)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->insertFrame(layerPos, position, name);
	}
}

/**
 * @~spanish
 * Elimina el frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos.
 */
void TimeLineWidget::removeFrame(int scenePos, int layerPos, int position)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->removeFrame( layerPos, position );
	}
}

/**
 * @~spanish
 * Cambia el nombre del frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos, por @p name.
 */
void TimeLineWidget::renameFrame(int scenePos, int layerPos, int position, const QString &name)
{
	Q_UNUSED(layerPos);
	Q_UNUSED(position);
	Q_UNUSED(name);
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
// 		framesTable->renameFrame( layerPos, position );
		//TODO: poner nombre en el item
	}
}

/**
 * @~spanish
 * Mueve el frame de la posición @p from a la posición @p to.
 */
void TimeLineWidget::moveFrame(int scenePos, int layerPos, int from, int to)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->moveFrame(layerPos, from, to);
	}
}

/**
 * @~spanish
 * Selecciona el frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos.
 */
void TimeLineWidget::setFrame(int scenePos, int layerPos, int position)
{
	setLayer(scenePos, layerPos);
	
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->setCurrentCell( framesTable->visualRow(layerPos), position );
	}
}

/**
 * @~spanish
 * Cambia el estado de visibilidad del frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos por @p isVisible, si es verdadero el frame se mostrara como visible, de lo contrario se mostrara como no visible.
 */
void TimeLineWidget::setFrameVisibility(int scenePos, int layerPos, int position, bool isVisible)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->setFrameVisibility(layerPos, position, isVisible);
	}
}

/**
 * @~spanish
 * Cambia el estado de bloqueo del frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos por @p locked, si es verdadero el frame se mostrara como bloqueado, de lo contrario se mostrara como desbloqueado.
 */
void TimeLineWidget::setFrameLocked(int scenePos, int layerPos, int position, bool locked)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->lockFrame(layerPos, position, locked);
	}
}

/**
 * @~spanish
 * Agrupa los frames desde la posición @p from hasta la posición @p to.
 */
void TimeLineWidget::groupFrames(int scenePos, int layerPos, int from, int to)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->groupFrames(layerPos, from, to);
	}
}

/**
 * @~spanish
 * Aumenta la duración del frame que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos a @p size.
 */
void TimeLineWidget::expandFrame(int scenePos, int layerPos, int position, int size)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->expandFrame(layerPos, position, size);
	}
}

/**
 * @~spanish
 * Destruye el grupo de frames que se encuentra en la posición @p position, del layer @p layerPos de la escena @p scenePos.
 */
void TimeLineWidget::destroyGroupFrames(int scenePos, int layerPos, int position, int size)
{
	FramesTable *framesTable = this->framesTable( scenePos );
	if ( framesTable )
	{
		framesTable->destroyGroupFrames(layerPos, position, size);
	}
}

/**
 * 
 */
void TimeLineWidget::playScene()
{
	if(d->timer.isActive ())
	{
		d->timer.stop();
		d->scenes.play->setIcon(DGui::IconLoader::self()->load("media-playback-start.svg"));
		return;
	}
	
	int scenePos = scene();
	
	FramesTable *framesTable = this->framesTable( scenePos );
	int layer = 0;
	if ( framesTable )
	{
		int maxFrames = 0;
	
		for(int i = 0; i < framesTable->rowCount(); i++)
		{
			int lastFrame = framesTable->lastFrameByLayer(i);
			
			if(maxFrames < lastFrame && !framesTable->isSoundLayer(i))
			{
				maxFrames = lastFrame;
				layer = i;
			}
		}
		
		setFrame(scenePos, layer, frame());
		d->scenes.play->setIcon(DGui::IconLoader::self()->load("media-playback-pause.svg"));
		d->timer.start(200);
	}
}

/**
 * @internal
 */
void TimeLineWidget::onItemClicked(QTreeWidgetItem *item, int)
{
	int index = d->scenes.container->indexOfTopLevelItem(item);
	d->frames.container->setCurrentIndex(index);
	d->layers.container->setCurrentIndex(index);
}

/**
 * @internal
 */
void TimeLineWidget::onItemChanged(QTreeWidgetItem *item, int column)
{
	Event event;
	event.scene = d->scenes.container->indexOfTopLevelItem(item);
	event.action = RenameScene;
	event.data = item->text(column);
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onFrameSelected(int layerPosition, int framePosition)
{
// 	int lastFrame = 0;
	
	emit frameSelected(d->frames.container->currentIndex(), layerPosition, framePosition);
	setLayer(d->frames.container->currentIndex(), layerPosition);
}

/**
 * @internal
 */
void TimeLineWidget::onLayerSelected(int layerPosition)
{
	setLayer(d->frames.container->currentIndex(), layerPosition);
}

/**
 * @internal
 */
void TimeLineWidget::onLayerRenamed(int layerPosition, const QString &newName)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.action = RenameLayer;
	event.data = newName;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onLayerLocked(int layerPosition, bool locked)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.action = LockLayer;
	event.data = locked;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onLayerVisibilityChanged(int layerPosition, bool isVisible)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.action = ChangeLayerVisibility;
	event.data = isVisible;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onButtonClicked(int action)
{
	Event e = d->createEvent(this, Action(action));
	
	if(e.action == InsertFrame)
	{
		int lastFrame = 0;
		
		FramesTable *frames = framesTable(scene());
		if(frames)
		{
			lastFrame = frames->lastFrameByLayer(e.layer);
		}
		
		int frame = e.frame;
		
		if(lastFrame < e.frame )
		{
			for(int i = lastFrame+1; i < frame+1; i++)
			{
				e.frame = i;
				emit postEvent(e);
			}
			return;
		}
		else
		{
			e.frame++;
		}
	}
	
	emit postEvent(e);
	
}

/**
 * @internal
 */
void TimeLineWidget::onRequestInsertFrame(int layerPosition, int framePosition)
{
	Event e = d->createEvent(this, InsertFrame);
	
	int lastFrame = 0;
	
	FramesTable *frames = framesTable(scene());
	if(frames)
	{
		lastFrame = frames->lastFrameByLayer(e.layer);
	}
	
	int frame = e.frame;
	
	if(lastFrame < e.frame )
	{
		for(int i = lastFrame+1; i < frame+1; i++)
		{
			e.frame = i;
			emit postEvent(e);
		}
		return;
	}
	else
	{
		e.frame++;
	}
	
	emit postEvent(e);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestRemoveFrame(int layerPosition, int framePosition)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = RemoveFrame;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestCopyFrame(int layerPosition, int framePosition)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = CopyFrame;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestPasteFrame(int layerPosition, int framePosition)
{
	Event e = d->createEvent(this, PasteFrame);
	
	int lastFrame = 0;
	
	FramesTable *frames = framesTable(scene());
	if(frames)
	{
		lastFrame = frames->lastFrameByLayer(e.layer);
	}
	
	int frame = e.frame;
	
	if(lastFrame < e.frame )
	{
		for(int i = lastFrame+1; i < frame+1; i++)
		{
			e.frame = i;
			emit postEvent(e);
		}
		return;
	}
	else
	{
		e.frame++;
	}
	
	emit postEvent(e);
	/*
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = PasteFrame;
	
	emit postEvent(event);*/
}

/**
 * @internal
 */
void TimeLineWidget::onRequestCutFrame(int layerPosition, int framePosition)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = CutFrame;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestLockFrame(int layerPosition, int framePosition, bool locked)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = LockFrame;
	event.data = locked;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestChangeFrameVisibility(int layerPosition, int framePosition, bool visibility)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = ChangeFrameVisibility;
	event.data = visibility;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::onRequestExpandFrame(int layerPosition, int framePosition, int size)
{
	Event event;
	event.scene = d->layers.container->currentIndex();
	event.layer = layerPosition;
	event.frame = framePosition;
	event.action = ExpandFrame;
	
	event.data = size;
	
	emit postEvent(event);
}

/**
 * @internal
 */
void TimeLineWidget::nextFrame()
{
	int scenePos = scene();
	int layerPos = layer();
	int framePos = frame();
	
	FramesTable *framesTable = this->framesTable(scenePos);
	
	if(framesTable)
	{
		if(framePos < framesTable->lastFrameByLayer(layerPos))
		{
			setFrame(scenePos, layerPos, framePos+1);
		}
		else
		{
			setFrame(scenePos, layerPos, 0);
			d->scenes.play->setIcon(DGui::IconLoader::self()->load("media-playback-start.svg"));
			d->timer.stop();
		}
	}
}

/**
 * @internal
 */
LayerManager *TimeLineWidget::layerManager(int sceneIndex)
{
	return qobject_cast<LayerManager *>(d->layers.container->widget(sceneIndex));
}

/**
 * @internal
 */
FramesTable *TimeLineWidget::framesTable(int sceneIndex)
{
	return qobject_cast<FramesTable *>(d->frames.container->widget(sceneIndex));
}

/**
 * @internal
 */
void TimeLineWidget::setupButtons()
{
	d->scenes.insert->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
	d->scenes.insert->setToolTip(tr("Insert a scene"));
	
	d->scenes.remove->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
	d->scenes.remove->setToolTip(tr("Remove the scene"));
	
	d->scenes.up->setIcon(DGui::IconLoader::self()->load("go-up.svg"));
	d->scenes.up->setToolTip(tr("Move up scene"));
	
	d->scenes.down->setIcon(DGui::IconLoader::self()->load("go-down.svg"));
	d->scenes.down->setToolTip(tr("Move down scene"));
	
	d->scenes.play->setIcon(DGui::IconLoader::self()->load("media-playback-start.svg"));
	d->scenes.play->setToolTip(tr("Play scene"));
	
	connect(d->scenes.play, SIGNAL(clicked()), this, SLOT(playScene()));
	
	d->layers.insert->setIcon(DGui::IconLoader::self()->load("add-layer.svg"));
	d->layers.insert->setToolTip(tr("Insert a layer"));
	
	d->layers.remove->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
	d->layers.remove->setToolTip(tr("Remove the layer"));
	
	d->layers.up->setIcon(DGui::IconLoader::self()->load("go-up.svg"));
	d->layers.up->setToolTip(tr("Move up layer"));
	
	d->layers.down->setIcon(DGui::IconLoader::self()->load("go-down.svg"));
	d->layers.down->setToolTip(tr("Move down layer"));
	
	d->frames.insert->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
	d->frames.insert->setToolTip(tr("Insert a frame"));
	
	d->frames.remove->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
	d->frames.remove->setToolTip(tr("Remove the frame"));
	
	d->frames.up->setIcon(DGui::IconLoader::self()->load("go-previous.svg"));
	d->frames.up->setToolTip(tr("Move up frame"));
	
	d->frames.down->setIcon(DGui::IconLoader::self()->load("go-next.svg"));
	d->frames.down->setToolTip(tr("Move down frame"));
	
	d->actions.addButton(d->scenes.insert, InsertScene);
	d->actions.addButton(d->scenes.remove, RemoveScene);
	d->actions.addButton(d->scenes.up, MoveSceneUp);
	d->actions.addButton(d->scenes.down, MoveSceneDown);
	
	d->actions.addButton(d->layers.insert, InsertLayer);
	d->actions.addButton(d->layers.remove, RemoveLayer);
	d->actions.addButton(d->layers.up, MoveLayerUp);
	d->actions.addButton(d->layers.down, MoveLayerDown);
	
	d->actions.addButton(d->frames.insert, InsertFrame);
	d->actions.addButton(d->frames.remove, RemoveFrame);
	d->actions.addButton(d->frames.up, MoveFrameUp);
	d->actions.addButton(d->frames.down, MoveFrameDown);
}

}
}
