TEMPLATE = lib

CONFIG += warn_on \
staticlib
SOURCES += framestable.cpp \
layermanager.cpp \
tlruler.cpp \
timeline.cpp \
timelinewidget.cpp
HEADERS += framestable.h \
layermanager.h \
tlruler.h \
timeline.h \
timelinewidget.h

include($$PWD/../components_config.pri)


FORMS += frames.ui \
layers.ui \
scenes.ui
