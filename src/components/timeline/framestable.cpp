/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "framestable.h"

#include <QPainter>
#include <QPaintEvent>
#include <QItemSelectionModel>
#include <QPainterPath>
#include <QScrollBar>
#include <QHeaderView>
#include <QMenu>
#include <QInputDialog>
#include <QApplication>
#include <QClipboard>

#include <dcore/debug.h>
#include <dcore/algorithm.h>

#include "tlruler.h"

namespace Dash {
namespace Component {

////////// FramesTableItemDelegate ///////////

class FramesTableItemDelegate : public QAbstractItemDelegate
{
	public:
		FramesTableItemDelegate(QObject * parent = 0 );
		~FramesTableItemDelegate();
		virtual void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
		virtual QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
};

/**
 * 
 */
FramesTableItemDelegate::FramesTableItemDelegate(QObject * parent) :  QAbstractItemDelegate(parent)
{
}

FramesTableItemDelegate::~FramesTableItemDelegate()
{
}



void FramesTableItemDelegate::paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	Q_ASSERT(index.isValid());
	
	FramesTable *table = qobject_cast<FramesTable *>(index.model()->parent());
	FramesTableItem *item = dynamic_cast<FramesTableItem *>(table->itemFromIndex(index));
	
	QVariant value;
	QStyleOptionViewItem opt = option;
	
	// draw the background color
	value = index.data( Qt::BackgroundColorRole );
	
	if (value.isValid())
	{
		painter->save();
		
		bool sound = table->isSoundLayer(index.row());
		
		if( !sound )
		{
			painter->fillRect(option.rect, value.value<QColor>() );
		}
		painter->restore();
	}
	else
	{
		painter->save();
		
		bool sound = table->isSoundLayer(index.row());
		painter->setPen(QPen(Qt::lightGray, 1));
		if( !sound )
		{
			if ( index.column() % 5 == 0 )
			{
				painter->setBrush(Qt::lightGray);
				painter->drawRect(option.rect ); //FIXME: user paleta de colores
			}
			else
			{
				painter->setBrush(Qt::white);
				painter->drawRect(option.rect);//FIXME: user paleta de color
			}
		}
		else
		{
			painter->fillRect(option.rect, Qt::white);
		}
		
		painter->restore();
	}
	
	// Draw attributes
	
	int offset = option.rect.width() - 4;
	
	if ( item && index.isValid() )
	{
		if(item->isUsed() )
		{
			painter->save();
			painter->setBrush(Qt::black);
			
			if( !item->isSound() )
			{
				QColor color = Qt::white; //FIXME: user paleta de color
				if ( item->inGroupPosition() == FramesTableItem::Begin )
				{
					painter->fillRect(option.rect, color);
					painter->drawLine(option.rect.topLeft(), option.rect.bottomLeft());
					painter->drawEllipse(option.rect.left()+2, option.rect.bottom() - (offset+2), offset, offset);
				}
				else if ( item->inGroupPosition() == FramesTableItem::End )
				{
					painter->fillRect(option.rect, color );
					painter->drawLine(option.rect.topRight(), option.rect.bottomRight());
				}
				else if( item->inGroupPosition() == FramesTableItem::Intermediate )
				{
					painter->fillRect(option.rect, color );
				}
				else
				{
					painter->drawEllipse(option.rect.left()+2, option.rect.bottom() - (offset+2), offset, offset);
				}
			}
			else
			{
				painter->setPen(QPen(Qt::blue, 1));
				
				int y = option.rect.center().y();
				int xi = option.rect.x();
				int xf = option.rect.width();
				
				painter->drawLine(QPoint(xi, y) , QPoint(xf, y));
			}
			
			painter->restore();
		}
		
		if ( item->isLocked() )
		{
			painter->save();
			painter->setBrush(Qt::red);
			
			painter->drawEllipse(option.rect.left()+2, option.rect.bottom() - (offset+2), offset, offset);
			
			painter->restore();
		}
	}
	
	// Selection!
	if (option.showDecorationSelected && (option.state & QStyle::State_Selected))
	{
		QPalette::ColorGroup cg = option.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
		
		painter->save();
		painter->setPen(QPen(option.palette.brush(cg, QPalette::Highlight), 2));
		
		QRectF rectSelection = option.rect.adjusted(1,1,-1,-1);
		painter->drawRect(rectSelection);
		painter->restore();
	}
	
	painter->save();
	QPalette::ColorGroup cg = option.state & QStyle::State_Enabled ? QPalette::Normal : QPalette::Disabled;
	painter->setPen(QPen(option.palette.brush(cg, QPalette::Foreground), 1)),
	
	painter->drawLine(option.rect.bottomLeft(), option.rect.bottomRight());
	painter->drawLine(option.rect.topLeft(), option.rect.topRight());
	painter->restore();
}

QSize FramesTableItemDelegate::sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	Q_ASSERT(index.isValid());
	const QAbstractItemModel *model = index.model();
	Q_ASSERT(model);

	QVariant value = model->data(index, Qt::FontRole);
	QFont fnt = value.isValid() ? qvariant_cast<QFont>(value) : option.font;
	QString text = model->data(index, Qt::DisplayRole).toString();
	QRect pixmapRect;
	if (model->data(index, Qt::DecorationRole).isValid())
		pixmapRect = QRect(0, 0, option.decorationSize.width(),
				   option.decorationSize.height());

	QFontMetrics fontMetrics(fnt);
	
	return (pixmapRect).size();
}


////////// FramesTableItem ////////

FramesTableItem::FramesTableItem()
{
}

FramesTableItem::~FramesTableItem()
{
}


bool FramesTableItem::isUsed()
{
	return data(IsUsed).toBool();
}

bool FramesTableItem::isLocked()
{
	return data(IsLocked).toBool();
}

bool FramesTableItem::isVisible()
{
	return data(IsVisible).toBool();
}

bool FramesTableItem::isSound()
{
	QVariant data = this->data(IsSound);
	
	if( data.canConvert<bool>() )
	{
		return data.toBool();
	}
	return false;
}

bool FramesTableItem::isClone()
{
	QVariant data = this->data(IsClone);
	
	if( data.canConvert<bool>() )
	{
		return data.toBool();
	}
	return false;
}

FramesTableItem::InGroupPositionType FramesTableItem::inGroupPosition()
{
	QVariant data = this->data(InGroupPosition);
	
	if( data.canConvert<int>() )
	{
		return InGroupPositionType(data.toInt());
	}
	return None;
}


//// FramesTable

struct FramesTable::Private
{
	struct LayerItem
	{
		LayerItem() : lastItem(-1), sound(false) {};
		int lastItem;
		bool sound;
	};
	
	
	void swapItem( QTableWidgetItem *it, QTableWidgetItem *nextIt  )
	{
		QVariant data = it->data(FramesTableItem::IsUsed);
			
		it->setData(FramesTableItem::IsUsed, nextIt->data(FramesTableItem::IsUsed).toBool() );
		nextIt->setData(FramesTableItem::IsUsed, data);
		
		data = it->data(FramesTableItem::IsLocked);
		it->setData(FramesTableItem::IsLocked, nextIt->data(FramesTableItem::IsLocked).toBool());
		nextIt->setData(FramesTableItem::IsLocked, data);
		
		
		data = it->data(FramesTableItem::IsVisible);
		it->setData(FramesTableItem::IsVisible, nextIt->data(FramesTableItem::IsVisible).toBool());
		nextIt->setData(FramesTableItem::IsVisible, data);
		
		data = it->data(FramesTableItem::IsSound);
		it->setData(FramesTableItem::IsSound, nextIt->data(FramesTableItem::IsSound).toBool());
		nextIt->setData(FramesTableItem::IsSound, data);
		
		data = it->data(FramesTableItem::InGroupPosition);
		it->setData(FramesTableItem::InGroupPosition, nextIt->data(FramesTableItem::InGroupPosition));
		nextIt->setData(FramesTableItem::InGroupPosition, data);
		
	}
	
	int groupSize(FramesTable *table, int layer, int beginFrame)
	{
		QTableWidgetItem *item = table->item(layer, beginFrame);
		
		if( ! item )
		{
			return 0;
		}
		
		int count = 0;
		
		dfDebug << "begin Frame " << beginFrame << " type " << item->data(FramesTableItem::InGroupPosition).toInt();
		
		if(item->data(FramesTableItem::InGroupPosition).toInt() == FramesTableItem::Begin)
		{
			while(item && item->data(FramesTableItem::InGroupPosition).toInt() != FramesTableItem::End)
			{
				count++;
				item = table->item(layer, beginFrame+count);
			}
		}
		
		return count+1;
	}
	
	int rectWidth, rectHeight;
	QList<LayerItem> layers;
	TLRuler *ruler;
	
	enum Action {
		InsertFrame = 0,
		RemoveFrame,
		ToggleLockFrame,
		ToggleFrameVisibility,
		ExpandFrame,
		CopyFrame,
		PasteFrame,
		CutFrame
	};
	
};

/**
 * @~spanish
 * Contructor
 */
FramesTable::FramesTable(QWidget *parent) : QTableWidget(0, 100, parent), d(new Private)
{
	d->ruler = new TLRuler;
	setup();
}

/**
 * @~spanish
 * Destructor
 */
FramesTable::~FramesTable()
{
	delete d;
}

/**
 * @internal
 */
void FramesTable::setup()
{
	setItemDelegate( new FramesTableItemDelegate(this) );
	
	setSelectionBehavior(QAbstractItemView::SelectItems);
	setSelectionMode (QAbstractItemView::SingleSelection);
	
	setHorizontalHeader(d->ruler);
	
	connect(d->ruler, SIGNAL(logicalSectionSelected( int )), this, SLOT(emitFrameSelected( int )));
// 	connect(this, SIGNAL(currentItemChanged( QTableWidgetItem *, QTableWidgetItem *)), this, SLOT(emitFrameSelected(QTableWidgetItem *, QTableWidgetItem *)));
	verticalHeader()->hide();
	
	
	connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(onItemSelectionChanged()));
	
	connect( this, SIGNAL(cellDoubleClicked ( int , int )), this, SLOT(onCellDoubleClicked(int, int)));
	
	setItemSize( 10, 25 );
	
	horizontalHeader()->setResizeMode(QHeaderView::Custom);
	verticalHeader()->setResizeMode(QHeaderView::Custom);
	
	
	setSelectionMode(QAbstractItemView::ContiguousSelection);
	setShowGrid(false);
}

/**
 * @internal
 */
void FramesTable::emitFrameSelected(int col)
{
	selectColumn(col);
	
	FramesTableItem *item = dynamic_cast<FramesTableItem *>(this->item(currentRow(), col));
	
	if( item )
	{
		if( item->isUsed())
		{
			emit frameSelected(visualRow(this->row(item)), visualColumn(this->column(item)));
		}
	}
}

/**
 * @internal
 */
void FramesTable::onItemSelectionChanged()
{
	FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
	if( item )
	{
		if( item->isUsed())
		{
			emit frameSelected(visualRow(this->row(item)), visualColumn(this->column(item)));
		}
	}
	else
	{
		emit layerSelected(visualRow(currentRow()));
	}
}

/**
 * @~spanish
 * Función sobrecargada para la implementación del menú contextual.
 */
void FramesTable::mousePressEvent ( QMouseEvent * event )
{
	
	QTableWidget::mousePressEvent(event);
	if(event->buttons() == Qt::RightButton )
	{
		QMenu *menu = new QMenu(tr("Frames"), this);
		
		FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
		
		menu->addAction( tr("Insert frame"))->setData(Private::InsertFrame);
		
		if(item)
		{
			if( item->isUsed())
			{
				menu->addAction( tr("Remove frame"))->setData(Private::RemoveFrame);
				menu->addAction( tr("Expand this frame"))->setData(Private::ExpandFrame);
				menu->addSeparator();
				menu->addAction(tr("Copy frame"))->setData(Private::CopyFrame);
				
				menu->addAction(tr("Cut frame"))->setData(Private::CutFrame);
			}
			else
			{
				menu->addSeparator();
			}
		}
		
		if(const QMimeData *mimeData = QApplication::clipboard()->mimeData())
		{
			if(mimeData->hasFormat("yamf-frame"))
			{
				menu->addAction( tr("Paste frame"))->setData(Private::PasteFrame);
				menu->addSeparator();
			}
		}
		
		if(item)
		{
			if( item->isLocked())
			{
				menu->addAction( tr("Unlock this frame"))->setData(Private::ToggleLockFrame);
			}
			else
			{
				menu->addAction( tr("Lock this frame"))->setData(Private::ToggleLockFrame);
			}
			
			if(item->isVisible())
			{
				menu->addAction( tr("Hide this frame"))->setData(Private::ToggleFrameVisibility);
			}
			else
			{
				menu->addAction( tr("Show this frame"))->setData(Private::ToggleFrameVisibility);
			}
		}
		
		connect(menu, SIGNAL(triggered(QAction *)), this, SLOT(onMenuActionTriggered(QAction * )));
		
		menu->exec( event->globalPos() );
	}
}

/**
 * @internal
 */
void FramesTable::onCellDoubleClicked(int row, int column)
{
	row = visualRow(row);
	if( lastFrameByLayer(row)+1 == column )
	{
		emit requestInsertFrame(row, column);
	}
}

/**
 * @internal
 */
void FramesTable::onMenuActionTriggered(QAction * action)
{
	int layerPos = visualRow(currentRow());
	int framePos = visualColumn(currentColumn());
	
	switch(action->data().toInt())
	{
		case Private::InsertFrame:
		{
			FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
			if(item)
			{
				if(item->isUsed())
				{
					emit requestInsertFrame(layerPos, framePos+1);
				}
			}
			else
			{
				emit requestInsertFrame(layerPos, framePos);
			}
			
		}
		break;
		case Private::RemoveFrame:
		{
			emit requestRemoveFrame(layerPos, framePos);
		}
		break;
		case Private::CopyFrame:
		{
			emit requestCopyFrame(layerPos, framePos);
		}
		break;
		case Private::PasteFrame:
		{
			emit requestPasteFrame(layerPos, framePos);
		}
		break;
		case Private::CutFrame:
		{
			emit requestCutFrame(layerPos, framePos);
		}
		break;
		case Private::ToggleLockFrame:
		{
			FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
			if(item)
			{
				 emit requestLockFrame(layerPos, framePos, !item->isLocked());
			}
		}
		break;
		case Private::ToggleFrameVisibility:
		{
			FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
			if(item)
			{
				 emit requestChangeFrameVisibility(layerPos, framePos, !item->isVisible());
			}
		}
		break;
		case Private::ExpandFrame:
		{
			FramesTableItem *item = dynamic_cast<FramesTableItem *>(currentItem());
			if(item)
			{
				bool ok = false;
				int size = QInputDialog::getInteger ( 0, tr("Expand frame"), tr("number of frames to enlarge this frame?"), 1, 1, 2000, 1,  &ok);
				if(ok)
				{
					emit requestExpandFrame(layerPos, framePos, size);
				}
			}
		}
		break;
	}
}

/**
 * @~spanish
 * Cambia el tamaño de los frames.
 */
void FramesTable::setItemSize(int w, int h)
{
	d->rectHeight = h;
	d->rectWidth = w;
	
	fixSize();
}

/**
 * @~spanish
 * Extiende el largo de un frame, para permitir la realimentación gráfica de la expansión del tiempo de un frame.
 */
void FramesTable::expandFrame( int layerPosition, int position, int size )
{
	groupFrames( layerPosition, position, position+size);
	
	for(int i = position; i < position+size; i++)
	{
		QTableWidgetItem *item = this->item(layerPosition, i);
		item->setData(FramesTableItem::IsClone, true);
	}
	
}

/**
 * @~spanish
 * Agrupa los frames de la posición @p from a la @p to del layer, para permitir la realimentación gráfica de la agrupación de un conjunto de frames.
 */
void FramesTable::groupFrames( int layerPosition, int from, int to )
{
	for(int i = from+1; i <= to; i++)
	{
		QTableWidgetItem *item = this->item(layerPosition, i);
		
		if(item)
		{
			item->setData(FramesTableItem::InGroupPosition, QVariant(  FramesTableItem::Intermediate));
		}
	}
	
	QTableWidgetItem *begin = this->item(layerPosition, from);
		
	if(begin)
	{
		begin->setData(FramesTableItem::InGroupPosition ,QVariant(FramesTableItem::Begin));
	}
	
	QTableWidgetItem *end = this->item(layerPosition, to);
		
	if(end)
	{
		QTableWidgetItem *nextEnd = this->item(layerPosition, to+1);
		
		if(nextEnd)
		{
			if(nextEnd->data(FramesTableItem::InGroupPosition).toInt() != FramesTableItem::None || nextEnd->data(FramesTableItem::InGroupPosition).toInt() != FramesTableItem::Begin)
			{
				end->setData(FramesTableItem::InGroupPosition, QVariant(FramesTableItem::Intermediate));
				viewport()->update();
				return;
			}
		}
		end->setData(FramesTableItem::InGroupPosition, QVariant(FramesTableItem::End));
		
	}
	viewport()->update();
}

/**
 * @~spanish
 * Destruye el grupo de frames que esta en la posición @p position del layer @p layerPosition, para permitir la realimentación gráfica de la destrucción de un conjunto de frames.
 */
void FramesTable::destroyGroupFrames(int layerPosition, int position, int size)
{
	for(int i = position; i <= position+size; i++)
	{
		QTableWidgetItem *item = this->item(layerPosition, i);
		
		if(item)
		{
			item->setData(FramesTableItem::InGroupPosition, QVariant(  FramesTableItem::None));
			item->setData(FramesTableItem::IsClone, false);
		}
	}
}

/**
 * @~spanish
 * Retorna verdadero si el layer en la posición @p row es de sonido, falso de lo contrario.
 */
bool FramesTable::isSoundLayer(int row)
{
	if( row < 0 && row >= d->layers.count() )
		return false;
	
	return d->layers[row].sound;
}

/**
 * @~spanish
 * Inserta un layer en la posición @p pos con el nombre @p name, permitiendo la realimentación gráfica de la inserción de un layer.
 */
void FramesTable::insertLayer(int pos, const QString &name)
{
	Q_UNUSED(name);
	insertRow( pos );
	
	Private::LayerItem layer;
	layer.sound = false;
	d->layers.insert(pos, layer);
	
	fixSize();
}

/**
 * @~spanish
 * Inserta un layer de sonido en la posicion @p pos con el nombre @p name. permitiendo la realimentación gráfica de la inserción de un layer de sonido.
 */
void FramesTable::insertSoundLayer(int layerPos, const QString &name)
{
	Q_UNUSED(name);
	insertRow(layerPos);
	
	Private::LayerItem layer;
	layer.sound = true;
	d->layers.insert(layerPos, layer);
	
	fixSize();
}

/**
 * @~spanish
 * Elimina el layer seleccionado actualmente, permitiendo la realimentación gráfica de la eliminación del layer actual.
 */
void FramesTable::removeCurrentLayer()
{
	int pos = verticalHeader()->logicalIndex(currentRow());
	removeLayer(pos);
}

/**
 * @~spanish
 * Elimina el layer de la posición @p pos, permitiendo la realimentación gráfica de la eliminación de un layer.
 */
void FramesTable::removeLayer(int pos)
{
	pos = verticalHeader()->logicalIndex(pos);
	removeRow( pos );
	d->layers.removeAt(pos);
}

/**
 * @~spanish
 * Mueve el layer de la posición @p position a la posición @p newPosition.
 */
void FramesTable::moveLayer(int position, int newPosition)
{
	if ( position < 0 || position >= rowCount() || newPosition < 0 || newPosition >= rowCount() ) return;
	
	blockSignals(true);
	
	verticalHeader()->moveSection(position, newPosition);
	
	blockSignals(false);
}

/**
 * @~spanish
 * Obtiene la posición del ultimo frame del layer que esta en la posición @p layerPos.
 */
int FramesTable::lastFrameByLayer(int layerPos)
{
	int pos = verticalHeader()->logicalIndex(layerPos);
	if ( pos < 0 || pos > d->layers.count() )
	{
		return -1;
	}
	return d->layers[pos].lastItem;
}

// FRAMES

/**
 * @~spanish
 * Añade un frame al layer de la posicion @p layerPos, con el nombre @p name.
 */
void FramesTable::addFrame(int layerPos, const QString &name)
{
	Q_UNUSED(name);
	if ( layerPos < 0 || layerPos >= d->layers.count() ) return;
	
	insertFrame(layerPos, lastFrameByLayer(verticalHeader()->logicalIndex(layerPos))+1, name)	;
}

/**
 * @~spanish
 * Inserta un frame en la posición @p framePos del layer @p layerPos, con el nombre @p name.
 */
void FramesTable::insertFrame(int layerPos, int framePos, const QString &name)
{
	Q_UNUSED(name);
	
	if ( layerPos < 0 || layerPos >= d->layers.count() ) return;
	
	layerPos = verticalHeader()->logicalIndex(layerPos);
	
	d->layers[layerPos].lastItem++;
	
	int position = framePos;
	
	if(!d->layers[layerPos].sound)
	{
		position = d->layers[layerPos].lastItem;
	}
	
	setAttribute( layerPos, position, FramesTableItem::IsUsed, true);
	setAttribute( layerPos, position, FramesTableItem::IsVisible, true);
	setAttribute( layerPos, position, FramesTableItem::IsSound, d->layers[layerPos].sound);
	item( layerPos, position)->setData(FramesTableItem::InGroupPosition, FramesTableItem::None);
	
	if(!d->layers[layerPos].sound)
	{
		moveFrame(layerPos, position, framePos);
	}
	if ( d->layers[layerPos].lastItem >= columnCount()-1 )
	{
		setColumnCount( d->layers[layerPos].lastItem+50 );
		fixSize();
	}
	
	viewport()->update();
}

/**
 * @~spanish
 * Selecciona el frame, representado por el ítem @p item.
 */
void FramesTable::setCurrentFrame(FramesTableItem *item)
{
	setCurrentItem(item);
}

/**
 * @~spanish
 * Selecciona el layer de la posición @p layerPos.
 */
void FramesTable::setCurrentLayer(int layerPos)
{
	setCurrentItem(item(verticalHeader()->logicalIndex(layerPos), 0));
}

/**
 * @~spanish
 * Selecciona el frame la posición @p index del layer actual.
 */
void FramesTable::selectFrame(int index)
{
	setCurrentItem( item( currentRow(), index ) );
}

/**
 * @~spanish
 * Remueve el frame que esta en la posición @p position del layer @p layerPos.
 */
void FramesTable::removeFrame(int layerPos, int position)
{
	if ( layerPos < 0 || layerPos >= d->layers.count() )
	{
		return;
	}
	
	QTableWidgetItem *it = this->item(layerPos, position);
	
	moveFrame(layerPos, position, d->layers[layerPos].lastItem);
	
	QTableWidgetItem *toRemove = takeItem(layerPos, d->layers[layerPos].lastItem);
	
	int type = toRemove->data(FramesTableItem::InGroupPosition).toInt();
	
	if(type == FramesTableItem::Begin)
	{
		it = item(layerPos, position);
		
		if(it)
		{
			if(it->data(FramesTableItem::InGroupPosition).toInt() == FramesTableItem::Intermediate)
			{
				it->setData(FramesTableItem::InGroupPosition, toRemove->data(FramesTableItem::InGroupPosition));
			}
			else if(it->data(FramesTableItem::InGroupPosition).toInt() == FramesTableItem::End)
			{
				it->setData(FramesTableItem::InGroupPosition, FramesTableItem::None);
				it->setData(FramesTableItem::IsClone, false);
			}
		}
	}
	else if(toRemove->data(FramesTableItem::InGroupPosition).toInt() == FramesTableItem::End)
	{
		it = item(layerPos, position);
		if(it)
		{
			if(it->data(FramesTableItem::InGroupPosition).toInt() == FramesTableItem::Begin)
			{
				it->setData(FramesTableItem::InGroupPosition, FramesTableItem::None);
			}
		}
		
	}
	
	delete toRemove;
	toRemove = 0;
	
	d->layers[layerPos].lastItem--;
	
	viewport()->update( visualRect(indexFromItem(item(layerPos, position) )) );
	viewport()->update();
}

/**
 * @~spanish
 * Mueve el frame de la posición @p from a la posición @p to dentro del layer @p layerPos.
 */
void FramesTable::moveFrame(int layerPos, int from, int to)
{
	if(from == to)
	{
		return;
	}
	
	layerPos = verticalHeader()->logicalIndex(layerPos);
	
	if(to > from)
	{
		int fromSize = d->groupSize(this, layerPos, from);
		int toSize = d->groupSize(this, layerPos, to);
		
		for(int i = 0; i < fromSize; i++)
		{
			for(int index = from; index < to+toSize-1; index++ )
			{
				QTableWidgetItem *it = item(layerPos, index);
				QTableWidgetItem *nextIt = item(layerPos, index+1);
				if(nextIt)
				{
					d->swapItem(it, nextIt);
				}
			}
		}
	}
	else
	{
		int fromSize = d->groupSize(this, layerPos, from);
		int toSize = d->groupSize(this, layerPos, to);
		Q_UNUSED(toSize);
		
		for(int i = 0; i < fromSize; i++)
		{
			for(int index = from+i; index > to+i; index-- )
			{
				QTableWidgetItem *it = item(layerPos, index);
				QTableWidgetItem *nextIt = item(layerPos, index-1);
				if(nextIt)
				{
					d->swapItem(it, nextIt);
				}
			}
		}
	}
	
	viewport()->update();
}

/**
 * @spanish
 * Actualiza el estado de bloqueo del frame que esta en la posición @p postín del layer @p layerPos al valor de @p locked.
 */
void FramesTable::lockFrame(int layerPos, int position, bool locked)
{
	if ( layerPos < 0 || layerPos >= d->layers.count() )
	{
		return;
	}
	
	layerPos = verticalHeader()->logicalIndex(layerPos);
	
	setAttribute( layerPos, position, FramesTableItem::IsLocked, locked );
	
	viewport()->update();
}

/**
 * @~spanish
 * Actualiza el estado de visibilidad del frame que esta en la posición @p position del layer @p layerPosition a el valor de @p isVisible.
 */
void FramesTable::setFrameVisibility(int layerPosition, int position, bool isVisible)
{
	if ( layerPosition < 0 || layerPosition >= d->layers.count() )
	{
		return;
	}
	
	layerPosition = verticalHeader()->logicalIndex(layerPosition);
	
	setAttribute( layerPosition, position, FramesTableItem::IsVisible, isVisible );
	
	viewport()->update();
}

/**
 * @~spanish
 * Modifica el estado de un frame.
 */
void FramesTable::setAttribute(int row, int col, FramesTableItem::Attributes att, bool value)
{
	QTableWidgetItem *item = this->item(row, col);
	
	if( !item )
	{
		item = new FramesTableItem;
		setItem(row, col, item);
	}
	
	item->setData(att, value);
}

/**
 * @~spanish
 * Arregla los tamaños de los frames mostrados en la tabla.
 */
void FramesTable::fixSize()
{
	for(int column = 0; column < columnCount(); column++)
	{
		horizontalHeader()->resizeSection(column, d->rectWidth);
	}
	for( int row = 0; row < rowCount(); row++)
	{
		verticalHeader()->resizeSection(row, d->rectHeight);
	}
}

}
}
