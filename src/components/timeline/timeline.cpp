/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QList>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QButtonGroup>

#include "timeline.h"
#include <dgui/application.h>
#include <dcore/debug.h>
#include <dgui/iconloader.h>

#include <yamf/model/scene.h>
#include <yamf/model/layer.h>
#include <yamf/model/frame.h>
#include <yamf/model/library.h>
#include <yamf/model/libraryobject.h>

// Dash
#include "dash/datasource.h"
#include "dash/project.h"

#include <QDebug>
#include <QXmlStreamReader>


namespace Dash {
namespace Component {

struct TimeLine::Private
{
	Private(TimeLine *q) :  q(q), tlWidget(0) {}
	
	
	TimeLine *q;
	TimeLineWidget *tlWidget;
	
	void updateScene( const Dash::DataSource * datasource, bool undo )
	{
		int visualIndex = datasource->values("location")["scene"].toInt();
		
		QString root = datasource->values("command")["name"];
		if( root == "insertscene")
		{
			if( undo )
			{
				tlWidget->removeScene(visualIndex);
			}
			else
			{
				tlWidget->insertScene(visualIndex, datasource->values("scene")["name"]);
				tlWidget->setScene(visualIndex);
			}
		}
		else if(root == "removescene")
		{
			if( undo )
			{
				
				YAMF::Model::Scene *scene = q->project()->scene(visualIndex);
				
				tlWidget->insertScene(visualIndex, scene->sceneName());
				
				foreach(YAMF::Model::Layer *layer , scene->layers().values())
				{
					int layerIndex = layer->visualIndex();
					tlWidget->insertLayer(visualIndex, layerIndex, layer->layerName());
					
					tlWidget->setLayerVisibility(visualIndex, layerIndex, layer->isVisible());
					tlWidget->setLayerLocked(visualIndex, layerIndex, layer->isLocked());
					
					foreach(YAMF::Model::Frame *frame, layer->frames().values())
					{
						tlWidget->insertFrame(visualIndex, layerIndex, frame->visualIndex(), frame->frameName());
						
						tlWidget->setFrameVisibility(visualIndex, layerIndex, frame->visualIndex(), frame->isVisible());
						tlWidget->setFrameLocked(visualIndex, layerIndex, frame->visualIndex(),frame->isLocked());
					}
				}
				tlWidget->setScene(visualIndex);
			}
			else
			{
				tlWidget->removeScene(visualIndex);
			}
		}
		else if(root == "movescene")
		{
			int to = datasource->values("information")["to"].toInt();
			int from = datasource->values("information")["from"].toInt();
			
			if( undo )
			{
				tlWidget->moveScene(to, from);
				tlWidget->setScene(from);
			}
			else
			{
				tlWidget->moveScene(from, to);
				tlWidget->setScene(to);
			}
		}
		else if(root == "renamescene")
		{
			tlWidget->blockSignals(true);
			if(undo)
			{
				tlWidget->renameScene(datasource->values("location")["scene"].toInt(), datasource->values("information")["oldname"]);
			}
			else
			{
				tlWidget->renameScene(datasource->values("location")["scene"].toInt(), datasource->values("information")["newname"]);
			}
			tlWidget->blockSignals(false);
		}
	}
	
	void updateLayer( const Dash::DataSource * datasource, bool undo )
	{
		int index = datasource->values("location")["layer"].toInt();
		QString root = datasource->values("command")["name"];
		
		if(root == "insertlayer")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int type = datasource->values("layer")["type"].toInt();
			
			if( undo )
			{
				tlWidget->removeLayer(scene, index);
			}
			else
			{
				switch(type)
				{
					case YAMF::Model::Layer::Vectorial:
					{
						tlWidget->insertLayer(scene, index, datasource->values("layer")["name"]);
						tlWidget->setLayer(scene, index);
					}
					break;
					case YAMF::Model::Layer::Audio:
					{
						tlWidget->insertAudioLayer(scene, index, datasource->values("layer")["name"], 0);
						tlWidget->setLayer(scene, index);
					}
				}
			}
		}
		else if(root == "removelayer")
		{
			int sceneIndex = datasource->values("location")["scene"].toInt();
// 			int type = datasource->values("layer")["type"].toInt();
			
			if( undo )
			{
				YAMF::Model::Scene *scene = q->project()->scene(sceneIndex);
				YAMF::Model::Layer *layer = scene->layer( index );
				
				tlWidget->insertLayer(sceneIndex, index, layer->layerName());
				
				foreach(YAMF::Model::Frame *frame, layer->frames().values())
				{
					tlWidget->insertFrame(sceneIndex, index, frame->visualIndex(), frame->frameName());
					
					tlWidget->setFrameVisibility(sceneIndex, index, frame->visualIndex(), frame->isVisible());
					tlWidget->setFrameLocked(sceneIndex, index, frame->visualIndex(),frame->isLocked());
				}
				
				tlWidget->setLayer(sceneIndex, index);
			}
			else
			{
				tlWidget->removeLayer(sceneIndex, index);
			}
		}
		else if( root == "locklayer")
		{
			int scene = datasource->values("location")["scene"].toInt();
			bool locked(datasource->values("information")["locked"].toInt());
			
			if( undo )
			{
				tlWidget->setLayerLocked(scene, index, !locked);
			}
			else
			{
				tlWidget->setLayerLocked(scene, index, locked);
			}
		}
		else if( root == "changelayervisibility")
		{
			int scene = datasource->values("location")["scene"].toInt();
			
			bool visible(datasource->values("information")["visible"].toInt());
			
			if( undo )
			{
				tlWidget->setLayerVisibility(scene, index, !visible);
			}
			else
			{
				tlWidget->setLayerVisibility(scene,  index, visible);
			}
		}
		else if(root == "movelayer")
		{
			int scene = datasource->values("location")["scene"].toInt();
			
			int from = datasource->values("information")["from"].toInt();
			int to = datasource->values("information")["to"].toInt();
			
			if( undo )
			{
				tlWidget->moveLayer(scene, to, from);
				
				tlWidget->setLayer(scene, to);
			}
			else
			{
				tlWidget->moveLayer(scene, from, to);
				
				tlWidget->setLayer(scene, to);
				
			}
		}
		else if(root == "renamelayer")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int index = datasource->values("location")["layer"].toInt();
			
			tlWidget->blockSignals(true);
			if(undo)
			{
				tlWidget->renameLayer(scene, index, datasource->values("information")["oldname"]);
			}
			else
			{
				tlWidget->renameLayer(scene, index, datasource->values("information")["newname"]);
			}
			tlWidget->blockSignals(false);
		}
	}
	
	void updateFrame( const Dash::DataSource * datasource, bool undo )
	{
		int visualIndex = datasource->values("location")["frame"].toInt();
		
		QString root = datasource->values("command")["name"];
		if(root == "insertframe")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			
			if( undo )
			{
				tlWidget->removeFrame(scene, layer, visualIndex);
			}
			else
			{
				tlWidget->insertFrame(scene, layer, visualIndex, datasource->values("frame")["name"]);
				tlWidget->setFrame(scene, layer, visualIndex);
			}
		}
		else if(root == "removeframe")
		{
			int sceneIndex = datasource->values("location")["scene"].toInt();
			int layerIndex = datasource->values("location")["layer"].toInt();
			
			if( undo )
			{
				tlWidget->insertFrame(sceneIndex, layerIndex, visualIndex, datasource->values("frame")["name"]);
			}
			else
			{
				tlWidget->removeFrame(sceneIndex, layerIndex, visualIndex);
			}
		}
		else if( root == "lockframe")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			
			bool locked(datasource->values("information")["locked"].toInt());
			
			if( undo )
			{
				tlWidget->setFrameLocked(scene, layer, visualIndex, !locked);
			}
			else
			{
				tlWidget->setFrameLocked(scene, layer, visualIndex, locked);
			}
		}
		else if( root == "changeframevisibility")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			
			bool visible(datasource->values("information")["visible"].toInt());
			
			if( undo )
			{
				tlWidget->setFrameVisibility(scene, layer, visualIndex, !visible);
			}
			else
			{
				tlWidget->setFrameVisibility(scene, layer, visualIndex, visible);
			}
		}
		else if(root == "moveframe")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			
			int from = datasource->values("information")["from"].toInt();
			int to = datasource->values("information")["to"].toInt();
			
			dfDebug << "move from " << from << " to " << to;
			
			if( undo )
			{
				tlWidget->moveFrame(scene, layer, to, from);
				tlWidget->setFrame(scene, layer, from);
			}
			else
			{
				tlWidget->moveFrame(scene, layer, from, to);
				tlWidget->setFrame(scene, layer, to);
			}
		}
		
		else if(root == "renameframe")
		{
			tlWidget->blockSignals(true);
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			
			if(undo)
			{
				tlWidget->renameFrame(scene, layer, datasource->values("location")["frame"].toInt(), datasource->values("information")["oldname"]);
			}
			else
			{
				tlWidget->renameFrame(scene, layer, datasource->values("location")["frame"].toInt(), datasource->values("information")["newname"]);
			}
			tlWidget->blockSignals(false);
		}
		else if(root == "expandframe")
		{
			int scene = datasource->values("location")["scene"].toInt();
			int layer = datasource->values("location")["layer"].toInt();
			int size = datasource->values("information")["size"].toInt();
			
			if( undo )
			{
				for(int i = 0; i < size; i++)
				{
					tlWidget->removeFrame(scene, layer, visualIndex+1);
				}
				tlWidget->destroyGroupFrames(scene, layer, visualIndex, size);
			}
			else
			{
				for(int i = 1; i < size+1; i++)
				{
					tlWidget->insertFrame(scene, layer, visualIndex+i, datasource->values("information")["name"]);
					tlWidget->setFrame(scene, layer, visualIndex+i);
				}
				tlWidget->groupFrames(scene, layer, visualIndex, visualIndex+size);
			}
		}
	}
};

/**
 * @~spanish
 * Constructor
 */
TimeLine::TimeLine(Dash::Project *const project, QWidget *parent) : Widget(project, parent), d(new Private(this))
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	setWindowTitle(tr("&Time Line"));
	setWindowIcon( DGui::IconLoader::self()->load("time_line.png"));
	d->tlWidget = new TimeLineWidget(this);
	layout->addWidget(d->tlWidget);
	
	connect(d->tlWidget, SIGNAL(postEvent(const TimeLineWidget::Event &)), this, SLOT(onEvent(const TimeLineWidget::Event &)));
	connect(d->tlWidget, SIGNAL(frameSelected(int , int , int)), this, SLOT(onFrameSelected(int , int , int)));
}

/**
 * @~spanish
 * Destructor
 */
TimeLine::~TimeLine()
{
	delete d;
}

/**
 * @~spanish
 * Retorna los espacios de trabajo donde sera visible el componente, este componente sera visible en todos los espacios de trabajo.
 */
Dash::Workspace TimeLine::workspace() const
{
	return Dash::All;
}

/**
 * @~spanish
 * Retorna la posición inicial del componente, la cual es en la parte abajo de la ventana principal.
 */
Qt::DockWidgetArea TimeLine::area() const
{
	return Qt::BottomDockWidgetArea;
}


/**
 * @~spanish
 * Retorna el tipo de comandos de los interesa que el componente sea notificado de su ejecución, para este componente se notificara de la ejecución todos los comandos.
 */
Dash::DataSource::Types TimeLine::sources() const
{
	Dash::DataSource::Type type = Dash::DataSource::All;
	return type;
}

/**
 * @~spanish
 * Actualiza el componente despues de la ejecución de un comando.
 */
void TimeLine::update(const Dash::DataSource *datasource, bool undo)
{
// 	dfDebug << undo;
// 	dfDebug << datasource->data("root").toString();
// 	dfDebug << datasource->values("location")["index"];
// 	dfDebug << "xml" << datasource->data("xml").toString();
	
	switch (datasource->type())
	{
			case DataSource::Scene:
			{
				d->updateScene(datasource, undo);
			}
			break;
			case DataSource::Layer:
			{
				d->updateLayer(datasource, undo);
			}
			break;
			case DataSource::Frame:
			{
				d->updateFrame(datasource, undo);
			}
			break;
			case DataSource::Library:
			case DataSource::Project:
			case DataSource::Object:
			case DataSource::Unknown:
			{
				dfDebug << "Unhandled package: " << datasource->values("command")["name"];
			}
			break;
			default:
			{
				
			}
	}
}

/**
 * @~spanish
 * Reinicia el componente, borra todas las escenas, frames y layers.
 */
void TimeLine::reset()
{
	d->tlWidget->closeAllScenes();
}

/**
 * @internal
 */
void TimeLine::onFrameSelected(int scenePos, int layerPos, int framePos)
{
	emit frameSelected(scenePos, layerPos, framePos);
}

/**
 * @internal
 */
void TimeLine::onEvent(const TimeLineWidget::Event &event )
{
	dfDebug << "scene = " << event.scene << " layer = " << event.layer << " frame = " << event.frame << " action " << event.action << " data " << event.data.toString();
	
	switch(event.action)
	{
		case TimeLineWidget::InsertFrame:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			if(scene)
			{
				YAMF::Model::Layer *layer = scene->layer(event.layer);
				if(layer)
				{
					layer->createFrame(event.frame);
				}
			}
		}
		break;
		case TimeLineWidget::RemoveFrame:
		{
			if(event.scene > -1 && event.layer > -1 &&  event.frame > -1)
			{
				YAMF::Model::Scene *scene = project()->scene(event.scene);
				if( scene )
				{
					YAMF::Model::Layer *layer = scene->layer(event.layer);
					if( layer )
					{
						YAMF::Model::Frame *frame = layer->frame(event.frame);
						if( frame )
						{
							layer->removeFrame(frame);
						}
					}
				}
			}
		}
		case TimeLineWidget::CopyFrame:
		{
			project()->copyFrameToClipboard(event.scene, event.layer, event.frame);
		}
		break;
		case TimeLineWidget::PasteFrame:
		{
			project()->pasteFrameFromClipboard(event.scene, event.layer, event.frame);
		}
		break;
		case TimeLineWidget::CutFrame:
		{
			project()->cutFrame(event.scene, event.layer, event.frame);
		}
		break;
		case TimeLineWidget::MoveFrameUp:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			
			YAMF::Model::Frame *frame = layer->frame(event.frame);
			if(frame)
			{
// 				int nclones = layer->clones(frame);
				layer->moveFrame(frame->visualIndex(), frame->visualIndex()-1);
			}
		}
		break;
		case TimeLineWidget::MoveFrameDown:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			YAMF::Model::Frame *frame = layer->frame(event.frame);
			
			if(frame)
			{
				int nclones = layer->clones(frame);
				layer->moveFrame(frame->visualIndex(), frame->visualIndex()+nclones+1);
			}
		}
		break;
		case TimeLineWidget::LockFrame:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			YAMF::Model::Frame *frame = layer->frame(event.frame);
			frame->setLocked(event.data.toBool());
		}
		break;
		case TimeLineWidget::RenameFrame:
		{
		}
		break;
		case TimeLineWidget::ChangeFrameVisibility:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			YAMF::Model::Frame *frame = layer->frame(event.frame);
			frame->setVisible(event.data.toBool());
		}
		break;
		case TimeLineWidget::ExpandFrame:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			layer->expandFrame(event.frame, event.data.toInt());
		}
		break;
		case TimeLineWidget::InsertLayer:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			if(scene)
			{
				YAMF::Model::Layer *layer = scene->createLayer( event.layer+1 );
				if(layer)
				{
					layer->createFrame();
				}
			}
		}
		break;
		case TimeLineWidget::RemoveLayer:
		{
			if(event.scene > -1 && event.layer > -1)
			{
				YAMF::Model::Scene *scene = project()->scene(event.scene);
				YAMF::Model::Layer *layer = scene->layer(event.layer);
				scene->removeLayer(layer);
			}
		}
		break;
		case TimeLineWidget::MoveLayerUp:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			scene->moveLayer(event.layer, event.layer-1);
		}
		break;
		case TimeLineWidget::MoveLayerDown:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			scene->moveLayer(event.layer, event.layer+1);
		}
		break;
		case TimeLineWidget::LockLayer:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			layer->setLocked(event.data.toBool());
		}
		break;
		case TimeLineWidget::RenameLayer:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			layer->setLayerName(event.data.toString());
		}
		break;
		case TimeLineWidget::ChangeLayerVisibility:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			YAMF::Model::Layer *layer = scene->layer(event.layer);
			layer->setVisible(event.data.toBool());
		}
		break;
		
		case TimeLineWidget::InsertScene:
		{
			YAMF::Model::Scene *scene = project()->createScene(event.scene+1);
			YAMF::Model::Layer *layer = scene->createLayer();
			layer->createFrame();
		}
		break;
		case TimeLineWidget::RemoveScene:
		{
			if(event.scene > -1)
			{
				YAMF::Model::Scene *scene = project()->scene(event.scene);
				if(scene)
				{
					project()->removeScene(scene);
				}
			}
		}
		break;
		case TimeLineWidget::MoveSceneUp:
		{
			project()->moveScene(event.scene, event.scene-1);
		}
		break;
		case TimeLineWidget::MoveSceneDown:
		{
			project()->moveScene(event.scene, event.scene+1);
		}
		break;
		case TimeLineWidget::LockScene:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			scene->setLocked(event.data.toBool());
		}
		break;
		case TimeLineWidget::RenameScene:
		{
			YAMF::Model::Scene *scene = project()->scene(event.scene);
			scene->setSceneName(event.data.toString());
		}
		break;
	}
	
}

}
}
