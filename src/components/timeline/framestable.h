/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef COMPONENTSLAYERTABLE_H
#define COMPONENTSLAYERTABLE_H

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QHash>

class QMenu;

namespace Dash {
namespace Component {

class FramesTable;
class FramesTableItemDelegate;

class FramesTableItem : public QTableWidgetItem
{
	public:
		enum Attributes
		{
			IsUsed = Qt::UserRole,
			IsLocked,
			IsVisible,
			IsSound,
			IsClone,
			InGroupPosition
		};
		
		
		enum InGroupPositionType
		{
			None = 0,
			Begin,
			Intermediate,
			End
		};
		
		FramesTableItem();
		virtual ~FramesTableItem();
		
		bool isUsed();
		bool isLocked();
		bool isVisible();
		bool isSound();
		bool isClone();
		InGroupPositionType inGroupPosition();
};

class TLRuler;

/**
 * @~spanish
 * @brief Esta clase provee de una interfaz gráfica para la visualización y gestión del estado de los frames del proyecto,
 * @author David Cuadrado <krawek@gmail.com>
*/
class FramesTable : public QTableWidget
{
	Q_OBJECT;
	
	friend class FramesTableItemDelegate;
	
	public:
		FramesTable(QWidget *parent = 0);
		~FramesTable();
		
		bool isSoundLayer(int row);
		
	public slots:
		// Layers
		
		void insertLayer(int layerPos, const QString &name);
		void insertSoundLayer(int layerPos, const QString &name);
		
		void removeCurrentLayer();
		void removeLayer(int pos);
		void moveLayer(int pos, int newPos);
		
		int lastFrameByLayer(int layerPos);
		
		// Frames
		void addFrame(int layerPos, const QString &name);
		void insertFrame(int layerPos, int framePos, const QString &name);
		void setCurrentFrame(FramesTableItem *);
		void setCurrentLayer(int layerPos);
		void selectFrame(int index);
		
		void setAttribute(int row, int col, FramesTableItem::Attributes att, bool value);
		
		void removeFrame(int layerPos, int position);
		void moveFrame(int layerPos, int from, int to);
		
		void lockFrame(int layerPosition, int position, bool locked);
		
		void setFrameVisibility(int layerPosition, int position, bool isVisible);
		
		void setItemSize(int w, int h);
		
		void expandFrame(int layerPosition, int position, int size );
		
		void groupFrames(int layerPosition, int from, int to );
		void destroyGroupFrames(int layerPos, int position, int size);
		
		
	private:
		void setup();
		
	protected:
		void fixSize();
		void mousePressEvent ( QMouseEvent * event );
		
	private slots:
		void emitFrameSelected(int col);
		void onItemSelectionChanged();
		
		void onCellDoubleClicked(int row, int column);
		void onMenuActionTriggered(QAction * action);
		
	signals:
		void frameSelected(int layerPos, int framePos);
		void layerSelected(int layerPos);
		void requestInsertFrame(int layerPos, int framePos);
		void requestRemoveFrame(int layerPos, int framePos);
		
		void requestCopyFrame(int layerPos, int framePos);
		void requestPasteFrame(int layerPos, int framePos);
		void requestCutFrame(int layerPos, int framePos);
		
		void requestChangeFrameVisibility(int layerPos, int framePos, bool isVisible);
		void requestLockFrame(int layerPos, int framePos, bool locked);
		void requestExpandFrame(int layerPos, int framePos, int size);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
