/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_COMPONENTSLAYERMANAGER_H
#define DASH_COMPONENTSLAYERMANAGER_H

#include <QPushButton>
#include <QToolTip>
#include <QLabel>
#include <QScrollBar>
#include <QButtonGroup>

#include <dgui/imagebutton.h>
#include <QTableWidget>

namespace Dash {
namespace Component {

/**
 * @brief Esta clase provee de una interfaz gráfica para la visualización y gestión del estado de los layers de un proyecto.
 * @author David Cuadrado <krawek@gmail.com>
*/
class LayerManager : public QTableWidget
{
	Q_OBJECT;
	
	friend class LayerManagerItemDelegate;
	
	public:
		enum Actions
		{
			NoAction = 0,
			ShowOutlines,
			LockLayers,
			ToggleLayerView,
			InsertLayer,
			RemoveLayer,
			MoveLayerUp,
			MoveLayerDown
		};
		
		LayerManager(QWidget *parent = 0);
		~LayerManager();
		
		void insertLayer(int position, const QString &name);
		void insertSoundLayer(int position, const QString &name);
		void removeLayer(int position);
		
		void renameLayer(int position, const QString &name);
		void moveLayer(int position, int newPosition);
		void lockLayer(int position, bool locked);
		
		void setRowHeight(int rowHeight);
		
	protected:
		void resizeEvent(QResizeEvent *e);
		virtual void fixSize();
		
	protected slots:
		void commitData ( QWidget * editor );
		void onItemChanged( QTableWidgetItem * item);
		
	signals:
		void layerRenamed(int layerPosition, const QString &newName);
		void layerLocked(int layerPosition, bool locked);
		void layerVisibilityChanged(int layerPosition, bool isVisible);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
