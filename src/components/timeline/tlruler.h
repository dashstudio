/***************************************************************************
 *   Copyright (C) 2005 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_COMPONENTSTLRULER_H
#define DASH_COMPONENTSTLRULER_H

#include <QHeaderView>


namespace Dash {
namespace Component {

/**
 * @~spanish
 * @brief Esta clase provee de una regla para la cabecera de la tabla de frames del componente Timeline.
 * @author David Cuadrado <krawek@gmail.com>
*/
class TLRuler : public QHeaderView
{
	Q_OBJECT
	public:
		TLRuler(QWidget *parent = 0);
		~TLRuler();
		
		void select(int logical);
		
	protected:
		void paintSection( QPainter *painter, const QRect & rect, int logicalIndex ) const;
		void mouseMoveEvent(QMouseEvent *e);
			
	private slots:
			void updateSelected(int logical);
			
	signals:
		void logicalSectionSelected(int logical);
};

}
}

#endif
