/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#ifndef COMPONENTTIMELINEWIDGET_H
#define COMPONENTTIMELINEWIDGET_H

#include <QSplitter>
#include <QVariant>

class QTreeWidgetItem;

namespace Dash {
namespace Component {

class LayerManager;
class FramesTable;

/**
 * @brief Esta clase representa la interfaz gráfica del componente "TimeLine".
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class TimeLineWidget : public QSplitter
{
	Q_OBJECT;
	public:
		enum Action {
			InsertFrame = 0,
			RemoveFrame,
			CopyFrame,
			PasteFrame,
			CutFrame,
			MoveFrameUp,
			MoveFrameDown,
			LockFrame,
			RenameFrame,
			ChangeFrameVisibility,
			ExpandFrame,
			
			InsertLayer,
			RemoveLayer,
			MoveLayerUp,
			MoveLayerDown,
			LockLayer,
			RenameLayer,
			ChangeLayerVisibility,
			
			InsertScene,
			RemoveScene,
			MoveSceneUp,
			MoveSceneDown,
			LockScene,
			RenameScene
		};
		
		struct Event
		{
			int scene;
			int layer;
			int frame;
			Action action;
			QVariant data;
		};
		
		TimeLineWidget(QWidget * parent = 0);
		~TimeLineWidget();
		
		int scene();
		int layer();
		int frame();
		
	public slots:
		void insertScene(int position, const QString &name);
		void removeScene(int position);
		void renameScene(int position, const QString &name);
		void moveScene(int from, int to);
		void setScene(int position);
		void closeAllScenes();
		
		void insertLayer(int scenePos, int position, const QString &name);
		void insertAudioLayer(int scenePos, int position, const QString &name, int startFrame = 0);
		void removeLayer(int scenePos, int position);
		void renameLayer(int scenePos, int layerPos, const QString &name);
		
		void setLayerVisibility(int scenePos, int position, bool isVisible);
		void setLayerLocked(int scenePos, int position, bool locked);
		void moveLayer(int scenePos, int from, int to);
		void setLayer(int scenePos, int position);
		
		void insertFrame(int scenePos, int layerPos, int position, const QString &name);
		void removeFrame(int scenePos, int layerPos, int position);
		void renameFrame(int scenePos, int layerPos, int position, const QString &name);
		void moveFrame(int scenePos, int layerPos, int from, int to);
		void setFrame(int scenePos, int layerPos, int position);
		void setFrameVisibility(int scenePos, int layerPos, int position, bool isVisible);
		void setFrameLocked(int scenePos, int layerPos, int position, bool locked);
		void groupFrames(int scenePos, int layerPos, int from, int to);
		void expandFrame(int scenePos, int layerPos, int position, int size);
		void destroyGroupFrames(int scenePos, int layerPos, int position, int size);
		
		void playScene();
		
	private slots:
		void onItemClicked(QTreeWidgetItem *item, int column);
		void onItemChanged(QTreeWidgetItem *, int);
		void onFrameSelected(int layerPosition, int framePosition);
		void onLayerSelected(int layerPosition);
		void onLayerRenamed(int layerPosition, const QString &newName);
		void onLayerLocked(int layerPosition, bool locked);
		void onLayerVisibilityChanged(int layerPosition, bool isVisible);
		void onButtonClicked(int action);
		
		void onRequestInsertFrame(int layerPosition, int framePosition);
		void onRequestRemoveFrame(int layerPosition, int framePosition);
		
		void onRequestCopyFrame(int layerPosition, int framePosition);
		void onRequestPasteFrame(int layerPosition, int framePosition);
		void onRequestCutFrame(int layerPosition, int framePosition);
		
		void onRequestLockFrame(int layerPosition, int framePosition, bool locked);
		void onRequestChangeFrameVisibility(int layerPosition, int framePosition, bool visibility);
		void onRequestExpandFrame(int layerPosition, int framePosition, int size);
		
		void nextFrame();
		
	private:
		LayerManager *layerManager(int sceneIndex);
		FramesTable *framesTable(int sceneIndex);
		
	private:
		void setupButtons();
		
	signals:
		void frameSelected(int scenePos, int layerPos, int framePos);
		void postEvent(const TimeLineWidget::Event & event);
		
	private:
		struct Private;
		Private *const d;
		
};

}
}

#endif
