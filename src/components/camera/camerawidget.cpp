/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "camerawidget.h"

#include <QVBoxLayout>

#include <yamf/render/area.h>
#include <yamf/model/project.h>

#include "dash/project.h"

#include "ui_camera.h"

namespace Dash {
namespace Component {

struct CameraWidget::Private
{
	Ui::Camera *ui;
};

CameraWidget::CameraWidget(Dash::Project *const project, QWidget *parent)
 : Dash::Component::Widget(project, parent), d(new Private)
{
	d->ui = new Ui::Camera;
	d->ui->setupUi(this);
	
	connect(d->ui->preview, SIGNAL(clicked()), this, SLOT(play()));
	
	connect(d->ui->fps, SIGNAL(valueChanged(int)), d->ui->area, SLOT(setFps(int)));
}


CameraWidget::~CameraWidget()
{
	delete d->ui;
	delete d;
}

QSize CameraWidget::sizeHint() const
{
	return QSize(300,300);
}

Dash::DataSource::Types CameraWidget::sources() const
{
	return Dash::DataSource::Scene;
}

void CameraWidget::update(const Dash::DataSource *datasource, bool undo)
{
	Q_UNUSED(datasource);
	Q_UNUSED(undo);
	
	d->ui->area->setScenes( project()->scenes().visualValues() );
}

void CameraWidget::reset()
{
}

void CameraWidget::play()
{
	d->ui->area->setScenes( project()->scenes().visualValues() );
	d->ui->area->setFps(d->ui->fps->value());
	d->ui->area->start();
}

}

}
