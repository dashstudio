/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "editmembers.h"

#include <QDialogButtonBox>
#include <QLineEdit>

#include <dgui/iconloader.h>
#include <dcore/debug.h>

#include <dash/network/package/source.h>
#include <dash/network/package/queryproject.h>
#include <dash/network/package/updateproject.h>

#include <dash/network/parser/userlistparser.h>
#include <dash/network/parser/projectactionparser.h>

#include <dash/network/socket.h>
#include <dash/network/permission.h>


#include "ui_editmembers.h"

using Dash::Network::Parser::ProjectAction;

namespace Dash {
namespace Component {
namespace Network {

EditMembers::EditMembers(const QString &project, Dash::Network::Socket *socket, QWidget *parent)
 : QDialog(parent), m_projectName(project), m_membersModified(false), m_socket(socket)
{
	setWindowTitle(tr("Edit members"));
	ui = new Ui::EditMembers;
	
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	QWidget *w = new QWidget;
	ui->setupUi(w);
	
	layout->addWidget(w);
	
	ui->selector->downButton()->setVisible(false);
	ui->selector->upButton()->setVisible(false);
	connect(ui->selector, SIGNAL(itemSelectionChanged(const QString&,  DGui::ItemSelector::ListType )), this, SLOT(changeCurrentMember(const QString&,  DGui::ItemSelector::ListType )));
	
	connect(ui->selector, SIGNAL(itemSelected(const QString&)), this, SLOT(addMember(const QString&)));
	
	connect(ui->selector, SIGNAL(itemUnselected(const QString&)), this, SLOT(removeMember(const QString&)));
	
	connect(ui->save, SIGNAL(clicked()), this, SLOT(saveCurrentMemeber()));
	
	QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	layout->addWidget(buttons);
	
	connect(ui->user, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(changeCurrentMember(const QString &)));
	
	connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
	connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
	
	if(!project.isEmpty())
	{
		socket->send("<listusers />");
		socket->send(Dash::Network::Package::QueryProject(project) );
	}
}


EditMembers::~EditMembers()
{
	delete ui;
}

void EditMembers::handlePackage(Dash::Network::Package::Source *const pkg)
{
	QString root = pkg->root();
	
	if( root == "userlist" )
	{
		m_users.clear();
		
		Dash::Network::Parser::UserList parser;
		
		if(parser.parse(pkg->xml()) )
		{
			foreach(Dash::Network::Parser::UserList::User user, parser.users())
			{
				m_users << user.login;
			}
		}
	}
	else if( root == "projectquery" )
	{
		Dash::Network::Parser::ProjectAction parser;
		if(parser.parse(pkg->xml()) )
		{
			QSet<QString> members;
			m_members = parser.members();
			
			foreach(ProjectAction::Member member, parser.members())
			{
				if(ui->job->findText(member.job) == -1)
					ui->job->addItem(member.job);
				
				members << member.login;
			}
			
			m_users.subtract(members);
			
			ui->user->addItems(members.toList());
			ui->selector->setItems(m_users.toList(), DGui::ItemSelector::Avaible);
			ui->selector->setItems(members.toList(), DGui::ItemSelector::Selected);
		}
	}
}

void EditMembers::addMember(const QString& login)
{
	ProjectAction::Member newMember;
	
	newMember.login = login;
	newMember.job = ui->job->currentText();
	
	newMember.profile = Dash::Network::UserProfile("", currentPermissions());
	
	ui->user->addItem(login);
	
	m_members << newMember;
	
	m_membersModified = true;
}

void EditMembers::removeMember(const QString& login)
{
	QList<ProjectAction::Member>::iterator it = m_members.begin();
	while(it != m_members.end())
	{
		if((*it).login == login)
		{
			m_members.erase(it);
			ui->user->removeItem(ui->user->findText(login));
			break;
		}
		++it;
	}
	m_membersModified = true;
}

void EditMembers::saveCurrentMemeber()
{
	QString login = ui->user->currentText();
	QList<ProjectAction::Member>::iterator it = m_members.begin();
	while(it != m_members.end())
	{
		if((*it).login == login)
		{
			(*it).job = ui->job->currentText();
			(*it).profile.setPermission(currentPermissions());
			m_membersModified = true;
			break;
		}
		++it;
	}
}

void EditMembers::accept()
{
	if(m_membersModified)
	{
		Dash::Network::Package::UpdateProject update(m_projectName);
		foreach(ProjectAction::Member member, m_members)
		{
			update.addUser( member.login, member.job, member.profile.permissions() );
		}
		m_socket->send(update);
	}
	QDialog::accept();
}

void EditMembers::changeCurrentMember(const QString& login,  DGui::ItemSelector::ListType list )
{
	if(list == DGui::ItemSelector::Selected)
	{
		foreach( ProjectAction::Member member, m_members)
		{
			if(login == member.login)
			{
				setPermissions(member.profile.permissions());
				
				int index = ui->job->findText(member.job);
				if(index == -1)
				{
					ui->job->addItem(member.job);
					ui->job->setCurrentIndex(ui->job->count());
				}
				else
				{
					ui->job->setCurrentIndex(index);
				}
				
				ui->user->setCurrentIndex(ui->user->findText(login));
				
				break;
			}
		}
	}
}

void EditMembers::setPermissions(const QList<Dash::Network::Permission>& permissions)
{
	foreach(Dash::Network::Permission permission, permissions)
	{
		if(permission.name() == "scene")
		{
			ui->sceneRead->setChecked(permission.read());
			ui->sceneWrite->setChecked(permission.write());
		}
		else if(permission.name() == "layer")
		{
			ui->layerRead->setChecked(permission.read());
			ui->layerWrite->setChecked(permission.write());
		}
		else if(permission.name() == "frame")
		{
			ui->frameRead->setChecked(permission.read());
			ui->frameWrite->setChecked(permission.write());
		}
		else if(permission.name() == "object")
		{
			ui->objectRead->setChecked(permission.read());
			ui->objectWrite->setChecked(permission.write());
		}
	}
}

QList<Dash::Network::Permission> EditMembers::currentPermissions()
{
	return QList<Dash::Network::Permission>()
			<< Dash::Network::Permission("scene", ui->sceneRead->isChecked(), ui->sceneWrite->isChecked())
			<< Dash::Network::Permission("layer", ui->layerRead->isChecked(), ui->layerWrite->isChecked())
			<< Dash::Network::Permission("frame", ui->frameRead->isChecked(), ui->frameWrite->isChecked())
			<< Dash::Network::Permission("object", ui->objectRead->isChecked(), ui->objectWrite->isChecked());
}


}

}

}
