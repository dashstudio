TEMPLATE = lib

CONFIG += staticlib \
warn_on
SOURCES += chatwindow.cpp \
 editmembers.cpp
HEADERS += chatwindow.h \
 editmembers.h
FORMS += chatwindow.ui \
 editmembers.ui

include(../components_config.pri)

QT += network 
