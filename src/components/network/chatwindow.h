/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QDialog>

namespace Ui {
	class ChatWindow;
}

namespace Dash {
namespace Component {
namespace Network {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class ChatWindow : public QDialog
{
	Q_OBJECT;
	public:
		enum {
			Chat = 0x01,
			Notice,
			Wall
		};
		ChatWindow(QWidget *parent = 0);
		~ChatWindow();
		
		void addMessage(const QString &from, const QString &text);
		void addNotice(const QString &from, const QString &text);
		void addWall(const QString &from, const QString &text);
		
		void addUsers(const QStringList & logins);
		void removeUser(const QString & login);
		
	private slots:
		void onEditFinished();
		
	signals:
		void sendText(const QString &text, int type);
		
	private:
		QString replaceText(const QString &text);
		void moveCursorToEnd();
		
	private:
		Ui::ChatWindow *ui;

};

}
}
}

#endif
