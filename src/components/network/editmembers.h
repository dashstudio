/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_COMPONENT_NETWORKEDITMEMBERS_H
#define DASH_COMPONENT_NETWORKEDITMEMBERS_H

#include <QDialog>
#include <QSet>

#include <dash/network/package/observer.h>
#include <dgui/itemselector.h>
#include <dash/network/parser/projectactionparser.h>

namespace Ui {
	class EditMembers;
}

namespace Dash {
namespace Network {
class Socket;
class Permission;
namespace Package {
	class Source;
}
}

namespace Component {
namespace Network {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class EditMembers : public QDialog, public Dash::Network::Package::Observer
{
	Q_OBJECT
	public:
		EditMembers(const QString &project, Dash::Network::Socket *socket, QWidget *parent = 0);
		~EditMembers();
		
		void handlePackage(Dash::Network::Package::Source *const pkg);
		
	public slots:
		void addMember(const QString& login);
		void removeMember(const QString& login);
		void accept();
		
	private slots:
		void saveCurrentMemeber();
		void changeCurrentMember(const QString& login,  DGui::ItemSelector::ListType list = DGui::ItemSelector::Selected);
		
	private:
		void setPermissions(const QList<Dash::Network::Permission>& permissions);
		QList<Dash::Network::Permission> currentPermissions();
		
	private:
		Ui::EditMembers *ui;
		QSet<QString> m_users;
		QList<Dash::Network::Parser::ProjectAction::Member> m_members;
		QString m_projectName;
		bool m_membersModified;
		Dash::Network::Socket *m_socket;
};

}

}
}

#endif
