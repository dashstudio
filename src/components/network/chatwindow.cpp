/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "chatwindow.h"

#include <QTextDocument>

#include <dgui/iconloader.h>

#include "ui_chatwindow.h"

namespace Dash {
namespace Component {
namespace Network {

ChatWindow::ChatWindow(QWidget *parent)
 : QDialog(parent)
{
	ui = new Ui::ChatWindow;
	ui->setupUi(this);
	
	QFont font = ui->area->document()->defaultFont();
	font.setPointSize(12);
	ui->area->document()->setDefaultFont(font);
	
	ui->nicks->setFont(font);
	
	connect(ui->text, SIGNAL(returnPressed()), this, SLOT(onEditFinished()));
	
	setWindowFlags(Qt::Window );
}


ChatWindow::~ChatWindow()
{
	delete ui;
}

void ChatWindow::addMessage(const QString &from, const QString &text)
{
	moveCursorToEnd();
	ui->area->append(QString("<%1> ").arg(from));
	ui->area->insertHtml(replaceText(text));
}

void ChatWindow::addNotice(const QString &from, const QString &text)
{
	moveCursorToEnd();
	ui->area->append(tr("<b>***Notice from %1:</b> ").arg(from));
	ui->area->insertHtml(replaceText(text));
}

void ChatWindow::addWall(const QString &from, const QString &text)
{
	moveCursorToEnd();
	ui->area->append(tr("<b>***Attention: %1 says:</b> ").arg(from));
	ui->area->insertHtml(replaceText(text));
}

void ChatWindow::onEditFinished()
{
	QString text = ui->text->text();
	ui->text->clear();
	
	int type = Chat;
	
	if( ui->type->currentIndex() == 1 )
	{
		type = Notice;
	}
	else if(ui->type->currentIndex() == 2 )
	{
		type = Wall;
	}
	
	if( ! text.isEmpty() )
	{
		emit sendText(text, type);
	}
}

QString ChatWindow::replaceText(const QString &text)
{
	QString rich = Qt::escape(text);
	
	rich = rich.replace("o:)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-angel.svg") +"\" />");
	rich = rich.replace(":'(", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-crying.svg") +"\" />");
	rich = rich.replace("}:)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-devilish.svg") +"\" />");
	rich = rich.replace("8)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-glasses.svg") +"\" />");
	rich = rich.replace(":D", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-grin.svg") +"\" />");
	rich = rich.replace(":*)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-kiss.svg") +"\" />");
	rich = rich.replace(":(|)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-monkey.svg") +"\" />");
	rich = rich.replace(":|", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-plain.svg") +"\" />");
	rich = rich.replace(":(", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-sad.svg") +"\" />");
	rich = rich.replace(":]", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-smile-big.svg") +"\" />");
	rich = rich.replace(":o", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-surprise.svg") +"\" />");
	rich = rich.replace(";)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-wink.svg") +"\" />");
	rich = rich.replace(":)", QString("<img width=\"22\" height=\"22\" src=\"")+ DGui::IconLoader::self()->path("face-smile.svg") +"\" />");
	
	
	return rich;
}

void ChatWindow::addUsers(const QStringList & logins)
{
	ui->nicks->addItems(logins);
	foreach(QString login, logins)
	{
		ui->area->append(tr("<b>***Connected:</b> %1").arg(login));
	}
}

void ChatWindow::removeUser(const QString & login)
{
	QList<QListWidgetItem *> items = ui->nicks->findItems(login , Qt::MatchFixedString );
	if(!items.isEmpty())
	{
		QListWidgetItem *toRemove = items.first();
		ui->nicks->takeItem(ui->nicks->row(toRemove));
		delete toRemove;
		ui->area->append(tr("<b>***Disconnected:</b> %1").arg(login));
	}
	
}

void ChatWindow::moveCursorToEnd()
{
	ui->area->moveCursor(QTextCursor::End);
}

}
}
}

