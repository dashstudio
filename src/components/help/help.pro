TEMPLATE = lib

QT += gui

include(../components_config.pri)

FORMS += help.ui

CONFIG += staticlib
HEADERS += helpwidget.h

SOURCES += helpwidget.cpp


INSTALLS += en es

en.path = /share/dash/contents/en
en.files = contents/en/toc contents/en/*.html contents/en/*.png

es.path = /share/dash/contents/es
es.files = contents/es/toc contents/es/*.html contents/es/*.png

