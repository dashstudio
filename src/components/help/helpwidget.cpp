/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "helpwidget.h"

#include <QDir>
#include <QHeaderView>
#include <QXmlStreamReader>
#include <QStack>

#include "ui_help.h"

#include <dcore/debug.h>

#define DATA_PATH __PREFIX__+QString("/share/dash/contents/")

namespace Dash {
namespace Component {

struct HelpWidget::Private {
	enum Data {
		Url = 0x01
	};
	
	Ui::Help *ui;
	
	QString dataPath;
	
	void createToc() const;
	QUrl path(const QString &fileName) const;
};

void HelpWidget::Private::createToc() const
{
	QFile f(dataPath+"/toc");
	
	if( f.open(QIODevice::ReadOnly) )
	{
		QXmlStreamReader reader(&f);
		
		
		QStack<QTreeWidgetItem *> sections;
		
		int section = 0;
		while(!reader.atEnd())
		{
			switch(reader.readNext())
			{
				case QXmlStreamReader::StartElement:
				{
					if( reader.name() == "section" )
					{
						QTreeWidgetItem *item = 0;
						if( section == 0)
						{
							item = new QTreeWidgetItem(ui->contents);
						}
						else
						{
							item = new QTreeWidgetItem(sections.last());
						}
						
						item->setText(0, reader.attributes().value("name").toString());
						item->setData(0, Url, reader.attributes().value("url").toString());
						
						sections.push(item);
						
						section++;
					}
				}
				break;
				case QXmlStreamReader::EndElement:
				{
					if( reader.name() == "section" )
					{
						sections.pop();
						section--;
					}
				}
				break;
				default: break;
			}
		}
		
		ui->contents->expandAll();
	}
}

QUrl HelpWidget::Private::path(const QString &fileName) const
{
	QFileInfo finfo(fileName);
	
	if( finfo.isAbsolute())
	{
		return fileName;
	}
	
	
	QDir dir(dataPath);
	
	return QUrl::fromLocalFile(dir.absoluteFilePath(fileName) );
}

HelpWidget::HelpWidget(QWidget *parent)
 : QWidget(parent), d(new Private)
{
	d->ui = new Ui::Help;
	d->ui->setupUi(this);
	
	d->dataPath = DATA_PATH+QLocale::system().name().left(2);
	d->ui->browser->setSearchPaths(QStringList() << d->dataPath << DATA_PATH+"/common");
	
	d->ui->browser->setSource(d->path("index.html"));
	
	d->ui->contents->header()->hide();
	setWindowFlags(Qt::Window );
	
	d->createToc();
	d->ui->filter->setFilterCaseSensitive(Qt::CaseInsensitive);
	d->ui->filter->setTreeWidget(d->ui->contents);
	
	
	connect(d->ui->contents, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(onItemActivated(QTreeWidgetItem*, int)));
}


HelpWidget::~HelpWidget()
{
	delete d->ui;
	delete d;
}

void HelpWidget::onItemActivated(QTreeWidgetItem *item, int)
{
	d->ui->browser->setSource( QUrl(item->data(0, Private::Url).toString()) );
}

}

}


