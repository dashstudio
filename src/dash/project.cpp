/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "project.h"

#include <QDir>

#include <dcore/algorithm.h>

namespace Dash {

struct Project::Private {
	QString projectPalette;
};

Project::Project(QObject *parent)  : YAMF::Model::Project(parent), d(new Private)
{
	d->projectPalette = QDir::temp().absoluteFilePath(QString("dash.palette.%1.dpl").arg(DCore::Algorithm::random(99999) ) );
}


Project::~Project()
{
	QFile::remove(d->projectPalette);
	delete d;
}

QString Project::projectPalette() const
{
	return d->projectPalette;
}

void Project::importPalette(const QString &path)
{
	if( QFile::exists(d->projectPalette) )
	{
		QFile::remove(d->projectPalette);
	}
	QFile::copy(path, d->projectPalette);
}


}

