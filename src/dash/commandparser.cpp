/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "commandparser.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <yamf/model/command/base.h>
#include "datasource.h"

#include <dcore/debug.h>

namespace Dash {

struct CommandParser::Private
{
	Private() : currentSource(0) {}
	
	DataSource *currentSource;
	QString commandName;
	
	
	QStringList objectCommands;
};

CommandParser::CommandParser() : DCore::XmlParserBase(), d(new Private)
{
	d->objectCommands << "addobject" << "removeobject" << "modifyitem" << "editnodesitem" << "convertitem" << "additemfilter" << "modifyfilter" << "addtweening" << "bringforwardsitem" << "bringtofrontitem" << "sendtobackitem" << "sendbackwardsitem" << "groupitem" << "ungroupitem" << "changetext" << "changetextwidth" << "changetextfont" << "changeitembrush" << "changeitempen";
}

CommandParser::~CommandParser()
{
	delete d;
}

bool CommandParser::startTag(const QString &tag, const QXmlAttributes &atts)
{
	if( atts.count() > 0 )
	{
		DataSource::Values values;
		
		for(int i = 0; i < atts.count(); i++)
		{
			QString qname = atts.qName(i);
			values[qname] = atts.value(qname);
		}
		
		d->currentSource->setValues(tag, values);
	}
	
	if( tag == "command" )
	{
		d->commandName = atts.value("name");
		
		if( d->objectCommands.contains(d->commandName) )
		{
			d->currentSource->setType(DataSource::Object);
		}
		else if( d->commandName == "insertscene" || d->commandName == "removescene" || d->commandName == "movescene" || d->commandName == "renamescene" )
		{
			d->currentSource->setType(DataSource::Scene);
		}
		else if( d->commandName == "insertlayer" || d->commandName == "removelayer" || d->commandName == "locklayer" || d->commandName == "changelayervisibility"|| d->commandName == "movelayer" || d->commandName == "renamelayer")
		{
			d->currentSource->setType(DataSource::Layer);
		}
		else if( d->commandName == "insertframe" || d->commandName == "removeframe" || d->commandName == "lockframe" || d->commandName == "changeframevisibility" || d->commandName == "moveframe" || d->commandName == "renameframe" || d->commandName == "expandframe" )
		{
			d->currentSource->setType(DataSource::Frame);
		}
		else if( d->commandName == "addlibraryobject" || d->commandName == "removelibraryobject"|| d->commandName ==  "renamelibraryobject"|| d->commandName == "modifysymbol" )
		{
			d->currentSource->setType(DataSource::Library);
		}
	}
	
	return true;
}

bool CommandParser::endTag(const QString &tag)
{
	return true;
}

void CommandParser::text(const QString &text)
{
	Q_UNUSED(text);
}

DataSource *CommandParser::build(const YAMF::Command::Base *command)
{
	d->currentSource = new DataSource;
	d->currentSource->setData("command", qVariantFromValue(command) );
	
	{
		QString xml = command->toXml();
		
		if( ! DCore::XmlParserBase::parse(xml) )
		{
			dError() << "Failed to parse";
			return 0;
		}
		
		d->currentSource->setData("root", d->commandName);
		
		if( d->commandName == "addlibraryobject" )
		{
			// Rewrite the xml to avoid the file tag
			QString newXml;
			QXmlStreamReader reader(xml);
			
			xml = QString();
			QXmlStreamWriter writer(&xml);
			
			bool read = true;
			while(!reader.atEnd())
			{
				reader.readNext();
				if( reader.tokenType() == QXmlStreamReader::StartElement )
				{
					if( reader.name().toString() == "file" )
					{
						read = false;
					}
				}
				else if( reader.tokenType() == QXmlStreamReader::EndElement )
				{
					if( reader.name().toString() == "file" )
					{
						read = true;
					}
				}
				
				if( read )
					writer.writeCurrentToken(reader);
			}
		}
		
		d->currentSource->setData("xml", xml);
	}
	
	DataSource *source = d->currentSource;
	d->currentSource = 0;
	
	return source;
}


}

