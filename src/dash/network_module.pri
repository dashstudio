
HEADERS += ./network/package/xmldocument.h \
           ./network/package/connect.h \
	   ./network/package/ack.h \
           ./network/package/error.h \
	   ./network/package/addbackup.h \
	   ./network/package/removebackup.h \
	   ./network/package/restorebackup.h \
	   ./network/package/backuplist.h \
	   ./network/package/banlist.h \
	   ./network/package/addban.h \
	   ./network/package/removeban.h \
	   ./network/package/adduser.h \
	   ./network/package/updateuser.h \
	   ./network/package/removeuser.h \
	   ./network/package/queryuser.h \
	   ./network/package/userlist.h \
	   ./network/package/listprojects.h \
	   ./network/package/newproject.h  \
	   ./network/package/openproject.h \
	   ./network/package/addproject.h \
	   ./network/package/updateproject.h \
	   ./network/package/removeproject.h \
	   ./network/package/queryproject.h \
	   ./network/package/projectlist.h \
	   ./network/package/chat.h \
	   ./network/package/notice.h  \
	   ./network/package/wall.h 

SOURCES += ./network/package/xmldocument.cpp \
           ./network/package/connect.cpp \
	   ./network/package/ack.cpp \
           ./network/package/error.cpp \
	   ./network/package/addbackup.cpp \
	   ./network/package/removebackup.cpp \
	   ./network/package/restorebackup.cpp \
	   ./network/package/backuplist.cpp \
	   ./network/package/banlist.cpp \
	   ./network/package/addban.cpp \
	   ./network/package/removeban.cpp \
	   ./network/package/adduser.cpp \
	   ./network/package/updateuser.cpp \
	   ./network/package/removeuser.cpp \
	   ./network/package/queryuser.cpp \
	   ./network/package/userlist.cpp \
	   ./network/package/listprojects.cpp \
	   ./network/package/newproject.cpp  \
	   ./network/package/openproject.cpp \
	   ./network/package/projectlist.cpp \
	   ./network/package/addproject.cpp \
	   ./network/package/updateproject.cpp \
	   ./network/package/removeproject.cpp \
	   ./network/package/queryproject.cpp \
	   ./network/package/chat.cpp \
	   ./network/package/notice.cpp  \
	   ./network/package/wall.cpp

HEADERS += ./network/parser/ackparser.h \
           ./network/parser/projectactionparser.h \
           ./network/parser/addbackupparser.h \
           ./network/parser/userqueryparser.h \
           ./network/parser/userlistparser.h 

SOURCES += ./network/parser/ackparser.cpp \
           ./network/parser/projectactionparser.cpp \
           ./network/parser/addbackupparser.cpp \ 
           ./network/parser/userqueryparser.cpp \
           ./network/parser/userlistparser.cpp 

HEADERS += ./network/socket.h \
./network/permission.h \
./network/userprofile.h \
./network/socketobserver.h \
./network/package/source.h \
./network/package/observer.h \
./network/package/dispatcher.h  \
./network/filereceiver.h \
./network/parser/projectlistparser.h \
./network/parser/projectparser.h \
./network/package/transfer.h \
./network/parser/commparser.h \
./network/package/connected.h \
./network/package/disconnected.h \
./network/package/uploadproject.h

SOURCES += ./network/socket.cpp \
./network/permission.cpp \
./network/userprofile.cpp \
./network/package/source.cpp \
./network/package/observer.cpp \
./network/package/dispatcher.cpp  \
./network/filereceiver.cpp \
./network/parser/projectlistparser.cpp \
./network/parser/projectparser.cpp \
./network/package/transfer.cpp \
./network/parser/commparser.cpp \
./network/package/connected.cpp \
./network/package/disconnected.cpp \
./network/package/uploadproject.cpp

INSTALLS += network package parser

network.path = /include/dash/network/
network.files = network/*.h

package.path = /include/dash/network/package
package.files = network/package/*.h

parser.path = /include/dash/network/parser
parser.files = network/parser/*.h


QT += network


