/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "datasource.h"

#include <QHash>

namespace Dash {

struct DataSource::Private
{
	Private() : type(DataSource::Unknown) {}
	
	DataSource::Type type;
	QHash<QString, QVariant> data;
	Sections sections;
};

DataSource::DataSource() : d(new Private)
{
}

DataSource::DataSource(Type type) : d(new Private)
{
	d->type = type;
}

DataSource::~DataSource()
{
	delete d;
}

void DataSource::setType(DataSource::Type type)
{
	d->type = type;
}

DataSource::Type DataSource::type() const
{
	return d->type;
}

void DataSource::setData(const QString &key, const QVariant &data)
{
	d->data[key.toLower()] = data;
}

QVariant DataSource::data(const QString &key) const
{
	return d->data.value(key.toLower());
}

void DataSource::setValues(const QString &section, const DataSource::Values &values)
{
	d->sections[section] = values;
}

DataSource::Values DataSource::values(const QString &section) const
{
	return d->sections.value(section);
}

DataSource::Sections DataSource::sections() const
{
	return d->sections;
}

}

