/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QVariant>
#include <QHash>

#include <dash/exports.h>

namespace Dash {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASH_EXPORT DataSource
{
	public:
		typedef QHash<QString, QString> Values;
		typedef QHash<QString, Values> Sections;
		
		enum Type {
			Unknown = 0x00,
			Project = 0x01,
			Scene = 0x02,
			Layer = 0x04,
			Frame = 0x08,
			Object = 0x10,
			Library = 0x20,
			All = Project | Scene | Layer | Frame | Object | Library
		};
		
		Q_DECLARE_FLAGS(Types, Type);
		
		DataSource();
		DataSource(Type type);
		~DataSource();
		
		void setType(DataSource::Type type);
		DataSource::Type type() const;
		
		void setData(const QString &key, const QVariant &data);
		QVariant data(const QString &key) const;
		
		void setValues(const QString &section, const Values &values);
		Values values(const QString &section) const;
		Sections sections() const;
		
	private:
		struct Private;
		Private *const d;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DataSource::Types);


}


#endif


