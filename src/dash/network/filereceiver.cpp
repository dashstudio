/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "filereceiver.h"

#include <QTemporaryFile>
#include <QtDebug>

namespace Dash {
namespace Network {

struct FileReceiver::Private
{
	Private() : bytes(0), readed(0), file(0), readData(false) {}
	qint64 bytes, readed;
	
	QTemporaryFile *file;
	bool readData;
};

FileReceiver::FileReceiver(QObject *parent)
 : QObject(parent), d(new Private)
{
}


FileReceiver::~FileReceiver()
{
	delete d->file;
	delete d;
}

void FileReceiver::start(qint64 size)
{
	d->bytes = size;
	d->readed = 0;
	
	if( d->file )
	{
		d->file->close();
		delete d->file;
	}
	
	d->file = new QTemporaryFile;
	
	
	d->readData = d->file->open();
	d->file->setAutoRemove(false);
	
	Q_ASSERT(d->readData);
}

void FileReceiver::stop()
{
	d->readData = false;
	d->readed = 0;
	d->bytes = 0;
}

qint64 FileReceiver::bytesReaded() const
{
	return d->readed;
}

qint64 FileReceiver::totalSize() const
{
	return d->bytes;
}

bool FileReceiver::isReading() const
{
	return d->readData;
}

void FileReceiver::readed(const QString &)
{
}

void FileReceiver::dataReaded(const QByteArray &data)
{
	if( d->readData )
	{
		d->readed += data.size();
		d->file->write(data);
		
		if( d->readed >= d->bytes )
		{
			d->readData = false;
			
			QString fileName = d->file->fileName();
			d->file->flush();
			d->file->close();
			
			emit finished(fileName);
		}
	}
}

}
}

