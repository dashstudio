/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "userprofile.h"
#include <dash/network/permission.h>
#include <dcore/debug.h>

namespace Dash {
namespace Network {

struct UserProfile::Private
{
	QList<Dash::Network::Permission> permissions;
	QString name;
};

UserProfile::UserProfile(): d(new Private)
{
}

UserProfile::UserProfile(const UserProfile & copy): d(new Private)
{
	d->name = copy.d->name;
	d->permissions = copy.d->permissions;
}

UserProfile::UserProfile(const QString & name, const QList<Dash::Network::Permission> & permissions) : d(new Private)
{
	d->permissions = permissions;
	d->name = name;
}


UserProfile::~UserProfile()
{
	delete d;
}

bool UserProfile::isThisProfile(const QList<Dash::Network::Permission> & permissions) const
{
	foreach(Dash::Network::Permission perm, permissions)
	{
		foreach(Dash::Network::Permission perm1, d->permissions)
		{
			if(perm.name() == perm1.name())
			{
				if(perm.write() != perm1.write() || perm.read() != perm1.read())
				{
					return false;
				}
			}
		}
	}
	return true;
}

QString UserProfile::name() const
{
	return d->name;
}

void UserProfile::setName( const QString& name)
{
	d->name = name;
}

QList<Dash::Network::Permission> UserProfile::permissions() const
{
	return d->permissions;
}

void UserProfile::setPermission(const QList<Dash::Network::Permission>& permissions)
{
	d->permissions = permissions;
}

UserProfile *UserProfile::operator=(const UserProfile& copy)
{
	d->name = copy.d->name;
	d->permissions = copy.d->permissions;
	return this;
}

}
}

