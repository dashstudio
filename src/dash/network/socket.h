/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_NETWORKSOCKET_H
#define DASH_NETWORKSOCKET_H

#include <QSslSocket>
#include <QByteArray>
#include <QString>
#include <QDomDocument>

#include <dash/exports.h>

namespace Dash {
namespace Network {

class SocketObserver;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASH_EXPORT Socket : public QTcpSocket //QSslSocket
{
	Q_OBJECT;
	
	public:
		enum Format {
			Text = 0x1,
			Binary
		};
		
		Socket(QObject *parent = 0);
		~Socket();
		
		void setFormat(Format format);
		Format format() const;
		
		void addObserver(SocketObserver *observer);
		void removeObserver(SocketObserver *observer);
		
		bool send(const QByteArray &data);
		bool send(const QDomDocument &doc);
		
	protected slots:
		virtual void readFromServer();
		
	public slots:
		void sendQueue();
		void clearQueue();
		
	private:
		struct Private;
		Private *const d;
		
};

}
}

#endif
