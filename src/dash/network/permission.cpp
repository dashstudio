/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "permission.h"

#include <dcore/debug.h>

namespace Dash {
namespace Network {

struct Permission::Private
{
	QString name;
	bool read;
	bool write;
};

Permission::Permission() : d(new Private)
{
}


Permission::Permission(const QString &name, bool read, bool write) : d(new Private())
{
	d->name = name;
	d->read = read;
	d->write = write;
}


Permission::Permission(const Permission& copy) :  d(new Private())
{
	d->name = copy.d->name;
	d->read = copy.d->read;
	d->write = copy.d->write;
}

Permission::~Permission()
{
	delete d;
}

void Permission::setName(const QString &name)
{
	d->name = name;
}

void Permission::setRead(bool read)
{
	d->read = read;
}

void Permission::setWrite(bool write)
{
	d->write = write;
}

QString Permission::name() const
{
	return d->name;
}

bool Permission::read() const
{
	return d->read;
}

bool Permission::write() const
{
	return d->write;
}


Permission *Permission::operator=(const Permission &copy)
{
	d->name = copy.d->name;
	d->read = copy.d->read;
	d->write = copy.d->write;
	return this;
}

}
}
