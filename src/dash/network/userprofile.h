/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PROJECTSUSERPROFILE_H
#define PROJECTSUSERPROFILE_H

#include <QList>
#include <QString>

#include <dash/exports.h>

namespace Dash {
namespace Network {
class Permission;

/**
 * @author Jorge Cuadrado <kuadrosxx@gmail.com>
*/
class DASH_EXPORT UserProfile
{
	public:
		UserProfile();
		UserProfile( const UserProfile & copy);
		UserProfile(const QString& name, const QList<Dash::Network::Permission> & modules);
		~UserProfile();
		
		bool isThisProfile(const QList<Dash::Network::Permission> & modules) const;
		
		QString name() const;
		void setName( const QString& name);
		
		QList<Dash::Network::Permission> permissions() const;
		
		void setPermission(const QList<Dash::Network::Permission>& permissions);
		
		UserProfile* operator=(const UserProfile& copy);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
