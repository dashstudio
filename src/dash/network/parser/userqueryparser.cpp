/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "userqueryparser.h"
#include <dash/network/permission.h>

#include <dcore/debug.h>

namespace Dash {
namespace Network {
namespace Parser {


struct UserQuery::Private
{
	QString login;
	QString name;
	QList<Network::Permission> permissions;
};

UserQuery::UserQuery(): DCore::XmlParserBase() , d(new Private)
{
	
}


UserQuery::~UserQuery()
{
	delete d;
}

bool UserQuery::startTag(const QString &tag, const QXmlAttributes &atts)
{
	if(root() == "userquery")
	{
		if(tag == "login" || tag == "name" )
		{
			setReadText(true);
		}
		else if(tag == "module")
		{
			d->permissions << Network::Permission(atts.value("name"), bool(atts.value("read").toInt()), bool(atts.value("write").toInt()));
		}
	}
	return true;
}

bool UserQuery::endTag(const QString &)
{
	return true;
}

void UserQuery::text(const QString &text)
{
	if(currentTag() == "login")
	{
		d->login = text;
	}
	else if(currentTag() == "name")
	{
		d->name = text;
	}
}

QString UserQuery::login()
{
	return d->login;
}

QString UserQuery::name()
{
	return d->name;
}

QList<Permission> UserQuery::permissions()
{
	return d->permissions;
}

}
}
}
