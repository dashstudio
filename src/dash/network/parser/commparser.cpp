/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "commparser.h"

namespace Dash {
namespace Network {
namespace Parser {

struct Comm::Private
{
	QString from;
	QString message;
};

Comm::Comm()
 : YAMF::Common::XmlParser(), d(new Private)
{
	
}


Comm::~Comm()
{
	delete d;
}

bool Comm::startTag(const QString &tag, const YAMF::Common::XmlAttributes &atts)
{
	if( tag == "message" )
	{
		d->from = atts.value("from");
		d->message = atts.value("text");
	}
	
	return true;
}

bool Comm::endTag(const QString &)
{
	return true;
}

QString Comm::from() const
{
	return d->from;
}

QString Comm::message() const
{
	return d->message;
}



}
}
}
