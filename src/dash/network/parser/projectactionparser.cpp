/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "projectactionparser.h"
#include <dash/network/permission.h>

namespace Dash {
namespace Network {
namespace Parser {

struct ProjectAction::Private
{
	QString name;
	QString author;
	QString description;
	QList<Dash::Network::Permission> tmpPermissions;
	
	QList<Member> members;
	Member member;
	QList<UserProfile> profiles;
	int customCount;
};

ProjectAction::ProjectAction() : DCore::XmlParserBase(), d(new Private)
{
	d->customCount = 0;
}


ProjectAction::~ProjectAction()
{
	delete d;
}

void ProjectAction::setProfiles(const QList<UserProfile>& profiles )
{
	d->profiles = profiles;
}

bool ProjectAction::startTag(const QString &tag, const QXmlAttributes &atts)
{
	if(root() == "projectquery" || root() == "updateproject" || root() == "addproject")
	{
		if(tag == "name")
		{
			setReadText(true);
		}
		else if(tag == "author")
		{
			setReadText(true);
		}
		else if(tag == "description" )
		{
			setReadText(true);
		}
		else if(tag == "user")
		{
			d->member.login = atts.value("login");
			d->member.job = atts.value("job");
		}
		else if(tag == "module")
		{
			bool read = atts.value("read") == "1" || atts.value("read") == "true";
			bool write = atts.value("write") == "1" || atts.value("write") == "true";
			d->tmpPermissions << Dash::Network::Permission(atts.value("name"), read, write);
		}
	}
	return true;
}

bool ProjectAction::endTag(const QString &tag)
{
	if(tag == "permissions")
	{
		QString nameProfile;
		
		foreach(UserProfile profile, d->profiles)
		{
			if(profile.isThisProfile(d->tmpPermissions))
			{
				nameProfile = profile.name();
			}
		}
		
		if(nameProfile.isEmpty())
		{
			d->customCount++;
			nameProfile = QString("Custom%1").arg(d->customCount);
			d->profiles << UserProfile(nameProfile, d->tmpPermissions);
		}
		
		d->member.profile = UserProfile(nameProfile, d->tmpPermissions);
		
		
		d->members << d->member;
		d->tmpPermissions.clear();
	}
	return true;
}

void ProjectAction::text(const QString &text)
{
	if(!text.trimmed().isEmpty())
	{
		if(currentTag() == "name" )
		{
			d->name = text;
		}
		else if(currentTag() == "author" )
		{
			d->author = text;
		}
		else if(currentTag() == "description" )
		{
			d->description = text;
		}
	}
}


QString ProjectAction::name() const
{
	return d->name;
}

QString ProjectAction::author() const
{
	return d->author;
}

QString ProjectAction::description() const
{
	return d->description;
}

QList<ProjectAction::Member> ProjectAction::members() const
{
	return d->members;
}

}
}
}

