/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "userlistparser.h"

namespace Dash {
namespace Network {
namespace Parser {

struct UserList::Private
{
	QList<UserList::User> users;
};

UserList::UserList() : DCore::XmlParserBase() , d(new Private)
{
}


UserList::~UserList()
{
}

bool UserList::startTag(const QString &tag, const QXmlAttributes &atts)
{
	if(root() == "userlist")
	{
		if(tag == "user")
		{
			User user;
			user.login = atts.value("login");
			user.name = atts.value("name");
			d->users << user;
		}
	}
	return true;
}

bool UserList::endTag(const QString &)
{
	return true;
}

void UserList::text(const QString &)
{
	
}

QList<UserList::User> UserList::users() const
{
	return d->users;
}

}
}
}
