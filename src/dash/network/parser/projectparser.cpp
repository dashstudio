/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "projectparser.h"

#include <QtDebug>
#include <QFile>

namespace Dash {
namespace Network {
namespace Parser {

struct Project::Private
{
	Private() : readData(false) {}
	
	bool readData;
	QByteArray project;
};

Project::Project()
 : YAMF::Common::XmlParser(), d(new Private)
{
}


Project::~Project()
{
	delete d;
}

bool Project::startTag(const QString &tag, const YAMF::Common::XmlAttributes &atts)
{
	if( tag == "data" )
	{
		d->readData = true;
	}
	return true;
}

bool Project::endTag(const QString &tag)
{
	if( tag == "data" )
	{
		d->readData = false;
	}
	return true;
}

bool Project::cdata(const QString &text)
{
	if( d->readData )
	{
		d->project = QByteArray::fromBase64(text.toLocal8Bit());
	}
	
	return true;
}

QByteArray Project::projectData() const
{
	return d->project;
}


}

}

}
