/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef FILERECEIVER_H
#define FILERECEIVER_H

#include <QObject>

#include <dash/exports.h>
#include <dash/network/socketobserver.h>

namespace Dash {
namespace Network {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASH_EXPORT FileReceiver : public QObject, virtual protected Dash::Network::SocketObserver
{
	Q_OBJECT;
	
	public:
		FileReceiver(QObject *parent = 0);
		~FileReceiver();
		
		void start(qint64 size);
		void stop();
		
		qint64 bytesReaded() const;
		qint64 totalSize() const;
		
		bool isReading() const;
		
	protected:
		void readed(const QString &);
		void dataReaded(const QByteArray &data);
		
	signals:
		void finished(const QString &file);
		
	private:
		struct Private;
		Private *const d;
};

}
}


#endif
