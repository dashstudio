/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "dispatcher.h"

#include <QSet>
#include <QQueue>
#include <QThread>
#include <QDomDocument>

#include "observer.h"
#include "source.h"

#include <dcore/debug.h>

namespace Dash {
namespace Network {
namespace Package {

class DispatcherThread// : public QThread
{
	public:
		DispatcherThread();
// 	protected:
		void run();
		
	public:
		QSet<Observer *> observers;
		QQueue<Source *> packages;
};

DispatcherThread::DispatcherThread()
{
}

void DispatcherThread::run()
{
	while( ! packages.isEmpty() )
	{
		Source *package = packages.dequeue();
		
		foreach(Observer *observer, observers)
		{
			observer->handlePackage(package);
			if( package->isAccepted() )
				break;
		}
		
		delete package;
	}
}

struct Dispatcher::Private
{
	DispatcherThread *thread;
};

Dispatcher::Dispatcher() : d(new Private)
{
	d->thread = new DispatcherThread;
}


Dispatcher::~Dispatcher()
{
	qDeleteAll(d->thread->packages);
	delete d->thread;
	delete d;
}

/**
 * @~english
 * Adds a package to be dispatched, the package is owned by the dispatcher.
 * @param pkg 
 */
void Dispatcher::addSource(Source *pkg)
{
	d->thread->packages.enqueue(pkg);
}

/**
 * @~english
 * Create and adds a package to be dispatched.
 * @param id 
 * @param xml 
 */
void Dispatcher::addSource(const QString &id, const QString &xml, Dash::Network::Socket *socket)
{
	addSource(new Source(id, xml, socket));
}

/**
 * @~english
 * Create and adds a package to be dispatched.
 * @param xml 
 */
void Dispatcher::addSource(const QString &xml, Dash::Network::Socket *socket)
{
	QDomDocument doc;
	if( doc.setContent(xml) )
	{
		addSource(doc.documentElement().tagName(), xml, socket);
	}
}


/**
 * Adds an observer to the dispatcher, The observer handle the package dispatched.
 * @param observer 
 */
void Dispatcher::addObserver(Observer *observer)
{
	d->thread->observers << observer;
}

/**
 * Removes an observer.
 * @param observer 
 */
void Dispatcher::removeObserver(Observer *observer)
{
	d->thread->observers.remove(observer);
}

/**
 * Processes all packages
 */
void Dispatcher::process()
{
// 	if( ! d->thread->isRunning() )
	{
// 		d->thread->start();
		d->thread->run();
	}
}

}
}
}
