/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "adduser.h"

namespace Dash {
namespace Network {
namespace Package {

struct AddUser::Private {
	QDomElement permissions;
};

AddUser::AddUser(const QString &login, const QString &password, const QString &name)
 : QDomDocument(), d(new Private)
{
	QDomElement root = createElement("adduser");
	
	root.appendChild(createElement("login") ).appendChild(createTextNode(login));
	root.appendChild(createElement("password") ).appendChild(createTextNode(password));
	root.appendChild(createElement("name") ).appendChild(createTextNode(name));
	
	d->permissions = createElement("permissions");
	root.appendChild(d->permissions);
	
	appendChild(root);
}


AddUser::~AddUser()
{
	delete d;
}

void AddUser::addModule(const QString &name, bool read, bool write)
{
	QDomElement module = createElement("module");
	module.setAttribute("name", name);
	module.setAttribute("read", read);
	module.setAttribute("write", write);
	
	d->permissions.appendChild(module);
}


}
}
}
