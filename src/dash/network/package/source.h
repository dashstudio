/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASH_BASEPACKAGE_H
#define DASH_BASEPACKAGE_H

#include <QString>
#include <dash/exports.h>

namespace Dash {
namespace Network {
class Socket;

namespace Package {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASH_EXPORT Source
{
	public:
		Source(const QString &root, const QString &xml, Dash::Network::Socket *socket);
		~Source();
		
		QString root() const;
		QString xml() const;
		
		void accept();
		void ignore();
		
		bool isAccepted() const;
		
		Dash::Network::Socket *socket() const;
		
	private:
		struct Private;
		Private *const d;
};

}
}
}

#endif
