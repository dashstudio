/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "openproject.h"

// <open version="0">
//         <project name="proyecto 1" />
// </open>

namespace Dash {
namespace Network {
namespace Package {

OpenProject::OpenProject(const QString& projectName): QDomDocument()
{
	QDomElement root = createElement("openproject");
	root.setAttribute("version", "0");
	appendChild(root);
	
	QDomElement m_project = createElement("project");
	m_project.setAttribute("name", projectName);
	root.appendChild(m_project);
	
}


OpenProject::~OpenProject()
{
}

void OpenProject::setProjectName( const QString& projectName )
{
	m_project.setAttribute("name", projectName);
}

}
}
}
