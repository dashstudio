/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "updateproject.h"
#include <QList>
#include <dash/network/permission.h>

namespace Dash {
namespace Network {
namespace Package {

struct UpdateProject::Private
{
	QDomText name;
	QDomElement author;
	QDomElement description;
	QDomElement members;
};

UpdateProject::UpdateProject(const QString &name): QDomDocument(), d(new Private)
{
	QDomElement root = createElement( "updateproject" );
	root.setAttribute( "version",  "0" );
	appendChild(root);
	d->name = createTextNode(name);
	root.appendChild(createElement("name")).appendChild(d->name);
}


UpdateProject::~UpdateProject()
{
	delete d;
}

void UpdateProject::setName(const QString &name)
{
	d->name.setData(name);
}

void UpdateProject::setAuthor(const QString &author)
{
	if(d->author.isNull())
	{
		d->author = createElement("author");
		d->author.appendChild(createTextNode(author));
		firstChild().appendChild(d->author);
	}
	else
	{
		d->author.firstChild().toText().setData(author);
	}
}

void UpdateProject::setDescription(const QString &description)
{
	if(d->description.isNull())
	{
		d->description = createElement("description");
		d->description.appendChild(createTextNode(description));
		firstChild().appendChild(d->description);
	}
	else
	{
		d->description.firstChild().toText().setData(description);
	}
}

void UpdateProject::addUser( const QString& login, const QString &job, const QList<Permission> & permissions )
{
	if(d->members.isNull())
	{
		d->members = createElement("members");
	}
	QDomElement user = createElement("user");
	user.setAttribute("login", login);
	user.setAttribute("job", job);
	
	QDomElement permissionsE = createElement("permissions");
	
	QList<Permission>::const_iterator it = permissions.begin();
	
	while(it != permissions.end())
	{
		QDomElement module = createElement("module");
		module.setAttribute("name", (*it).name());
		module.setAttribute("read", (*it).read());
		module.setAttribute("write", (*it).write());
		permissionsE.appendChild(module);
		++it;
	}
	user.appendChild(permissionsE);
	
	d->members.appendChild(user);
	firstChild().appendChild(d->members);
}
}
}

}
