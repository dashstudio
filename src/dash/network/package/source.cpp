/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "source.h"

#include <dash/network/socket.h>

namespace Dash {
namespace Network {
namespace Package {

struct Source::Private
{
	QString root;
	QString xml;
	
	bool accepted;
	
	Dash::Network::Socket *socket;
};

Source::Source(const QString &root, const QString &xml, Dash::Network::Socket *socket) : d(new Private)
{
	d->root = root;
	d->xml = xml;
	d->accepted = false;
	
	d->socket = socket;
}


Source::~Source()
{
	delete d;
}

void Source::accept()
{
	d->accepted = true;
}

void Source::ignore()
{
	d->accepted = false;
}

bool Source::isAccepted() const
{
	return d->accepted;
}

Dash::Network::Socket *Source::socket() const
{
	return d->socket;
}

QString Source::root() const
{
	return d->root;
}

QString Source::xml() const
{
	return d->xml;
}

}
}
}

