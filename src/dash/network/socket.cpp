/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "socket.h"

#include <QTextStream>
#include <QQueue>

#include <dcore/debug.h>


// Dash
#include "socketobserver.h"

namespace Dash {
namespace Network {

struct Socket::Private
{
	Private() : format(Text) {}
	
	QQueue<QByteArray> queue;
	QList<SocketObserver *> observers;
	
	Format format;
	
	QString readed;
};

Socket::Socket(QObject *parent)
 : /*QSslSocket*/QTcpSocket(parent), d(new Private)
{
	connect(this, SIGNAL(readyRead ()), this, SLOT(readFromServer()) );
	connect(this, SIGNAL(connected()), this, SLOT(sendQueue()));
	connect(this, SIGNAL(disconnected()), this, SLOT(clearQueue()));
}


Socket::~Socket()
{
	delete d;
}

void Socket::setFormat(Format format)
{
	d->format = format;
}

Socket::Format Socket::format() const
{
	return d->format;
}

void Socket::addObserver(SocketObserver *observer)
{
	d->observers << observer;
}

void Socket::removeObserver(SocketObserver *observer)
{
	d->observers.removeAll(observer);
}

void Socket::sendQueue()
{
	while( d->queue.count() > 0 )
	{
		if ( state() == QAbstractSocket::ConnectedState )
		{
			send(d->queue.dequeue());
		}
		else break;
	}
}

void Socket::clearQueue()
{
	d->queue.clear();
}

bool Socket::send(const QByteArray &data)
{
	if( state() == QAbstractSocket::ConnectedState )
	{
		switch(d->format)
		{
			case Text:
			{
				QByteArray toSend = data.toBase64()+"%%\n";
				QByteArray block;
				
				for(int i = 0; i < toSend.size(); i++)
				{
					block += toSend[i];
					if( i % 1024 == 0 )
					{
						if( write(block) == -1 )
						{
							flush();
							return false;
						}
						block = QByteArray();
					}
				}
				
				if( !block.isEmpty() )
				{
					return write(block) != -1;
				}
				
				flush();
				
				return true;
			}
			break;
			case Binary:
			{
				return write(data) != -1;
			}
			break;
		}
	}
	else
	{
		d->queue.enqueue(data);
	}
}

bool Socket::send(const QDomDocument &doc)
{
	return send(doc.toString(0).toLocal8Bit());
}

void Socket::readFromServer()
{
	switch(d->format)
	{
		case Text:
		{
			while(bytesAvailable() > 0 )
			{
				int bytes = bytesAvailable();
				
				bool process = false;
				{
					QString line = this->readLine();
					
					if( line.isEmpty() )
					{
						d->readed += this->read(1024);
					}
					else
					{
						d->readed += line;
					}
				}
				if ( d->readed.endsWith("%%\n") )
				{
					d->readed.remove(d->readed.lastIndexOf("%%"), 2);
					process = true;
				}
			
				if( process && !d->readed.isEmpty() )
				{
					d->readed = QByteArray::fromBase64(d->readed.toLocal8Bit());
					
					foreach(SocketObserver *o, d->observers)
					{
						o->readed(d->readed);
					}
					d->readed = QString();
					
					if( d->format != Text ) break;
				}
			}
		}
		break;
		case Binary:
		{
			d->readed = QString();
			while(bytesAvailable() > 0)
			{
				QByteArray data = this->read(1024);
				foreach(SocketObserver *o, d->observers)
				{
					o->dataReaded(data);
				}
			}
		}
		break;
	}
	
	if (this->bytesAvailable() > 0) emit readyRead();
}

}
}

