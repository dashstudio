/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASHPROJECT_H
#define DASHPROJECT_H

#include <dash/exports.h>
#include <yamf/model/project.h>

namespace Dash {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASH_EXPORT Project : public YAMF::Model::Project
{
	Q_OBJECT;
	
	public:
		Project(QObject *parent = 0);
		~Project();
		
		QString projectPalette() const;
		void importPalette(const QString &path);
		
	private:
		struct Private;
		Private *const  d;
};

}

#endif
