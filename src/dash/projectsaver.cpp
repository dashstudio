/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "projectsaver.h"

// Qt
#include <QDir>

// DLib
#include <dcore/zipper.h>
#include <dcore/algorithm.h>
#include <dcore/debug.h>

// YAMF
#include <yamf/model/project.h>
#include <yamf/model/scene.h>
#include <yamf/model/frame.h>
#include <yamf/model/library.h>
#include <yamf/model/command/manager.h>

// Dash
#include "dash/project.h"

namespace Dash {

static bool deleteProjectDir(const QString &projectDir)
{
	if(!projectDir.startsWith(".") && QFile::exists( projectDir ) )
	{
		QDir dir(projectDir);
		
		if( (dir.exists("audio") && dir.exists("video") && dir.exists("images")) || dir.exists("project") )
		{
			dDebug() << "Removing " << dir.absolutePath() << "...";
			
			dir.remove("project");
			dir.remove("library");
			dir.remove("palette");
			
			foreach(QString scene, dir.entryList(QStringList() << "scene*", QDir::Files ))
			{
				dir.remove(scene);
			}
			
			foreach( QString subdir, QStringList() << "audio" << "video" << "images" << "svg" )
			{
				if( dir.exists(subdir) )
				{
					dir.cd(subdir);
					foreach(QString file, dir.entryList() )
					{
						QString absolute = dir.absolutePath() + "/" + file;
						
						if( !file.startsWith(".") )
						{
							QFileInfo finfo(absolute);
							
							if( finfo.isFile() )
							{
								QFile::remove(absolute);
							}
						}
					}
					dir.cdUp();
					dir.rmdir(subdir);
				}
			}
			
			if( ! dir.rmdir(dir.absolutePath()) )
			{
				dError("project") << "Cannot remove project data directory!";
			}
		}
		return true;
	}
	
	return false;
}


ProjectSaver::ProjectSaver()
{
}


ProjectSaver::~ProjectSaver()
{
}

bool ProjectSaver::save(Dash::Project *project, const QString &path)
{
	QDir projectDir = QDir::tempPath()+"/dash."+QString::number(DCore::Algorithm::random());
	
	if ( !projectDir.exists() )
	{
		if ( ! projectDir.mkpath(projectDir.absolutePath()) )
		{
			dError() << "Cannot create: " << projectDir.absolutePath();
			return false;
		}
	}
	
	QString current = QDir::currentPath();
	QDir::setCurrent(projectDir.absolutePath());
	
	QStringList filesToZip;
	{
		// Save project
		QFile prj(projectDir.absolutePath()+"/project");
		
		if ( prj.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			QTextStream ts(&prj);
			
			QDomDocument doc;
			doc.appendChild(project->toXml(doc));
			
			ts << doc.toString();
			
			prj.close();
			
			QFileInfo finfo(prj);
			filesToZip << finfo.absoluteFilePath();
		}
	}
	
	// Save scenes
	{
		int index = 0;
		foreach ( YAMF::Model::Scene *scene, project->scenes().visualValues() )
		{
			QDomDocument doc;
			doc.appendChild(scene->toXml(doc));
			
			QFile scn(projectDir.path()+"/scene"+QString::number(index));
			
			if ( scn.open(QIODevice::WriteOnly | QIODevice::Text) )
			{
				QTextStream st(&scn);
				st << doc.toString();
				scn.close();
				index++;
				
				QFileInfo finfo(scn);
				filesToZip << finfo.absoluteFilePath();
			}
		}
	}
	
	// Save library
	{
		QString libraryPath = projectDir.path()+"/library";
		project->library()->save(libraryPath);
		
		filesToZip << libraryPath;
		
		QStringList dirs;
		dirs << "audio" << "video" << "images" << "svg";
		
		foreach(QString dir, dirs)
		{
			QString path = dir;
			if( QFile::exists(projectDir.absoluteFilePath(dir)) )
			{
				filesToZip << path;
			}
		}
	}
	
	// Save palette
	{
		if( QFile::copy(project->projectPalette(), projectDir.path()+"/palette") )
		{
			filesToZip << "palette";
		}
	}
	
	DCore::Zipper zipper;
	
	D_SHOW_VAR(QDir::currentPath());
	bool ok = zipper.write(filesToZip, path);
	QDir::setCurrent(current);
	
	if( ! ok )
		return false;
	
	deleteProjectDir(projectDir.absolutePath());
	
	return true;
}

bool ProjectSaver::load(Dash::Project *project, const QString &path)
{
	QDir projectDir = QDir::tempPath()+"/dash."+QString::number(DCore::Algorithm::random());
	
	DCore::Zipper zipper;
	
	if( ! zipper.read(path, projectDir.absolutePath() ) )
		return false;
	
	project->clear();
	project->setOpen(false);
	
	// Load project
	if( projectDir.exists("project") )
	{
		QFile prj(projectDir.absoluteFilePath("project"));
		
		if ( prj.open(QIODevice::ReadOnly | QIODevice::Text) )
		{
			project->fromXml(QString::fromLocal8Bit(prj.readAll()));
			prj.close();
		}
	}
	
	// Cargar libreria!
	{
		project->loadLibrary(projectDir.path()+"/library");
	}
	
	// Load scenes
	{
		QStringList scenes = projectDir.entryList(QStringList() << "scene*", QDir::Readable | QDir::Files);
		
		foreach(QString scenePath, scenes)
		{
			scenePath = projectDir.absoluteFilePath(scenePath);
			YAMF::Model::Scene *scene = project->createScene();
			
			QFile f(scenePath);
			
			if ( f.open(QIODevice::ReadOnly | QIODevice::Text) )
			{
				QString xml = QString::fromLocal8Bit(f.readAll());
				scene->fromXml(xml);
				
				f.close();
			}
		}
	}
	
	// Load palette
	{
		QString path = projectDir.path()+"/palette";
		if( QFile::exists( path ) )
		{
			project->importPalette(path);
		}
	}
	
	project->setOpen(true);
	deleteProjectDir(projectDir.absolutePath());
	
	project->commandManager()->clear();
	
	return true;
}

}
