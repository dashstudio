
DEFINES += _BUILD_DASH_
TEMPLATE = lib

win32{
    CONFIG += dll
}else{
    CONFIG += staticlib \
warn_on
}

HEADERS += dash.h \
module.h \
datasource.h \
commandparser.h \
exports.h \
projectsaver.h \
 project.h
TARGET = dash

SOURCES += module.cpp \
datasource.cpp \
commandparser.cpp  \
projectsaver.cpp \
 project.cpp
INCLUDEPATH += ..

include(../../config.pri)
include(network_module.pri)

INSTALLS += headers target

target.path = /lib
headers.path = /include/dash/
headers.files = *.h



