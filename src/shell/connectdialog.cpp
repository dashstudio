/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "connectdialog.h"

#include <dcore/config.h>
#include <dgui/iconloader.h>

#include <QDialogButtonBox>

#include "ui_connect.h"

ConnectDialog::ConnectDialog(QWidget *parent)
 : QDialog(parent)
{
	QVBoxLayout *layout = new QVBoxLayout(this);
	
	QWidget *container = new QWidget;
	ui = new Ui::Connect;
	ui->setupUi(container);
	
	ui->imageNetwork->setPixmap(DGui::IconLoader::self()->load("applications-internet.svg").pixmap(100, 100));
	ui->port->setValue(6821);
	
	layout->addWidget(container);
	
	QDialogButtonBox *box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	
	layout->addWidget(box);
	
	restoreSettings();
	
	connect(box, SIGNAL(accepted()), this, SLOT(accept()));
	connect(box, SIGNAL(rejected()), this, SLOT(reject()));
}


ConnectDialog::~ConnectDialog()
{
	saveSettings();
	delete ui;
}

QString ConnectDialog::login() const
{
	return ui->user->text();
}

QString ConnectDialog::password() const
{
	return ui->password->text();
}

QString ConnectDialog::server() const
{
	return ui->server->text();
}

int ConnectDialog::port() const
{
	return ui->port->value();
}

void ConnectDialog::saveSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("Network");
	
	config->setValue("login", login());
	
	if( ui->storePassword->isChecked() )
	{
		config->setValue("password", password());
	}
	else
	{
		config->remove("password");
	}
	
	config->setValue("server", server());
	config->setValue("port", port());
	
	config->endGroup();
}

void ConnectDialog::restoreSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("Network");
	
	ui->user->setText(config->value("login").toString());
	
	if( config->contains("password") )
	{
		ui->password->setText(config->value("password").toString());
		ui->storePassword->setChecked(true);
	}
	
	ui->server->setText(config->value("server").toString());
	ui->port->setValue(config->value("port").toInt());
	
	config->endGroup();
}
