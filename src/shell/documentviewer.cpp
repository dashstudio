/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "documentviewer.h"

#include "dash/project.h"

// YAMF

#include <yamf/drawing/paintarea.h>
#include <yamf/drawing/photogram.h>
#include <yamf/drawing/view.h>
#include <yamf/drawing/brushmanager.h>

#include <yamf/model/project.h>
#include <yamf/model/scene.h>
#include <yamf/model/layer.h>
#include <yamf/model/audiolayer.h>
#include <yamf/model/frame.h>
#include <yamf/model/object.h>

#include <yamf/model/command/manager.h>

#include <yamf/gui/tools.h>
#include <yamf/gui/drawingactions.h>
#include <yamf/gui/paintareaactions.h>

// Dash

DocumentViewer::DocumentViewer(Dash::Project *const project, QWidget *parent)
 : QMainWindow(parent), Dash::Module(project)
{
	{
		m_view = new YAMF::Drawing::View(project);
		YAMF::Drawing::PaintArea *paintarea = m_view->paintArea();
		
		paintarea->setCurrentFrame(0, 0, 0);
		
		setCentralWidget(m_view);
		
		addToolBar(Qt::LeftToolBarArea, new YAMF::Gui::Tools(paintarea, this));
		
		addToolBar(Qt::TopToolBarArea, new YAMF::Gui::DrawingActions(paintarea, this));
		
		addToolBar(Qt::TopToolBarArea, m_view->paintArea()->toolsConfigBar());
		addToolBar(Qt::TopToolBarArea, m_view->paintArea()->effectsConfigBar());
		
		addToolBar(Qt::BottomToolBarArea, new YAMF::Gui::ToolsWidget(paintarea, this));
		
		addToolBar(Qt::BottomToolBarArea, new YAMF::Gui::PaintAreaActions(paintarea, this));
	}
}


DocumentViewer::~DocumentViewer()
{
}

YAMF::Drawing::View *DocumentViewer::view()
{
	return m_view;
}

void DocumentViewer::update(const Dash::DataSource *, bool)
{
	m_view->paintArea()->photogram()->drawCurrentPhotogram();
}

void DocumentViewer::reset()
{
	m_view->setCurrentFrame(-1, -1, -1);
}

