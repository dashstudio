/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "projectmanager.h"

#include "networkhandler.h"

// DLIB
#include <dcore/algorithm.h>
#include <dcore/zipper.h>

#include <dgui/osd.h>

// YAMF
#include <yamf/model/scene.h>
#include <yamf/model/layer.h>
#include <yamf/model/audiolayer.h>
#include <yamf/model/frame.h>
#include <yamf/model/object.h>
#include <yamf/model/library.h>

#include <yamf/model/command/manager.h>
#include <yamf/model/command/base.h>

// DASH
#include "dash/project.h"
#include "dash/module.h"
#include "dash/commandparser.h"
#include "dash/projectsaver.h"

#include "connectdialog.h"

// Qt
#include <QWidget>
#include <QDir>
#include <QFileInfo>


#define CHECK_CONNECTION {bool ok = false; if( d->networkHandler ) if( d->networkHandler->isValid() ) ok = true; if( !ok ) { DGui::Osd::self()->display(QObject::tr("You are not connected to server."), DGui::Osd::Error ); return; }}

#define CONNECT_TO_SERVER {bool ok = false; if( d->networkHandler ) if( d->networkHandler->isOpen() ) ok = true; if( !ok ) { if(!connectToServer()) return; }}

ProjectParams::ProjectParams() : isLocal(true), port(5123)
{
}

ProjectParams::~ProjectParams()
{
}


struct ProjectManager::Private
{
	Private() : isLocal(true), project(0), commandCounter(0), networkHandler(0) {}
	
	bool isLocal;
	Dash::Project *project;
	
	QList<Dash::Module *> modules;
	
	int commandCounter;
	
	NetworkHandler *networkHandler;
};

ProjectManager::ProjectManager(QObject *parent)
 : QObject(parent), d(new Private)
{
	d->project = new Dash::Project(this);
	d->project->commandManager()->setObserver(this);
}


ProjectManager::~ProjectManager()
{
	delete d->networkHandler;
	delete d;
}

void ProjectManager::createProject(const ProjectParams &params)
{
	d->project->clear();
	d->project->setProjectName(params.projectName);
	d->project->setAuthor(params.author);
	d->project->setDescription(params.description);
	
	d->isLocal = params.isLocal;
	
	if( !d->isLocal ) // FIXME: Do not create the project
	{
		this->connectToServer(params.server, params.port, params.login, params.password);
		
		d->networkHandler->newProject(&params);
	}
	
	d->project->setOpen(true);
}

void ProjectManager::createScene(const QString &name, int frames)
{
	if( ! d->project->isOpen() || !d->isLocal ) return;
	
	YAMF::Model::Scene *scn = d->project->createScene(-1, name);
	YAMF::Model::Layer *lyr = scn->createLayer();
	
	if( frames < 1 ) frames = 1;
	
	while(frames--)
		lyr->createFrame();
}

void ProjectManager::closeProject()
{
	foreach(Dash::Module *module, d->modules)
	{
		module->reset();
	}
	
	d->project->clear();
	
	if( !d->isLocal )
	{
		d->networkHandler->reset();
	}
	
	d->project->setOpen(false);
}

Dash::Project *ProjectManager::project() const
{
	return d->project;
}

void ProjectManager::registerModule(Dash::Module *module)
{
	d->modules << module;
}

bool ProjectManager::aboutToRedo(YAMF::Command::Base *command)
{
	if( d->isLocal )
	{
		return false;
	}
	
// 	QString xml = command->toXml();
	
	// TODO: Enviar por socket
// 	d->networkHandler->send(xml);
	d->networkHandler->execute(command);
	
	return true;
}

bool ProjectManager::aboutToUndo(YAMF::Command::Base *command)
{
	if( d->isLocal )
	{
		return false;
	}
	
// 	QString xml = command->toXml();
	
	// TODO: Enviar por socket
// 	d->networkHandler->send(xml);
	qFatal("Undo is not supported yet!");
	
	return true;
}

void ProjectManager::executed(const YAMF::Command::Base *command)
{
	if( d->isLocal )
	{
		Dash::CommandParser parser;
		Dash::DataSource *source = parser.build(command);
		
		Q_ASSERT(source != 0);
		
		if( !source )
		{
			return;
		}
		
		Q_ASSERT(source->type() != 0);
		
		foreach(Dash::Module *module, d->modules)
		{
			if( module->sources() & source->type() )
			{
				module->update(source, false);
			}
		}
		
		delete source;
		
		d->commandCounter++;
	}
}

void ProjectManager::unexecuted(const YAMF::Command::Base *command)
{
	if( d->isLocal )
	{
		Dash::CommandParser parser;
		
		Dash::DataSource *source = parser.build(command);
		
		if( !source ) return;
		
		foreach(Dash::Module *module, d->modules)
		{
			if( module->sources() & source->type() )
			{
				module->update(source, true);
			}
		}
		
		d->commandCounter--;
		delete source;
	}
}

bool ProjectManager::isModified() const
{
	return d->commandCounter > 0;
}

void ProjectManager::setLocal(bool l)
{
	d->isLocal = l;
}

bool ProjectManager::isLocal() const
{
	return d->isLocal;
}

void ProjectManager::showChatWindow()
{
	if( !d->isLocal && d->networkHandler)
	{
		d->networkHandler->showChatWindow();
	}
	else
	{
		DGui::Osd::self()->display(tr("Cannot open a chat window working on a local project..."), DGui::Osd::Error );
	}
}

void ProjectManager::addPackageObserver(Dash::Network::Package::Observer *observer)
{
	d->networkHandler->addObserver(observer);
}

void ProjectManager::removePackageObserver(Dash::Network::Package::Observer *observer)
{
	d->networkHandler->removeObserver(observer);
}

NetworkHandler *ProjectManager::networkHandler() const
{
	return d->networkHandler;
}

void ProjectManager::connectToServer(const QString &host, quint16 port, const QString &login, const QString &password)
{
	delete d->networkHandler;
	d->networkHandler = new NetworkHandler(this);
	d->networkHandler->connectToHost(host, port, login, password);
}

bool ProjectManager::connectToServer()
{
	ConnectDialog dialog;
	
	if( dialog.exec() == QDialog::Accepted )
	{
		connectToServer(dialog.server(), dialog.port(), dialog.login(), dialog.password());
		return true;
	}
	
	return false;
}

void ProjectManager::openCollaborativeProject()
{
	CONNECT_TO_SERVER;
	
	d->networkHandler->openProject();
}

bool ProjectManager::saveProject(const QString &path)
{
	if( d->isLocal )
	{
		if( Dash::ProjectSaver::save(d->project, path) )
		{
			d->commandCounter = 0;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// TODO: Network
	
	return false;
}

bool ProjectManager::loadProject(const QString &path)
{
	closeProject();
	d->project->commandManager()->blockSignals(true);
	if( Dash::ProjectSaver::load(d->project, path) )
	{
		d->project->commandManager()->blockSignals(false);
		
		d->commandCounter = 0;
		
		return true;
	}
	
	d->project->commandManager()->blockSignals(false);
	
	return false;
}


