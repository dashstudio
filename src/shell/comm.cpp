/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "comm.h"

#include <dgui/osd.h>

#include <dash/network/package/source.h>
#include <dash/network/package/chat.h>
#include <dash/network/package/notice.h>
#include <dash/network/package/wall.h>

#include <dash/network/parser/commparser.h>

#include "networkhandler.h"
#include "components/network/chatwindow.h"

Comm::Comm(NetworkHandler *handler)
 : Dash::Network::Package::Observer(), m_handler(handler)
{
	m_chatWindow = new Dash::Component::Network::ChatWindow;
	connect(m_chatWindow, SIGNAL(sendText(const QString &, int)), this, SLOT(onSendChat(const QString &, int)));
}


Comm::~Comm()
{
	delete m_chatWindow;
}

void Comm::handlePackage(Dash::Network::Package::Source *const pkg)
{
	QString root = pkg->root();
	
	if( root == "chat" )
	{
		Dash::Network::Parser::Comm parser;
		
		if( parser.parse(pkg->xml()) )
		{
			m_chatWindow->addMessage(parser.from(), parser.message());
		}
	}
	else if(root == "notice" )
	{
		Dash::Network::Parser::Comm parser;
		
		if( parser.parse(pkg->xml()) )
		{
			m_chatWindow->addNotice(parser.from(), parser.message());
		}
	}
	else if( root == "wall" )
	{
		Dash::Network::Parser::Comm parser;
		
		if( parser.parse(pkg->xml()) )
		{
			DGui::Osd::self()->display(tr("%1 says: %2").arg(parser.from()).arg(parser.message()));
			m_chatWindow->addWall(parser.from(), parser.message());
		}
	}
}

Dash::Component::Network::ChatWindow *Comm::chatWindow() const
{
	return m_chatWindow;
}

void Comm::onSendChat(const QString &text, int type)
{
	switch(type)
	{
		case Dash::Component::Network::ChatWindow::Chat:
		{
			Dash::Network::Package::Chat chat(text);
			m_handler->send(chat.toString());
		}
		break;
		case Dash::Component::Network::ChatWindow::Notice:
		{
			Dash::Network::Package::Notice notice(text);
			m_handler->send(notice.toString());
		}
		break;
		case Dash::Component::Network::ChatWindow::Wall:
		{
			Dash::Network::Package::Wall wall(text);
			m_handler->send(wall.toString());
		}
		break;
	}
}

