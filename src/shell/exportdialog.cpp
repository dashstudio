/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "exportdialog.h"

#include <QFileDialog>

#include "ui_export_plugins.h"
#include "ui_scene_selector.h"
#include "ui_export_settings.h"


#include <dcore/debug.h>

#include <dgui/iconloader.h>
#include <dgraphics/moviegenerator.h>

#include <yamf/model/project.h>
#include <yamf/model/scene.h>

#include <yamf/render/exporter.h>
#include <yamf/render/renderer.h>

#ifdef YAMF_HAVE_MOVIEEXPORTER
#include <yamf/render/movieexporter.h>
#endif

#ifdef YAMF_HAVE_SWFEXPORTER
#include <yamf/render/swfexporter.h>
#endif


class PngGenerator : public DGraphics::MovieGenerator
{
	public:
		PngGenerator(const QSize &size);
		
		void setRate(int ) {};
		
	protected:
		void handle(const QImage &image);
		void __saveMovie(const QString &filename);
		
	private:
		int m_counter;
		QStringList m_names;
};

PngGenerator::PngGenerator(const QSize &size) : DGraphics::MovieGenerator(size.width(), size.height()), m_counter(0)
{
}

void PngGenerator::handle(const QImage &image)
{
	QString base = QString::number(m_counter++);
	for(int i = 0; i < 10-base.length(); i++)
	{
		base.prepend("0");
	}
	
	QString fname = QDir::temp().absoluteFilePath(base);
	m_names << fname;
	
	image.save(fname, "PNG");
}

void PngGenerator::__saveMovie(const QString &fileName)
{
	QFileInfo finfo(fileName);
	
	QString baseName = finfo.baseName();
	
	foreach(QString fname, m_names)
	{
		QFileInfo finfo(fname);
		
		QString dest = baseName+finfo.fileName()+".png";
		QFile::copy(fname, dest );
		QFile::remove(fname);
	}
}

class PngExporter : public YAMF::Render::Exporter
{
	public:
		PngExporter();
		~PngExporter();
		
	protected:
		virtual DGraphics::MovieGeneratorInterface *createGenerator(DGraphics::MovieGeneratorInterface::Format format, const QSize &size, int fps);
		
};

PngExporter::PngExporter()
{
}

PngExporter::~PngExporter()
{
}

DGraphics::MovieGeneratorInterface *PngExporter::createGenerator(DGraphics::MovieGeneratorInterface::Format, const QSize &size, int)
{
	return new PngGenerator(size);
}

class ExportPluginPage : public DGui::WizardPage
{
	Q_OBJECT;
	
	public:
		ExportPluginPage();
		~ExportPluginPage();
		
		virtual bool isComplete() const;
		virtual void reset();
		
		void loadExporters();
		void addExporter(const QString &name, YAMF::Render::Exporter *exporter);
		void setFormats(DGraphics::MovieGeneratorInterface::Formats formats);
		
		YAMF::Render::Exporter *currentExporter() const;
		DGraphics::MovieGeneratorInterface::Format currentFormat() const;
		
	public slots:
		void selectedExporterItem(QListWidgetItem *item);
		void selectedFormatItem(QListWidgetItem *item);
		
	private:
		Ui::ExportPlugins ui;
		QHash<QListWidgetItem *, YAMF::Render::Exporter *> m_exporters;
};

ExportPluginPage::ExportPluginPage() : DGui::WizardPage(tr("Select format..."))
{
	QWidget *container = new QWidget;
	ui.setupUi(container);
	
	loadExporters();
	
	addWidget(container);
	
	connect(ui.generator, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(selectedExporterItem(QListWidgetItem *)));
	connect(ui.format, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(selectedFormatItem(QListWidgetItem *)));
}

ExportPluginPage::~ExportPluginPage()
{
	qDeleteAll(m_exporters);
}


bool ExportPluginPage::isComplete() const
{
	if( ui.generator->currentItem() )
	{
		return ui.format->currentItem() || ui.generator->currentItem()->text() == tr("png");
	}
	
	return false;
}

void ExportPluginPage::reset()
{
}

void ExportPluginPage::loadExporters()
{
#ifdef YAMF_HAVE_MOVIEEXPORTER
	addExporter(tr("movie"), new YAMF::Render::MovieExporter);
#endif
	
#ifdef YAMF_HAVE_SWFEXPORTER
	addExporter(tr("swf"), new YAMF::Render::SwfExporter);
#endif
	
	addExporter(tr("png"), new PngExporter);
}

void ExportPluginPage::addExporter(const QString &name, YAMF::Render::Exporter *exporter)
{
	QListWidgetItem *item = new QListWidgetItem(name, ui.generator);
	
	m_exporters[item] = exporter;
}

void ExportPluginPage::setFormats(DGraphics::MovieGeneratorInterface::Formats formats)
{
	ui.format->clear();
	
	if ( formats & DGraphics::MovieGeneratorInterface::SWF )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("Macromedia flash"), ui.format);
		
		format->setData(3124,DGraphics::MovieGeneratorInterface::SWF);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::MPEG )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("MPEG Video"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::MPEG);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::AVI )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("AVI Video"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::AVI);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::RM )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("RealMedia Video"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::RM);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::ASF )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("ASF Video"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::ASF);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::MOV )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("QuickTime Video"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::MOV);
	}
	
	if ( formats & DGraphics::MovieGeneratorInterface::GIF )
	{
		QListWidgetItem *format = new QListWidgetItem(tr("Gif Image"), ui.format);
		
		format->setData(3124, DGraphics::MovieGeneratorInterface::GIF);
	}
	
// 	if ( formats & DGraphics::MovieGeneratorInterface::PNG )
// 	{
// 		QListWidgetItem *format = new QListWidgetItem(tr("PNG Image Array"), ui.format);
// 		
// 		format->setData(3124, DGraphics::MovieGeneratorInterface::PNG);
// 	}
// 	
// 	if ( formats & DGraphics::MovieGeneratorInterface::JPEG )
// 	{
// 		QListWidgetItem *format = new QListWidgetItem(tr("JPEG Image Array"), ui.format);
// 		
// 		format->setData(3124, DGraphics::MovieGeneratorInterface::JPEG);
// 	}
// 	
// 	if ( formats & DGraphics::MovieGeneratorInterface::SMIL )
// 	{
// 		QListWidgetItem *format = new QListWidgetItem(tr("SMIL"), ui.format);
// 		
// 		format->setData(3124, DGraphics::MovieGeneratorInterface::SMIL);
// 	}
}

YAMF::Render::Exporter *ExportPluginPage::currentExporter() const
{
	return m_exporters.value(ui.generator->currentItem());
}

DGraphics::MovieGeneratorInterface::Format ExportPluginPage::currentFormat() const
{
	QListWidgetItem *item = ui.format->currentItem();
	
	if( !item ) return DGraphics::MovieGeneratorInterface::NONE;
	
	return static_cast<DGraphics::MovieGeneratorInterface::Format>(item->data(3124).toInt());
}


void ExportPluginPage::selectedExporterItem(QListWidgetItem *item)
{
	if( item )
	{
		ui.format->clear();
		
		if( item->text() == tr("movie")  )
		{
			setFormats(DGraphics::MovieGeneratorInterface::AVI | DGraphics::MovieGeneratorInterface::SWF | DGraphics::MovieGeneratorInterface::MPEG | DGraphics::MovieGeneratorInterface::MOV | DGraphics::MovieGeneratorInterface::ASF );
		}
		else if ( item->text() == tr("swf") )
		{
			setFormats(DGraphics::MovieGeneratorInterface::SWF );
		}
	}
	
	emit completed();
}

void ExportPluginPage::selectedFormatItem(QListWidgetItem *)
{
	emit completed();
}


////////////////////////////////////////////////

class SceneSelectorPage : public DGui::WizardPage
{
	Q_OBJECT;
	
	public:
		SceneSelectorPage();
		~SceneSelectorPage();
		
		void load(YAMF::Model::Project *project);
		
		virtual bool isComplete() const;
		virtual void reset();
		
		QList<YAMF::Model::Scene *> selectedScenes() const;
		
	private:
		Ui::SceneSelector ui;
		QHash<QString, YAMF::Model::Scene *> m_scenes;
};

SceneSelectorPage::SceneSelectorPage() : DGui::WizardPage(tr("Select scenes..."))
{
	QWidget *container = new QWidget;
	ui.setupUi(container);
	
	addWidget(container);
	
	connect(ui.scenes, SIGNAL(changed()), this, SIGNAL(completed()));
}

SceneSelectorPage::~SceneSelectorPage()
{
}

void SceneSelectorPage::load(YAMF::Model::Project *project)
{
	m_scenes.clear();
	
	QList<YAMF::Model::Scene *> scenes = project->scenes().values();
	foreach(YAMF::Model::Scene *scene, scenes)
	{
		QString name = scene->sceneName();
		ui.scenes->addItem(name);
		m_scenes[name] = scene;
	}
	
	emit completed();
}

bool SceneSelectorPage::isComplete() const
{
	return ui.scenes->selectedItems().count() > 0;
}

void SceneSelectorPage::reset()
{
}

QList<YAMF::Model::Scene *> SceneSelectorPage::selectedScenes() const
{
	QList<YAMF::Model::Scene *> scenes;
	
	QStringList selectedNames = ui.scenes->selectedItems();
	
	foreach(QString scene, selectedNames)
	{
		scenes << m_scenes.value(scene);
	}
	
	return scenes;
}


////////////////////////////////////////////////

class ExportSettingsPage : public DGui::WizardPage
{
	Q_OBJECT;
	
	public:
		ExportSettingsPage();
		~ExportSettingsPage();
		
		virtual bool isComplete() const;
		virtual void reset();
		
		void setScenes(const QList<YAMF::Model::Scene *> &scenes);
		
		QString fileName() const;
		int fps() const;
		
	private slots:
		void onBrowseClicked();
		
	private:
		Ui::ExportSettings ui;
};

ExportSettingsPage::ExportSettingsPage() : DGui::WizardPage(tr("Settings..."))
{
	QWidget *container = new QWidget;
	ui.setupUi(container);
	
	ui.browse->setIcon(DGui::IconLoader::self()->load("folder.svg"));
	connect(ui.browse, SIGNAL(clicked()), this, SLOT(onBrowseClicked()));
	
	ui.size->setX(640);
	ui.size->setY(480);
	
	ui.fps->setValue(24);
	
	addWidget(container);
	
	connect(ui.preview, SIGNAL(clicked()), ui.animation, SLOT(start()));
	connect(ui.fps, SIGNAL(valueChanged(int)), ui.animation, SLOT(setFps(int)));
}

ExportSettingsPage::~ExportSettingsPage()
{
}


bool ExportSettingsPage::isComplete() const
{
	return true;
}

void ExportSettingsPage::reset()
{
}

void ExportSettingsPage::setScenes(const QList<YAMF::Model::Scene *> &scenes)
{
	ui.animation->setScenes(scenes);
}

QString ExportSettingsPage::fileName() const
{
	return ui.fileName->text();
}

int ExportSettingsPage::fps() const
{
	return ui.fps->value();
}


void ExportSettingsPage::onBrowseClicked()
{
	QString file = QFileDialog::getSaveFileName(this);
	
	
	if( !file.isEmpty() )
	{
		ui.fileName->setText(file);
	}
}

////////////////////////////////////////////////

struct ExportDialog::Private
{
	ExportPluginPage *pluginPage;
	ExportSettingsPage *settingsPage;
	SceneSelectorPage *scenes;
};

ExportDialog::ExportDialog(YAMF::Model::Project *const project, QWidget *parent)
 : DGui::Wizard(parent), d(new Private)
{
	d->pluginPage = new ExportPluginPage;
	addPage(d->pluginPage);
	
	d->scenes = new SceneSelectorPage;
	d->scenes->load(project);
	addPage(d->scenes);
	
	d->settingsPage = new ExportSettingsPage;
	d->settingsPage->setScenes(project->scenes().values());
	addPage(d->settingsPage);
}


ExportDialog::~ExportDialog()
{
	delete d;
}

bool ExportDialog::generate()
{
	YAMF::Render::Exporter *exporter = d->pluginPage->currentExporter();
	DGraphics::MovieGeneratorInterface::Format format = d->pluginPage->currentFormat();
	
	if( exporter == 0 ) return false;
// 	if( format == DGraphics::MovieGeneratorInterface::NONE) return false;
	
	QList<YAMF::Model::Scene *> scenes = d->scenes->selectedScenes();
	
	exporter->generate(scenes, d->settingsPage->fileName(), format, d->settingsPage->size(), d->settingsPage->fps());
	
	return true;
}


#include "exportdialog.moc"

