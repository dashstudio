/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "mainwindow.h"

// DLib
#include <dcore/config.h>
#include <dcore/debug.h>

#include <dgui/application.h>
#include <dgui/iconloader.h>
#include <dgui/osd.h>
#include <dgui/tipdialog.h>

// YAMF
#include <yamf/common/yamf.h>

#include <yamf/model/project.h>
#include <yamf/model/library.h>
#include <yamf/model/libraryobject.h>

#include <yamf/gui/projectview.h>

#include <yamf/drawing/view.h>
#include <yamf/drawing/paintarea.h>
#include <yamf/drawing/brushmanager.h>
#include <yamf/drawing/photogram.h>

// DASH

#include "documentviewer.h"
#include "projectmanager.h"
#include "settingsdialog.h"
#include "newprojectdialog.h"
#include "exportdialog.h"
#include "dash/project.h"

#include "dash/network/socket.h"
#include "dash/network/package/transfer.h"
#include "dash/network/package/uploadproject.h"

#include <components/timeline/timeline.h>
#include <components/projectbrowser/projectbrowser.h>
#include <components/library/librarywidget.h>
#include <components/base/manager.h>
#include "components/help/helpwidget.h"

#include <components/colorpalette/mixer.h>
#include <components/colorpalette/palettes.h>

#include <components/camera/camerawidget.h>

#include "components/network/editmembers.h"
#include "networkhandler.h"

// Qt

#include <QMessageBox>
#include <QMenuBar>
#include <QMenu>
#include <QMdiArea>
#include <QApplication>
#include <QFileDialog>
#include <QTimer>
#include <QSettings>
#include <QCloseEvent>

#define COMING_SOON QMessageBox::warning(this, tr("Coming soon"), tr("Not implemented yet"));

MainWindow::MainWindow(QWidget *parent)
 : Ideality::StackedMainWindow(parent)
{
	setWindowTitle(tr("Dash"));
	setWindowIcon(QIcon(__PREFIX__+QString("/share/icons/dash.png")));
	
	YAMF::initialize();
	
	m_actionManager = new DGui::ActionManager(this);
	m_componentManager = new Dash::Component::Manager(this);
	
	m_projectManager = new ProjectManager(this);
	m_viewer = new DocumentViewer(m_projectManager->project(), this);
	m_brushSynchronizer = new Component::BrushSynchronizer(this);
	
	loadSettings();
	
	setupFileActions();
	setupViewActions();
	setupEditActions();
	
	setupMenu();
	setupComponents();
	
	m_projectManager->registerModule(m_viewer);
	foreach(Dash::Component::Widget *module, m_componentManager->widgets())
	{
		m_projectManager->registerModule(module);
	}
	
	{
		addWidget(m_viewer, Dash::Drawing);
		
		QMdiArea *ws = new QMdiArea(this);
		Dash::Component::CameraWidget *camera = new Dash::Component::CameraWidget(m_projectManager->project());
		
		m_projectManager->registerModule(camera);
		ws->addSubWindow(camera, Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);
		
		addWidget(ws, Dash::Animation);
	}
	
	QTimer::singleShot(100, this, SLOT(loadLastProject()));
}


MainWindow::~MainWindow()
{
	delete m_actionManager;
}


void MainWindow::setupFileActions()
{
	QAction *newProject = new QAction( DGui::IconLoader::self()->load("project.png"), tr("New project"), this);
	newProject->setShortcut(QKeySequence::New);
	connect(newProject, SIGNAL(triggered()), this, SLOT(newProject()));
	
	newProject->setStatusTip(tr( "Opens a new project"));
	m_actionManager->add( newProject, "newproject", "file" );


	QAction *openFile = new QAction( DGui::IconLoader::self()->load("open.png"), tr( "Open project" ), this);
	openFile->setShortcut(QKeySequence::Open);
	
	connect(openFile, SIGNAL(triggered()), this, SLOT(openProject()));
	
	m_actionManager->add( openFile, "openproject", "file" );
	openFile->setStatusTip(tr("Loads an existent project"));
	
	{
		QAction *openFromServer = new QAction(DGui::IconLoader::self()->load("open.png"), tr("Open collaborative project..."), this);
		
		connect(openFromServer, SIGNAL(triggered()), this, SLOT(openCollaborativeProject()));
		m_actionManager->add(openFromServer, "opencollaborativeproject", "file");
		
		QAction *disconnectFromServer = new QAction(DGui::IconLoader::self()->load("close.png"), tr("Disconnect from server..."), this);
		
		connect(disconnectFromServer, SIGNAL(triggered()), this, SLOT(disconnectFromServer()));
		m_actionManager->add(disconnectFromServer, "disconnect", "network");
	}
	

	QAction *pushToServer = new QAction(QIcon(), tr("Push project to server..."), this);
	
	connect(pushToServer, SIGNAL(triggered()), this, SLOT(pushProjectToServer()));
	m_actionManager->add(pushToServer, "pushprojecttoserver", "file");

	QAction *save = new QAction( DGui::IconLoader::self()->load("save.png"), tr( "Save project..." ), this);
	save->setShortcut(QKeySequence::Save);
	
	connect(save, SIGNAL(triggered()), this, SLOT(saveProject()));
	m_actionManager->add( save, "saveproject", "file" );
	save->setStatusTip(tr("Saves the current project in the current location"));
	
	QAction *saveAs = new QAction( tr( "Save project &As..." ), this);
	connect(saveAs, SIGNAL(triggered()), this, SLOT(saveProjectAs()));
	saveAs->setStatusTip(tr("Opens a dialog box to save the current project in any location"));
	m_actionManager->add( saveAs, "saveprojectas", "file" );

	QAction *close = new QAction(DGui::IconLoader::self()->load("close.png"), tr( "Cl&ose project" ), this);
	close->setShortcut(QKeySequence::Close);
	connect(close, SIGNAL(triggered()), this, SLOT(closeProject()));
	close->setStatusTip(tr("Closes the active project"));
	m_actionManager->add( close, "closeproject", "file" );
	
	QAction *exportProject = new QAction( DGui::IconLoader::self()->load("export.png"), tr( "&Export..." ), this);
	 
	connect(exportProject, SIGNAL(triggered()), this, SLOT(exportProject()));
	exportProject->setStatusTip(tr("Exports project to different formats"));
	m_actionManager->add( exportProject, "export", "file" );
	
	QAction *importBitmap = new QAction(DGui::IconLoader::self()->load("import.png"), tr( "&Import bitmap..." ), this);
	importBitmap->setStatusTip(tr("Import a bitmap to the project"));
	m_actionManager->add( importBitmap, "importbitmap", "file" );
	
	QAction *importSound = new QAction(DGui::IconLoader::self()->load("import.png"), tr( "&Import sounds..." ), this);
	importSound->setStatusTip(tr("Import a sound to the project"));
	m_actionManager->add( importSound, "importsound", "file" );
	
	QAction *importSvg = new QAction(DGui::IconLoader::self()->load("import.png"), tr( "&Import svg..." ), this);
	importSvg->setStatusTip(tr("Import a SVG to the project"));
	m_actionManager->add( importSvg, "importsvg", "file" );
}

void MainWindow::setupViewActions()
{
	QAction *showChatWindow = new QAction(QIcon(), tr( "Show chat window" ), this);
	showChatWindow->setVisible(false);
	
	connect(showChatWindow, SIGNAL(triggered()), this, SLOT(showChatWindow()));
// 	settings->setStatusTip(tr(""));
	m_actionManager->add( showChatWindow, "showchatwindow", "network" );
}

void MainWindow::setupEditActions()
{
	QAction *settings = new QAction(DGui::IconLoader::self()->load("settings.png"), tr( "Settings..." ), this);
	connect(settings, SIGNAL(triggered()), this, SLOT(configure()));
	m_actionManager->add( settings, "settings", "edit" );
	
	QAction *editMembers = new QAction(DGui::IconLoader::self()->load("users.png"), tr( "Edit project members..." ), this);
	connect(editMembers, SIGNAL(triggered()), this, SLOT(editProjectMembers()));
	m_actionManager->add( editMembers, "editprojectmembers", "network" );
	
	
}

void MainWindow::setupMenu()
{
	{
		QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
		
		{
			QMenu *newMenu = fileMenu->addMenu(tr("New..."));
			newMenu->addAction(m_actionManager->find("newproject"));
		}
		
		fileMenu->addAction(m_actionManager->find("openproject"));
		
		QMenu *recent = fileMenu->addMenu(tr("Open recent"));
		foreach(QString filePath, m_recentProjects)
		{
			recent->addAction(filePath);
		}
		connect(recent, SIGNAL(triggered( QAction* )), this, SLOT(openRecent(QAction *)));
		
		fileMenu->addAction(m_actionManager->find("opencollaborativeproject"));
		fileMenu->addAction(m_actionManager->find("pushprojecttoserver"));
		fileMenu->addAction(m_actionManager->find("disconnect"));
		
		fileMenu->addSeparator();
		
		fileMenu->addAction(m_actionManager->find("saveproject"));
		fileMenu->addAction(m_actionManager->find("saveprojectas"));
		
		fileMenu->addSeparator();
		
		fileMenu->addAction(m_actionManager->find("closeproject"));
		
		fileMenu->addSeparator();
		
		fileMenu->addAction(m_actionManager->find("export"));
		
		QMenu *importMenu = fileMenu->addMenu(tr("Import"));
		importMenu->addAction(m_actionManager->find("importbitmap"));
		importMenu->addAction(m_actionManager->find("importsvg"));
		importMenu->addAction(m_actionManager->find("importsound"));
		
		fileMenu->addSeparator();
		
		QAction *quit = fileMenu->addAction(tr("&Quit"));
		connect(quit, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));
	}
	
	{
		QMenu *viewMenu = menuBar()->addMenu(tr("&View"));
		viewMenu->addAction( m_actionManager->find("showchatwindow") );
	}
	
	{
		QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
		
		editMenu->addAction( m_actionManager->find("editprojectmembers", "network" ) );
		editMenu->addSeparator();
		editMenu->addAction( m_actionManager->find("settings") );
	}
	
	{
		QMenu *workspaceMenu = menuBar()->addMenu(tr("&Workspace"));
		connect( workspaceMenu, SIGNAL(triggered( QAction * )), this, SLOT(changePerspective( QAction *)));
		
		QActionGroup *workspaces = new QActionGroup(this);
		workspaces->setExclusive(true);
		
		QAction *drawingAction = new QAction( tr("Drawing"), workspaceMenu );
		drawingAction->setShortcut(QKeySequence(Qt::Key_F9));
		drawingAction->setCheckable(true);
		drawingAction->setChecked(true);
		drawingAction->setData(Dash::Drawing );
		
		QAction *animationAction = new QAction( tr("Animation"), workspaceMenu );
		animationAction->setShortcut(QKeySequence(Qt::Key_F10));
		animationAction->setCheckable(true);
		animationAction->setData(Dash::Animation);
		
		workspaces->addAction(drawingAction);
		workspaces->addAction(animationAction);
		
		workspaceMenu->addActions(workspaces->actions());
	}
	
	{
		QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
		
		helpMenu->addAction(tr("Contents..."), this, SLOT(showHelp()))->setShortcut(QKeySequence::HelpContents);
		helpMenu->addSeparator();
		helpMenu->addAction(tr("About Dash..."), this, SLOT(aboutDash()));
		helpMenu->addAction(tr("About Qt..."), qApp, SLOT(aboutQt()));
	}
	
	m_actionManager->setVisible("network", false);
}

void MainWindow::setupComponents()
{
	Dash::Component::TimeLine *tl = new Dash::Component::TimeLine(m_projectManager->project());
	m_componentManager->add(tl);
	
	connectToPaintArea(tl);
	
	
// 	m_viewer->view()->paintArea()->brushManager()->setPen(QPen(Qt::blue,3));
	
// 	Component::ColorPalette *colorPalette = new  Component::ColorPalette(m_projectManager->project());
// 	m_componentManager->add(colorPalette);
	
	Dash::Component::LibraryWidget *library = new Dash::Component::LibraryWidget(m_projectManager->project());
	connect(m_actionManager->find("importbitmap", "file"), SIGNAL(triggered()), library, SLOT(importBitmap()));
	connect(m_actionManager->find("importsvg", "file"), SIGNAL(triggered()), library, SLOT(importSvg()));
	connect(m_actionManager->find("importsound", "file"), SIGNAL(triggered()), library, SLOT(importSound()));
	connect(library, SIGNAL(addSymbolToProject(const QString&)), m_viewer->view(), SLOT(addSymbol(const QString&)));
	
	m_componentManager->add(library);
	connectToPaintArea(library);
	
	
	connectToPaintArea(m_brushSynchronizer);
	addToolView(m_brushSynchronizer->mixer(), Qt::LeftDockWidgetArea, Dash::Drawing);
	addToolView(m_brushSynchronizer->palettes(), Qt::LeftDockWidgetArea, Dash::Drawing);
	
	/*********************/
	m_projectBrowser = new Dash::Component::ProjectBrowser(0, m_viewer->view()->paintArea()->scene());
	
	m_componentManager->add(m_projectBrowser);
	
	connectToPaintArea(m_projectBrowser);
	
// 	m_projectView = new YAMF::Gui::ProjectView(m_projectManager->project());
// 	addToolView(m_projectView, Qt::RightDockWidgetArea, Dash::All);
}

void MainWindow::newProject()
{
	if(!closeProject()) return;
	
	NewProjectDialog dialog;
	
	if( dialog.exec() == QDialog::Accepted )
	{
		ProjectParams params;
		params.projectName = dialog.projectName();
		params.author = dialog.author();
		params.description = dialog.description();
		
		params.isLocal = !dialog.useNetwork();
		
		if( ! params.isLocal )
		{
			params.login = dialog.login();
			params.password = dialog.password();
			params.server = dialog.server();
			params.port = dialog.port();
			
			m_actionManager->setVisible("network", true);
		}
		else
		{
			m_actionManager->setVisible("network", false);
		}
		
		m_projectManager->createProject(params);
		
		foreach(QString scene, dialog.scenes())
		{
			m_projectManager->createScene(scene, dialog.framesPerScene());
		}
	}
}

bool MainWindow::openProject()
{
	QString filePath = QFileDialog::getOpenFileName(this, QString(), QString(), tr("Dash project (*.dsh)") );
	
	if( filePath.isEmpty() )
	{
		return false;
	}
	
	return openProject(filePath);
}

bool MainWindow::openProject(const QString &filePath)
{
	if(!closeProject()) return false;
	
	setUpdatesEnabled(false);
	if( m_projectManager->loadProject(filePath) )
	{
		{
			m_recentProjects.removeAll(filePath);
			m_recentProjects.prepend(filePath);
		}
		
		m_projectBrowser->setProject(0);
		
		m_brushSynchronizer->palettes()->setProjectPalette(m_projectManager->project()->projectPalette());
		
		m_currentFile = filePath;
// 		m_projectView->setProject(m_projectManager->project());
		
		m_viewer->view()->setCurrentFrame(0, 0, 0);
		m_actionManager->setVisible("network", false);
		
		m_projectBrowser->setProject(m_projectManager->project());
		setUpdatesEnabled(true);
		return true;
	}
	
	setUpdatesEnabled(true);
	return false;
}

void MainWindow::openCollaborativeProject()
{
	if( closeProject() )
	{
		m_projectManager->openCollaborativeProject();
		m_actionManager->setVisible("network", true);
	}
}

bool MainWindow::connectToServer()
{
	if( m_projectManager->connectToServer() )
	{
		m_actionManager->setVisible("network", true);
		return true;
	}
	
	return false;
}

void MainWindow::disconnectFromServer()
{
	if( closeProject() )
	{
		m_projectManager->networkHandler()->socket()->disconnectFromHost();
		m_actionManager->setVisible("network", false);
	}
}

void MainWindow::pushProjectToServer()
{
	if( connectToServer() )
	{
		QString filePath = QFileDialog::getOpenFileName( this, tr("Choose a project file..."), QDir::currentPath(), tr("Dash Studio project (*.dsh)"));
		
		if( !filePath.isEmpty() )
		{
			Dash::Network::Socket *socket = m_projectManager->networkHandler()->socket();
			
			
			Dash::Network::Package::UploadProject upload;
			socket->send(upload.toString().toLocal8Bit());
			
			QFile f(filePath);
			Dash::Network::Package::Transfer transfer(f.size());
			socket->send(transfer.toString().toLocal8Bit());
			
			socket->setFormat(Dash::Network::Socket::Binary);
			if( f.open(QIODevice::ReadOnly) )
			{
				socket->flush();
				
				while(!f.atEnd())
				{
					socket->send(f.read(1024));
				}
				
				socket->flush();
			}
			socket->setFormat(Dash::Network::Socket::Text);
		}
	}
}

void MainWindow::saveProject()
{
	if( m_currentFile.isEmpty() )
	{
		saveProjectAs();
		return;
	}
	
	if( m_projectManager->saveProject(m_currentFile) )
	{
		DGui::Osd::self()->display(tr("Project %1 saved").arg(m_projectManager->project()->projectName()), DGui::Osd::Info);
	}
	else
	{
		m_currentFile = QString();
		DGui::Osd::self()->display(tr("Cannot save the project!"), DGui::Osd::Error );
	}
}

void MainWindow::autoSaveProject()
{
	if( m_projectManager->project()->isOpen() && !m_currentFile.isEmpty() )
	{
		QTimer::singleShot(100, this, SLOT(saveProject()));
	}
}

void MainWindow::saveProjectAs()
{
	QString filePath = QFileDialog::getSaveFileName(this);
	
	if( !filePath.isEmpty() )
	{
		m_currentFile = filePath;
		
		if( !m_currentFile.endsWith(".dsh") )
			m_currentFile += ".dsh";
		
		QTimer::singleShot(0, this, SLOT(saveProject()));
	}
}

bool MainWindow::closeProject()
{
	if(!m_projectManager->project()->isOpen())
	{
		return true;
	}
	
	if ( m_projectManager->isModified() )
	{
		QMessageBox mb(QApplication::applicationName (), tr("Do you want to save?"),
			QMessageBox::Information,
			QMessageBox::Yes | QMessageBox::Default,
			QMessageBox::No,
			QMessageBox::Cancel | QMessageBox::Escape);
		mb.setButtonText(QMessageBox::Yes, tr("Save"));
		mb.setButtonText(QMessageBox::No, tr("Discard"));
		
		switch(mb.exec())
		{
			case QMessageBox::Yes:
			{
				saveProject();
			}
			break;
			case QMessageBox::No:
			{
			}
			break;
			case QMessageBox::Cancel:
			{
				return false;
			}
			break;
		}
	}
	
	setUpdatesEnabled(false);
	
	m_projectManager->closeProject();
	m_currentFile = QString();
	
	setUpdatesEnabled(true);
	
	return true;
}

void MainWindow::exportProject()
{
	ExportDialog exportDialog(m_projectManager->project());
	if( exportDialog.exec() == QDialog::Accepted )
	{
		if( exportDialog.generate() )
		{
			DGui::Osd::self()->display(tr("Export successfully!"));
		}
		else
		{
			DGui::Osd::self()->display(tr("Export failed!"), DGui::Osd::Error);
		}
	}
	
	m_viewer->view()->paintArea()->photogram()->drawCurrentPhotogram();
}


// View

void MainWindow::showChatWindow()
{
	if( !m_projectManager->isLocal() )
	{
		m_projectManager->showChatWindow();
	}
}

void MainWindow::loadLastProject()
{
	DCore::Config *config = dApp->config("General");
	QString lastProject = config->value("last_project", QString()).toString();
	config->endGroup();
	
	if( !lastProject.isEmpty() )
	{
		openProject(lastProject);
	}
	
	DGui::TipDialog dialog;
	
	if( dialog.showOnStart() )
	{
		dialog.exec();
	}
}

// Network
void MainWindow::editProjectMembers()
{
	if( !m_projectManager->isLocal() )
	{
		Dash::Component::Network::EditMembers editMembers( m_projectManager->project()->projectName(), m_projectManager->networkHandler()->socket());
		
		m_projectManager->addPackageObserver(&editMembers);
		editMembers.exec();
		m_projectManager->removePackageObserver(&editMembers);
	}
}

// EDIT

void MainWindow::configure()
{
	SettingsDialog settings(m_viewer->view()->paintArea());
	if( settings.exec() != QDialog::Rejected )
	{
		loadSettings();
	}
}

// HELP

void MainWindow::showHelp()
{
	Dash::Component::HelpWidget *help = new Dash::Component::HelpWidget;
	help->show();
}

void MainWindow::aboutDash()
{
	QMessageBox::about(this, tr("About Dash..."), tr("Dash is a tool to build up animations and ...") );
}

void MainWindow::changePerspective( QAction *action )
{
	bool ok = false;
	
	int id = action->data().toInt(&ok);
	
	if(ok)
	{
		setCurrentPerspective(id);
	}
}

void MainWindow::connectToPaintArea(QObject *object)
{
	if( object->metaObject()->indexOfSignal("frameSelected(int,int,int)") > -1 )
	{
		connect(object, SIGNAL(frameSelected(int, int, int)), m_viewer->view(), SLOT(setCurrentFrame(int, int, int)));
	}
	
	if( object->metaObject()->indexOfSignal("layerSelected(int,int)") > -1 )
	{
		connect(object, SIGNAL(layerSelected(int, int)), m_viewer->view(), SLOT(setCurrentLayer(int, int)));
	}
	
	if( object->metaObject()->indexOfSignal("sceneSelected(int)") > -1 )
	{
		connect(object, SIGNAL(sceneSelected(int)), m_viewer->view(), SLOT(setCurrentScene(int)));
	}
	
	if( object->metaObject()->indexOfSignal("brushChanged(QBrush)") > -1 )
	{
		connect(object, SIGNAL(brushChanged(const QBrush &)), m_viewer->view(), SLOT(setBrush(const QBrush &)));
	}
	
	if( object->metaObject()->indexOfSignal("addCurrentItemsToLibrary()") > -1 )
	{
		connect(object, SIGNAL(addCurrentItemsToLibrary()), m_viewer->view(), SLOT(addToLibrary()));
	}
}

void MainWindow::saveSettings()
{
	DCore::Config *config = dApp->config("General");
	config->setValue("last_project", m_currentFile);
	config->setValue("recents", m_recentProjects);
	
	config->endGroup();
}

void MainWindow::loadSettings()
{
	// Section General
	DCore::Config *config = dApp->config("General");
	{
		m_saver.setInterval(config->value("save_project_interval", 15).toInt() * 60 * 1000);
		
		if(config->value("auto_save_project", true).toBool() )
		{
			m_saver.start();
		}
		else
		{
			m_saver.stop();
		}
		
		m_recentProjects = config->value("recents").toStringList();
		
		config->endGroup();
	}
}

void MainWindow::openRecent(QAction *act)
{
	
	QString filePath = act->text();
	openProject(filePath);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	saveSettings();
	
	if (! closeProject() )
	{
		event->ignore();
		return;
	}
	
	Ideality::StackedMainWindow::closeEvent(event);
}

