TEMPLATE = app

HEADERS += mainwindow.h \
documentviewer.h \
projectmanager.h \
settingsdialog.h \
newprojectdialog.h \
exportdialog.h \
networkhandler.h \
connectdialog.h  \
 splashscreen.h comm.h
SOURCES += main.cpp mainwindow.cpp \
documentviewer.cpp \
projectmanager.cpp \
settingsdialog.cpp \
newprojectdialog.cpp \
exportdialog.cpp \
networkhandler.cpp \
connectdialog.cpp  \
 splashscreen.cpp comm.cpp
CONFIG += warn_on


TARGET = dash-studio

QT += network

include($$PWD/../../config.pri)
link_with(yamfmovie)
link_with(yamfswf)

SRC_DIR = ..
include($$SRC_DIR/components/components.pri)

LIBS += -ldash
include(shell_config.pri)

FORMS += connect.ui displayprojects.ui general_page.ui paintarea_page.ui \
newproject.ui \
export_settings.ui \
export_plugins.ui \
scene_selector.ui
INSTALLS += target
target.path = /bin/



