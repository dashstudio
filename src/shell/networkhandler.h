/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H

#include <QObject>
#include <QQueue>

#include <dash/network/socketobserver.h>
#include <dash/network/package/dispatcher.h>

class Comm;
class ProjectManager;
class ProjectParams;

namespace YAMF {
namespace Command {
	class Processor;
	class Base;
}
}

namespace Dash { 
namespace Network {
class Socket;
class FileReceiver;

namespace Package {
class Observer;
}
}
}


/**
 * @author David Cuadrado <krawek@gmail.com>
 */
class NetworkHandler : public QObject, Dash::Network::SocketObserver
{
	Q_OBJECT;
	
	public:
		NetworkHandler(ProjectManager *manager, QObject *parent = 0);
		~NetworkHandler();
		
		void connectToHost(const QString &host, quint16 port, const QString &login, const QString &password);
		bool send(const QString &xml);
		
		void addObserver(Dash::Network::Package::Observer *observer);
		void removeObserver(Dash::Network::Package::Observer *observer);
		
		void execute(YAMF::Command::Base *cmd);
		
		void newProject(const ProjectParams *params);
		
		void openProject();
		void openProject(const QString &projectName);
		
		bool isOpen() const;
		bool isValid() const;
		
		Dash::Network::Socket *socket() const;
		
		void reset();
		
		void showChatWindow();
		
	protected:
		virtual void readed(const QString &txt);
		virtual void dataReaded(const QByteArray &data);
		
	private slots:
		 void onReceiverFinished(const QString &fileName);
		 void onDisconnected();
		
	private:
		ProjectManager *m_manager;
		Dash::Network::Socket *m_socket;
		Dash::Network::FileReceiver *m_receiver;
		
		QString m_sign;
		
		Dash::Network::Package::Dispatcher *m_dispatcher;
		
		YAMF::Command::Processor *m_processor;
		
		QQueue<YAMF::Command::Base *> m_commands;
		bool m_waitingResponse;
		
		QString m_currentPackage;
		
		Comm *m_comm;
};

#endif
