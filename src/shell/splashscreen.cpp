/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "splashscreen.h"

#include <QApplication>
#include <QGraphicsRectItem>
#include <QGraphicsTextItem>
#include <QTimeLine>

#include <dcore/algorithm.h>

class ViewPort : public QWidget
{
	public:
		ViewPort();
		~ViewPort();
		
	public:
		void repaint();
};

ViewPort::ViewPort()
{
}

ViewPort::~ViewPort()
{
}


void ViewPort::repaint()
{
	QWidget::repaint();
	qApp->processEvents();
}


struct SplashScreen::Private {
	QGraphicsTextItem *message;
	QTimeLine *timeline;
};

SplashScreen::SplashScreen(QWidget *parent)
	: QGraphicsView(parent), d(new Private)
{
	setWindowFlags(Qt::SplashScreen | Qt::WindowStaysOnTopHint );
	setAttribute(Qt::WA_DeleteOnClose, true);
	
	
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	
	setViewport(new ViewPort);
	
	
	QRectF rect(0, 0, 400, 300);
	
	QGraphicsScene *scene = new QGraphicsScene;
	scene->setSceneRect(rect);
	setScene(scene);
	
	QColor c(DCore::Algorithm::randomColor());
	{
		QRectF br = scene->sceneRect();
		QLinearGradient gradient(QPointF(0,0), QPointF(0,300));
		gradient.setSpread(QGradient::RepeatSpread);
		
// 		QColor c(0x168000);
		c.setAlpha(180);
		
		gradient.setColorAt(0.0, c);
		gradient.setColorAt(1.0, DCore::Algorithm::randomColor());
		
		gradient.setColorAt(0.65, DCore::Algorithm::randomColor());
		
		setBackgroundBrush(gradient);
		
		setCacheMode(QGraphicsView::CacheBackground);
	}
	
	
	QGraphicsTextItem *dash = new QGraphicsTextItem(tr("Dash"));
	QFont font = this->font();
	font.setPointSize(64);
	dash->setFont(font);
	
	QTransform transform = dash->transform();
	transform.rotate(50, Qt::XAxis);
	transform.rotate(30, Qt::YAxis);
	dash->setTransform(transform);
	
	{
// 		c.setAlpha(180);
		dash->setDefaultTextColor(c);
	}
	
	{
		QRectF br = dash->boundingRect();
		dash->setPos( (rect.width() - br.width())/2, (rect.height() - br.height()) /2);
	}
	
	scene->addItem(dash);
	
	d->message = new QGraphicsTextItem;
	
	repaint();
	
	d->timeline = new QTimeLine(1000, this);
	d->timeline->setFrameRange(0, 100);
	
	connect(d->timeline, SIGNAL(frameChanged(int)), this, SLOT(onFrameChanged(int)));
}


SplashScreen::~SplashScreen()
{
	delete d;
}

QSize SplashScreen::sizeHint() const
{
	return QSize(400, 300);
}

bool SplashScreen::event(QEvent *e)
{
	return QGraphicsView::event(e);
}

void SplashScreen::repaint()
{
	QGraphicsView::repaint();
	static_cast<ViewPort *>(viewport())->repaint();
	qApp->processEvents();
}

void SplashScreen::start()
{
	show();
	d->timeline->start();
}

void SplashScreen::finish()
{
	// TODO: Fade and close!
	close();
}

void SplashScreen::showMessage(const QString &message, int alignment, const QColor &color)
{
	d->message->setDefaultTextColor(color);
	d->message->setPlainText(message);
	
	if( !message.isEmpty() )
	{
		if( d->message->scene() == 0 )
		{
			scene()->addItem(d->message);
		}
	}
	
	QRectF br = d->message->boundingRect();
	double width = br.width();
	double height = br.height();
	
	QRectF sceneRect = scene()->sceneRect();
	
	switch(alignment)
	{
		case Qt::AlignCenter:
		{
			d->message->setPos( (sceneRect.width() - width)/2, sceneRect.height()-height);
		}
		break;
		case Qt::AlignLeft:
		{
			d->message->setPos( 0, sceneRect.height()-height );
		}
		break;
		case Qt::AlignRight:
		default:
		{
			d->message->setPos( sceneRect.width() - width, sceneRect.height()-height);
		}
		break;
	}
	
	repaint();
}

void SplashScreen::onFrameChanged(int frame)
{
	Q_UNUSED(frame);
	repaint();
}

