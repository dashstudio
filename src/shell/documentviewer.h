/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DOCUMENTVIEWER_H
#define DOCUMENTVIEWER_H

#include <QMainWindow>
#include <dash/module.h>

namespace YAMF {
namespace Drawing {
	class View;
}
}

namespace Dash {
	class Project;
}


/**
	@author David Cuadrado <krawek@gmail.com>
*/
class DocumentViewer : public QMainWindow, public Dash::Module
{
	Q_OBJECT;
	
	public:
		DocumentViewer(Dash::Project *const project, QWidget *parent = 0);
		~DocumentViewer();
		
		YAMF::Drawing::View *view();
		
		virtual void update(const Dash::DataSource *datasource, bool undo = false);
		virtual void reset();
		
	private:
		YAMF::Drawing::View *m_view;
};

#endif
