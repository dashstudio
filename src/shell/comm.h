/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef COMM_H
#define COMM_H

#include <QObject>
#include <dash/network/package/observer.h>

class NetworkHandler;

namespace Dash {

namespace Component {
	namespace Network {
		class ChatWindow;
	}
}

namespace Network {
namespace Package {
class Source;
}
}
}

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class Comm : public QObject, public Dash::Network::Package::Observer
{
	Q_OBJECT;
	
	public:
		Comm(NetworkHandler *handler);
		~Comm();
		
		void handlePackage(Dash::Network::Package::Source *const pkg);
		
		Dash::Component::Network::ChatWindow *chatWindow() const;
		
	private slots:
		void onSendChat(const QString &text, int type);
		
	private:
		NetworkHandler *m_handler;
		Dash::Component::Network::ChatWindow *m_chatWindow;
};

#endif
