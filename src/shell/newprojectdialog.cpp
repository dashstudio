/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "newprojectdialog.h"

#include <dgui/iconloader.h>
#include <dgui/osd.h>

#include <dcore/config.h>

#include "ui_newproject.h"
#include "ui_connect.h"

NewProjectDialog::NewProjectDialog(QWidget *parent)
 : QDialog(parent), m_sceneCounter(1)
{
	ui = new Ui::NewProject;
	ui->setupUi(this);
	
	ui->author->setText(tr("Anonymous"));
	ui->sceneList->item(0)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
	
	ui->image->setPixmap(DGui::IconLoader::self()->load("applications-multimedia.svg").pixmap(100, 200));
	
	{
		uiNetwork = new Ui::Connect;
		QWidget *networkPage = new QWidget;
		uiNetwork->setupUi(networkPage);
		
		uiNetwork->imageNetwork->setPixmap(DGui::IconLoader::self()->load("applications-internet.svg").pixmap(100, 100));
		uiNetwork->port->setValue(6821);
		
		ui->tabWidget->addTab(networkPage, tr("Network"));
	}
	
	ui->addScene->setIcon(DGui::IconLoader::self()->load("list-add.svg"));
	ui->removeScene->setIcon(DGui::IconLoader::self()->load("list-remove.svg"));
	ui->moveUpScene->setIcon(DGui::IconLoader::self()->load("go-up.svg"));
	ui->moveDownScene->setIcon(DGui::IconLoader::self()->load("go-down.svg"));
	
	connect(ui->addScene, SIGNAL(clicked()), this, SLOT(addScene()));
	connect(ui->removeScene, SIGNAL(clicked()), this, SLOT(removeScene()));
	connect(ui->moveUpScene, SIGNAL(clicked()), this, SLOT(moveUpScene()));
	connect(ui->moveDownScene, SIGNAL(clicked()), this, SLOT(moveDownScene()));
	
	connect(ui->useNetwork, SIGNAL(toggled(bool)), this, SLOT(enableNetwork(bool)));
	
	
	enableNetwork(ui->useNetwork->isChecked());
	
	connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(onAccepted()));
	
	restoreSettings();
}


NewProjectDialog::~NewProjectDialog()
{
	delete uiNetwork;
	delete ui;
}

QString NewProjectDialog::projectName() const
{
	return ui->projectName->text();
}

QString NewProjectDialog::author() const
{
	return ui->author->text();
}

QString NewProjectDialog::description() const
{
	return ui->description->toPlainText();
}

QStringList NewProjectDialog::scenes() const
{
	QStringList scenes;
	for(int i = 0; i < ui->sceneList->count(); i++)
	{
		scenes << ui->sceneList->item(i)->text();
	}
	
	return scenes;
}

int NewProjectDialog::framesPerScene() const
{
	return ui->framesPerScene->value();
}


bool NewProjectDialog::useNetwork() const
{
	return ui->useNetwork->isChecked();
}

QString NewProjectDialog::login() const
{
	return uiNetwork->user->text();
}

QString NewProjectDialog::password() const
{
	return uiNetwork->password->text();
}

QString NewProjectDialog::server() const
{
	return uiNetwork->server->text();
}

int NewProjectDialog::port() const
{
	return uiNetwork->port->value();
}


void NewProjectDialog::enableNetwork(bool e)
{
	ui->tabWidget->setTabEnabled(2, e);
}

void NewProjectDialog::addScene()
{
	m_sceneCounter++;
	QListWidgetItem *item = new QListWidgetItem(ui->sceneList);
	item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
	item->setText(tr("Scene %1").arg(m_sceneCounter));
}

void NewProjectDialog::removeScene()
{
	delete ui->sceneList->takeItem(ui->sceneList->currentRow());
}

void NewProjectDialog::moveUpScene()
{
	int row = ui->sceneList->currentRow();
	if( row >= 0 )
	{
		QListWidgetItem *item = ui->sceneList->takeItem(row);
		
		ui->sceneList->insertItem(row-1,item);
		
		ui->sceneList->setCurrentItem(item);
	}
}

void NewProjectDialog::moveDownScene()
{
	int row = ui->sceneList->currentRow();
	if( row >= 0 )
	{
		QListWidgetItem *item = ui->sceneList->takeItem(row);
		
		ui->sceneList->insertItem(row+1,item);
		ui->sceneList->setCurrentItem(item);
	}
}

void NewProjectDialog::onAccepted()
{
	if( ui->projectName->text().isEmpty() )
	{
		DGui::Osd::self()->display(tr("Please fill the project name field"), DGui::Osd::Error);
		return;
	}
	
	if( ui->author->text().isEmpty() )
	{
		DGui::Osd::self()->display(tr("Please fill the author field"), DGui::Osd::Error);
		return;
	}
	
	if( ui->useNetwork->isChecked() )
	{
		if( uiNetwork->user->text().isEmpty() )
		{
			DGui::Osd::self()->display(tr("Please fill the user field"), DGui::Osd::Error);
			return;
		}
		
		if( uiNetwork->server->text().isEmpty() )
		{
			DGui::Osd::self()->display(tr("Please fill the server field"), DGui::Osd::Error);
			return;
		}
	}
	
	saveSettings();
	
	QDialog::accept();
}


void NewProjectDialog::saveSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("Network");
	
	config->setValue("login", login());
	
	if( uiNetwork->storePassword->isChecked() )
	{
		config->setValue("password", password());
	}
	else
	{
		config->remove("password");
	}
	
	config->setValue("server", server());
	config->setValue("port", port());
	
	config->endGroup();
}

void NewProjectDialog::restoreSettings()
{
	DCore::Config *config = DCore::Config::self();
	config->beginGroup("Network");
	
	uiNetwork->user->setText(config->value("login").toString());
	
	if( config->contains("password") )
	{
		uiNetwork->password->setText(config->value("password").toString());
		uiNetwork->storePassword->setChecked(true);
	}
	
	uiNetwork->server->setText(config->value("server").toString());
	uiNetwork->port->setValue(config->value("port").toInt());
	
	config->endGroup();
}

