/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <dgui/configurationdialog.h>

namespace YAMF {
namespace Drawing {
	class PaintArea;
}
}

namespace Ui {
	class GeneralPage;
	class PaintAreaPage;
}

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class SettingsDialog : public DGui::ConfigurationDialog
{
	Q_OBJECT;
	
	public:
		SettingsDialog(YAMF::Drawing::PaintArea *const paintArea, QWidget *parent = 0);
		~SettingsDialog();
		
		void apply();
		void ok();
		
	private:
		Ui::GeneralPage *m_generalPage;
		Ui::PaintAreaPage *m_paintAreaPage;
		YAMF::Drawing::PaintArea *const m_paintArea;
};

#endif
