/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "settingsdialog.h"

#include <QPainter>

#ifdef Q_OPENGL_LIB
#include <QGLWidget>
#endif

// DLib
#include <dgui/iconloader.h>
#include <dgui/application.h>
#include <dcore/config.h>

// YAMF
#include <yamf/drawing/paintarea.h>

// Dash
#include "ui_general_page.h"
#include "ui_paintarea_page.h"

SettingsDialog::SettingsDialog(YAMF::Drawing::PaintArea *const paintArea, QWidget *parent)
 : DGui::ConfigurationDialog(parent), m_paintArea(paintArea)
{
	setWindowTitle(tr("Dash settings..."));
	
	// Section General
	DCore::Config *config = dApp->config("General");
	{
		m_generalPage = new Ui::GeneralPage;
		
		QWidget *general = new QWidget;
		m_generalPage->setupUi(general);
		
		m_generalPage->minutes->setValue(config->value("save_project_interval", 15).toInt());
		m_generalPage->autoSaveProject->setChecked(config->value("auto_save_project", true).toBool());
		
		addPage(general, tr("General"), DGui::IconLoader::self()->load("preferences-system.svg"));
		
		config->endGroup();
	}
	
	// Section: TipOfDay
	{
		config->beginGroup("TipOfDay");
		m_generalPage->showTips->setChecked(config->value("show_on_start", true).toBool() );
		config->endGroup();
	}
	
	// Section PaintArea
	{
		m_paintAreaPage = new Ui::PaintAreaPage;
		
		QWidget *paintArea = new QWidget;
		m_paintAreaPage->setupUi(paintArea);
		
		m_paintAreaPage->antialiasing->setChecked(m_paintArea->renderHints() & QPainter::Antialiasing);
		
		m_paintAreaPage->showGrid->setChecked( m_paintArea->drawGrid() );
		m_paintAreaPage->showBorder->setChecked(m_paintArea->drawBorder());
		
#ifdef Q_OPENGL_LIB
		bool openGL = dynamic_cast<QGLWidget *>(m_paintArea->viewport()) != 0;
		m_paintAreaPage->openGL->setChecked(openGL);
#else
		m_paintAreaPage->openGL->setEnabled(false);
#endif
		
		m_paintAreaPage->backgroundColor->setColor(m_paintArea->canvasColor());
		
		m_paintAreaPage->separation->setValue(m_paintArea->gridSeparation());
		
		addPage(paintArea, tr("Paint area"), DGui::IconLoader::self()->load("applications-graphics.svg"));
	}
}


SettingsDialog::~SettingsDialog()
{
	delete m_generalPage;
	delete m_paintAreaPage;
}


void SettingsDialog::apply()
{
	ok(); // FIXME
	accept();
}

void SettingsDialog::ok()
{
	// Section: General
	DCore::Config *config = dApp->config("General");
	{
		config->setValue("save_project_interval", m_generalPage->autoSaveProject->isChecked() ? m_generalPage->minutes->value() : 0);
		config->setValue("auto_save_project", m_generalPage->autoSaveProject->isChecked());
		
		config->endGroup();
	}
	
	// Section: TipOfDay
	{
		config->beginGroup("TipOfDay");
		
		config->setValue("show_on_start", m_generalPage->showTips->isChecked());
		
		config->endGroup();
	}
	
	{
		m_paintArea->setAntialiasing(m_paintAreaPage->antialiasing->isChecked());
		m_paintArea->setDrawGrid(m_paintAreaPage->showGrid->isChecked());
		m_paintArea->setDrawBorder(m_paintAreaPage->showBorder->isChecked());
		
		m_paintArea->setUseOpenGL(m_paintAreaPage->openGL->isChecked());
		
		m_paintArea->setGridSeparation(m_paintAreaPage->separation->value());
		m_paintArea->setCanvasColor(m_paintAreaPage->backgroundColor->color());
		
		m_paintArea->saveSettings();
	}
	
	config->sync();
	
	accept();
}




