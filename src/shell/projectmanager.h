/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include <yamf/model/command/observer.h>

namespace Dash {
	class Module;
	class Project;
	namespace Network {
		namespace Package {
		class Observer;
		}
	}
}

namespace YAMF {

namespace Command {
	class Base;
}

}

class NetworkHandler;

class ProjectParams
{
	public:
		ProjectParams();
		~ProjectParams();
		
		QString projectName;
		QString description;
		QString author;
		
		bool isLocal;
		
		QString server;
		quint16 port;
		QString login;
		QString password;
};

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class ProjectManager : public QObject, public YAMF::Command::Observer
{
	Q_OBJECT;
	
	public:
		ProjectManager(QObject *parent = 0);
		~ProjectManager();
		
		void createProject(const ProjectParams &params);
		void createScene(const QString &name, int frames);
		
		void closeProject();
		
		Dash::Project *project() const;
		
		void registerModule(Dash::Module *module);
		
		virtual bool aboutToRedo(YAMF::Command::Base *command);
		virtual bool aboutToUndo(YAMF::Command::Base *command);
		
		virtual void executed(const YAMF::Command::Base *command);
		virtual void unexecuted(const YAMF::Command::Base *command);
		
		bool isModified() const;
		
		void setLocal(bool l);
		bool isLocal() const;
		
		void showChatWindow();
		void addPackageObserver(Dash::Network::Package::Observer *observer);
		void removePackageObserver(Dash::Network::Package::Observer *observer);
		
		NetworkHandler *networkHandler() const;
		
	public slots:
		void connectToServer(const QString &host, quint16 port, const QString &login, const QString &password);
		bool connectToServer();
		void openCollaborativeProject();
		
	public:
		bool saveProject(const QString &path);
		bool loadProject(const QString &path);
		
	private:
		struct Private;
		Private *const d;
};

#endif
