/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <ideality/stackedmainwindow.h>
#include <dgui/actionmanager.h>
#include <dash/dash.h>
#include <components/colorpalette/brushsynchronizer.h>

#include <QTimer>

class DocumentViewer;
class ProjectManager;

namespace Dash {
namespace Component {
class Manager;
class ProjectBrowser;
}
}

namespace YAMF {
namespace Gui {
	class ProjectView;
}
}

/**
 * @author David Cuadrado <krawek@gmail.com>
 */
class MainWindow : public Ideality::StackedMainWindow
{
	Q_OBJECT;
	
	public:
		MainWindow(QWidget *parent = 0);
		~MainWindow();
		
	private:
		void setupFileActions();
		void setupViewActions();
		void setupEditActions();
		void setupMenu();
		void setupComponents();
		
	private slots: // Project
		void newProject();
		bool openProject();
		bool openProject(const QString &file);
		void openCollaborativeProject();
		bool connectToServer();
		void disconnectFromServer();
		void pushProjectToServer();
		void saveProject();
		void saveProjectAs();
		void autoSaveProject();
		bool closeProject();
		void exportProject();
		
		void loadLastProject();
		
	private slots: // Network
		void editProjectMembers();
		
	private slots: // View
		void showChatWindow();
		
	private slots: // Edit
		void configure();
		
	private slots: // Help
		void showHelp();
		void aboutDash();
		
	private slots:
		void changePerspective( QAction *action );
		void connectToPaintArea(QObject *object);
		void saveSettings();
		void loadSettings();
		void openRecent(QAction *);
		
	protected:
		virtual void closeEvent(QCloseEvent *e);
		
	private:
		ProjectManager *m_projectManager;
		DGui::ActionManager *m_actionManager;
		DocumentViewer *m_viewer;
		Dash::Component::ProjectBrowser *m_projectBrowser;
		
		Dash::Component::Manager *m_componentManager;
		
		QString m_currentFile;
		
		QTimer m_saver;
		Component::BrushSynchronizer *m_brushSynchronizer;
		
		QStringList m_recentProjects;
};

#endif

