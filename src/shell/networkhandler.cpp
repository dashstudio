/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "networkhandler.h"

// Qt
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QTemporaryFile>

// DCore
#include <dcore/debug.h>

// DGui
#include <dgui/osd.h>

// YAMF
#include <yamf/model/command/processor.h>
#include <yamf/model/command/base.h>

// Dash
#include "dash/project.h"

#include "dash/network/socket.h"
#include "dash/network/filereceiver.h"

#include "dash/network/package/newproject.h"
#include "dash/network/package/connect.h"
#include "dash/network/package/listprojects.h"
#include "dash/network/package/openproject.h"

#include "dash/network/parser/ackparser.h"

#include "dash/network/parser/projectlistparser.h"
#include "dash/network/parser/projectparser.h"

#include "projectmanager.h"

#include "ui_displayprojects.h"

#include "comm.h"
#include "components/network/chatwindow.h"

NetworkHandler::NetworkHandler(ProjectManager *manager, QObject *parent) : QObject(parent), m_manager(manager), m_socket(new Dash::Network::Socket), m_dispatcher(new Dash::Network::Package::Dispatcher), m_waitingResponse(false)
{
	m_processor = new YAMF::Command::Processor(manager->project());
	m_socket->addObserver(this);
	m_receiver = new Dash::Network::FileReceiver;
	m_socket->addObserver(m_receiver);
	
	m_comm = new Comm(this);
	addObserver(m_comm);
	
	connect(m_receiver, SIGNAL(finished(const QString &)), this, SLOT(onReceiverFinished(const QString &)));
	connect(m_socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
}


NetworkHandler::~NetworkHandler()
{
	disconnect(m_socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
	
	delete m_dispatcher;
	delete m_socket;
	delete m_receiver;
	delete m_comm;
}

void NetworkHandler::connectToHost(const QString &host, quint16 port, const QString &login, const QString &password)
{
	m_commands.clear();
	
	m_socket->connectToHost(host, port);
	m_socket->waitForConnected(500);
	
	Dash::Network::Package::Connect cnx(login, password, 0);
	m_socket->send(cnx.toString().toLocal8Bit());
	
	if( m_socket->state() == QAbstractSocket::UnconnectedState )
	{
		if( m_socket->error() != QAbstractSocket::UnknownSocketError )
		{
			DGui::Osd::self()->display(tr("Error connecting to server: ")+m_socket->errorString(), DGui::Osd::Error);
			return;
		}
		else
		{
			dWarning() << "Error connecting: " << m_socket->errorString();
		}
	}
}

bool NetworkHandler::send(const QString &xml)
{
	QString tosend;
	if( !m_sign.isEmpty() )
	{
		QXmlStreamReader reader(xml);
		QXmlStreamWriter writer(&tosend);
		
		bool isRoot = false;
		while( ! reader.atEnd() )
		{
			reader.readNext();
			
			writer.writeCurrentToken(reader);
			if( reader.tokenType() == QXmlStreamReader::StartElement && !isRoot )
			{
				dfDebug << "Writing sing on " << reader.name().toString();
				writer.writeAttribute("sign", m_sign);
				isRoot = true;
			}
		}
	}
	else
	{
		tosend = xml;
	}
	
	dfDebug << "SENDING: " << tosend.left(100) << " SIGN IS : " << m_sign;
	
	return m_socket->send(tosend.toLocal8Bit());
}

void NetworkHandler::addObserver(Dash::Network::Package::Observer *observer)
{
	m_dispatcher->addObserver(observer);
}

void NetworkHandler::removeObserver(Dash::Network::Package::Observer *observer)
{
	m_dispatcher->removeObserver(observer);
}

void NetworkHandler::execute(YAMF::Command::Base *cmd)
{
	if( ! m_waitingResponse )
	{
		m_commands.clear();
	}
	
	bool isEmpty = m_commands.isEmpty();
	
	dfDebug << "ENQUEING: " << cmd->toXml().left(100);
	D_SHOW_VAR(m_commands.size());
	
	
	if( m_waitingResponse )
	{
		qWarning("WARNING: Waiting command response!");
	}
	
	m_commands.enqueue(cmd);
	if( isEmpty && !m_waitingResponse && !m_sign.isEmpty() )
	{
		m_waitingResponse = this->send(cmd->toXml());
		
		if( !m_waitingResponse)
		{
			qWarning("Failed sending command");
			m_commands.dequeue();
		}
	}
}

void NetworkHandler::newProject(const ProjectParams *params)
{
	Dash::Network::Package::NewProject newProjectPackage(params->projectName, params->author, params->description);
	this->send(newProjectPackage.toString());
}

void NetworkHandler::openProject()
{
	Dash::Network::Package::ListProjects list;
	this->send(list.toString());
}

void NetworkHandler::openProject(const QString &projectName)
{
	m_currentPackage = "openproject";
	
	Dash::Network::Package::OpenProject project(projectName);
	this->send(project.toString());
}

bool NetworkHandler::isOpen() const
{
	return m_socket->state() == QAbstractSocket::ConnectedState;
}

bool NetworkHandler::isValid() const
{
	return this->isOpen() && !m_sign.isEmpty();
}

Dash::Network::Socket *NetworkHandler::socket() const
{
	return m_socket;
}

void NetworkHandler::reset()
{
	m_commands.clear();
	m_waitingResponse = false;
	m_currentPackage = "";
}

void NetworkHandler::showChatWindow()
{
	if( isValid() )
	{
		m_comm->chatWindow()->show();
	}
	else
	{
		DGui::Osd::self()->display(tr("You are not authenticated."), DGui::Osd::Error );
	}
}

void NetworkHandler::readed(const QString &txt)
{
	dfDebug << "READED: " << txt.left(100);
	
	QString root;
	
	{
		QXmlStreamReader reader(txt);
		while( !reader.atEnd() )
		{
			if( reader.readNext() == QXmlStreamReader::StartElement )
			{
				root = reader.name().toString();
				break;
			}
		}
		
		if( root.isEmpty() )
		{
			dWarning() << "Invalid package: " << txt.left(100)+"...."+txt.right(100);
			dWarning() << "Error was: " << reader.errorString();
		}
	}
	
	
	if( !root.isEmpty() )
	{
		if( root == "ack" )
		{
			Dash::Network::Parser::AckParser parser;
			if( parser.parse(txt) )
			{
				m_sign = parser.sign();
				DGui::Osd::self()->display(parser.motd(), DGui::Osd::Info);
				
				if( !m_commands.isEmpty() )
				{
					m_waitingResponse = true;
					send(m_commands.first()->toXml());
				}
			}
		}
		else if( root == "error")
		{
			QString error;
			int level = 0;
			
			QXmlStreamReader reader(txt);
			while( !reader.atEnd() )
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if(reader.name().toString() == "message")
					{
						level = reader.attributes().value("level").toString().toInt();
						error = reader.readElementText ();
						break;
					}
				}
			}
			DGui::Osd::self()->display(error, DGui::Osd::Level(level));
		}
		else if( root == "command" )
		{
			D_SHOW_VAR(m_commands.size());
			bool executed = false;
			
			if( m_waitingResponse && !m_commands.isEmpty() )
			{
				m_waitingResponse = false;
				
				QXmlStreamReader reader(txt);
				
				reader.readNext();
				reader.readNext();
				QString sign = reader.attributes().value("sign").toString();
				QString cmdname = reader.attributes().value("name").toString();
				
				if( cmdname.isEmpty() )
				{
					m_commands.clear();
					DGui::Osd::self()->display(tr("Cannot execute command"), DGui::Osd::Error);
					return;
				}
				
				if( sign == m_sign )
				{
					m_manager->setLocal(true);
					m_commands.dequeue()->redo(); // FIXME: add to stack?
					m_manager->setLocal(false);
					
					executed = true;
					
					if( ! m_commands.isEmpty() )
					{
						m_waitingResponse = true;
						send(m_commands.first()->toXml());
					}
				}
			}
			
			if( !executed ) // External command
			{
				m_manager->setLocal(true);
				m_processor->execute(txt);
				m_manager->setLocal(false);
			}
		}
		else if( root == "projectlist" )
		{
			Dash::Network::Parser::ProjectList projectList;
			
			if( projectList.parse(txt) )
			{
				QDialog choose;
				Ui::DisplayProjects ui;
				ui.setupUi(&choose);
				
				foreach( Dash::Network::Parser::ProjectList::Project p, projectList.projects())
				{
					QTreeWidgetItem *item = new QTreeWidgetItem(ui.projects);
					item->setText(0, p.name);
					item->setText(1, p.author);
					item->setText(2, p.description);
				}
				
				if( choose.exec() == QDialog::Accepted )
				{
					if( QTreeWidgetItem *ci = ui.projects->currentItem() )
					{
						QString projectName = ci->text(0);
						
						openProject(projectName);
					}
				}
			}
			else
			{
				DGui::Osd::self()->display(QObject::tr("Error displaying the project list"), DGui::Osd::Error);
			}
		}
		else if( root == "project" ) // FIXME: Remove
		{
			qFatal("OOops!");
		}
		else if( root == "transfer" )
		{
			QXmlStreamReader reader(txt);
			while(! reader.atEnd() )
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					int bytes = reader.attributes().value("bytes").toString().toInt();
					
					m_socket->setFormat(Dash::Network::Socket::Binary);
					m_receiver->start(bytes);
					break;
				}
			}
		}
		else if( root == "connected" )
		{
			QXmlStreamReader reader(txt);
			QStringList logins;
			while(! reader.atEnd() )
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if(reader.name() == "user")
					{
						logins << reader.attributes().value("login").toString();
					}
				}
			}
			m_comm->chatWindow()->addUsers(logins);
		}
		else if( root == "disconnected" )
		{
			QString login;
			
			QXmlStreamReader reader(txt);
			while(! reader.atEnd() )
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if(reader.name() == "user")
					{
						login = reader.attributes().value("login").toString();
						break;
					}
				}
			}
			
			m_comm->chatWindow()->removeUser(login);
		}
		else
		{
			m_dispatcher->addSource(root, txt, m_socket);
			m_dispatcher->process();
		}
	}
}

void NetworkHandler::dataReaded(const QByteArray &data)
{
	Q_UNUSED(data);
}

/**
 * 
 * @param fileName 
 */
void NetworkHandler::onReceiverFinished(const QString &fileName)
{
	m_socket->setFormat(Dash::Network::Socket::Text);
	
	dfDebug << m_currentPackage << " file: " << fileName;
	
	if( m_currentPackage == "openproject" )
	{
		m_manager->setLocal(true);
		
		qDebug() << QFile::exists(fileName);
		
		if( !m_manager->loadProject(fileName) )
		{
			qFatal("Cannot load the project!");
		}
		m_manager->setLocal(false);
		QFile::remove(fileName);
	}
}

void NetworkHandler::onDisconnected()
{
	qWarning("Disconnected!");
	DGui::Osd::self()->display(tr("Disconnected!"), DGui::Osd::Warning);
	m_manager->closeProject();
}

