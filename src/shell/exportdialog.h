/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <dgui/wizard.h>

namespace YAMF {
namespace Model {
class Project;
}
}

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class ExportDialog : public DGui::Wizard
{
	Q_OBJECT;
	public:
		ExportDialog(YAMF::Model::Project *const project, QWidget *parent = 0);
		~ExportDialog();
		
		bool generate();
		
	private:
		struct Private;
		Private *const d;
};

#endif
