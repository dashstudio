/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
 
#include "commanager.h"

//base
#include "package.h"
#include "logger.h"

//Qt
#include <QVariant>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

//server/core
#include "server.h"
#include "connection.h"

//server/users
#include "users/user.h"

#include "project/store.h"
#include "project/projectmanager.h"

namespace DashServer {
namespace Com {

struct Manager::Private
{
	QString writeFromAttribute(const QString &pkg, const QString &from);
};

QString Manager::Private::writeFromAttribute(const QString &pkg, const QString &from)
{
	QString xml;
	
	QXmlStreamReader reader(pkg);
	QXmlStreamWriter writer(&xml);
	
	while(!reader.atEnd() )
	{
		reader.readNext();
		writer.writeCurrentToken(reader);
		
		if( reader.tokenType() == QXmlStreamReader::StartElement)
		{
			if( reader.name().toString() == "message" )
			{
				writer.writeAttribute("from", from);
			}
		}
	}
	
	return xml;
}

Manager::Manager()
 : DashServer::Observer(), d(new Private)
{
}


Manager::~Manager()
{
	delete d;
}

void Manager::handlePackage(DashServer::Package *const pkg)
{
	
	DashServer::Connection *cnn = pkg->source();
	DashServer::TcpServer *server = cnn->server();
	
	if( pkg->root() == "chat" )
	{
		QString projectName = cnn->data(Project::Store::ProjectName).toString();
		
		if( !projectName.isEmpty() )
		{
			server->projectManager()->sendToMembers(projectName, d->writeFromAttribute(pkg->xml(), cnn->user()->login()));
			pkg->accept();
		}
	}
	else if( pkg->root() == "notice" )
	{
		QString projectName = cnn->data(Project::Store::ProjectName).toString();
		
		if( !projectName.isEmpty() )
		{
			server->projectManager()->sendToMembers(projectName, d->writeFromAttribute(pkg->xml(), cnn->user()->login()));
			pkg->accept();
		}
	}
	else if( pkg->root() == "wall" )
	{
		cnn->sendToAll(d->writeFromAttribute(pkg->xml(), cnn->user()->login()), true);
		pkg->accept();
	}
}

}
}

