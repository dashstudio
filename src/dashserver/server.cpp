/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "server.h"
#include "connection.h"

#include <QHostInfo>
#include <QTimer>
#include <QQueue>
#include <QHostAddress>

#include <dcore/debug.h>

#include "packagehandlerbase.h"
#include "defaultpackagehandler.h"

#include "bans/banmanager.h"
#include "backups/backupmanager.h"
#include "users/usermanager.h"

#include "registers/regmanager.h"
#include "project/projectmanager.h"

#include "package.h"
#include "observer.h"
#include "logger.h"

#include "databaseobjectfactory.h"
#include "xdbms/manager.h"

#include <dash/network/socket.h>

using namespace Dash::Network;

namespace DashServer {

class TcpServer::Private
{
	public:
		QSet<DashServer::Connection *> connections;
		QSet<DashServer::Connection *> admins;
		
		Backups::Manager *backupManager;
		Bans::Manager *banManager;
		Users::Manager *userManager;
		Project::Manager *projectManager;
		
		QList<DashServer::Observer *> observers;
};

TcpServer::TcpServer(QObject *parent) : QTcpServer(parent), d(new Private)
{
	D_INIT;
	
	XDBMS::Manager::self()->setObjectFactory(new DatabaseObjectFactory);
	
	d->backupManager = new Backups::Manager;
	d->observers << d->backupManager;
	
	d->banManager = new Bans::Manager;
	d->observers << d->banManager;
	
	d->userManager = new Users::Manager;
	d->observers << d->userManager;
	
	d->projectManager = new DashServer::Project::Manager;
	d->observers << d->projectManager;
}


TcpServer::~TcpServer()
{
	D_END;
	
	DashServer::Logger::self()->info("DashServer finished");
	
	qDebug("=> Deleting observers");
	qDeleteAll(d->observers);
	
	qDebug("=> Deleting connections");
	foreach(DashServer::Connection *cnn, d->connections)
	{
		removeConnection(cnn);
	}
	
	delete d;
}

bool TcpServer::openConnection(const QString &host, int port)
{
	DashServer::Logger::self()->info(QObject::tr("Initialized server on %1:%2").arg(host).arg(port));
	
	QList<QHostAddress> addrs = QHostInfo::fromName(host).addresses();
	if ( !addrs.isEmpty() )
	{
		if(! listen(QHostAddress(addrs[0]), port) )
		{
			dError() << "Can't connect to " << host<<":"<<port<< " error was: " << errorString();
			return false;
		}
	}
	else
	{
		dError() << "Error while try to resolve " << host;
		return false;
	}
	return true;
}

void TcpServer::addAdmin(DashServer::Connection *cnx)
{
	d->admins << cnx;
}

Backups::Manager *TcpServer::backupManager() const
{
	return d->backupManager;
}

Bans::Manager *TcpServer::banManager() const
{
	return d->banManager;
}

Users::Manager *TcpServer::userManager() const
{
	return d->userManager;
}

Project::Manager *TcpServer::projectManager() const
{
	return d->projectManager;
}

void TcpServer::addObserver(DashServer::Observer *observer)
{
	d->observers << observer;
}

bool TcpServer::removeObserver(DashServer::Observer *observer)
{
	return d->observers.removeAll(observer) > 0;
}

void TcpServer::incomingConnection(int socketDescriptor)
{
	D_SHOW_VAR(d->connections.count());
	D_SHOW_VAR(socketDescriptor);
	
	DashServer::Connection *newConnection = new DashServer::Connection(socketDescriptor,this);
	
	QString ip = newConnection->client()->peerAddress().toString();
	d->banManager->initialize(ip);
	
	if ( !d->banManager->isBanned(ip) )
	{
		handle(newConnection);
		d->connections << newConnection;
	}
	else
	{
		connect(newConnection, SIGNAL(finished()), newConnection, SLOT(deleteLater()));
		newConnection->sendError(tr("You're banned, Please contact to server administrator if you think is an error!"), Dash::Network::Package::Error::Err);
	}
}

void TcpServer::handle(DashServer::Connection *cnx)
{
	connect(cnx, SIGNAL(requestSendToAll( const QString& )), this, SLOT(sendToAll( const QString& )));
	
	connect(cnx, SIGNAL(packageReaded(DashServer::Connection*, const QString&, const QString&)), this, SLOT(handlePackage(DashServer::Connection*, const QString&, const QString&)));
	
	connect(cnx, SIGNAL(connectionClosed(DashServer::Connection*)), this, SLOT(removeConnection(DashServer::Connection*)));
}


void TcpServer::sendToAll(const QString &msg)
{
	foreach(DashServer::Connection *connection, d->connections)
	{
		connection->sendToClient(msg, false);
	}
}

void TcpServer::sendToAll(const QDomDocument &pkg)
{
	D_FUNCINFO;
	sendToAll(pkg.toString(0));
}

void TcpServer::sendToAdmins(const QString &str)
{
	foreach(DashServer::Connection *cnn, d->admins)
	{
		cnn->sendToClient(str, false);
	}
}

void TcpServer::removeConnection(DashServer::Connection *cnx)
{
	D_FUNCINFO;
	
	if( ! cnx ) return;
	
	if( !d->connections.contains(cnx) ) return;
	
	d->connections.remove(cnx);
	d->admins.remove(cnx);
	
	foreach(DashServer::Observer *observer, d->observers)
	{
		observer->connectionClosed(cnx);
	}
	
	cnx->blockSignals(true);
	cnx->close();
	cnx->blockSignals(false);
	
	if( !cnx->isRunning() )
	{
		cnx->deleteLater();
	}
	else
	{
		connect(cnx, SIGNAL(finished()), cnx, SLOT(deleteLater()));
		cnx->terminate();
	}
}


void TcpServer::handlePackage(DashServer::Connection* client, const QString &root, const QString&package)
{
	DashServer::Package *pkg = new DashServer::Package(root, package, client);
	
	foreach(DashServer::Observer *observer, d->observers)
	{
		observer->handlePackage(pkg);
		
		if ( pkg->isAccepted() )
			break;
	}
	
	if( ! pkg->isAccepted() )
		qWarning() << "NOT ACCEPTED: " << pkg->root();
	
	delete pkg;
	
	client->process();
}


}


