/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "permission.h"

namespace DashServer {

namespace Users {

Permission::Permission()
{
	setClass("permission");
}

Permission::Permission(const QString &module, Opts opts)
 : XDBMS::Object(module, "permission")
{
	setClass("permission");
	
	if(opts & Read )
	{
		setRead(true);
	}
	
	if( opts & Write )
	{
		setWrite(true);
	}
	
	setName(id());
}


Permission::~Permission()
{
}

void Permission::afterLoad()
{
	setName(id());
	
	setWrite( value("write") == "1" || value("write") == "true" );
	setRead( value("read") == "1" || value("read") == "true" );
}

void Permission::aboutToSave()
{
	setId(name());
	add("write", write() ? "1" : "0");
	add("read", read() ? "1" : "0");
}



}

}
