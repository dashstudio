/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DASHSERVER_USERSPERMISSION_H
#define DASHSERVER_USERSPERMISSION_H

#include <dashserver/global.h>
#include <dashserver/xdbms/object.h>

#include <dash/network/permission.h>

namespace DashServer {
namespace Users {

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Permission : public Dash::Network::Permission, public XDBMS::Object
{
	public:
		enum Opt {
			None = 0x0,
			Read = 0x1,
			Write = 0x2
		};
		
		Q_DECLARE_FLAGS(Opts, Opt);
		
		Permission();
		Permission(const QString &name, Opts opts = None);
		~Permission();
		
		void afterLoad();
		void aboutToSave();
		
// 		Q_DISABLE_
};

}

}

Q_DECLARE_OPERATORS_FOR_FLAGS(DashServer::Users::Permission::Opts);

#endif
