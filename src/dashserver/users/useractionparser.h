/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PARSERSUSERACTIONPARSER_H
#define PARSERSUSERACTIONPARSER_H

#include <yamf/common/xmlparser.h>
#include <dashserver/exports.h>

namespace DashServer {
namespace Users {
	class User;
}

namespace Parsers {

/**
 * @author Jorge Cuadrado <kuadrosx@gmail.com>
*/
class DASHSERVER_EXPORT UserActionParser : public YAMF::Common::XmlParser
{
	public:
		UserActionParser();
		~UserActionParser();
		
		Users::User user() const;
		
	protected:
		virtual bool startTag(const QString &tag, const YAMF::Common::XmlAttributes &atts);
		virtual bool endTag(const QString &tag);
		virtual bool characters(const QString &text);
		
	private:
		struct Private;
		Private * const d;

};

}
}

#endif
