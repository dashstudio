/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "usermanager.h"

#include <QHash>
#include <QDomDocument>
#include <QXmlStreamReader>

#include "user.h"

#include <dcore/debug.h>
#include <dcore/fortunegenerator.h>

#include "package.h"
#include "logger.h"

#include "server.h"
#include "connection.h"

#include "useractionparser.h"
#include "dash/network/package/userlist.h"
#include "dash/network/package/ack.h"
#include "bans/banmanager.h"
#include "dashserver/xdbms/manager.h"
#include "dashserver/xdbms/table.h"

#include <dash/network/socket.h>

namespace DashServer {
namespace Users {

class Manager::Private
{
	public:
		Private()
		{
		}
		
		~Private()
		{
		}
};

Manager::Manager() : d(new Private())
{
}


Manager::~Manager()
{
	delete d;
}

QList<User *> Manager::users() const
{
	QList<User *> users;
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("users");
	foreach(QString login, table->objects())
	{
		User *user = new User;
		user->setLogin(login);
		table->load(user);
		
		users << user;
	}
	
	
	return users;
}

void Manager::handlePackage(DashServer::Package *const pkg)
{
	QString root = pkg->root();
	QString package = pkg->xml();
	
	DashServer::Connection *cnn = pkg->source();
	DashServer::TcpServer *server = cnn->server();
	
	
	if ( root == "connect" )
	{
		pkg->accept();
		cnn->setValid(false);
		
		User *user = parseConnect(package);
		
		if( user )
		{
			cnn->setUser(user);
			
			if( user->type() == 1 )
			{
				server->addAdmin(cnn);
			}
			
			QList<Users::Permission *> permissions = user->permissions();
			QList<Dash::Network::Permission *> userPerms;
			foreach(Permission *perm, permissions)
			{
				userPerms << perm;
			}
			
			Dash::Network::Package::Ack ack(QObject::tr("<center><b>Message of the day:</b></center><br/> ")+QObject::tr("Hello, world!"), cnn->sign(), userPerms);
			
			cnn->sendToClient(ack.toString(), false);
		}
		else
		{
			Dash::Network::Package::Error error(QObject::tr("Invalid login or password"), Dash::Network::Package::Error::Err);
			
			dfDebug << error.toString();
			
			QString ip = cnn->client()->peerAddress().toString();
			server->banManager()->failed(ip);
			
			cnn->sendToClient(error.toString(), true);
			cnn->close();
		}
	}
	else if(root == "adduser")
	{
		if ( cnn->user()->canWriteOn(DashServer::Module::Admin) )
		{
			Parsers::UserActionParser parser;
			if(parser.parse(package))
			{
				Users::User user = parser.user();
				
				if( XDBMS::Manager::self()->addObject("users", &user) )
				{
					QDomDocument doc;
					doc.setContent(package);
					
					doc.firstChild().removeChild( doc.firstChild().firstChildElement ("password"));
					
					server->sendToAdmins(doc.toString());
					pkg->accept();
				}
				else
				{
					cnn->sendError(QObject::tr("Error adding user"), Dash::Network::Package::Error::Err);
				}
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if ( root == "listusers" )
	{
		if ( cnn->user()->canWriteOn(DashServer::Module::Admin) )
		{
			Dash::Network::Package::UserList info;
			
			QList<User *> users = this->users();
			
			foreach(Users::User *u, users )
			{
				info.addUser(u->login(), u->name());
			}
			
			qDeleteAll(users);
			users.clear();
			
			cnn->sendToClient(info, true);
			pkg->accept();
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if(root == "removeuser")
	{
		if ( cnn->user()->canWriteOn( DashServer::Module::Admin ) )
		{
			Parsers::UserActionParser parser;
			if(parser.parse(package))
			{
				User user = parser.user();
				
				if( XDBMS::Manager::self()->remove("users", &user) )
				{
					server->sendToAdmins(package);
					pkg->accept();
				}
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if(root == "queryuser")
	{
		if ( cnn->user()->canReadOn( DashServer::Module::Admin ) )
		{
			Parsers::UserActionParser parser;
			if(parser.parse(package))
			{
				Users::User user = parser.user();
				
				if(XDBMS::Manager::self()->load("users", &user))
				{
					QString xml;
					
					{
						QXmlStreamWriter writer(&xml);
						
						writer.writeStartDocument();
						writer.writeStartElement("userquery");
						
						user.toXml(&writer);
						
						writer.writeEndElement();
						writer.writeEndDocument();
					}
					
					cnn->sendToClient(xml, true);
					pkg->accept();
				}
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if(root == "updateuser")
	{
		if ( cnn->user()->canWriteOn( DashServer::Module::Admin ) )
		{
			Parsers::UserActionParser parser;
			User user = parser.user();
			
			if(parser.parse(package))
			{
				if( XDBMS::Manager::self()->update("users", &user) )
				{
					server->sendToAdmins(package);
					pkg->accept();
				}
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
}


User *Manager::parseConnect(const QString &package)
{
	QString login, password;
	int type = 0;
	
	QXmlStreamReader reader(package);
	
	while( !reader.atEnd() )
	{
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				QString tagName = reader.name().toString();
				
				if( tagName == "client" )
				{
					type = reader.attributes().value("type").toString().toInt();
				}
				else if( tagName == "login" )
				{
					reader.readNext();
					if( reader.isCharacters() )
					{
						login = reader.text().toString();
					}
				}
				else if(tagName == "password" )
				{
					reader.readNext();
					
					if( reader.isCharacters() )
					{
						password = reader.text().toString();
					}
				}
			}
			break;
			default: break;
			
		}
	}
	
	if( ! login.isEmpty() )
	{
		Users::User *user = new Users::User;
		user->setLogin(login);
		
		XDBMS::Manager::self()->load("users", user);
		
		if( user->password() == password ) // FIXME: encrypt
		{
			user->setType(type);
			return user;
		}
		
		delete user;
	}
	
	return 0;
}

}
}



