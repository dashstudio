/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "useractionparser.h"
#include "users/user.h"


#include <dcore/debug.h>

namespace DashServer {
namespace Parsers {

struct UserActionParser::Private
{
	Users::User user;
	QString currentTag;
	QString root;
};

UserActionParser::UserActionParser() : YAMF::Common::XmlParser(), d(new Private)
{
	
}


UserActionParser::~UserActionParser()
{
}


bool UserActionParser::startTag(const QString &tag, const YAMF::Common::XmlAttributes &atts)
{
	if( d->root.isEmpty() )
	{
		d->root = tag;
	}
	
	d->currentTag = tag;
	
	if(tag == "module")
	{
		Users::Permission *permission = new Users::Permission(atts.value("name"));	
		
		permission->setRead(atts.boolean("read"));
		permission->setWrite(atts.boolean("write"));
		
		d->user.addPermission(permission);
 		
	}
	
	return true;
}

bool UserActionParser::endTag(const QString &tag)
{
	Q_UNUSED(tag);
	d->currentTag = QString();
	return true;
}

bool UserActionParser::characters(const QString &text)
{
	if( d->currentTag.isEmpty() )
	{
		return true;
	}
	
	if(d->currentTag == "login")
	{
		d->user.setLogin(text);
	}
	else if(d->currentTag == "password")
	{
		d->user.setPassword(text);
	}
	else if(d->currentTag == "name")
	{
		d->user.setName(text);
	}
	return true;
}

Users::User UserActionParser::user() const
{
	return d->user;
}

}
}

