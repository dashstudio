/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef USER_H
#define USER_H

#include <QString>
#include <QList>

#include <dashserver/global.h>
#include <dashserver/xdbms/object.h>
#include <dashserver/users/permission.h>


namespace DashServer {
namespace Users {

class Permission;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT User : public XDBMS::Object
{
	public:
		User();
		~User();
		
		void setName(const QString &name);
		void setLogin(const QString &login);
		void setPassword(const QString &password);
		
		void setType(int type);
		
		QString name() const;
		QString login() const;
		QString password() const;
		
		int type() const;
		
		QList<Permission *> permissions() const;
		
		void addPermission(int module, Permission::Opts opts);
		void addPermission(const QString &module, Permission::Opts opts);
		void addPermission(Permission *perm);
		
		
		bool canReadOn(int module) const;
		bool canWriteOn(int module) const;
		
		
		void toXml(QXmlStreamWriter *writer);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif


