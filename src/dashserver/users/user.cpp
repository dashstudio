/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "user.h"

#include <QStringList>
#include <QXmlStreamWriter>
#include <QSet>

#include <dcore/debug.h>

#include "dashserver/users/permission.h"

namespace DashServer {
namespace Users {

struct User::Private
{
	Private(User *q) : q(q), initialized(false) {}
	
	User *q;
	bool initialized;
	
	QHash<QString, Users::Permission *> permissions;
	
	void loadPermissions();
};

void User::Private::loadPermissions()
{
	permissions.clear();
	
	QList<XDBMS::Object *> objects = q->objects();
	
	foreach(Object *obj, objects)
	{
		if( obj->klass() == "permission" )
		{
			Users::Permission *perm = static_cast<Users::Permission *>(obj);
			permissions[perm->name()] = perm;
		}
	}
	
	initialized = true;
}

User::User() : d(new Private(this))
{
	setClass("user");
	
	setName(QString());
	setLogin(QString());
	setPassword(QString());
}


User::~User()
{
	delete d;
}

void User::setName(const QString &name)
{
	add("name", name);
}

void User::setLogin(const QString &login)
{
	setId(login);
	add("login", login);
}

void User::setPassword(const QString &password)
{
	add("password", password);
}

void User::setType(int type)
{
	add("type", QString::number(type) );
}

QString User::name() const
{
	return value("name");
}

QString User::login() const
{
	return value("login");
}

QString User::password() const
{
	return value("password");
}

int User::type() const
{
	return value("type").toInt();
}

QList<Permission *> User::permissions() const
{
	return d->permissions.values();
}

void User::addPermission(int module, Permission::Opts opts)
{
	QString m = DashServer::Module::name(module);
	
	addPermission(m, opts);
}

void User::addPermission(const QString &module, Permission::Opts opts)
{
	addPermission(new Permission(module, opts));
}

void User::addPermission(Permission *perm)
{
	QString module = perm->name();
	
	if( !d->permissions.contains(module) )
	{
		d->permissions[module] = perm;
		add(perm);
	}
	else
	{
		delete perm;
		perm = 0;
	}
}

bool User::canReadOn(int module) const
{
	if( !d->initialized ) d->loadPermissions();
	
	QString m = DashServer::Module::name(module);
	
	if( d->permissions.contains(m) )
	{
		return d->permissions[m]->read();
	}
	
	return false;
}

bool User::canWriteOn(int module) const
{
	if( !d->initialized ) d->loadPermissions();
	
	QString m = DashServer::Module::name(module);
	
	if( d->permissions.contains(m) )
	{
		return d->permissions[m]->write();
	}
	
	return false;
}

void User::toXml(QXmlStreamWriter *writer)
{
	if( !d->initialized ) d->loadPermissions();
	
	writer->writeStartElement("login");
	writer->writeCharacters(login());
	writer->writeEndElement();
	
	writer->writeStartElement("name");
	writer->writeCharacters(name());
	writer->writeEndElement();
	
	writer->writeStartElement("permissions");
	
	foreach(Permission *perm, d->permissions)
	{
		writer->writeStartElement("module");
		writer->writeAttribute("name", perm->name());
		writer->writeAttribute("read", perm->read() ? "1" : "0" );
		writer->writeAttribute("write", perm->write() ? "1" : "0" );
		writer->writeEndElement();
	}
	
	writer->writeEndElement();
}


}
}



