
DEFINES += _BUILD_DASHSERVER_
TEMPLATE = lib

win32 {
    CONFIG += dll
}else {
    CONFIG += staticlib
}

TARGET = dashserver
QT += xml network

include(../../config.pri)

INCLUDEPATH += ..

# Input
HEADERS += logger.h \
           observer.h \
           package.h \
           connection.h \
           defaultpackagehandler.h \
           packagehandlerbase.h \
           server.h \
           global.h  \
           exports.h \
           databaseobjectfactory.h 
SOURCES += logger.cpp \
           observer.cpp \
           package.cpp \
           connection.cpp \
           defaultpackagehandler.cpp \
           packagehandlerbase.cpp \
           server.cpp \
           global.cpp \
           databaseobjectfactory.cpp 

LIBS += -ldash

include(users_module.pri)
include(backups_module.pri)
include(bans_module.pri)
include(com_module.pri)
include(registers_module.pri)
include(project_module.pri)

include(xdbms.pri)


INSTALLS += target server packages users backups bans comunications registers project xdbms

target.path = /lib

server.files = *.h
server.path = /include/dashserver/

packages.files = packages/*.h
packages.path = /include/dashserver/packages

users.files = users/*.h
users.path = /include/dashserver/users/

backups.files = backups/*.h
backups.path = /include/dashserver/backups/

bans.files = bans/*.h
bans.path = /include/dashserver/bans

comunications.files = comunications/*.h
comunications.path = /include/dashserver/comunications

registers.files = registers/*.h
registers.path = /include/dashserver/registers

project.files = project/*.h
project.path = /include/dashserver/project

xdbms.files = xdbms/*.h
xdbms.path = /include/dashserver/xdbms

