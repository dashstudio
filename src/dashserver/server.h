/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _SERVER_H__
#define _SERVER_H__

#include <QStringList>
#include <QTcpServer>
#include <QDomDocument>

#include <dashserver/exports.h>


namespace DashServer {
class Observer;
class Connection;
class Client;
class PackageHandlerBase;

namespace Backups {
	class Manager;
}

namespace Bans {
	class Manager;
}

namespace Users {
	class Manager;
}

namespace Project {
	class Manager;
}

/**
 * Esta es la clase controladora, esta clase representa el servidor.
 * @author David Cuadrado \<krawek@gmail.com\>
 */
class DASHSERVER_EXPORT TcpServer : public QTcpServer
{
	Q_OBJECT;
	
	public:
		TcpServer(QObject *parent = 0);
		~TcpServer();
		void sendToAll(const QDomDocument &pkg);
		bool openConnection(const QString &host, int port);
		
		void sendToAdmins(const QString &str);
		
		void addAdmin(DashServer::Connection *cnx);
		Backups::Manager *backupManager() const;
		Bans::Manager *banManager() const;
		Users::Manager *userManager() const;
		Project::Manager *projectManager() const;
		
		void addObserver(DashServer::Observer *observer);
		bool removeObserver(DashServer::Observer *observer);
		
	public slots:
		void sendToAll(const QString &msg);
		void removeConnection(DashServer::Connection *cnx);
		void handlePackage( DashServer::Connection *client, const QString &root, const QString & packages  );
	
	private:
		void handle(DashServer::Connection *cnx);
		
	protected:
		void incomingConnection(int socketDescriptor);
		
	private:
		class Private;
		Private * const d;
};

}

#endif


