/***************************************************************************
 *   Copyright (C) 2007 by Jorge Cuadrado                                  *
 *   kuadrosxx@gmail.com                                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "backup.h"

namespace DashServer {
namespace Backups {

Backup::Backup()
{
	setClass("backup");
}


Backup::~Backup()
{
}

QDateTime Backup::date() const
{
	return QDateTime::fromString(value("date"), Qt::ISODate);
}

void Backup::setDate(const QDateTime & date)
{
	QString sdate = date.toString(Qt::ISODate);
	add("date", sdate);
	
	setId(name()+"-"+sdate);
}

QString Backup::name() const
{
	return value("name");
}

void Backup::setName(const QString &name)
{
	add("name", name);
	setId(name+"-"+value("date"));
}


QString Backup::fileName() const
{
	return value("filename");
}

void Backup::setFileName(const QString &filename)
{
	add("filename", filename);
}

}
}
