/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "backupmanager.h"

#include <QDir>
#include <QFileInfo>
#include <QDateTime>

#include <dcore/debug.h>
#include <dcore/config.h>

#include "backup.h"

#include "connection.h"
#include "server.h"

#include "users/user.h"

#include "dash/network/package/addbackup.h"
#include "dash/network/package/backuplist.h"
#include "dash/network/parser/addbackupparser.h"

#include "removebackupparser.h"

#include "package.h"

#include "dashserver/project/project.h"

#include "dashserver/xdbms/manager.h"
#include "dashserver/xdbms/table.h"
#include "dashserver/xdbms/object.h"

#include "dashserver/project/projectmanager.h"

namespace DashServer {
namespace Backups {

struct Manager::Private
{
	QString fileName(const QString &origFile, const QDateTime &date)
	{
		QFileInfo fi(origFile);
		
		QString destfile = "backup_"+fi.baseName()+"-"+date.toString(Qt::ISODate);
		
		if( ! fi.completeSuffix().isEmpty() )
		{
			destfile += "."+fi.completeSuffix();
		}
		
		return destfile;
	}
};

Manager::Manager() : d(new Private)
{
}


Manager::~Manager()
{
	delete d;
}

bool Manager::makeBackup(const QString &filepath, const QDateTime &date, const QString &name)
{
	dDebug() << "Making backup: " << filepath;
	QFile file(filepath);
	
	if( file.exists() )
	{
		QString destfile = d->fileName(filepath, date);
		
		Backups::Backup backup;
		
		backup.setDate(date);
		backup.setName(name);
		backup.setFileName(destfile);
		
		QDir dir = XDBMS::Manager::self()->databaseDir();
		
		if(file.copy( dir.absoluteFilePath(destfile) ) )
		{
			XDBMS::Manager::self()->addObject("backups", &backup);
			return true;
		}
	}
	return false;
}

bool Manager::removeBackup(const QString &name, const QDateTime &date)
{
	QString id = name+"-"+date.toString(Qt::ISODate);
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("backups");
	
	if( table->contains(id) )
	{
		Backup backup;
		backup.setId(id);
		
		table->load(&backup);
		
		QDir dir = XDBMS::Manager::self()->databaseDir();
		
		QFile::remove(dir.absoluteFilePath(backup.fileName()) );
		
		return table->remove(&backup);
	}
	
	return false;
}

bool Manager::restoreBackup(const QString &name, const QDateTime &date)
{
	QString id = name+"-"+date.toString(Qt::ISODate);
	
	dfDebug << "Restoring backup: " << id;
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("backups");
	
	if( table->contains(id) )
	{
		Backup backup;
		backup.setId(id);
		
		if( table->load(&backup) )
		{
			Project::Project project;
			project.setId(name);
			
			if( XDBMS::Manager::self()->load("projects", &project) )
			{
				QDir dir = XDBMS::Manager::self()->databaseDir();
				
				QFile::remove(project.fileName());
				if( QFile::copy(dir.absoluteFilePath(backup.fileName()), project.fileName() ) )
				{
					return true;
				}
			}
		}
	}
	
	return false;
}

QList<Backup *> Manager::backups() const
{
	QList<Backup *> backups;
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("backups");
	foreach(QString id, table->objects())
	{
		Backup *backup = new Backup;
		backup->setId(id);
		
		table->load(backup);
		
		backups << backup;
	}
	
	return backups;
}


void Manager::handlePackage(DashServer::Package *const pkg)
{
	QString root = pkg->root();
	DashServer::Connection *cnn = pkg->source();
	DashServer::TcpServer *server = cnn->server();
	
	if( root == "addbackup" )
	{
		if( cnn->user()->canReadOn( DashServer::Module::Admin ) )
		{
			Dash::Network::Parser::AddBackup parser;
			
			if( parser.parse(pkg->xml()) )
			{
				QStringList backups = parser.backups();
				
				Dash::Network::Package::AddBackup response;
				bool ok = false;
				
				foreach(QString pname, backups)
				{
					Project::Project *project = new Project::Project;
					project->setId(pname);
					
					if( XDBMS::Manager::self()->load("projects", project) )
					{
						QDateTime date = QDateTime::currentDateTime();
						if( this->makeBackup(project->fileName(), date, pname) )
						{
							response.addEntry(pname, date);
							ok = true;
						}
					}
				}
				
				if( ok )
				{
					cnn->sendToClient(response.toString(), true);
					pkg->accept();
				}
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	if( root == "listbackups" )
	{
		if( cnn->user()->canReadOn( DashServer::Module::Admin ) )
		{
			QHash<QString, QStringList > entries;
			
			Dash::Network::Package::BackupList info;
			QList<Backup *> backups = this->backups();
			
			foreach(Backup *b, backups )
			{
				entries[b->name()] << b->date().toString(Qt::ISODate);
				
				delete b;
			}
			
			QHash<QString, QStringList >::iterator it = entries.begin();
			
			while(it != entries.end())
			{
				info.addEntry(it.key(), it.value());
				it++;
			}
			
			cnn->sendToClient(info, true);
			pkg->accept();
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if( root == "removebackup" )
	{
		if( cnn->user()->canWriteOn( DashServer::Module::Admin ) )
		{
			Parsers::RemoveBackupParser parser;
			
			if(parser.parse(pkg->xml()))
			{
				QHashIterator<QString, QDateTime > it(parser.entries());
				
				while(it.hasNext())
				{
					it.next();
					this->removeBackup(it.key(), it.value());
				}
				
				server->sendToAdmins(pkg->xml());
				pkg->accept();
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if ( root == "restorebackup" )
	{
		if( cnn->user()->canWriteOn( DashServer::Module::Admin ) )
		{
			Parsers::RemoveBackupParser parser;
			
			if(parser.parse(pkg->xml()))
			{
				QHashIterator<QString, QDateTime > it(parser.entries());
				
				while(it.hasNext())
				{
					it.next();
					if( restoreBackup(it.key(), it.value()) )
					{
						server->projectManager()->resendProject(it.key());
					}
				}
				
				server->sendToAdmins(pkg->xml());
				pkg->accept();
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else
	{
		pkg->ignore();
	}
}

}
}

