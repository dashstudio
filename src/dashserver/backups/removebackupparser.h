/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PARSERSREMOVEBACKUPPARSER_H
#define PARSERSREMOVEBACKUPPARSER_H

#include <dcore/xmlparserbase.h>
#include <dashserver/exports.h>
#include <QDateTime>
#include <QHash>

namespace DashServer {
namespace Parsers {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT RemoveBackupParser : public DCore::XmlParserBase
{
	public:
		RemoveBackupParser();
		~RemoveBackupParser();
		
		bool startTag(const QString &tag, const QXmlAttributes &atts);
		bool endTag(const QString &tag);
		void text(const QString &msg);
		
		QHash<QString, QDateTime > entries() const;
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
