/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DOMSERVERCONNECTION_H
#define DOMSERVERCONNECTION_H

#include <QThread>
#include <QTcpSocket>
#include <QDomDocument>

#include <dash/network/socketobserver.h>
#include <dash/network/package/error.h>

#include <dashserver/exports.h>

namespace Dash {
namespace Network {
	class Socket;
}
}


namespace DashServer {
namespace Users {
	class User;
}

class TcpServer;

/**
 * Esta clase representa cada conexion de un cliente al servidor, es un hilo.
 * @author David Cuadrado \<krawek@gmail.com\>
 */
class DASHSERVER_EXPORT Connection : public QThread, virtual protected Dash::Network::SocketObserver
{
	Q_OBJECT;

	public:
		Connection(int socketDescriptor, DashServer::TcpServer *server);
		~Connection();
		
		void sendToClient(const QString &xml, bool sign) const;
		void sendToAll(const QString &xml, bool sign);
		
		void sendToClient(QDomDocument &doc, bool sign);
		void sendToAll(QDomDocument &doc, bool sign);
		
		void sendBinaryData(const QByteArray &data);
		
		void setData(int key, const QVariant &value);
		QVariant data(int key) const;
		
		Dash::Network::Socket *client() const;
		TcpServer *server() const;
		
		void setUser(Users::User *user);
		Users::User *user() const;
		
		void generateSign();
		
		QString sign() const;
		QString signXml(const QString &xml) const;
		
		void setValid(bool v);
		bool isValid() const;
		
		void process();
		
		void receiveProject();
		
	protected:
		void run();
		void readed(const QString &readed);
		void dataReaded(const QByteArray &data);
		
	public slots:
		void close();
		void sendError(const QString & message, Dash::Network::Package::Error::Level level);
		
	private slots:
		void removeConnection();
		void onProjectReceived(const QString &fileName);
		void cancelUpload();
		
	signals:
		void error(QTcpSocket::SocketError socketError);
		void requestSendToAll(const QString &msg);
		void connectionClosed( DashServer::Connection *cnn);
		void packageReaded(DashServer::Connection *cnn, const QString& root,const QString & packages  );
		
		
	private:
		class Private;
		Private *const d;
};

}

#endif


