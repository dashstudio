/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "connection.h"

// Qt
#include <QCryptographicHash>
#include <QQueue>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QTimer>

// DLib
#include <dcore/algorithm.h>
#include <dcore/debug.h>

// Dash
#include <dash/network/socket.h>

// Dashemon
#include "server.h"
#include "logger.h"
#include "users/user.h"
#include "dash/network/package/error.h"
#include "dash/network/filereceiver.h"

#include "dashserver/project/projectmanager.h"


namespace DashServer {

class Connection::Private
{
	public:
		Private(TcpServer *server) : server(server), waitingForProject(false), user(0), finishing(false)
		{
		}
		
		~Private()
		{
			delete client;
			delete user;
		}
		
		Dash::Network::Socket *client;
		DashServer::TcpServer *server;
		Dash::Network::FileReceiver *receiver;
		bool waitingForProject;
		
		bool isValid;
		QQueue<QString> readed;
		QHash<int, QVariant> data;
		QString sign;
		
		Users::User *user;
		bool finishing;
		
		QTimer *timeout;
		
		void signPackage(QDomDocument &doc);
};

void Connection::Private::signPackage(QDomDocument &doc)
{
	doc.documentElement().setAttribute("sign", sign);
}

Connection::Connection(int socketDescriptor, DashServer::TcpServer *server) : QThread(server), d(new Private(server))
{
	D_INIT;
	
	d->client = new Dash::Network::Socket(this);
	d->client->setSocketDescriptor(socketDescriptor);
	connect(d->client, SIGNAL(disconnected()), this, SLOT(removeConnection()));
	d->isValid = true;
	d->client->addObserver(this);
	
	d->receiver = new Dash::Network::FileReceiver;
	d->client->addObserver(d->receiver);
	connect(d->receiver, SIGNAL(finished(const QString &)), this, SLOT(onProjectReceived(const QString &)));
	
	d->timeout = new QTimer(this);
	connect(d->timeout, SIGNAL(timeout()), this, SLOT(cancelUpload()));
}

Connection::~Connection()
{
	D_END;
	delete d->receiver;
	delete d;
}

void Connection::run()
{
	while(! d->readed.isEmpty() )
	{
		if ( !d->isValid ) break;
		
		if( !d->user )
			d->isValid = false;
		
		QString readed = d->readed.dequeue();
		
		dDebug(/*"server"*/) << "Reicieved: " << readed;
		
		QString root;
		{
			QXmlStreamReader reader(readed);
			while( !reader.atEnd())
			{
				if( reader.readNext() == QXmlStreamReader::StartElement)
				{
					root = reader.name().toString();
					break;
				}
			}
		}
		
		if ( !root.isEmpty() )
		{
			if( d->waitingForProject && root == "transfer" )
			{
				d->waitingForProject = false;
				
				QXmlStreamReader reader(readed);
				while(! reader.atEnd() )
				{
					if( reader.readNext() == QXmlStreamReader::StartElement )
					{
						int bytes = reader.attributes().value("bytes").toString().toInt();
						
						d->receiver->start(bytes);
						d->client->setFormat(Dash::Network::Socket::Binary);
						
						d->timeout->start(30*1000);
						
						break;
					}
				}
			}
			else
			{
				emit packageReaded(this, root, readed);
			}
		}
		else
		{
			dError("server") << "Cannot set document content!";
		}
	}
	
	d->client->flush();
}


void Connection::removeConnection()
{
	emit connectionClosed(this);
}

void Connection::onProjectReceived(const QString &fileName)
{
	d->client->setFormat(Dash::Network::Socket::Text);
	
	d->server->projectManager()->importProject(this, fileName);
}

void Connection::cancelUpload()
{
	d->timeout->stop();
	d->receiver->stop();
	d->client->setFormat(Dash::Network::Socket::Text);
}

void Connection::close()
{
	d->isValid = false;
	d->finishing = true;
	
	d->readed.clear();
	if ( d->client->state() != QAbstractSocket::UnconnectedState )
	{
		d->client->flush();
		
		d->client->disconnectFromHost();
		d->client->waitForDisconnected(100);
	}
}

void Connection::readed(const QString &readed)
{
	if( d->finishing ) return;
	
	dDebug(/*"server"*/) << "Enqueing: " << readed;
	d->readed.enqueue(readed);
	
	process();
}

void Connection::dataReaded(const QByteArray &data)
{
	if( d->timeout->isActive() )
	{
		d->timeout->stop();
		d->timeout->start(40*1000);
	}
	Q_UNUSED(data);
}

void Connection::sendToClient(const QString &xml, bool sign) const
{
	if( sign )
	{
		d->client->send( signXml(xml).toLocal8Bit() );
	}
	else
	{
		d->client->send( xml.toLocal8Bit() );
	}
}

void Connection::setData(int key, const QVariant &value)
{
	d->data.insert(key, value);
}

QVariant Connection::data(int key) const
{
	return d->data.value(key);
}

Dash::Network::Socket *Connection::client() const
{
	return d->client;
}

TcpServer *Connection::server() const
{
	return d->server;
}

void Connection::sendToAll(const QString &xml, bool sign)
{
	emit requestSendToAll(signXml(xml));
}

void Connection::sendToClient(QDomDocument &doc, bool sign)
{
	if ( sign)
		d->signPackage(doc);
	
	dDebug() << "sending " << doc.toString();
	
	d->client->send(doc);
	
}

void Connection::sendToAll(QDomDocument &doc, bool sign)
{
	if( sign )
		d->signPackage(doc);
	emit requestSendToAll(doc.toString(0));
}

void Connection::sendBinaryData(const QByteArray &data)
{
	d->client->setFormat(Dash::Network::Socket::Binary);
	d->client->send(data);
	d->client->setFormat(Dash::Network::Socket::Text);
}

QString Connection::sign() const
{
	return d->sign;
}

QString Connection::signXml(const QString &xml) const
{
	QString tosend;
	
	if( !d->sign.isEmpty() )
	{
		QXmlStreamReader reader(xml);
		QXmlStreamWriter writer(&tosend);
		
		bool isRoot = false;
		while( ! reader.atEnd() )
		{
			reader.readNext();
			
			writer.writeCurrentToken(reader);
			if( reader.tokenType() == QXmlStreamReader::StartElement && !isRoot )
			{
				dfDebug << "Writing sign on " << reader.name().toString();
				
				if( reader.attributes().value("sign").isEmpty() )
					writer.writeAttribute("sign", d->sign);
				isRoot = true;
			}
		}
		
		if ( !isRoot )
			tosend = xml;
	}
	else
	{
		tosend = xml;
	}
	
	return tosend;
}

void Connection::setUser(Users::User *user)
{
	d->user = user;
	generateSign();
	
	d->isValid = true;
}

Users::User *Connection::user() const
{
	return d->user;
}

void Connection::generateSign()
{
	if ( d->user )
	{
		d->sign =
 		QCryptographicHash::hash((d->user->login()+DCore::Algorithm::randomString(DCore::Algorithm::random() % 10)).toLocal8Bit(), QCryptographicHash::Md5).toBase64();
	}
}


void Connection::sendError(const QString & message, Dash::Network::Package::Error::Level level)
{
	Dash::Network::Package::Error error(message, level);
	sendToClient(error.toString(), true);
}

void Connection::setValid(bool v)
{
	d->isValid = v;
}

bool Connection::isValid() const
{
	return d->isValid;
}

void Connection::process()
{
	if( !isRunning() && !d->finishing )
	{
		start();
	}
}

void Connection::receiveProject()
{
	d->waitingForProject = true;
}

}



