/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "banmanager.h"

#include <QHash>
#include <QString>
#include <QApplication>

#include <dcore/debug.h>

#include "package.h"

#include "connection.h"
#include "server.h"

#include "users/user.h"

#include "dash/network/package/banlist.h"
#include "removebanparser.h"
#include "addbanparser.h"

#include "banmanager.h"
#include "ban.h"

#include "dashserver/xdbms/manager.h"
#include "dashserver/xdbms/table.h"

namespace DashServer {
namespace Bans {

struct Manager::Private
{
	int maxFails;
	QHash<QString, int> fails;
};

Manager::Manager() : d(new Private)
{
	d->maxFails = 10;
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("bans");
	
	foreach(QString ip, table->objects())
	{
		d->fails[ip] = d->maxFails;
	}
}


Manager::~Manager()
{
	delete d;
}

void Manager::initialize(const QString &pt)
{
	if(!d->fails.contains(pt))
	{
		d->fails.insert(pt, 0);
	}
}

bool Manager::isBanned(const QString &pt) const
{
	if ( d->fails.contains(pt) )
	{
		if ( d->fails[pt] >= d->maxFails )
		{
			return true;
		}
		
	}
	return false;
}

void Manager::failed(const QString &pt)
{
	d->fails[pt] += 1;
	
	if( d->fails[pt] >= d->maxFails )
	{
		ban(pt);
	}
}

void Manager::ban(const QString &pt)
{
	d->fails[pt] = d->maxFails;
	
	Ban ban;
	ban.setPattern(pt);
	XDBMS::Manager::self()->addObject("bans", &ban);
}

void Manager::unban(const QString &pt)
{
	d->fails[pt] = 0;
	
	Ban ban;
	ban.setPattern(pt);
	XDBMS::Manager::self()->remove("bans", &ban);
}

QStringList Manager::allBanned() const
{
	QStringList allBanned;
	
	QHash<QString, int>::const_iterator i = d->fails.constBegin();
	
	while (i != d->fails.constEnd())
	{
		if ( isBanned(i.key()))
			allBanned << i.key();
			++i;
	}
	
	return allBanned;
}

void Manager::handlePackage(DashServer::Package *const pkg)
{
	QString root = pkg->root();
	QString package = pkg->xml();
	DashServer::Connection *cnn = pkg->source();
	DashServer::TcpServer *server = cnn->server();
	
	if ( root == "listbans" )
	{
		if ( cnn->user()->canReadOn( DashServer::Module::Admin ) )
		{
			pkg->accept();
			
			QStringList bans = this->allBanned();
			
			Dash::Network::Package::BanList pkg;
			pkg.setBans(bans);
			
			cnn->sendToClient(pkg, true);
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if ( root == "removeban" )
	{
		if( cnn->user()->canWriteOn(DashServer::Module::Admin) )
		{
			Parsers::RemoveBanParser parser;
			if ( parser.parse(package) )
			{
				this->unban(parser.pattern());
				
				server->sendToAdmins(package);
				pkg->accept();
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else if ( root == "addban" )
	{
		if( cnn->user()->canWriteOn(DashServer::Module::Admin ) )
		{
			Parsers::AddBanParser parser;
			if ( parser.parse(package) )
			{
				this->ban(parser.pattern());
				server->sendToAdmins(package);
				pkg->accept();
			}
		}
		else
		{
			cnn->sendError(QObject::tr("Permission denied."), Dash::Network::Package::Error::Err);
		}
	}
	else
	{
		pkg->ignore();
	}
}

}
}




