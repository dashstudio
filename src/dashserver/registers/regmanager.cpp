/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@gmail.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "regmanager.h"

#include <QFile>
#include <QCryptographicHash>

#include <dcore/debug.h>

#include "package.h"

#include "connection.h"
#include "server.h"

#include "actionregisteruserparser.h"

#include "dashserver/users/usermanager.h"
#include "dashserver/users/user.h"

#include "dash/network/package/adduser.h"

namespace DashServer {
namespace Registers {

struct Manager::Private
{
};

Manager::Manager() : DashServer::Observer(),d( new Private)
{
}


Manager::~Manager()
{
	delete d;
}


void Manager::handlePackage(DashServer::Package* const pkg)
{
	DashServer::TcpServer *server = pkg->source()->server();
	
	if( pkg->root() == "listregisters" )
	{
// 		QString fname = d->db->fileName();
// 		
// 		QFile f(fname);
// 		if( f.exists() )
// 		{
// 			if( f.open(QIODevice::ReadOnly | QIODevice::Text) )
// 			{
// 				pkg->source()->sendToClient(f.readAll());
// 			}
// 		}
	}
	else if ( pkg->root() == "registeruser" )
	{
		Parsers::ActionRegisterUserParser parser;
		
		if( parser.parse(pkg->xml()))
		{
// 			QHash<QString, QString> data = parser.data();
// 			QString email = data["email"];
// 			
// 			data = d->db->findRegisterByEmail(email);
// 			d->db->removeRegister(email);
// 			
// 			dDebug() << data["login"];
// 			
// 			Users::Manager *manager = server->userManager();
// 			
// 			Users::User user;
// 			user.setLogin(data["login"]);
// 			user.setName(data["name"]);
// 			user.setPassword(QCryptographicHash::hash("", QCryptographicHash::Md5));
// 			
// // 			manager->addUser(user); FIXME
// 			
// 			server->sendToAdmins(pkg->xml());
// 			Packages::AddUser adduser(data["login"], data["name"]);
// 			server->sendToAdmins(adduser.toString());
		}
	}
}

}
}


