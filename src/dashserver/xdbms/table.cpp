/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "table.h"

#include "manager.h"

#include <QXmlStreamWriter>
#include <QSet>
#include <QTemporaryFile>

#include <dcore/debug.h>

namespace XDBMS {


class XmlReader : public QXmlStreamReader
{
	public:
		XmlReader(const QString &str);
		XmlReader(QIODevice *io);
		~XmlReader();
};


XmlReader::XmlReader(const QString &str) : QXmlStreamReader(str)
{
}

XmlReader::XmlReader(QIODevice *io) : QXmlStreamReader(io)
{
}

XmlReader::~XmlReader()
{
}

struct Table::Private
{
	Private(Table *q) : q(q) {}
	
	QSet<QString> objects;
	QString id;
	
	Table *q;
	
	QString objectXml(const QString &id, const QString &klass) const;
	bool writeObject(Object *object);
	bool removeObject(const QString &id);
	bool replaceObject(const QString &id, Object *object);
};

QString Table::Private::objectXml(const QString &id, const QString &klass) const
{
	QString xml;
	
	QFile f(q->fileName());
	
	if( !f.open(QIODevice::ReadOnly) ) return xml;
	
	{
		XmlReader reader(&f);
		QXmlStreamWriter writer(&xml);
		bool read = false;
		int level = 0;
		
		while(!reader.atEnd() )
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::StartElement )
			{
				QString tagName = reader.name().toString();
				
				if( tagName == "object" )
				{
					dfDebug << reader.attributes().value("class").toString();
					
					if( reader.attributes().value("id").toString() == id && reader.attributes().value("class").toString() == klass )
					{
						read = true;
					}
					
					level++;
				}
			}
			else if( reader.tokenType() == QXmlStreamReader::EndElement )
			{
				QString tagName = reader.name().toString();
				
				if( tagName == "object" )
				{
					level--;
				}
			}
			
			if( read )
			{
				writer.writeCurrentToken(reader);
			}
			
			if( read && level == 0)
			{
				read = false;
				break;
			}
		}
	}
	
	return xml;
}

bool Table::Private::writeObject(Object *object)
{
	QFile f(q->fileName());
	
	bool exists = f.exists();
	
	if( !f.open(QIODevice::WriteOnly | QIODevice::ReadOnly) )
		return false;
	
	if( exists )
	{
		QXmlStreamReader reader(&f);
		QTemporaryFile outFile;
		outFile.open();
		
		QXmlStreamWriter writer(&outFile);
		writer.setAutoFormatting(true);
		
		while(!reader.atEnd())
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::Invalid )
			{
				exists = false;
				break;
			}
			
			if( reader.tokenType() == QXmlStreamReader::EndElement)
			{
				if( reader.name() == "table" )
				{
					object->save(&writer);
				}
			}
			
			writer.writeCurrentToken(reader);
		}
		
		if( exists )
		{
			QString fname = outFile.fileName();
			QString dest = f.fileName();
			outFile.setAutoRemove(false);
			outFile.close();
			f.close();
			
			if( f.remove() )
			{
				bool ret = QFile::copy(fname, dest);
				QFile::remove(fname);
				
				objects << object->id();
				
				return ret;
			}
		}
	}
	
	if(!exists)
	{
		QXmlStreamWriter writer(&f);
		writer.setAutoFormatting(true);
		
		writer.writeStartDocument();
		writer.writeStartElement("table");
		
		object->save(&writer);
		
		writer.writeEndElement();
		writer.writeEndDocument();
		
		objects << object->id();
		
		return true;
	}
	
	return false;
}

bool Table::Private::removeObject(const QString &id)
{
	dDebug() << "XDBMS: REMOVE OBJECT: " << id;
	QFile f(q->fileName());
	QTemporaryFile output("output");
	
	if( !f.open(QIODevice::ReadOnly) ) return false;
	if( !output.open() ) return false;
	
	QXmlStreamReader reader(&f);
	QXmlStreamWriter writer(&output);
	writer.setAutoFormatting(true);
	
	bool write = true;
	bool modified = false;
	int level = 0;
	
	while(!reader.atEnd() )
	{
		reader.readNext();
		
		if( reader.tokenType() == QXmlStreamReader::StartElement )
		{
			if( reader.name() == "object" )
			{
				if( reader.attributes().value("id").toString() == id && level == 0 )
				{
					modified = true;
					write = false;
				}
				
				level++;
			}
		}
		else if( reader.tokenType() == QXmlStreamReader::EndElement )
		{
			if( reader.name() == "object" )
			{
				level--;
			}
		}
		
		if( write )
		{
			writer.writeCurrentToken(reader);
		}
		
		if( level == 0)
		{
			write = true;
		}
	}
	
	if( modified )
	{
		QString fname = output.fileName();
		QString dest = f.fileName();
		output.setAutoRemove(false);
		output.close();
		f.close();
		
		if( f.remove() )
		{
			bool ret = QFile::copy(fname, dest);
			QFile::remove(fname);
			
			
			objects.remove(id);
			return ret;
		}
	}
	
	return false;
}

bool Table::Private::replaceObject(const QString &id, Object *object)
{
	bool ok = removeObject(id);
	
	if( !ok ) return false;
	
	return writeObject(object);
}

Table::Table(const QString &id) : d(new Private(this))
{
	d->id = id;
	reload();
}


Table::~Table()
{
	delete d;
}

QStringList Table::objects() const
{
	return d->objects.values();
}

bool Table::contains(const QString &objectId) const
{
	return d->objects.contains(objectId);
}

bool Table::addObject(Object *object)
{
	QString id = object->id();
	if( !d->objects.contains(id) )
	{
		d->objects << id;
		
		d->writeObject(object);
		
		return true;
	}
	
	return false;
}

bool Table::load(Object *object) const
{
	QString id = object->id();
	
	dfDebug << object->klass();
	
	if( id.isEmpty() ) return false;
	
	if( d->objects.contains(id) )
	{
		dDebug() << "=> Loading object: " << id;
		return object->load(d->objectXml(id, object->klass()));
	}
	
	return false;
}

Object *Table::object(const QString &id) const
{
	if( d->objects.contains(id) )
	{
		Object *obj = new Object(id);
		load(obj);
		
		return obj;
	}
	
	return 0;
}

bool Table::update(Object *object)
{
	QString id = object->id();
	
	if( d->objects.contains(id) )
	{
		return d->replaceObject(id, object);
	}
	
	return false;
}

bool Table::remove(Object *object)
{
	QString id = object->id();
	
	if( d->objects.contains(id) )
	{
		return d->removeObject(id);
	}
	
	return false;
}

void Table::reload()
{
	QFile f(fileName());
	
	if( f.open(QIODevice::ReadOnly) )
	{
		dDebug() << "=> Loading table from: " << fileName();
		
		QXmlStreamReader reader(&f);
		
		int level = 0;
		
		while(!reader.atEnd() )
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::StartElement )
			{
				if( reader.name() == "object" )
				{
					if( level == 0 )
					{
						QStringRef str = reader.attributes().value("id");
						
						if( !str.isEmpty() )
						{
							d->objects << str.toString();
						}
					}
					
					level++;
				}
			}
			else if( reader.tokenType() == QXmlStreamReader::EndElement )
			{
				if( reader.name() == "object" )
				{
					level--;
				}
			}
		}
		
		dDebug() << "=> Objects: " << d->objects.values();
		
		f.close();
	}
}

QString Table::fileName() const
{
	return Manager::self()->databaseDir().absoluteFilePath(d->id+".xdt");
}


}


