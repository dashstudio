/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "manager.h"
#include "table.h"
#include "objectfactory.h"

#include <QHash>
#include <QDir>

#include <dcore/globaldeleter.h>
#include <dcore/debug.h>

static DCore::GlobalDeleter<XDBMS::Manager> deleter;

namespace XDBMS {

struct Manager::Private
{
	Private() : initialized(false), factory(new ObjectFactory) {}
	
	Manager *self;
	QHash<QString, Table *> tables;
	QString databasePath;
	
	bool initialized;
	ObjectFactory *factory;
};

Manager::Private *const Manager::d = new Manager::Private;

Manager::Manager(QObject *parent)
 : QObject(parent)
{
}


Manager::~Manager()
{
	delete d->factory;
	delete d;
}

bool Manager::initialize(const QString &databasePath)
{
	if( d->initialized ) return false;
	
	QDir dir(databasePath);
	
	if( !dir.exists() )
	{
		bool ok = dir.mkdir(dir.absolutePath());
		
		if( ok )
		{
			d->initialized = true;
			d->databasePath = dir.absolutePath();
		}
		
		return ok;
	}
	
	d->databasePath = dir.absolutePath();
	d->initialized = true;
	
	// Read all tables
	foreach(QFileInfo table, dir.entryInfoList(QStringList() << "*.xdt"))
	{
		QString name = table.baseName();
		
		dDebug() << "=> Loading table: " << name;
		
		Table *t = this->table(name);
		Q_UNUSED(t);
	}
	
	return d->initialized;
}

Table *Manager::table(const QString &tableId)
{
	Table *table = d->tables.value(tableId);
	
	if( !table )
	{
		table = new Table(tableId);
		d->tables[tableId] = table;
	}
	
	return table;
}

bool Manager::addObject(const QString &tableId, Object *object)
{
	Table *table = this->table(tableId);
	if( table )
	{
		return table->addObject(object);
	}
	
	return false;
}

bool Manager::load(const QString &tableId, Object *object) const
{
	Table *table = d->tables.value(tableId);
	if( table )
	{
		return table->load(object);
	}
	
	return false;
}

Object *Manager::object(const QString &tableId, const QString &id) const
{
	Table *table = d->tables.value(tableId);
	if( table )
	{
		return table->object(id);
	}
	
	return 0;
}

bool Manager::update(const QString &tableId, Object *object)
{
	Table *table = d->tables.value(tableId);
	if( table )
	{
		return table->update(object);
	}
	
	return false;
}

bool Manager::remove(const QString &tableId, Object *object)
{
	Table *table = d->tables.value(tableId);
	if( table )
	{
		return table->remove(object);
	}
	
	return false;
}

QDir Manager::databaseDir() const
{
	return QDir(d->databasePath);
}

void Manager::setObjectFactory(ObjectFactory *factory)
{
	if(d->factory == factory)
		return;
	
	delete d->factory;
	d->factory = factory;
}

ObjectFactory *Manager::objectFactory() const
{
	return d->factory;
}


Manager *Manager::self()
{
	if( d->self )
	{
		d->self = new Manager;
		deleter.setObject(d->self);
	}
	
	return d->self;
}

}
