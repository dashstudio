/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "object.h"

#include <QXmlStreamWriter>
#include <QStringList>
#include <QSet>

#include <dcore/debug.h>

#include "manager.h"
#include "objectfactory.h"

namespace XDBMS {

struct Object::Private
{
	Private() : klass("object") {}
	
	QString id, klass;
	Properties properties;
	QHash<QString, QStringList > lists;
	QHash<QString, Object *> objects;
	
	struct ChildObjectData
	{
		QString xml;
		QString klass;
	};
};

Object::Object() : d(new Private)
{
}

Object::Object(const QString &id, const QString &klass) : d(new Private)
{
	d->id = id;
	d->klass = klass;
}


Object::~Object()
{
	d->lists.clear();
	
	qDeleteAll(d->objects);
	
	delete d;
}

void Object::setId(const QString &id)
{
	d->id = id;
}

void Object::setClass(const QString &klass)
{
	d->klass = klass;
}

QString Object::id() const
{
	return d->id;
}

QString Object::klass() const
{
	return d->klass;
}

bool Object::isValid() const
{
	return !d->id.isEmpty();
}

QList<Object *> Object::objects() const
{
	return d->objects.values();
}

/**
 *  \<[id] key1="value1" ... keyN="valueN" />
 */
void Object::add(const QString &id, const QString &value)
{
	d->properties[id] = value;
}

/**
 * \<id>
 *   \<item key1="value1" ... keyN="valueN" />
 * \</id>
 */
void Object::addList(const QString &id, const QStringList &items)
{
	d->lists[id] = items;
}

void Object::addToList(const QString &id, const QString &value)
{
	d->lists[id] << value;
}

void Object::add(Object *object)
{
	d->objects[object->id()] = object;
}

void Object::remove(Object *object)
{
	if( d->objects.contains(object->id()) )
	{
		delete d->objects.take(object->id());
	}
}

QString Object::value(const QString &property) const
{
	return d->properties.value(property);
}

QStringList Object::list(const QString &id) const
{
	return d->lists.value(id);
}

bool Object::contains(const QString &property) const
{
	return d->properties.contains(property) || d->lists.contains(property) ;
}

void Object::save(QXmlStreamWriter *writer)
{
	aboutToSave();
	
	Q_ASSERT(!d->id.isEmpty());
	
	writer->writeStartElement("object");
	writer->writeAttribute("id", d->id);
	if( !d->klass.isEmpty() )
	{
		writer->writeAttribute("class", d->klass);
	}
	
	{
		Properties::const_iterator it = d->properties.begin();
		
		while(it != d->properties.end() )
		{
			writer->writeEmptyElement( it.key() );
			writer->writeAttribute("value", it.value());
			
			++it;
		}
	}
	
	{
		QHash<QString, QStringList >::const_iterator it = d->lists.begin();
		
		while(it != d->lists.end() )
		{
			writer->writeStartElement( it.key() );
			
			foreach(QString v, it.value() )
			{
				writer->writeEmptyElement("item");
				writer->writeAttribute("value", v);
			}
			
			writer->writeEndElement();
			
			++it;
		}
	}
	
	{
		foreach(Object *obj, d->objects)
		{
			obj->save(writer);
		}
	}
	
	
	writer->writeEndElement();
}

bool Object::load(const QString &xml)
{
	dfDebug << xml;
	QXmlStreamReader reader(xml);
	QString lastId;
	QStringList items;
	bool ret = false;
	
	bool init = false;
	
	
	QList<Private::ChildObjectData> objects;
	
	QList<QXmlStreamWriter *> writers;
	int level = 0;
	
	while(! reader.atEnd() )
	{
		switch(reader.readNext())
		{
			case QXmlStreamReader::StartElement:
			{
				QString tagName = reader.name().toString();
				
				if( level > 1 )
				{
					writers.last()->writeCurrentToken(reader);
				}
				
				if( tagName == "object" )
				{
					if( !init )
					{
						QString id = reader.attributes().value("id").toString();
						
						if( !id.isEmpty() )
						{
							d->id = id;
							ret = true;
						}
						
						QString klass = reader.attributes().value("class").toString();
						
						if( !klass.isEmpty() )
						{
							d->klass = klass;
						}
						
						init = true;
					}
					else if( level == 1 )
					{
						Private::ChildObjectData data;
						data.klass = reader.attributes().value("class").toString();
						
						objects << data;
						writers << new QXmlStreamWriter(&objects.last().xml);
						
						writers.last()->writeCurrentToken(reader);
					}
					
					level++;
				}
				else if( tagName == "item" && lastId != "item" )
				{
					foreach( QXmlStreamAttribute att, reader.attributes())
					{
						items << att.value().toString();
					}
				}
				else
				{
					QString v = reader.attributes().value("value").toString();
					
					if( !v.isEmpty() )
					{
						d->properties[tagName] = v;
					}
					
					lastId = tagName;
				}
			}
			break;
			case QXmlStreamReader::EndElement:
			{
				if( level > 1 )
				{
					writers.last()->writeCurrentToken(reader);
				}
				
				if( reader.name() == "object" )
				{
					level--;
					if( level == 1 )
					{
						delete writers.takeLast();
					}
				}
				else if( reader.name().toString() == lastId )
				{
					if( !items.isEmpty() )
					{
						d->lists[lastId] = items;
						items.clear();
					}
					
					lastId = QString();
				}
			}
			break;
			case QXmlStreamReader::StartDocument:
			{
			}
			break;
			default: break;
		}
	}
	
	foreach(Private::ChildObjectData objdata, objects)
	{
		Object *ob = Manager::self()->objectFactory()->create(objdata.klass);
		ob->load(objdata.xml);
		
		D_SHOW_VAR(ob->id());
		
		add(ob);
	}
	
	afterLoad();
	
	return ret;
}

void Object::aboutToSave()
{
}

void Object::afterLoad()
{
}

}


