/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef XDBMSOBJECT_H
#define XDBMSOBJECT_H

#include <QHash>
#include <dashserver/exports.h>

class QXmlStreamWriter;

namespace XDBMS {

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Object
{
	public:
		typedef QHash<QString, QString> Properties;
		
		Object();
		Object(const QString &id, const QString &klass = QString());
		virtual ~Object();
		
		void setId(const QString &id);
		void setClass(const QString &klass);
		
		QString id() const;
		QString klass() const;
		bool isValid() const;
		
		
		QList<Object *> objects() const;
		
		void add(const QString &id, const QString &value);
		void addList(const QString &id, const QStringList &items);
		void addToList(const QString &id, const QString &value);
		
		void add(Object *object);
		void remove(Object *object);
		
		QString value(const QString &property) const;
		QStringList list(const QString &list) const;
		
		bool contains(const QString &att) const;
		
		virtual void save(QXmlStreamWriter *writer);
		virtual bool load(const QString &xml);
		
	protected:
		virtual void aboutToSave();
		virtual void afterLoad();
		
	private:
		struct Private;
		Private *const d;
};

}

#endif
