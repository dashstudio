/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef XDBMSMANAGER_H
#define XDBMSMANAGER_H

#include <QObject>
#include <QDir>

#include <dashserver/xdbms/object.h>
#include <dashserver/exports.h>

namespace XDBMS {
class Table;
class Object;
class ObjectFactory;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Manager : public QObject
{
	Q_OBJECT;
	
	public:
		Manager(QObject *parent = 0);
		~Manager();
		
		bool initialize(const QString &databasePath);
		
		Table *table(const QString &tableId);
		
		bool addObject(const QString &table, Object *object);
		bool load(const QString &table, Object *object) const;
		Object *object(const QString &table, const QString &id) const;
		
		bool update(const QString &tableId, Object *object);
		bool remove(const QString &tableId, Object *object);
		
		QDir databaseDir() const;
		
		void setObjectFactory(ObjectFactory *factory);
		ObjectFactory *objectFactory() const;
		
		static Manager *self();
		
	private:
		struct Private;
		static Private *const d;
};

}

#endif

