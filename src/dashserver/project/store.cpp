/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "store.h"

#include <QVariant>
#include <QDir>

#include <dcore/debug.h>
#include <dcore/algorithm.h>

#include "dash/projectsaver.h"

#include "project.h"
#include "member.h"

#include "dashserver/connection.h"
#include "dashserver/users/user.h"
#include "dashserver/xdbms/manager.h"
#include "dashserver/xdbms/table.h"

#include "dash/network/package/transfer.h"
#include "dash/network/package/connected.h"
#include "dash/network/package/disconnected.h"
#include "dash/network/package/error.h"

namespace DashServer {
namespace Project {

struct Store::Private
{
	QHash<QString, Project *> projects;
	
};

Store::Store() : d(new Private)
{
}


Store::~Store()
{
	qDeleteAll(d->projects);
	delete d;
}

QStringList Store::projects() const
{
	XDBMS::Table *table = XDBMS::Manager::self()->table("projects");
	
	return table->objects();
}

Project *Store::project(const QString& name)
{
	XDBMS::Table *table = XDBMS::Manager::self()->table("projects");
	
	Project *project = new Project;
	project->setId(name);
	if(table->load(project))
	{
		project->afterLoad();
		project->setProjectName(name);
		return project;
	}
	return 0;
}

QList<Project *> Store::allProjects() const
{
	QList<Project *> projects;
	
	XDBMS::Table *table = XDBMS::Manager::self()->table("projects");
	foreach(QString projectName, table->objects())
	{
		Project *project = new Project;
		project->setId(projectName);
		table->load(project);
		
		project->afterLoad();
		project->setProjectName(projectName);
		
		projects << project;
	}
	
	
	return projects;
}

bool Store::createProject(const QString &name, const QString &author, const QString &description, const QString& owner, const QList<Member *> &members)
{
	if( !d->projects.contains(name) )
	{
		Project *project = new Project;
		project->setProjectName(name);
		project->setAuthor(author);
		project->setDescription(description);
		
		if(members.isEmpty())
		{
			Member *director = new Member;
			director->setJob(QObject::tr("Director"));
			director->setUser(owner);
			director->addPermission(Project::All, Users::Permission::Read | Users::Permission::Write);
			director->addPermission(Project::Scene, Users::Permission::Read | Users::Permission::Write);
			director->addPermission(Project::Layer, Users::Permission::Read | Users::Permission::Write);
			director->addPermission(Project::Frame, Users::Permission::Read | Users::Permission::Write);
			director->addPermission(Project::Object, Users::Permission::Read | Users::Permission::Write);
			
			project->addMember(director);
		}
		
		foreach(Member *memeber, members)
		{
			project->addMember(memeber);
		}
		
		project->setOpen(true);
		
		d->projects[name] = project;
		
		project->aboutToSave();
		XDBMS::Manager::self()->addObject("projects", project);
		
		return project->save();
	}
	
	return false;
}

void Store::addToProject(const QString &projectName, DashServer::Connection *cnx)
{
	if( d->projects.contains(projectName) )
	{
		Project *project = d->projects.value(projectName);
		
		cnx->setData(ProjectName, projectName);
		
		QStringList connections;
		foreach(DashServer::Connection *connection, project->connections())
		{
			Dash::Network::Package::Connected pkg(cnx->user()->login());
			connection->sendToClient(pkg.toString(), false);
			
			connections << connection->user()->login();
		}
		
		project->addClient(cnx);
		
		if( !connections.isEmpty() )
		{
			Dash::Network::Package::Connected pkg(connections);
			dfDebug << pkg.toString();
			cnx->sendToClient(pkg.toString(), true);
		}
	}
}

bool Store::contains(const QString &projectName) const
{
	return d->projects.contains(projectName);
}

bool Store::execute(const QString &xml, DashServer::Connection *source)
{
	QString projectName = source->data(ProjectName).toString();
	
	if( d->projects.contains(projectName) )
	{
		Project *project = d->projects.value(projectName);
		project->execute(xml, source);
		
		return true;
	}
	
	return false;
}

bool Store::openProject(const QString &projectName, DashServer::Connection *source)
{
	if( !d->projects.contains(projectName) )
	{
		Project *project = new Project;
		project->setId(projectName);
		
		if( ! XDBMS::Manager::self()->load("projects", project) ) return false;
		
		if( ! project->load() )
		{
			qWarning("Cannot load project");
			return false;
		}
		
		d->projects[projectName] = project;
	}
	
	{
		Project *project = d->projects.value(projectName);
		
		if( project->save() )
		{
			addToProject(projectName, source);
			project->sendToClient(source);
		}
		
		return true;
	}
	
	return false;
}

QList<Project *> Store::activeProjects() const
{
	return d->projects.values();
}

void Store::removeClient(DashServer::Connection *client)
{
	QString projectName = client->data(ProjectName).toString();
	
	if( d->projects.contains(projectName) )
	{
		Dash::Network::Package::Disconnected pkg(client->user()->login());
		sendToMembers(projectName, pkg.toString());
		
		Project *project = d->projects.value(projectName);
		project->removeClient(client);
		
		if( project->clientCount() == 0 )
		{
			d->projects.remove(projectName);
			delete project;
		}
	}
}

void Store::sendToMembers(const QString &projectName, const QString &pkg)
{
	if( d->projects.contains(projectName) )
	{
		Project *project = d->projects.value(projectName);
		foreach(DashServer::Connection *cnx, project->connections())
		{
			cnx->sendToClient(pkg, false);
		}
	}
}

void Store::resendProject(const QString &projectName)
{
	if( d->projects.contains(projectName) )
	{
		Project *project = d->projects.value(projectName);
		project->sendToAll();
	}
}

bool Store::removeProject(const QString &projectName)
{
	bool ok = false;
	Project *project = new Project;
	if(!d->projects.contains(projectName))
	{
		project->setId(projectName);
		if( ! XDBMS::Manager::self()->load("projects", project) )
		{
			delete project;
			return false;
		}
	}
	else
	{
		project = d->projects.value(projectName);
	}
	
	if(XDBMS::Manager::self()->remove("projects", project))
	{
		d->projects.remove(projectName);
		foreach(DashServer::Connection *connection, project->connections() )
		{
			connection->sendError(QObject::tr("Project removed"), Dash::Network::Package::Error::Info);
			connection->close();
		}
		ok = true;
	}
	delete project;
	return ok;
}

void Store::importProject(DashServer::Connection *cnx, const QString &fileName)
{
	if( QFile::exists(fileName) )
	{
		Project::Project *project = new Project::Project;
		if( !Dash::ProjectSaver::load(project, fileName) )
		{
			cnx->sendError( QObject::tr("Cannot import the project"), Dash::Network::Package::Error::Err);
			delete project;
			return;
		}
		
		QString projectName = project->projectName();
		
		if( !d->projects.contains(projectName) && !projectName.isEmpty() )
		{
			QString owner = cnx->user()->login();
			{
				Member *director = new Member;
				director->setJob(QObject::tr("Director"));
				director->setUser(owner);
				director->addPermission(Project::All, Users::Permission::Read | Users::Permission::Write);
				director->addPermission(Project::Scene, Users::Permission::Read | Users::Permission::Write);
				director->addPermission(Project::Layer, Users::Permission::Read | Users::Permission::Write);
				director->addPermission(Project::Frame, Users::Permission::Read | Users::Permission::Write);
				director->addPermission(Project::Object, Users::Permission::Read | Users::Permission::Write);
			
				project->addMember(director);
			}
			
			project->setOpen(true);
			d->projects[projectName] = project;
			
			project->aboutToSave();
			XDBMS::Manager::self()->addObject("projects", project);
			
			if( project->save() )
			{
				cnx->sendError( QObject::tr("Project imported successfully"), Dash::Network::Package::Error::Info);
			}
		}
		else
		{
			cnx->sendError( QObject::tr("Cannot import the project"), Dash::Network::Package::Error::Err);
			delete project;
		}
	}
}

}
}

