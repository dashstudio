/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DASHSERVER_PROJECTPROJECT_H
#define DASHSERVER_PROJECTPROJECT_H

#include <dash/project.h>
#include <dashserver/xdbms/object.h>
#include <dashserver/exports.h>

namespace DashServer {
class Connection;

namespace Project {
class Member;

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Project : public Dash::Project, public XDBMS::Object
{
	public:
		enum Part
		{
			None = 0x0,
			All = 0x01,
			Scene = 0x02,
			Layer = 0x08,
			Frame = 0x010,
			Object = 0x20,
			Library = 0x40
		};
		
		Project(QObject *parent = 0);
		~Project();
		
		void execute(const QString &xml, DashServer::Connection *source);
		void addClient(DashServer::Connection *cnx);
		void removeClient(DashServer::Connection *client);
		
		void sendToClient(DashServer::Connection *client);
		void sendToAll();
		
		QHash<QString, Member *> members() const;
		QSet<DashServer::Connection *> connections() const;
		
		int clientCount() const;
		
		void afterLoad();
		void aboutToSave();
		
		bool save();
		bool load();
		
		QString fileName() const;
		
		void addMember(Member *member);
		
		
		static QString partName(int part);
		static int partFromCommand(const QString &cmdname);
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
