/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "project.h"

#include <QSet>
#include <QXmlStreamReader>

#include <yamf/model/command/processor.h>

#include "dashserver/connection.h"
#include "dashserver/logger.h"
#include "dashserver/xdbms/manager.h"

#include "dash/projectsaver.h"
#include "dash/network/package/transfer.h"

#include "member.h"
#include "commandprocessor.h"

#include <dcore/debug.h>

namespace DashServer {
namespace Project {

struct Project::Private
{
	Private(Project *q) : q(q), initialized(false) {}
	
	Project *q;
	bool initialized;
	CommandProcessor *processor;
	
	QSet<DashServer::Connection *> connections;
	
	QHash<QString, Member *> members;
	
	
	void loadMembers();
};

void Project::Private::loadMembers()
{
	members.clear();
	
	QList<XDBMS::Object *> objects = q->objects();
	
	foreach(XDBMS::Object *obj, objects)
	{
		if( obj->klass() == "member" )
		{
			Member *member = static_cast<Member *>(obj);
			members[member->id()] = member;
		}
	}
	
	initialized = true;
}

Project::Project(QObject *parent) : Dash::Project(parent), XDBMS::Object(), d(new Private(this))
{
	d->processor = new CommandProcessor(this);
	
	setClass("project");
}


Project::~Project()
{
	delete d->processor;
	delete d;
}

void Project::execute(const QString &xml, DashServer::Connection *source)
{
	if( d->connections.contains(source) )
	{
		QString cmdname;
		
		{
			QXmlStreamReader reader(xml);
			while(!reader.atEnd())
			{
				if( reader.readNext() == QXmlStreamReader::StartElement )
				{
					if( reader.name() == "command" )
					{
						cmdname = reader.attributes().value("name").toString();
						break;
					}
				}
			}
		}
		
		Member *member = d->members.value(source->user()->login());
		int part = Project::partFromCommand(cmdname);
		
		if( ! member->canWriteOn(part) )
		{
			DashServer::Logger::self()->info(QString("Permission denied: User %1 trying to write on %2").arg(member->id(), Project::partName(part)));
			
			source->sendToClient( "<command />\n", true );
			source->sendError(tr("Permission denied"), Dash::Network::Package::Error::Err);
			return;
		}
		
		if( d->processor->execute(xml) )
		{
			QString tosend = source->signXml(xml);
			
			foreach(DashServer::Connection *cnx, d->connections)
			{
				if( d->members[cnx->user()->login()]->canReadOn(part) )
				{
					cnx->sendToClient( tosend, false );
				}
			}
		}
		else
		{
			source->sendToClient( "<command />", true ); // FIXME: create a package
		}
	}
	else
	{
		qWarning("INVALID SOURCE");
	}
}

void Project::addClient(DashServer::Connection *cnx)
{
	d->connections << cnx;
}

void Project::removeClient(DashServer::Connection *client)
{
	d->connections.remove(client);
	
	if( d->connections.isEmpty() )
	{
		save();
	}
}

void Project::sendToClient(DashServer::Connection *client)
{
	// FIXME: Poner en un thread
	{
		QFile f(this->fileName());
		Dash::Network::Package::Transfer transfer(f.size());
		client->sendToClient(transfer.toString(), true);
		
		if( f.open(QIODevice::ReadOnly) )
		{
			while(!f.atEnd())
			{
				client->sendBinaryData(f.read(1024));
			}
		}
	}
}

void Project::sendToAll()
{
	save();
	
	{
		QFile f(this->fileName());
		
		foreach(DashServer::Connection *client, d->connections)
		{
			Dash::Network::Package::Transfer transfer(f.size());
			client->sendToClient(transfer.toString(), true);
		}
		
		if( f.open(QIODevice::ReadOnly) )
		{
			while(!f.atEnd())
			{
				foreach(DashServer::Connection *client, d->connections)
				{
					client->sendBinaryData(f.read(1024));
				}
			}
		}
	}
}

QHash<QString, Member *> Project::members() const
{
	return d->members;
}

QSet<DashServer::Connection *> Project::connections() const
{
	return d->connections;
}

int Project::clientCount() const
{
	return d->connections.size();
}

void Project::aboutToSave()
{
	if( projectName().isEmpty() ) return;
	
	setId(projectName());
	
	add("description", description());
	add("author", author());
	
	if(!d->initialized) d->loadMembers();
}

void Project::afterLoad()
{
	setProjectName(id());
	setDescription(value("description"));
	setAuthor(value("author"));
	
	if(!d->initialized) d->loadMembers();
}

bool Project::save()
{
	if(!d->initialized) d->loadMembers();
	
	QString pname = projectName();
	
	if( pname.isEmpty() ) return false;
	
	return Dash::ProjectSaver::save(this, fileName());
}

bool Project::load()
{
	if(!d->initialized) d->loadMembers();
	
	QString pname = projectName();
	
	if( pname.isEmpty() )
	{
		qWarning("Warning: Project name is empty");
		return false;
	}
	
	clear();
	
	return Dash::ProjectSaver::load(this, fileName());
}

QString Project::fileName() const
{
	QString pname = projectName();
	
	if( pname.isEmpty() )
		return QString();
	
	return XDBMS::Manager::self()->databaseDir().absoluteFilePath(pname+".dsh");
}

void Project::addMember(Member *member)
{
	if( d->members.contains(member->id()) )
	{
		d->members.remove(member->id());
		remove(member);
	}
	
	d->members[member->id()] = member;
	add(member);
}

QString Project::partName(int part)
{
	switch(part)
	{
		case All:
		{
			return "all";
		}
		break;
		case Scene:
		{
			return "scene";
		}
		break;
		case Layer:
		{
			return "layer";
		}
		break;
		case Frame:
		{
			return "frame";
		}
		break;
		case Object:
		{
			return "object";
		}
		break;
	}
	
	return QString();
}


int Project::partFromCommand(const QString &pkgname)
{
	Project::Part part = Project::None;
	
	// LIBRARY
	if( pkgname == "addlibraryfolder" )
	{
		part = Project::Library;
	}
	else if( pkgname == "addlibraryobject" )
	{
		part = Project::Library;
	}
	else if( pkgname == "modifysymbol" )
	{
		part = Project::Library;
	}
	else if( pkgname == "renamelibraryobject" )
	{
		part = Project::Library;
	}
	
	
	// OBJECT AND ITEM
	if( pkgname == "additemfilter" )
	{
		part = Project::Object;
	}
	else if( pkgname == "addobject" )
	{
		part = Project::Object;
	}
	else if( pkgname == "addtweening" )
	{
		part = Project::Object;
	}
	else if( pkgname == "bringforwardsitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "bringtofrontitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changelayervisibility" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changetextfont" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changetext" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changetextwidth" )
	{
		part = Project::Object;
	}
	else if( pkgname == "convertitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "editnodesitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changeitembrush" )
	{
		part = Project::Object;
	}
	else if( pkgname == "changeitempen" )
	{
		part = Project::Object;
	}
	else if( pkgname == "groupitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "modifyfilter" )
	{
		part = Project::Object;
	}
	else if( pkgname == "modifyitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "removelibraryobject" )
	{
		part = Project::Object;
	}
	else if( pkgname == "removeobject" )
	{
		part = Project::Object;
	}
	else if( pkgname == "sendbackwardsitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "sendtobackitem" )
	{
		part = Project::Object;
	}
	else if( pkgname == "ungroupitem" )
	{
		part = Project::Object;
	}
	
	
	// FRAME
	if( pkgname == "changeframevisibility" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "expandframe" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "insertframe" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "lockframe" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "moveframe" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "removeframe" )
	{
		part = Project::Frame;
	}
	else if( pkgname == "renameframe" )
	{
		part = Project::Frame;
	}
	
	// LAYER
	if( pkgname == "insertlayer" )
	{
		part = Project::Layer;
	}
	else if( pkgname == "locklayer" )
	{
		part = Project::Layer;
	}
	else if( pkgname == "movelayer" )
	{
		part = Project::Layer;
	}
	else if( pkgname == "removelayer" )
	{
		part = Project::Layer;
	}
	else if( pkgname == "renamelayer" )
	{
		part = Project::Layer;
	}
	
	
	// SCENE
	if( pkgname == "insertscene" )
	{
		part = Project::Scene;
	}
	else if( pkgname == "movescene" )
	{
		part = Project::Scene;
	}
	else if( pkgname == "removescene" )
	{
		part = Project::Scene;
	}
	else if( pkgname == "renamescene" )
	{
		part = Project::Scene;
	}
	
	return part;
}


}
}


