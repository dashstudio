/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DASHSERVER_PROJECTSTORE_H
#define DASHSERVER_PROJECTSTORE_H

#include <QString>
#include <QList>
#include <dashserver/exports.h>

namespace DashServer {
class Connection;

namespace Project {
class Project;
class Member;

/**
 * @author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Store
{
	public:
		enum DataKey {
			ProjectName = 1
		};
		
		Store();
		~Store();
		
		QStringList projects() const;
		Project *project(const QString& name);
		QList<Project *> allProjects() const;
		
		bool createProject(const QString &name, const QString &author, const QString &description, const QString& owner, const QList<Member *> &members = QList<Member *>());
		void addToProject(const QString &projectName, DashServer::Connection *cnx);
		
		bool contains(const QString &projectName) const;
		bool execute(const QString &xml, DashServer::Connection *source);
		
		bool openProject(const QString &projectName, DashServer::Connection *source);
		
		QList<Project *> activeProjects() const;
		
		void removeClient(DashServer::Connection *client);
		
		void sendToMembers(const QString &projectName, const QString &pkg);
		void resendProject(const QString &projectName);
		
		bool removeProject(const QString &projectName);
		
		void importProject(DashServer::Connection *cnx, const QString &fileName);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
