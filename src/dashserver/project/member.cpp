/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "member.h"

#include <QHash>
#include <QString>

#include <dcore/debug.h>

#include "dashserver/users/user.h"
#include "dashserver/xdbms/manager.h"

#include "project.h"

namespace DashServer {
namespace Project {

struct Member::Private
{
	Private(Member *q) : q(q), initialized(false) {}
	
	QHash<QString, Users::Permission *> permissions;
	
	Member *q;
	bool initialized;
	void loadPermissions();
};

void Member::Private::loadPermissions()
{
	permissions.clear();
	
	QList<XDBMS::Object *> objects = q->objects();
	
	foreach(Object *obj, objects)
	{
		if( obj->klass() == "permission" )
		{
			Users::Permission *perm = static_cast<Users::Permission *>(obj);
			permissions[perm->name()] = perm;
		}
	}
	
	initialized = true;
}

Member::Member() : d(new Private(this))
{
	setClass("member");
}


Member::~Member()
{
	delete d;
}

void Member::setUser(const QString &login)
{
	setId(login);
}

Users::User Member::user() const
{
	QString login = id();
	
	Users::User user;
	user.setLogin(login);
	
	XDBMS::Manager::self()->load("users", &user);
	
	return user;
}

void Member::setJob(const QString &job)
{
	add("job", job);
}

QString Member::job() const
{
	return value("job");
}

void Member::addPermission(int part, Users::Permission::Opts opts)
{
	QString m = Project::partName(part);
	
	addPermission(m, opts);
}

void Member::addPermission(const QString &part, Users::Permission::Opts opts)
{
	addPermission(new Users::Permission(part, opts));
}

void Member::addPermission(Users::Permission *perm)
{
	QString module = perm->name();
	
	if( d->permissions.contains(module) )
	{
		d->permissions.remove(module);
		remove(perm);
	}
	
	d->permissions[module] = perm;
	add(perm);
}

QHash<QString, Users::Permission *> Member::permissions() const
{
	return d->permissions;
}

bool Member::canReadOn(int part) const
{
	if( !d->initialized ) d->loadPermissions();
	QString m = Project::partName(part);
	
	if( d->permissions.contains(m) )
	{
		return d->permissions[m]->read();
	}
	
	return false;
}

bool Member::canWriteOn(int part) const
{
	if( !d->initialized ) d->loadPermissions();
	
	QString m = Project::partName(part);
	
	if( d->permissions.contains(m) )
	{
		return d->permissions[m]->write();
	}
	
	return false;
}

void Member::afterLoad()
{
	d->loadPermissions();
}

void Member::aboutToSave()
{
	if( ! d->initialized )
	{
		d->loadPermissions();
	}
}

}

}
