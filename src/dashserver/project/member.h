/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DASHSERVER_PROJECTMEMBER_H
#define DASHSERVER_PROJECTMEMBER_H

#include <dashserver/exports.h>
#include <xdbms/object.h>

#include <dashserver/users/user.h>

namespace DashServer {
namespace Project {

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Member : public XDBMS::Object
{
	public:
		Member();
		~Member();
		
		void setUser(const QString &login);
		Users::User user() const;
		
		void setJob(const QString &job);
		QString job() const;
		
		void addPermission(int part, Users::Permission::Opts opts);
		void addPermission(const QString &part, Users::Permission::Opts opts);
		void addPermission(Users::Permission *perm);
		
		QHash<QString, Users::Permission *> permissions() const;
		
		bool canReadOn(int part) const;
		bool canWriteOn(int part) const;
		
		void afterLoad();
		void aboutToSave();
		
		
	private:
		struct Private;
		Private *const d;
};

}
}

#endif
