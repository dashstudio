/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DASHSERVER_PROJECTPROJECTMANAGER_H
#define DASHSERVER_PROJECTPROJECTMANAGER_H

#include <QObject>
#include <QString>
#include <dashserver/observer.h>
#include <dashserver/exports.h>

namespace DashServer {

class Package;

namespace Project {

/**
	@author David Cuadrado <krawek@gmail.com>
*/
class DASHSERVER_EXPORT Manager : public DashServer::Observer
{
	public:
		Manager();
		~Manager();
		
		virtual void handlePackage(DashServer::Package* const pkg);
		void connectionClosed(DashServer::Connection *cnx);
		
		void sendToMembers(const QString &projectName, const QString &pkg);
		void resendProject(const QString &projectName);
		
		void importProject(DashServer::Connection *cnx, const QString &fileName);
		
	private:
		struct Private;
		Private *const d;
};

}

}

#endif
