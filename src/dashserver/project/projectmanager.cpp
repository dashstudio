/***************************************************************************
 *   Copyright (C) 2007 by David Cuadrado                                  *
 *   krawek@gmail.com                                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "projectmanager.h"

#include <QVariant>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <dashserver/server.h>
#include <dashserver/package.h>
#include <dashserver/connection.h>

#include <dash/network/package/error.h>
#include <dash/network/package/projectlist.h>
#include <dash/network/parser/projectactionparser.h>
#include <dash/network/filereceiver.h>

#include <dcore/debug.h>

#include "store.h"
#include "project.h"
#include "member.h"

#include "dashserver/users/user.h"
#include "dashserver/xdbms/manager.h"
#include "dashserver/xdbms/table.h"


namespace DashServer {
namespace Project {

struct Manager::Private
{
	Store *store;
};

Manager::Manager()
 : DashServer::Observer(), d(new Private)
{
	d->store = new Store;
}


Manager::~Manager()
{
	delete d->store;
	delete d;
}

void Manager::handlePackage(DashServer::Package* const pkg)
{
	DashServer::TcpServer *server = pkg->source()->server();
	
	if( pkg->root() == "command" )
	{
		if( !d->store->execute(pkg->xml(), pkg->source() ) )
		{
			pkg->source()->sendError(QObject::tr("Cannot execute command"), Dash::Network::Package::Error::Err);
		}
		pkg->accept();
	}
	else if( pkg->root() == "listprojects" )
	{
		QList<Project *> projects = d->store->allProjects();
		
		Dash::Network::Package::ProjectList plist;
		
		foreach(Project *p, projects)
		{
			plist.addProject(p->projectName(), p->author(), p->description());
		}
		
		pkg->source()->sendToClient(plist.toString(), true);
		
		
		qDeleteAll(projects);
		pkg->accept();
	}
	else if( pkg->root() == "openproject" )
	{
		QString project;
		{
			QXmlStreamReader reader(pkg->xml());
			while(!reader.atEnd())
			{
				reader.readNext();
				
				if( reader.tokenType() == QXmlStreamReader::StartElement )
				{
					if( reader.name().toString() == "project" )
					{
						project = reader.attributes().value("name").toString();
						break;
					}
				}
			}
		}
		
		if( !project.isEmpty() )
		{
			d->store->openProject(project, pkg->source());
			pkg->accept();
		}
		else
		{
			// TODO: Enviar mensaje de error.
		}
	}
	else if(pkg->root() == "uploadproject" )
	{
		pkg->accept();
		pkg->source()->receiveProject();
	}
	else if( pkg->root() == "newproject" )
	{
		QString projectName, author, description;
		
		QXmlStreamReader reader(pkg->xml());
		while(!reader.atEnd())
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "name" )
				{
					projectName = reader.readElementText();
				}
				else if( reader.name().toString() == "author" )
				{
					author = reader.readElementText();
				}
				else if( reader.name().toString() == "description" )
				{
					description = reader.readElementText();
				}
			}
		}
		
		if( !d->store->contains(projectName) )
		{
			if( d->store->createProject(projectName, author, description, pkg->source()->user()->login()) )
			{
				dfDebug << "Created project: " << projectName;
				d->store->addToProject(projectName, pkg->source());
			}
		}
		else
		{
			// TODO: enviar mensaje de error
		}
		
		pkg->accept();
	}
	else if(pkg->root() == "addproject" )
	{
		Dash::Network::Parser::ProjectAction parser;
		
		if(parser.parse(pkg->xml()))
		{
			QString projectName = parser.name();
			QString description = parser.description();
			QString author =  parser.author();
			
			if( !d->store->contains(projectName) )
			{
				QList<Member *> projectMembers;
				
				QList<Dash::Network::Parser::ProjectAction::Member> members = parser.members();
				foreach(Dash::Network::Parser::ProjectAction::Member member, members)
				{
					Member *mem = new Member();
					mem->setUser(member.login);
					mem->setJob(member.job);
					QList<Dash::Network::Permission> permissions = member.profile.permissions();
					foreach(Dash::Network::Permission permission, permissions)
					{
						Users::Permission *perm = new Users::Permission();
						perm->setId(permission.name());
						perm->setName(permission.name());
						perm->setRead(permission.read());
						perm->setWrite(permission.write());
						
						mem->addPermission(perm);
					}
					projectMembers << mem;
				}
				
				if(d->store->createProject(projectName, author, description, pkg->source()->user()->login(), projectMembers))
				{
					dfDebug << "Created project: " << projectName;
					server->sendToAdmins(pkg->xml());
				}
				else
				{
					// TODO: enviar mensaje de error
				}
			}
			else
			{
				// TODO: enviar mensaje de error
			}
			pkg->accept();
		}
	}
	else if(pkg->root() == "updateproject" )
	{
		Dash::Network::Parser::ProjectAction parser;
		
		if(parser.parse(pkg->xml()))
		{
			QString projectName = parser.name();
			QString newDescription = parser.description();
			QString newAuthor =  parser.author();
			
			if(Project::Project *project = d->store->project(projectName))
			{
				if(!projectName.isEmpty())
				{
					project->setProjectName(projectName);
				}
				
				if(!newDescription.isEmpty())
				{
					project->setDescription(newDescription);
				}
				if(!newAuthor.isEmpty())
				{
					project->setAuthor(newAuthor);
				}
				
				
				QList<Dash::Network::Parser::ProjectAction::Member> members = parser.members();
				
				foreach(Dash::Network::Parser::ProjectAction::Member member, members)
				{
					Member *mem = new Member();
					mem->setUser(member.login);
					mem->setJob(member.job);
					D_SHOW_VAR(member.login);
					QList<Dash::Network::Permission> permissions = member.profile.permissions();
					
					
					foreach(Dash::Network::Permission permission, permissions)
					{
						Users::Permission *perm = new Users::Permission();
						
						perm->setId(permission.name());
						perm->setName(permission.name());
						perm->setRead(permission.read());
						perm->setWrite(permission.write());
						
						mem->addPermission(perm);
					}
					
					D_SHOW_VAR(mem->objects().size());
					project->addMember(mem);
				}
				
				project->aboutToSave();
				
				if( XDBMS::Manager::self()->update("projects", project) )
				{
					server->sendToAdmins(pkg->xml());
					pkg->accept();
				}
				delete project;
			}
			else
			{
				pkg->source()->sendError(QObject::tr("Project not exist"), Dash::Network::Package::Error::Err);
			}
			
			pkg->accept();
		}
	}
	else if(pkg->root() == "queryproject" )
	{
		QString projectName;
		QXmlStreamReader reader(pkg->xml());
		while(!reader.atEnd())
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "name" )
				{
					projectName = reader.readElementText();
				}
			}
		}
		
		if(!projectName.isEmpty())
		{
			if(Project::Project *project = d->store->project(projectName))
			{
				QString xml;
				{
					QXmlStreamWriter writer(&xml);
					writer.writeStartElement("projectquery");
					
					writer.writeStartElement("name");
					writer.writeCharacters(project->projectName());
					writer.writeEndElement();
					
					writer.writeStartElement("description");
					writer.writeCharacters(project->description());
					writer.writeEndElement();
					
					writer.writeStartElement("author");
					writer.writeCharacters(project->author());
					writer.writeEndElement();
					
					writer.writeStartElement("members");
					
					QHash<QString, Member *> members = project->members();
					foreach(Member *member, members)
					{
						writer.writeStartElement("user");
						writer.writeAttribute("login", member->user().login());
						writer.writeAttribute("job", member->job());
						
						QHash<QString, Users::Permission *> permissions = member->permissions();
						QHash<QString, Users::Permission *>::iterator it = permissions.begin();
						
						writer.writeStartElement("permissions");
						
						while(it != permissions.end())
						{
							writer.writeStartElement("module");
							writer.writeAttribute("name", it.key());
							writer.writeAttribute("read", QString::number(it.value()->read()));
							writer.writeAttribute("write", QString::number(it.value()->write()));
							writer.writeEndElement();
							
							it++;
						}
						
						writer.writeEndElement();
						writer.writeEndElement();
					}
					writer.writeEndElement();
					writer.writeEndElement();
					
					DashServer::Connection *cnn = pkg->source();
					cnn->sendToClient(xml, true);
					delete project;
				}
			}
		}
		else
		{
			pkg->source()->sendError(QObject::tr("Project not exist"), Dash::Network::Package::Error::Err);
		}
		pkg->accept();
	}
	else if(pkg->root() == "removeproject")
	{
		QString projectName;
		
		QXmlStreamReader reader(pkg->xml());
		while(!reader.atEnd())
		{
			reader.readNext();
			
			if( reader.tokenType() == QXmlStreamReader::StartElement )
			{
				if( reader.name().toString() == "name" )
				{
					projectName = reader.readElementText();
				}
			}
		}
		
		if(d->store->removeProject(projectName))
		{
			server->sendToAdmins(pkg->xml());
			pkg->accept();
		}
		else
		{
			dfDebug << QString("Project %1 not exist").arg(projectName);
// 			pkg->source()->sendError(QObject::tr("Project not exist"), Dash::Network::Package::Error::Err); // enviar error a admins
		}
	}
	else
	{
		dfDebug << "Unhandled package: " << pkg->root();
	}
}

void Manager::connectionClosed(DashServer::Connection *cnx)
{
	d->store->removeClient(cnx);
}

void Manager::sendToMembers(const QString &projectName, const QString &pkg)
{
	d->store->sendToMembers(projectName, pkg);
}

void Manager::resendProject(const QString &projectName)
{
	d->store->resendProject(projectName);
}

void Manager::importProject(DashServer::Connection *cnx, const QString &fileName)
{
	d->store->importProject(cnx, fileName);
}

}

}
