/***************************************************************************
 *   Copyright (C) 2006 by David Cuadrado                                  *
 *   krawek@toonka.com                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QApplication>
#include <QAbstractEventDispatcher>
#include <QProcess>
#include <QXmlStreamWriter>

#include <dgui/application.h>
#include <dcore/debug.h>
#include "dashserver/server.h"

#include <dcore/applicationproperties.h>
#include <dcore/config.h>

#include "dashserver/logger.h"
#include "dashserver/users/user.h"

#include <dashserver/registers/regmanager.h>
#include <dashserver/com/commanager.h>
#include <dashserver/xdbms/manager.h>


#ifdef Q_OS_UNIX
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

static void cleanup(int);
#endif

int main(int argc, char **argv)
{
	DCore::Debug::setForceDisableGUI();
	
	DGui::Application app(argc, argv, false); // Text Item requires X11
	app.setApplicationName("dashd");
	
	XDBMS::Manager::self()->initialize("database");
	// Add the admin user
	{
			DashServer::Users::User user;
			user.setLogin( "admin" );
			user.setPassword("admin");
			user.addPermission(DashServer::Module::Admin, DashServer::Users::Permission::Read|DashServer::Users::Permission::Write);
			user.addPermission(DashServer::Module::Users, DashServer::Users::Permission::Read|DashServer::Users::Permission::Write);
			
			QString xml;
			{
				QXmlStreamWriter writer(&xml);
				user.toXml(&writer);
			}
			dfDebug << xml;

			XDBMS::Manager::self()->addObject( "users", &user);
	}
	
#ifdef Q_OS_UNIX
	signal(SIGINT, cleanup);
	signal(SIGTERM, cleanup);
	signal( SIGSEGV, cleanup);
#endif
	
	QString host;
	qint16 port;
	{
		DCore::Config *config = DCore::Config::self();
		config->beginGroup("Connection");
		host = config->value("host", "localhost").toString();
		port = config->value("port", 5123).toInt();
		config->endGroup();
	}
	
	DashServer::TcpServer server;
	server.addObserver(new DashServer::Com::Manager);
	server.addObserver(new DashServer::Registers::Manager);
	
	server.openConnection( host, port );

	dDebug() << QObject::tr("Running server on %1:%2!").arg(server.serverAddress().toString()).arg(server.serverPort());
	
	int ret = app.exec();
	
	if( ret == 0 || ret == 2)
	{
		DCore::Config *config = DCore::Config::self();
		config->beginGroup("Connection");
		config->setValue("host", server.serverAddress().toString());
		config->setValue("port", server.serverPort());
		config->endGroup();
	}
	
	return ret;
}

#ifdef Q_OS_UNIX
void cleanup(int s)
{
	dDebug() << "Finishing with signal: " << s;

	static bool finishing = false;
	
	if( finishing )
	{
		if( s == 11 )
		{
			qWarning("Crashed! Exiting!...");
			exit(-256);
		}
		return;
	}

	finishing = true;
	
	DashServer::Logger::self()->info(QObject::tr("Finishing with signal: ")+QString::number(s));
	
	if( s == 15 ) // Restart to reload the configuration
	{
		QProcess::startDetached(qApp->arguments().first());
	}
	else
	if( s == 11 )
	{
		exit(-256);
	}
	
	qApp->exit(s);
}
#endif

