
TEMPLATE = app

QT += network xml


!include($$PWD/../../config.pri){
    error(cannot include config.pri)
}

LIBS += -L$$DESTDIR/lib -ldashserver -ldash

INCLUDEPATH += ..

TARGET = dashd
SOURCES += main.cpp

INSTALLS += target
target.path = /bin

CONFIG += console

linux-g++ {
    PRE_TARGETDEPS += $$BUILD_DIR/lib/libdash.a
    PRE_TARGETDEPS += $$BUILD_DIR/lib/libdashserver.a
}

