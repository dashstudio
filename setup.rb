
def configure(setup)
  setup.qt >= 4.3
  setup.add_option(:name => "dlib-dir", :type => "path", :optional => true, :description => "Sets the dlib dir")
  setup.add_option(:name => "yamf-dir", :type => "path", :optional => true, :description => "Sets the yamf dir")
  
  setup.find_package(:name => "yamf", :optional => false, :global => true)
  
  setup.add_test(:id => "yamfswf", :name => "SWF exporter", :defines  => ["YAMF_HAVE_SWFEXPORTER"], :optional => true, :headers => ["yamf/render/swfexporter.h"], :custom => "int main() { YAMF::Render::SwfExporter swf; }"  )
  
  setup.add_test(:id => "yamfmovie", :name => "Movie exporter", :defines  => ["YAMF_HAVE_MOVIEEXPORTER"], :optional => true, :headers => ["yamf/render/movieexporter.h"], :custom => "int main() { YAMF::Render::MovieExporter g; }"  )
  
  
  
  setup.generate_pkgconfig(:package_name => "dash", :name => "Dash", :description => "", :version => "0.1alpha", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldash", :cflags => "-I${includedir}", :requires => ["yamf"] )
  setup.generate_pkgconfig(:package_name => "dashserver", :name => "Dash Server", :description => "", :version => "0.1alpha", :libdir => nil, :includedir => nil, :libs => "-L${libdir} -ldashserver", :cflags => "-I${includedir}", :requires => ["dash"] )
end

def setup_pkgconfig(pkgconfig, args)
  pkgconfig.add_search_path(args["dlib-dir"]+"/lib/pkgconfig")
  pkgconfig.add_search_path(args["yamf-dir"]+"/lib/pkgconfig")
end

def setup_test(id, test, args)
end

def setup_config(cfg, args)
  cfg.add_define("__STDC_CONSTANT_MACROS")
  cfg.add_qtmodule("xml")
  cfg.add_qtmodule("svg")
end


